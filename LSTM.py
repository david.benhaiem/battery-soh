from Cleaning import *
from Training import *
from torch.utils.data import Dataset, DataLoader
import torch.nn.functional as F
import torch.nn as nn
import re
from LSTM_Base import LSTM_Base, Dataset_Base

class LSTM_v1(LSTM_Base):
    """LSTM type neural net for Battery SOH estimation.

    This class is part of the "regular" LSTMs, or the "normal" ``variant``.

    ..
        Check the documentation or the code before this class for more
        information about the architecture itself.

    .. note::
        This class is made for versions 1-3, for versions 4-5 check
        :class:`LSTM.LSTM_v4`.

    Version 1 takes ``delta`` current points and the correponding ``delta``
    voltage points per :math:`x_i`. Its number of features is thus ``2*delta``.

    Version 2 takes ``delta`` current, voltage and temperature points per
    :math:`x_i`. The temperature points are temperature measurements done during
    battery cycling and may not be available for all datasets.
    Its number of features is thus ``3*delta``.

    Version 3 takes ``delta`` current and voltage points, plus the ambient
    temperature per :math:`x_i`. Its number of features is thus ``2*delta+1``.

    Parameters
    ----------
    delta : int
        The size of the sliding window used to generate the :math:`x_i` s.
    hidden_dim : int
        Hidden dimension of the LSTM (also its main output dimension).
    pooling_layer : {'last', 'mean', 'bi_mean"}, default = 'bi_mean'
        Decides the type of pooling layer:

        * ``'last'`` (only takes the last result),
        * ``'mean'`` (takes the mean of the results) and
        * ``'bi_mean'`` (takes the mean of the product).

        The default value is strongly recommended. It's the one that's had the
        best results, in the article and in our tests.

    drop_out : float, optional
        The probability of the dropout layer that comes right after the LSTM.
        Must be a number between 0 and 1.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.

        .. note::
            This isn't a necessary parameter and **isn't** used in this model in
            any way. Instead, it relies on recieving data previously normalized
            by :class:`LSTM.Torch_Dataset_LSTM` (for LSTMs that do normalization
            check :std:doc:`LSTMwithNorm`).

            However, it's useful to define a ``norm`` value (such as the
            training dataset's) for documentation purposes, it is very useful
            when calling a saved model. It's good practice.

    version : int, default = 1
        The model version to be used.

        * ``version = 1`` corresponds to the model created by the article cited.
        * ``version = 2`` corresponds to the same model, but adapted to take the
          cycle's temperature data as well.

        * ``version = 3`` is similar to version 1, but takes the ambient
          temperature as an extra input.

    lin_layers : int, default = 1
        The number of linear regression layers. As of now, cannot be greater
        than two (**any value different than 1 will set it to two**).
    lin_dropout : float, optional
        The value for the dropout layer that comes after the first linear layer
        in the regression step. Will only be taken into accouunt if
        ``lin_layers`` > 1.

    Raises
    ------
    AssertionError
        If the version in different than 1, 2 or 3;
        If an unsupported ``pooling_layer`` is informed.

    Examples
    --------
    >>> from torch.utils.data import Dataset
    >>> from LSTM import *
    >>> delta = 10
    >>> hidden_dim = 20
    >>> version = 2
    >>> model = LSTM_v1(delta, hidden_dim, pooling_layer='last', version=version)
    >>> with open('B0005.txt', 'rb') as f:
    ...     Battery = pickle.load(f)
    ...
    >>> train_dataset = Torch_Dataset_LSTM([Battery], delta, version, fixed_len=150)
    >>> estimated_capacity = model(train_dataset.inputs[0])
    >>> loss_function = th.nn.MSELoss()
    >>> loss = loss_function(estimated_capacity, train_dataset.output[0])
    2.2862
    """
    def __init__(self, delta, hidden_dim, pooling_layer='bi_mean', drop_out=0,
                 norm=None, version=1, lin_layers=1, lin_dropout=0):

        super().__init__()

        assert version in [1, 2, 3], \
            "Unsupported version. For versions 4 and 5, call LSTM_v4 instead."

        # Model information:
        self.version = version                      # The kind of model
        self.variant = 'normal'
        self.pooling_layer = pooling_layer          # The type of pooling layer
        # Normalization information:
        self.norm = norm                            # The normalization used

        # Important dimensions
        self.delta = delta                          # The sliding window size
        self.hidden_dim = hidden_dim                # Hidden dimension (LSTM)

        # Input Dimension (number of input variables)
        if version == 1:
            self.inputs_dim = 2 * delta             # (I, V) * delta
        elif version == 2:
            self.inputs_dim = 3 * delta             # (I, V, T) * delta
        elif version == 3:
            self.inputs_dim = 2 * delta + 1         # (I, V) * delta + T_amb

        self.output_dim = 1

        self.dropout_value = drop_out

        assert pooling_layer in ['last', 'mean', 'bi_mean'], "pooling_layer"\
               " must be either 'last', 'mean' or 'bi_mean."

        # If the pooling layer requires the LSTM to be of bidirectional type
        if pooling_layer == 'bi_mean':
            bi_bool = True
        else:
            bi_bool = False

        # Definition of the LSTM itself
        self.lstm = nn.LSTM(self.inputs_dim, hidden_dim, batch_first=True,
                            bidirectional=bi_bool)

        # The linear layer that maps from hidden state space to tag space
        if lin_layers == 1:
            self.lin1 = nn.Linear(hidden_dim, self.output_dim)
            self.nb_layers = 1
        else:
            if lin_layers != 2:
                import warnings
                warnings.warn(
                    "Number of linear layers automatically changed to two",
                    RuntimeWarning)
            else:
                pass

            self.nb_layers = 2
            self.lin0 = nn.Linear(hidden_dim, hidden_dim//2)
            self.actf = nn.LeakyReLU()
            self.lin2 = nn.Linear(hidden_dim//2, self.output_dim)
            # self.lin3 = nn.Linear(hidden_dim//4, self.output_dim)
            self.lin_dropout_fn = nn.Dropout(lin_dropout)
            self.lin1 = nn.Sequential(
                self.lin0,
                self.actf,
                self.lin_dropout_fn,
                self.lin2,
            )

    def forward(self, inputs):

        # Input dimensions: [Batch size, Number of Xis (sequence length), 2*Delta]
        if len(inputs.shape) < 3:   # When Batch size equals 1, the first dimension is ignored,
                                    # but it's necesssary for the LSTM. So we have to reshape the inputs.
            inputs = inputs.view(1, inputs.shape[0], inputs.shape[1])

        lstm_out, _ = self.lstm(inputs)     # LSTM output
        shp_out = lstm_out.shape            # Its shape ([Batch size, Number of Xis (sequence length), self.hidden_dim])
        lstm_out = F.dropout(lstm_out, self.dropout_value)      # dropout layer

        # Pooling layer, according to the type chosen:
        if self.pooling_layer == 'last':
            pool_out = lstm_out[:, -1, :].view(shp_out[0], self.hidden_dim)         # Only the last result
        elif self.pooling_layer == 'mean':
            pool_out = th.mean(lstm_out, dim=1).view(shp_out[0], self.hidden_dim)   # Mean of the results
        elif self.pooling_layer == 'bi_mean':
            pool_out = th.mean(lstm_out[:, :, :self.hidden_dim]*                    # Mean of the multiplication
                               lstm_out[:, :, self.hidden_dim:], dim=1).view(shp_out[0], self.hidden_dim)
        else:
            raise NameError("Unknown pooling layer type: "+self.pooling_layer)

        # Linear layer
        output = self.lin1(pool_out)

        return output

class LSTM_v4(LSTM_Base):
    """LSTM type neural net for Battery SOH estimation.

    This class is part of the "regular" LSTMs, or the "normal" ``variant``.

    The difference between this class and :class:`LSTM.LSTM_v1` is that it
    always takes the ambient temperature as an input, but directly at the final
    regression layers, instead of the LSTM layer.

    ..
        Check the documentation or the code before this class for more
        information about the architecture itself.

    .. note::
        This class is made for versions 4-5, for versions 1-3 check
        :class:`LSTM.LSTM_v1`.

    Version 4 is analogous to version 1: it takes ``delta`` current and voltage
    points per :math:`x_i`. Its number of features (of the LSTM) is thus
    ``2*delta`` (its input size will always be one more of that, ``2*delta+1``).

    Version 5 is analogous to a version 2: it takes current, voltage and
    temperature points per :math:`x_i`. The temperature points are temperature
    measurements done during battery cycling and may not be available for all
    datasets. Its number of features (LSTM) is thus ``3*delta``.

    Parameters
    ----------
    delta : int
        The size of the sliding window used to generate the :math:`x_i` s.
    hidden_dim : int
        Hidden dimension of the LSTM (also its main output dimension).
    pooling_layer : {'last', 'mean', 'bi_mean"}, default = 'bi_mean'
        Decides the type of pooling layer:

        * ``'last'`` (only takes the last result),
        * ``'mean'`` (takes the mean of the results) and
        * ``'bi_mean'`` (takes the mean of the product).

        The default value is strongly recommended. It's the one that's had the
        best results, in the article and in our tests.

    drop_out : float
        The probability of the dropout layer that comes right after the LSTM.
        Must be a number between 0 and 1.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.

        .. note::
            This isn't a necessary parameter and **isn't** used in this model in
            any way. Instead, it relies on recieving data previously normalized
            by :class:`LSTM.Torch_Dataset_LSTM` (for LSTMs that do normalization
            check :std:doc:`LSTMwithNorm`).

            However, it's useful to define a ``norm`` value (such as the
            training dataset's) for documentation purposes, it is very useful
            when calling a saved model. It's good practice.

    version : int, default = 4
        The model version to be used.
        It will take the ambient temperature
        as an input directly into the linear layers.

        * ``version`` = 4 is analogous to an :class:`LSTM.LSTM_v1`'s version 1
        * ``version`` = 5 is analogous to an :class:`LSTM.LSTM_v1`'s version 2.

    lin_layers : int, default = 1
        The number of linear regression layers. As of now, cannot be greater
        than two (**any value different than 1 will set it to two**).
    lin_dropout : float, optional
        The value for the dropout layer that comes after the first linear layer
        in the regression step. Will only be taken into accouunt if
        ``lin_layers`` > 1.

    Raises
    ------
    AssertionError
        If the version in different than 4 or 5;
        If an unsupported ``pooling_layer`` is informed.

    Examples
    --------
    >>> from torch.utils.data import Dataset
    >>> from LSTM import *
    >>> delta = 10
    >>> hidden_dim = 20
    >>> version = 4
    >>> model = LSTM_v4(delta, hidden_dim, pooling_layer='last', version=version)
    >>> with open('B0005.txt', 'rb') as f:
    ...     Battery = pickle.load(f)
    ...
    >>> train_dataset = Torch_Dataset_LSTM([Battery], delta, version, fixed_len=150)
    >>> estimated_capacity = model(train_dataset.inputs[0])
    >>> loss_function = th.nn.MSELoss()
    >>> loss = loss_function(estimated_capacity, train_dataset.output[0])
    2.2862

    """
    def __init__(self, delta, hidden_dim, pooling_layer='bi_mean', drop_out=0,
                 norm=None, version=4, lin_layers=1, lin_dropout=0):

        super().__init__()

        assert version in [4, 5], "This version is not supported. For versions 1 to 3, call LSTM_v1 instead."

        # Model information:
        self.version = version                      # The kind of model
        self.variant = 'normal'
        self.pooling_layer = pooling_layer          # The type of pooling layer
        # Normalization information:
        self.norm = norm                            # The normalization used

        # Important dimensions
        self.delta = delta                          # The sliding window size
        self.hidden_dim = hidden_dim                # Hidden dimension (LSTM)

        # Input Dimension (number of input variables)
        if version == 4:
            self.inputs_dim = 2 * delta         # (I, V) * delta
        if version == 5:
            self.inputs_dim = 3 * delta         # (I, V, T) * delta

        self.output_dim = 1

        self.dropout_value = drop_out

        assert pooling_layer in ['last', 'mean', 'bi_mean'], "pooling_layer" \
        " must be either 'last', 'mean' or 'bi_mean."

        # If the pooling layer requires the LSTM to be of bidirectional type
        if pooling_layer == 'bi_mean':
            bi_bool = True
        else:
            bi_bool = False

        # Definition of the LSTM itself
        self.lstm = nn.LSTM(self.inputs_dim, hidden_dim, batch_first=True,
                            bidirectional=bi_bool)

        # The linear layer that maps from hidden state space to tag space
        if lin_layers == 1:
            self.lin1 = nn.Linear(hidden_dim+1, self.output_dim)
            self.nb_layers = 1
        else:
            self.nb_layers = 2
            self.lin0 = nn.Linear(hidden_dim+1, hidden_dim // 2)
            self.actf = nn.LeakyReLU()
            self.lin2 = nn.Linear(hidden_dim // 2, self.output_dim)
            # self.lin3 = nn.Linear(hidden_dim//4, self.output_dim)
            self.lin_dropout_fn = nn.Dropout(lin_dropout)
            self.lin1 = nn.Sequential(
                self.lin0,
                self.actf,
                self.lin_dropout_fn,
                self.lin2,
            )

    def forward(self, inputs):

        # Input dimensions: [Batch size,Number of Xis (sequence length),2*Delta]
        if len(inputs.shape) < 3:
            # When Batch size equals 1, the first dimension is ignored,
            # but it's necesssary for the LSTM. So we have to reshape the inputs.
            inputs = inputs.view(1, inputs.shape[0], inputs.shape[1])

        amb_T = inputs[:, 0, -1]  # It doesn't change throughout the cycle
        lstm_inputs = inputs[:, :, :-1]

        lstm_out, _ = self.lstm(lstm_inputs)  # LSTM output
        shp_out = lstm_out.shape              # Its shape
        lstm_out = F.dropout(lstm_out, self.dropout_value)  # dropout layer

        # Pooling layer, according to the type chosen:
        if self.pooling_layer == 'last':
            pool_out = lstm_out[:, -1, :].view(shp_out[0], self.hidden_dim)
        elif self.pooling_layer == 'mean':
            pool_out = th.mean(lstm_out, dim=1).view(shp_out[0],
                                                     self.hidden_dim)
        elif self.pooling_layer == 'bi_mean':
            pool_out = th.mean(lstm_out[:, :, :self.hidden_dim]*
                               lstm_out[:, :, self.hidden_dim:],
                               dim=1).view(shp_out[0], self.hidden_dim)
        else:
            raise NameError("Unknown pooling layer type: "+self.pooling_layer)

        # Linear layer
        output = self.lin1(th.cat([pool_out, amb_T.view(shp_out[0], 1)], dim=1))

        return output

class Torch_Dataset_LSTM(Dataset_Base):
    """Creates a Dataset class that can readily be loaded through a DataLoader
    class for training and validation.

    .. note::
        This class can only take in one type of dataset at a time.

    Parameters
    ----------
    Battery_list : list[clean_battery_like]
        List with the Batteries that we want to use for creating the dataset.
        Must be a clean battery class, such as
        :class:`~Nasa_Classes.Clean_Battery`,
        :class:`~MIT_Classes.Clean_MIT_Battery` or
        :class:`~Coin_Classes.Clean_Coin_Battery`.

    delta : int
        The size of the sliding window used to generate the :math:`x_i` s.
    model_version : {1, 2, 3, 4, 5}
        An integer indicating the LSTM version.

        * ``model_version = 1``: then only I and V data will be used.
        * ``model_version = 2``: then I, V and T will be taken into account.
        * ``model_version = 3``: then I, V and ambient T will be taken into
        account.

        * ``model_version = 4``: then I, V and ambient T will be taken into
        account, but the ambient T will enter directly in the last step, the NN.

        * ``model_version = 5``: then I, V, T and amb_T will be taken into
        account, mixing models 2 and 4. Redundant.

    print_bool : bool, default = False
        Whether or not to print some information while the code runs.
        Namely, if we want to print the battery's name and the total number of
        valid points in the dataset.

    time_step : float, optional
        The desired time step for the resampling procedure.
        If ``None`` is given, then a time step will be automatically generated
        by the method :func:`time_step_function`.

    threshold : float, default = 0.1
        The minimum capacity value to be considered as relevant [A.h].
    min_len : int, default = 10
        The minimum amount of points a charge cycle must have after resampling
        to be considered as relevant.

        This parameter may be overridden by the value of ``fixed_len`` and
        ``delta`` (very probable). I wouldn't bother about it.

    fixed_len : int, default = 150
        If different than ``None``, then the input sequence
        length will be truncated at ``fixed_len`` points. This will override
        ``min_len``.

    norm_type : {'zscore', 'minmax'}, default = 'zscore'
        A string indicating which kind of normalization to do.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.

        If different than ``None``, then ``norm_type`` will be overridden by
        ``norm[4]`` and the given information will be used for the
        normalization.

    adimensionalize : bool, default = True
        Whether or not to make variables dimensionless before adding them to the
        inputs and output lists.

        * The Capacity (output) is normalized by the nominal capacity
          (``Q_nominal``);

        * The Current is normalized by the C rate (``C_rate``) -
          which is the current necessary for charging the battery in one hour
          (theoretically);

        * The Voltage is normalized by the nominal voltage (``V_nominal``),
          the voltage at the plateau in the CV part of the charge curve.

        The values of these parameters are defined inside of each battery class.

    cycle_lim : int, optional
        The greatest cycle number to take into account for the dataset creation.
        Default equals ``1e6`` meaning that no cycles will be ignored.

    Attributes
    ----------
    dataset_name : str
        The name of the give clean battery class ("``Clean_MIT_Battery``",
        for instance).
    batteries : list[str]
        A list with the file name of each of the batteries used.
    min_len : int
        The final ``min_len`` value after overrides.
    time_step : float
        The final time_step value decided.
    dataset_format : str
        A string indicating the type of data format used: 'You et al. [56]'.
    Battery_list : list[clean_battery_like]
        A list with the clean batteries used for dataset creations. This is the
        same as the ``Battery_list`` input for all datasets except for
        Tarascon's coin batteries. This is because the individual
        :class:`~Coin_Classes.Clean_Cell` s are separated to be treated
        individually. Check :std:doc:`Tarascon's Coin dataset <COIN>`
        documentation for more information on why.

    inputs : list[Tensors]
        A list with the input tensors for each cycle.
    output : Tensor
        A tensor with the respective output capacity values for each cycle.
        If ``adimensionalize = True``, then values are dimensionless, and
        capacity should be between 0 and 1. Otherwise, values should be in A.h.

    norm : tuple(Tensor, Tensor, Tensor, Tensor, string)
        A tuple containing all of the information necessary for normalization.

    Raises
    ------
    AssertionError
        If the specified value of ``delta`` is < 1;
        If no batteries are given;
        If the specified version is invalid;
        If ``model_version`` is 2 or 5 and ``self.dataset_name`` is
        ``Clean_Coin_Battery``.
    ValueError
        If the ``model_version`` is unknown.

    Examples
    --------
    >>> from torch.utils.data import Dataset
    >>> from LSTM import Torch_Dataset_LSTM
    >>> delta = 10
    >>> version = 2
    >>> with open('B0005.txt', 'rb') as f:
    ...     Battery = pickle.load(f)
    ...
    >>> train_dataset = Torch_Dataset_LSTM([Battery], delta, version,
    ...                                    fixed_len=150, print_bool=True)
    B0005
    165
    """
    def __init__(self, Battery_list, delta, model_version, print_bool=False,
                 time_step=None, threshold=0.1, min_len=150, fixed_len=150,
                 norm_type='zscore', norm=None, adimensionalize=True,
                 cycle_lim=1e6
                 ):

        super().__init__()

        assert delta >= 1, 'delta must be greater or equal to one'
        assert len(Battery_list) > 0, 'No Batteries informed'
        assert model_version in [1, 2, 3, 4, 5], 'Invalid version'

        if fixed_len is None:
            if delta != 1:
                min_len = max(delta - 1, min_len)
                # min_len is the minimum sequence length (number of points) that
                # the charge cycle must have to be taken into account.
            else:
                min_len = 1
        else:
            min_len = fixed_len + delta

        # Dataset related
        self.dataset_name = type(Battery_list[0]).__name__  # The dataset used
        self.batteries = []
        self.adimensionalize = adimensionalize

        for Battery in Battery_list:
            self.batteries.append(Battery.Name+'.txt')

        if self.dataset_name == 'Clean_Coin_Battery':
            assert model_version in [1, 3, 4], "Can't use models 2 or 5 with" \
                "Clean_Coin_Battery. No data on temperature during cycling."
            new_battery_list = []
            for Battery in Battery_list:
                for cell in Battery.cell:
                    new_battery_list.append(cell)
            Battery_list = new_battery_list

        self.min_len = min_len
        self.fixed_len = fixed_len
        self.Battery_list = Battery_list

        if time_step is None:
            time_step = self.time_step_function()

        self.dataset_format = 'You et al. [56]'   # The data format used
        # LSTM related
        self.delta = delta                        # The amount of points per xi
        self.model_version = model_version        # The model version
        # Cleaning related:
        self.time_step = time_step                # The time step for resampling
        self.threshold = threshold                # The threshold for cleaning
        self.cycle_lim = cycle_lim

        ent = []        # Inputs list
        sai = []        # Output list

        for Battery in Battery_list:
            try:
                threshold = threshold/(Battery.mass)
            except AttributeError:
                pass

            C_rate = Battery.C_rate
            Q_nominal = Battery.Q_nominal
            V_nominal = Battery.V_nominal

            if print_bool:
                print(Battery.Name)
            for index, cycle_couple in enumerate(Battery.cycle_couple):
                if index > cycle_lim:
                    # If we've gone over the limit number of cycles to test.
                    break
                chr_cycle = cycle_couple[0]
                dis_cycle = cycle_couple[1]

                # print(time_step)
                Data = Resampling_flexible(chr_cycle, variable='Time',
                                           time_step=time_step, min_len=min_len)

                if Data is None:
                    # When there aren't enough points in the cycle, Cycle_t is
                    # returned as None
                    continue

                Cycle_i = Data[:, 1]
                Cycle_v = Data[:, 2]
                Cycle_T = Data[:, 3] # NOT THE TEMPERATURE IF DATASET_TYPE =
                                     # COIN OR MXENE

                if adimensionalize:
                    Cycle_i = Cycle_i/C_rate
                    Cycle_v = Cycle_v/V_nominal

                # Cleaning will take away NaN's, the beginning and the end of
                # the discharge curves (that is, the part before the current
                # reaches the CC value and the part after when the current
                # reaches its cut-off value ``i_cutoff``) and resample the time
                # according to the given time step ``time_step``.

                # Calculating the real number of xis we've got
                if fixed_len is None:
                    Nb = len(Cycle_i) - delta + 1  # Number of xis in this cycle
                else:
                    Nb = fixed_len
                # min_len already take care of the cases where Nb <= 0.

                if self.model_version == 1:
                    XT = np.zeros((Nb, 2 * delta))
                    # Regrouped xi info for one whole cycle

                    for t in range(Nb):  # We're going to make one xi for each t
                        xi = np.concatenate([Cycle_i[t:t + delta],  # Current
                                             Cycle_v[t:t + delta]])  # Voltage
                        # "Add" it to the XT tensor that regroups them.
                        XT[t, :] = xi

                elif self.model_version == 2:
                    XT = np.zeros((Nb, 3 * delta))
                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             Cycle_T[t:t + delta]])# Temperature

                        XT[t, :] = xi

                elif (self.model_version == 3) or (self.model_version == 4):
                    XT = np.zeros((Nb, 2 * delta + 1))

                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             chr_cycle.amb_T*np.ones([1])])
                                             # Ambient temperature

                        XT[t, :] = xi

                elif self.model_version == 5:
                    XT = np.zeros((Nb, 3 * delta + 1))

                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             Cycle_T[t:t + delta],
                                             chr_cycle.amb_T*np.ones([1])])

                        XT[t, :] = xi
                else:
                    raise ValueError("Unknown version: "+str(self.model_version))

                Capacity = dis_cycle.data.capacity*np.ones(1)

                if Capacity.item() < threshold:
                    # If it's considered to be too close to zero
                    continue

                if adimensionalize:
                    Capacity = Capacity/Q_nominal

                # The XT tensor represents one cycle and is thus one input
                ent.append(th.from_numpy(XT).float())
                # The capacity is one output.
                sai.append(Capacity)

            # The number of inputs and outputs must be the same
            assert len(ent) == len(sai), 'Unequal input and output lengths'

        inputs, output, norm = Normalize(ent, sai, norm_type=norm_type,
                                         norm=norm)

        if print_bool:
            print(len(inputs))

        self.inputs = inputs  # saving the inputs
        self.output = output  # saving the outputs
        self.norm = norm  # saving the norm

        self.path = None
        self.transform = None

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, item):

        ent = self.inputs[item]
        sai = self.output[item]

        return ent, sai
