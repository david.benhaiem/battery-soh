Raw data for the NASA dataset can be found at:
https://ti.arc.nasa.gov/tech/dash/groups/pcoe/prognostic-data-repository/#battery

Raw data for the MIT dataset can be found at (Batch 2017-06-30):
https://data.matr.io/1/projects/5c48dd2bc625d700019f3204

Information on how the data can be cleaned cleaned and stored using the developed classes is given in the documentation at https://david.benhaiem.gitlab.io/battery-soh/rst%20files/Battery%20Classes.html for these specific datasets. Instructions are also given if new datasets are to be used.