import torch as th
from Nasa_Classes import *
from Coin_Classes import *
import main
from LSTM import *
from MIT_Classes import *
import seaborn as sns
sns.set()
device = th.device('cuda:0' if th.cuda.is_available() else 'cpu')

def dataset_jointplot(dataset_name, full_dataset=True, fixed_len=150,
                      norm=None, norm_type='zscore', cycle_lim=1e6, delta=1,
                      lstm_version=1, time_step=None, batteries=None):

    """Function for creating joint plots of the different datasets.

    The joint plots consist of a 2D scatter plot of the (t, V) couples, with
    approximated distribution functions on the sides. An example is shown below.

    .. figure:: Images/B0047.png
        :alt: Example of a joint plot.
        :figwidth: 80 %

        Example of a joint plot.

    This function does not support mixing different datsets (for it, check
    :func:`dataset_displot`). This function can only create separate joint plots
    for one or many batteries of a same dataset.

    Parameters
    ----------
    dataset_name : {'NASA', 'MIT', 'Tarascon Coin'}
        String indicating which dataset we want to create the joint plot of.
    full_dataset : bool
        Whether or not we want to create a joint plot of the whole dataset in
        one image (True) of one image for each battery (False).
    fixed_len : int, optional
        If different than `None` (default value), then the charge and discharge
        curves will be limited to `fixed_len` points.
    norm_type : {'zscore', 'minmax'}
        A string indicating which kind of normalization to do.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.
        If entered as input, `norm_type` will be overridden by norm[4] and the
        given information will be used for the normalization
    cycle_lim : int, optional
        The greatest cycle number to take into account for the dataset creation.
        Default equals ``1e6`` meaning that no cycles will be ignored.
    delta : int, optional
        The size of the sliding window used to generate the x_i's.
        This value only needs to be updated if the given ``norm`` is not
        ``None``, in which case shapes and sizes have to agree.
        See ``Torch_Dataset_LSTM``'s docstring in `LSTM.py` for more information.
    lstm_version : int, optional
        The LSTM's version used for creating the normalisation parameters
        (``norm``). This only needs to be given if ``norm`` is not ``None``.
    time_step : int, optional
        The desired time step for the resampling procedure.
        If ``None`` is given, than a time step will be automatically generated
        by the method ``time_step_function``.
    batteries : list[str], optional
        A list with the battery files' names to create plots for. If ``None``,
        than all of the available batteries are used.

    Raises
    ------
    AssertionError
        If the given ``dataset_name`` is not one of the valid ones.
    TypeError
        If ``fixed_len`` is ``None``.

    Examples
    --------
    >>> from DistributionPlots import *
    >>> dataset_jointplot('NASA', batteries=['B0005.txt'])
    32.532073974609375      # Calculated time step
    B0005                   # Battery currently analysing
    165                     # Number of capacity points
    # Plot saved in './/Datasets/NASA/B. Saha and K Goebel/Separate_pickles':

    .. figure:: Images/B0005.png
        :alt: The plot created
        :figwidth: 80 %

        Plot saved as "B0005.png"

    """

    if batteries is None:
        batteries = ['B'+str(i).zfill(4)+'.txt' for i in range(100)]

    assert dataset_name in ['NASA', 'MIT', 'Tarascon Coin', 'MXene'], \
        "Invalid dataset name, valid dataset names are: 'NASA', 'MIT', " \
        "'Tarascon Coin' and MXene."

    if fixed_len is None:
        raise TypeError("The following code only works for an integer fixed_len.")

    if time_step is None and not full_dataset:
        print("Can't use time_step None with full_dataset False.\n")
        string = 'A'
        while string.upper() != 'Y' and string.upper() != 'N':
            string = input(
                "Do you wish to use the default time_step for this dataset "
                "( "+str(main.default_time_step_dict[dataset_name])+" )? [y/n]"
            )

        if string.upper() == 'Y':
            time_step = main.default_time_step_dict[dataset_name]
        else:
            time_step = float(input("Input the desired time step."))

    # Entering the dataset folder
    os.chdir(main.path_dict[dataset_name])
    ignore = main.ignore_dict[dataset_name]

    # Searching for battery files
    files = os.listdir()    # All files in the folder
    file_list = []          # List with the valid files
    for file_name in files:
        match = re.search(r'B\d{4}.txt', file_name)
        # Only take into acount the battery files
        if match == None:
            continue
        else:
            file_list.append(file_name)
    file_list.sort()

    dataset_list = []
    if full_dataset:    # If we want  to do all of the batteries together:
        # Training files:
        Train_list = []
        for file_name in file_list:
            if (file_name in ignore) or (file_name not in batteries):
                continue
            else:
                with open(file_name, 'rb') as f:
                    Battery = pickle.load(f)

                Train_list.append(Battery)

        train_dataset = Torch_Dataset_LSTM(
            Train_list, delta, lstm_version, print_bool=True,
            time_step=time_step, fixed_len=fixed_len, norm_type=norm_type,
            adimensionalize=True, min_len=150, cycle_lim=cycle_lim,
            norm=norm
        )
        dataset_list.append(train_dataset)

    else:
        for file_name in file_list:
            if (file_name in ignore) or (file_name not in batteries):
                continue
            else:
                with open(file_name, 'rb') as f:
                    Battery = pickle.load(f)

                train_dataset = Torch_Dataset_LSTM(
                    [Battery], delta, lstm_version, print_bool=True,
                    time_step=time_step, fixed_len=fixed_len,
                    norm_type=norm_type, adimensionalize=True, min_len=150,
                    cycle_lim=cycle_lim, norm=norm
                )
                dataset_list.append(train_dataset)

    # Creating the dataframe from the data
    for dataset in dataset_list:
        # Current and Voltage information
        nb = -1     # The amount of cycles to take into account, -1 is all of them.
        inputs = th.stack(dataset.inputs)     # The input tensors
        I = inputs[:nb, :, 0]                       # Selecting only the Current
        V = inputs[:nb, :, delta]                   # Selecting only the Voltage
        I = I.view(len(I)*fixed_len, 1).numpy()           # Turning them into np arrays
        V = V.view(len(V)*fixed_len, 1).numpy()           # of the correct shape

        # Create the correct "time" numbering column
        lin_array = np.linspace(1, fixed_len, num=fixed_len).reshape(fixed_len, 1)
        t = np.tile(lin_array, (int(len(I)/fixed_len), 1))

        # Dataset Name Column
        Name = np.array([dataset_name]*len(I)).reshape(len(I), 1)
        df = pd.DataFrame(data=np.concatenate([t, I, V], axis=1),
                          columns=['t', 'I', 'V'])

        df['Name'] = Name       # I've added this separately because otherwise the pd.df
                                # would become of type string.

        # Creating the joint plot:
        sns_plot = sns.jointplot(
            data=df, x='t', y='V', hue='Name', palette=main.palette, height=10, marker='+',
            # stat='density',
            # common_norm=False,
            # kind='kde',
        )

        # Defining the plot title and saving the image.
        # Note that the image will be saved in the correspondingn dataset folder.
        if len(dataset.batteries) == 1:
            plt.title(dataset.batteries[0])
            image_name = dataset.batteries[0][:-4] + '.png'
        else:
            plt.title(dataset_name)
            image_name = dataset_name + '_joint_dist.png'

        sns_plot.savefig(image_name)
        plt.close()

    os.chdir('../../../../')

def dataset_displot(battery_lists=[None, None, None], twoD=False,
                    norm_list=[None, None, None], delta=1, lstm_version=1,
                    norm_type='zscore', cycle_lim=1e6,
                    fixed_len=150, time_step=None, kind='hist',
                    image_name=None, ignore_dict=None, yscale='linear'):

    """Function for creating histogram distributions of the different datasets.

    This function creates histograms and contour plots for comparing the
    different datasets' distributions.

    .. figure:: Images/Figure_4.png
        :figwidth: 80 %
        :alt: Example of an histogram plot (``kind = 'hist'``, ``twoD=False``)

        Example of an histogram plot (``kind = 'hist'``, ``twoD=False``)

    .. figure:: Images/Figure_3.png
        :figwidth: 80 %
        :alt: Example of a countour plot (``kind = 'kde'``, ``twoD=True``)

        Example of a countour plot (``kind = 'kde'``, ``twoD=True``)

    It takes as inputs a list of lists with the batteries to include in the
    histograms. It should be structured as follows:

    .. code-block::

        battery_lists = [
            ['B0005.txt', 'B0006.txt'],     # Nasa Batteries
            ['B0047.txt'],                  # MIT Batteries
            ['B0001.txt', 'B0004.txt']      # Tarascon's Coin Batteries
        ]

    If all of the dataset's batteries are to be used, then a simple ``None``
    can be passed instead of a list. **If no batteries are to be used, an emtpy
    list must be passed**.

    .. code-block::

        battery_lists = [
            None,                           # ALL of Nasa's Batteries
            [],                             # None of MIT Batteries
            ['B0001.txt', 'B0004.txt']      # Tarascon's Coin Batteries
        ]

    By default, each dataset is normalized separately on its own. If you want to
    specify the normalisation parameters, you can use input ``norm_list`` for
    that:

    .. code-block::

        norm_nasa = (th.Tensor([1.9, 4.3]), th.Tensor([0.01, 0.4]),
                     th.Tensor([0.8]), th.Tensor([0.04]), 'zscore')
        norm_list = [
            norm_nasa,      # Nasa dataset will be normalised with norm_nasa
            None,           # MIT dataset will be normalised with respect to itself
            None            # Same as MIT for Tarascon's Coin batteries.
        ]

    .. note::
        Make sure the value of ``delta`` and ``lstm_version`` are in accordance
        with the normalisation information provided.

    .. note::
        When inputting a ``norm`` in ``norm_list``, the input ``norm_type`` will
        automatically be overridden by ``Torch_Dataset_LSTM``, but only for the
        dataset for which the normalisation parameters were specified.

    Parameters
    ----------
    battery_lists : list[list[str]], optional
        List with three lists (of strings). The first list corresponds to the
        batteries for the Nasa dataset, the second one for the MIT dataset and
        the last one for Tarascon's Coin dataset.
        Each list is composed of several strings with the name of the battery
        files, in the format "B0025.txt" (``'B'+str(number).zfill(4)+'.txt'``).

        By default, it will use all of the available batteries for all datasets.
    twoD : bool
        Whether or not to create a 2D distribution (V x t).
    norm_list : list[tuple]
        A list with the normalisation information for each dataset.
        See function ``Normalize`` in ``Cleaning.py`` for more information on
        how this information is structured.
    norm_type : {'zscore', 'minmax'}
        A string indicating which kind of normalization to do.
    cycle_lim : int, optional
        The greatest cycle number to take into account for the dataset creation.
        Default equals ``1e6`` meaning that no cycles will be ignored.
    fixed_len : int, optional
        If different than `None` (default value), then the charge and discharge
        curves will be limited to `fixed_len` points.
    kind : str, default = 'hist'
        The kind of seaborn plot to create. Check seaborn's ``displot``
        documentation for more information.
    image_name : str, optional
        If an image_name is given, then the image will be saved as a png file
        with that name.
    yscale : str, default = 'linear'
        The y-axis scale ('linear' is regular and 'log' for log scale').
    
    Examples
    --------
    >>> from DistributionPlots import *
    >>> dataset_displot(battery_lists=[['B0005.txt'], [], None])
    ... # Nasa's Battery Nº5, no batteries from MIT's dataset, All of Tarascon's
    ... # batteries
    ... # See Figure 3_ for results

    >>> os.chdir('.//Reports/2021-07-12 09_28_11.807309')
    >>> # LSTM_v4 trained with Nasa dataset
    >>> with open('best_model.txt', 'rb') as f:
    ...     Model = pickle.load(f)
    ...
    >>> nasa_norm = Model.norm      # Taking the model's norm
    >>> dataset_displot(battery_lists=[
    ...      ['B0005.txt'], ['B0047.txt'], ['B0001.txt']
    ... ], twoD=True, kind='kde',
    ... norm_list=[nasa_norm, nasa_norm, nasa_norm], # datasets normalized with the same norm
    ... lstm_version=4, delta=10) # LSTM version and delta have to match the norm's.
    ... # See Figure 4 for results

    .. _Figure 3:

    .. figure:: Images/Figure_1.png
        :figwidth: 80 %

        Figure 3: Result of first example.

    .. _Figure 4:

    .. figure:: Images/Figure_2.png
        :figwidth: 80 %

        Figure 4: Result of the second example.

    """

    for index, entry in enumerate(battery_lists):
        if entry is None:
            battery_lists[index] = ['B' + str(i).zfill(4) + '.txt' for i in range(100)]

    lin_array = np.linspace(1, fixed_len, num=fixed_len).reshape(fixed_len, 1)
    t_array = np.array([[1.]])
    I_array = np.array([[1.]])
    V_array = np.array([[1.]])
    Name_array = np.array([[1.]])

    if ignore_dict is None:
        ignore_dict = main.ignore_dict

    for index, batteries in enumerate(battery_lists):

        dataset_name = main.dataset_dict[index]

        os.chdir(main.path_dict[dataset_name])
        ignore = ignore_dict[dataset_name]

        print(dataset_name)

        file_list = os.listdir()
        file_list.sort()
        Train_list = []
        for file_name in file_list:
            if (file_name in ignore) or (file_name not in batteries):
                continue
            else:
                with open(file_name, 'rb') as f:
                    Battery = pickle.load(f)

                Train_list.append(Battery)

        os.chdir('../../../../')
        if len(Train_list) == 0:
            continue
        else:
            train_dataset = Torch_Dataset_LSTM(
                Train_list, delta, lstm_version, print_bool=True,
                time_step=time_step, fixed_len=fixed_len, norm_type=norm_type,
                adimensionalize=True, min_len=150, cycle_lim=cycle_lim,
                norm=norm_list[index]
            )

        print("norm =", train_dataset.norm)
        nb = -1  # The amount of cycles to take into account, -1 is all of them.
        inputs = th.stack(train_dataset.inputs)  # The input tensors
        I = inputs[:nb, :, 0]  # Selecting only the Current
        V = inputs[:nb, :, delta]  # Selecting only the Voltage
        I = I.view(len(I) * fixed_len, 1).numpy()  # Turning them into np arrays
        V = V.view(len(V) * fixed_len, 1).numpy()  # of the correct shape

        # Create the correct "time" numbering column
        t = np.tile(lin_array, (int(len(I) / fixed_len), 1))
        # Dataset Name Column
        Name = np.array([dataset_name] * len(I)).reshape(len(I), 1)

        t_array = np.concatenate([t_array, t], axis=0)
        I_array = np.concatenate([I_array, I], axis=0)
        V_array = np.concatenate([V_array, V], axis=0)
        Name_array = np.concatenate([Name_array, Name], axis=0)

    data = np.concatenate([t_array[1:, :],
                           I_array[1:, :],
                           V_array[1:, :]], axis=1)

    df = pd.DataFrame(data=data, columns=['t', 'I', 'V'])
    df['Name'] = Name_array[1:, :]
    # I've added this separately because otherwise the pd.df would be str

    # Palette to differentiate between datasets (arbitrary):

    if twoD:
        sns_plot = sns.displot(df, x='t',
                               y='V',
                               hue='Name', palette=main.palette,
                               kind=kind,
                               height=5,
                               #    binwidth=0.2,
                               stat='density', common_norm=False)
    else:
        sns_plot = sns.displot(df, x='V',
                               hue='Name', palette=main.palette,
                               kind=kind,
                               height=5,
                               aspect=5,
                               #    binwidth=0.2,
                               stat='density', common_norm=False)

        plt.yscale(yscale)

    if image_name is not None:
        sns_plot.savefig(image_name+'.png')
        plt.close()


def SOH_displot(battery_lists=[None, None, None], twoD=False,
                norm_list=[None, None, None], delta=1, lstm_version=1,
                norm_type='zscore', cycle_lim=1e6,
                fixed_len=150, time_step=None, kind='hist',
                image_name=None, ignore_dict=None):

    """Function for creating histogram distributions of the different datasets.

    This function creates histograms and contour plots for comparing the
    different datasets' distributions.

    .. figure:: Images/Figure_4.png
        :figwidth: 80 %
        :alt: Example of an histogram plot (``kind = 'hist'``, ``twoD=False``)

        Example of an histogram plot (``kind = 'hist'``, ``twoD=False``)

    .. figure:: Images/Figure_3.png
        :figwidth: 80 %
        :alt: Example of a countour plot (``kind = 'kde'``, ``twoD=True``)

        Example of a countour plot (``kind = 'kde'``, ``twoD=True``)

    It takes as inputs a list of lists with the batteries to include in the
    histograms. It should be structured as follows:

    .. code-block::

        battery_lists = [
            ['B0005.txt', 'B0006.txt'],     # Nasa Batteries
            ['B0047.txt'],                  # MIT Batteries
            ['B0001.txt', 'B0004.txt']      # Tarascon's Coin Batteries
        ]

    If all of the dataset's batteries are to be used, then a simple ``None``
    can be passed instead of a list. **If no batteries are to be used, an emtpy
    list must be passed**.

    .. code-block::

        battery_lists = [
            None,                           # ALL of Nasa's Batteries
            [],                             # None of MIT Batteries
            ['B0001.txt', 'B0004.txt']      # Tarascon's Coin Batteries
        ]

    By default, each dataset is normalized separately on its own. If you want to
    specify the normalisation parameters, you can use input ``norm_list`` for
    that:

    .. code-block::

        norm_nasa = (th.Tensor([1.9, 4.3]), th.Tensor([0.01, 0.4]),
                     th.Tensor([0.8]), th.Tensor([0.04]), 'zscore')
        norm_list = [
            norm_nasa,      # Nasa dataset will be normalised with norm_nasa
            None,           # MIT dataset will be normalised with respect to itself
            None            # Same as MIT for Tarascon's Coin batteries.
        ]

    .. note::
        Make sure the value of ``delta`` and ``lstm_version`` are in accordance
        with the normalisation information provided.

    .. note::
        When inputting a ``norm`` in ``norm_list``, the input ``norm_type`` will
        automatically be overridden by ``Torch_Dataset_LSTM``, but only for the
        dataset for which the normalisation parameters were specified.

    Parameters
    ----------
    battery_lists : list[list[str]], optional
        List with three lists (of strings). The first list corresponds to the
        batteries for the Nasa dataset, the second one for the MIT dataset and
        the last one for Tarascon's Coin dataset.
        Each list is composed of several strings with the name of the battery
        files, in the format "B0025.txt" (``'B'+str(number).zfill(4)+'.txt'``).

        By default, it will use all of the available batteries for all datasets.
    twoD : bool
        Whether or not to create a 2D distribution (V x t).
    norm_list : list[tuple]
        A list with the normalisation information for each dataset.
        See function ``Normalize`` in ``Cleaning.py`` for more information on
        how this information is structured.
    norm_type : {'zscore', 'minmax'}
        A string indicating which kind of normalization to do.
    cycle_lim : int, optional
        The greatest cycle number to take into account for the dataset creation.
        Default equals ``1e6`` meaning that no cycles will be ignored.
    fixed_len : int, optional
        If different than `None` (default value), then the charge and discharge
        curves will be limited to `fixed_len` points.
    kind : str, default = 'hist'
        The kind of seaborn plot to create. Check seaborn's ``displot``
        documentation for more information.
    image_name : str, optional
        If an image_name is given, then the image will be saved as a png file
        with that name.

    Examples
    --------
    >>> from DistributionPlots import *
    >>> dataset_displot(battery_lists=[['B0005.txt'], [], None])
    ... # Nasa's Battery Nº5, no batteries from MIT's dataset, All of Tarascon's
    ... # batteries
    ... # See Figure 1 for results

    >>> os.chdir('.//Reports/2021-07-12 09_28_11.807309')
    >>> # LSTM_v4 trained with Nasa dataset
    >>> with open('best_model.txt', 'rb') as f:
    ...     Model = pickle.load(f)
    ...
    >>> nasa_norm = Model.norm      # Taking the model's norm
    >>> dataset_displot(battery_lists=[
    ...      ['B0005.txt'], ['B0047.txt'], ['B0001.txt']
    ... ], twoD=True, kind='kde',
    ... norm_list=[nasa_norm, nasa_norm, nasa_norm], # datasets normalized with the same norm
    ... lstm_version=4, delta=10) # LSTM version and delta have to match the norm's.
    ... # See Figure 2 for results

    .. _Figure 1:

    .. figure:: Images/Figure_1.png
        :figwidth: 80 %

        Figure 1: Result of first example.

    .. _Figure 2:

    .. figure:: Images/Figure_2.png
        :figwidth: 80 %

        Figure 2: Result of the second example.

    """
    for index, entry in enumerate(battery_lists):
        if entry is None:
            battery_lists[index] = ['B' + str(i).zfill(4) + '.txt' for i in range(100)]

    t_array = np.array([[1.]])
    Q_array = np.array([[1.]])
    Name_array = np.array([[1.]])

    if ignore_dict is None:
        ignore_dict = main.ignore_dict

    for index, batteries in enumerate(battery_lists):

        dataset_name = main.dataset_dict[index]

        os.chdir(main.path_dict[dataset_name])
        ignore = ignore_dict[dataset_name]

        print(dataset_name)

        file_list = os.listdir()
        file_list.sort()
        Train_list = []
        for file_name in file_list:
            if (file_name in ignore) or (file_name not in batteries):
                continue
            else:
                with open(file_name, 'rb') as f:
                    Battery = pickle.load(f)

                Train_list.append(Battery)

        os.chdir('../../../../')
        if len(Train_list) == 0:
            continue
        else:
            train_dataset = Torch_Dataset_LSTM(
                Train_list, delta, lstm_version, print_bool=True,
                time_step=time_step, fixed_len=fixed_len, norm_type=norm_type,
                adimensionalize=True, min_len=150, cycle_lim=cycle_lim,
                norm=norm_list[index]
            )

        print("norm =", train_dataset.norm)
        # print(train_dataset.norm)
        Q = train_dataset.output  # The output tensor
        Q = Q.view(len(Q), 1).numpy()
        # Create the correct "time" numbering column
        t = np.linspace(1, len(Q), num=len(Q)).reshape(len(Q), 1)
        # Dataset Name Column
        Name = np.array([dataset_name] * len(t)).reshape(len(t), 1)

        t_array = np.concatenate([t_array, t], axis=0)
        Q_array = np.concatenate([Q_array, Q], axis=0)
        Name_array = np.concatenate([Name_array, Name], axis=0)

    data = np.concatenate([t_array[1:, :],
                           Q_array[1:, :]], axis=1)

    df = pd.DataFrame(data=data, columns=['t', 'Q'])
    df['Name'] = Name_array[1:, :]
    # I've added this separately because otherwise the pd.df would be str

    # Palette to differentiate between datasets (arbitrary):

    if twoD:
        sns_plot = sns.displot(df, x='t',
                               y='Q',
                               hue='Name', palette=main.palette,
                               kind=kind,
                               height=5,
                               # binwidth=0.01,
                               stat='density', common_norm=False)
    else:
        sns_plot = sns.displot(df, x='Q',
                               hue='Name', palette=main.palette,
                               kind=kind,
                               height=5,
                               aspect=5,
                               # binwidth=0.01,
                               stat='density', common_norm=False)

    if image_name is not None:
        sns_plot.savefig(image_name+'.png')
        plt.close()