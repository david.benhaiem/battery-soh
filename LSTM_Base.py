import torch as th
import torch.nn as nn
from torch.utils.data import Dataset
import pickle

class LSTM_Base(nn.Module):
    def __init__(self):
        super().__init__()
        self.model_name = 'LSTM'                    # The kind of model
        self.dataset_format = 'You et al. [56]'     # The dataset format


    def report(self):
        """Creates a little report with all of the information about the class.

                Returns
                -------
                str
                    A string with the report information.
                """

        string = \
            f"  Model name: {self.model_name}\n" \
            f"  Variant: {self.variant}\n" \
            f"  Version: {self.version}\n" \
            f"  Dataset Format: {self.dataset_format}\n" \
            f"  Delta: {self.delta}\n" \
            f"  Hidden layer dimension: {self.hidden_dim}\n" \
            f"  Pooling_layer: {self.pooling_layer}\n" \
            f"  Dropout prob.: {self.dropout_value}\n" \
            f"  Number of linear layers: {self.nb_layers}\n"

        try:
            string += f"  Dropout prob. (lin layers): {self.lin_dropout_fn.p}\n"
        except AttributeError:
            pass

        return string

class Dataset_Base(Dataset):
    def __init__(self):
        super().__init__()

    def report(self):
        """Creates a little report with all of the information about the class.

        Returns
        -------
        str
            A string with the report information.
        """

        string = \
            f"  Dataset Name: {self.dataset_name}\n" \
            f"  Size of the training set: {len(self.inputs)}\n" \
            f"  Threshold (minimum capacity): {self.threshold}\n" \
            f"  Dimensionless variables: {self.adimensionalize}\n" \
            f"  Minimum length: {self.min_len}\n" \
            f"  Resampling time step: {self.time_step}\n" \
            f"  Fixed Length: {self.fixed_len}\n" \
            f"  Batteries used: {self.batteries}\n" \
            f"  Normalization information: {self.norm}\n"

        if self.cycle_lim != 1e6:
            string += f"  Maximum cycle number: {self.cycle_lim}\n"

        return string

    def time_step_function(self):
        """Function for generating a time step when ``None`` is informed.

        This function makes some extremely arbitrary choices, but that are based
        on values of ``time_step`` that are known to work well with the
        available datasets.

        The function calculates the mean duration of the charge cycles in the
        given ``Battery_list``, and divides it by ``2*min_len`` to get the
        time_step. The factor 2 is there to ensure that most of the cycles will
        have more than ``fixed_len`` points after resampling. It could be lower
        or higher, but the results currently obtained for the available datasets
        match well the manually tested ones.

        Returns
        -------
        time_step : float
            The time step [s].
        """

        time_tensor = th.Tensor([[0, 0]])  # empty tensor
        for Battery in self.Battery_list:
            for cycle_couple in Battery.cycle_couple:
                chr_cycle = cycle_couple[0]
                dt = th.Tensor([[chr_cycle.data.time[0, 0].item(),
                                 chr_cycle.data.time[-1, 0].item()]])

                time_tensor = th.cat([time_tensor,
                                      dt], dim=0)

        time_tensor = time_tensor[1:, :]
        mean_times = th.mean(time_tensor, dim=0)
        time_step = (mean_times[1] - mean_times[0]) / (2 * self.min_len)
        print("Time step: (in seconds)", time_step.item())
        return time_step.item()


    def dataset_info(self, name):
        """Method for creating and information tuple that can be loaded for
        creating a similar dataset in the future.

        Parameters
        ----------
        name : str
            The name of the file where the tuple will be dumped.
        """
        info_tuple = (self.batteries, self.time_step, self.fixed_len, self.norm)

        with open(name+'_info_tuple_.txt', 'wb') as f:
            pickle.dump(info_tuple, f)