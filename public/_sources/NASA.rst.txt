Nasa dataset
============
Corresponds to the ``Nasa_Classes`` module.

Path for the clean batteries:
"/Datasets/NASA/B. Saha and K Goebel/Separate_pickles"
or simply ``import main`` and do ``main.path_dict['NASA']``

The Nasa Dataset is composed of a total of 30 useful batteries. They have
varying discharge protocols but their charge protocols are always the same
(CC at 1,5A until Voltage reaches 4,2V, then CV until current decreases to
20mA) Their nominal capacity is 2,1 A.h.
Their discharge protocol varies but this isn't taken in by our model
so no problem.

Check "Notes.xlsx" file in the dataset folder for more information.

.. _raw_nasa:

Classes for storing the raw data
--------------------------------

.. autoclass:: Nasa_Classes.Dataset_Nasa
  :members:

.. autoclass:: Nasa_Classes.Nasa_Cycle
  :members:

.. autoclass:: Nasa_Classes.Nasa_Data
  :members:


.. _clean_nasa:

Classes for cleaning and storing cleaned data
---------------------------------------------

.. autoclass:: Nasa_Classes.Clean_Battery
  :members:

.. autoclass:: Nasa_Classes.Clean_Cycle
  :members:

.. autoclass:: Nasa_Classes.Clean_Data
  :members: