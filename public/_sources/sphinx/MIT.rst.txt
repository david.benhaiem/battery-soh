MIT dataset
===========
Corresponds to the ``MIT_Classes`` module.

Path for the clean batteries:
"Datasets/Severson et al./2. 2017-06-30/Clean Dataset"
or simply ``import main`` and do ``main.path_dict['MIT']``

The MIT dataset is composed of a total of 48 useful batteries. They have
the same discharge protocol but varying charge protocols composed of 2 CC steps,
followed by a rest period and a final CCCV stage which is the same for all
batteries (CC at 1,1A (1C) until Voltage reaches 3,6V, then CV until current
reaches C/50 (I think)). Their nominal capacity is 1,1 A.h. Upon initialization
of the :class:`Clean_MIT_Cycle` class, the charge curve is cut as to only save
the CCCV stage. This is the only part of the charge that is used in model
training etc..

.. _raw_mit:

Classes for storing the raw data
--------------------------------

.. autoclass:: MIT_Classes.Dataset_MIT
  :members:

.. autoclass:: MIT_Classes.MIT_Cycle
  :members:

.. autoclass:: MIT_Classes.MIT_Data
  :members:

.. _clean_mit:

Classes for cleaning and storing cleaned data
---------------------------------------------

.. autoclass:: MIT_Classes.Clean_MIT_Battery
  :members:

.. autoclass:: MIT_Classes.Clean_MIT_Cycle
  :members:

.. autoclass:: MIT_Classes.Clean_MIT_Data
  :members: