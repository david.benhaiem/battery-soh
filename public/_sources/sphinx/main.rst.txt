Useful Dictionaries
===================

Corresponds to the ``main.py`` module.
This simply defines a few useful dictionaries that can be called in other codes.
This is specially done in the :std:doc:`Google Colabs <Colabs>` through
``import main``.

These dictionaries usually include default values or information for each
dataset. The dataset names (dictionary keys) used are ``'NASA'``, ``'MIT'`` and
``'Tarascon Coin'``.

The dictionaries are:

* ``default_time_step_dict``: default time step for resampling for each dataset.

* ``path_dict``: the path from the project folder to the clean dataset folder
  for each dataset.

* ``dataset_function_dict``: The dataset function to use for each
  :ref:`LSTM variant <lstm_variants>`.The keys are the variant name
  (``'normal'``, ``'chemistry'`` and ``'with norm'``).

* ``ignore_dict``: Batteries that should be ignored (not included in the
  training or the validation datasets) for each dataset.

* ``train_dict``: Batteries that should be included in the training dataset.

* ``palette``: Color palette for creating :ref:`distribution plots <dist_plots>`
  , for each dataset.