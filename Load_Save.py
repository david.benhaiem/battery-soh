from Training import *
import LSTM_with_norm as wn
import LSTM_chemistry as chem
import main
from LSTM import *
import main


def Load_Save(report_name, device='cpu', plot_bool=False, analysis=True,
              epoch=None, dataset_name='NASA', report_bool=True, complete=False,
              denormalize_bool=True, base_capacity=1, threshold=0.1):

    """Function for loading a report's model and simulating its performance.

    The function reaches into the report directory and loads both the best and
    last epochs' models. It also reads which batteries were used for training
    and which were ignored (through the *load_info.txt* file).
    It will then evaluate the model in each battery that hasn't been
    ignored and save an image of each result in folders *Images_Best* (for the
    best epoch) and *Images_Final* (for the last epoch) inside of the original
    report folder (``report_name``).

    This function is perfect for being called just after training, for creating
    the ``analysis`` file and plots.

    Parameters
    ----------
    report_name : str
        The name of the report directory. It's usually a date.
    device : torch.device or str, default = 'cpu'
        In which device the model and evaluation should be done.
        Can either be 'cpu' or a cuda ('cuda:0', 'cuda:1', ...).
    plot_bool : bool, default = False
        A boolean variable indicating whether or not to plot the battery
        simulations as they are done.
    analysis : bool, default = True
        Whether or not to run a more individual analysis of the different
        batteries.
    epoch : int, optional
        The epoch number if the user wants to load the model's parameter for
        a specific epoch.

        .. note::
            This may not always be possible since the epoch's folder is usually
            deleted after training (see :func:`Training.Train`'s and
            :func:`Report.Report`'s ``remove_bool`` variable).

    dataset_name : {'NASA', 'MIT', 'Tarascon Coin'}
        A string explaining which dataset to use.
    report_bool : bool, default = True
        A boolean variable for controlling whether or not to create the images
        for each battery prediction.
    complete : bool, default = False
        Whether or not to run the analysis for all of the batteries in the
        dataset (regardless of whether or not they were used in training and
        validation).

    denormalize_bool : bool, default = True
        Whether or not we want the plot to show the de-normalized values or not,
        and whether or not to calculate the errors in function of the normalized
        or denormalized values.

        .. warning::
            Should be set to ``False`` for
            :std:doc:`LSTMs with norm <LSTMwithNorm>`, since the output
            denormalisation is already done inside the model. For others,
            ``True`` is the best option.

    base_capacity : float, optional
        The nominal capacity to be used in the :func:`~Load_Save.Analysis`
        function. This should be defined only when using dimensional capacity
        values for the model's output.

    Raises
    ------
    NameError
        When an unknown ``dataset_name`` or ``model_name`` is given.
    AttributeError
        When the model does not have attribute ``model_name``.
    """

    #####
    # Opening the report:
    os.chdir('.//Reports')
    report_name = report_name.replace('/', ':')
    os.chdir('.//' + str(report_name))

    # Opening the best model:
    with open('best_model.txt', 'rb') as f:
        Model = pickle.load(f)
    dir_name = '_Best'

    # Opening training information:
    with open('train_info_tuple_.txt', 'rb') as f:
        train_tuple = pickle.load(f)
    # Opening validation information:
    with open('valid_info_tuple_.txt', 'rb') as f:
        valid_tuple = pickle.load(f)

    # Leaving the report folder:
    os.chdir('../../')
    #####

    # Defining the dataset function based on the model variant:
    try:
        if Model.variant == 'chemistry' or Model.variant == 'tech':
            dataset_fn = chem.Torch_Dataset_LSTM
        elif Model.variant == 'normal':
            dataset_fn = Torch_Dataset_LSTM
        elif Model.variant == 'with norm':
            dataset_fn = wn.Torch_Dataset_LSTM
        else:
            raise NameError(
                "Model variant not yet supported: " + str(Model.model_name)
            )
    except AttributeError:
        raise AttributeError(
            "Model does not have ``variant`` attribute,  call this function"
            " from Old_Load_Save.py instead"
        )

    # Getting information from the tuples:
    train = train_tuple[0]
    time_step = train_tuple[1]
    fixed_len = train_tuple[2]
    norm = train_tuple[3]

    if complete:
        # All batteries in the dataset:
        complete_battery_list = ["B" + str(i).zfill(4) + ".txt" for i in
                                 range(100)]
    else:
        # Only batteries used in training and validation:
        complete_battery_list = train_tuple[0] + valid_tuple[0]

    #####
    # Entering the dataset folder:
    os.chdir(main.path_dict[dataset_name])

    # Sorting battery names:
    files = os.listdir()  # All files in the folder
    file_list = []  # List with the valid files
    for file_name in files:
        match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
        # the battery files
        if match == None:
            continue
        else:
            file_list.append(file_name)
    file_list.sort()

    loader_list = []

    # Creating the loader object for each battery:
    for file_name in file_list:
        if file_name not in complete_battery_list:
            continue
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)

        Battery_list = [Battery]
        try:
            th_B = dataset_fn(
                Battery_list, Model.delta, Model.version, norm=norm,
                fixed_len=fixed_len, time_step=time_step, print_bool=True,
                threshold=threshold
            )
        except RuntimeError:  # Empty list of tensors
            continue

        B_ld = DataLoader(dataset=th_B, batch_size=len(th_B), shuffle=False,
                          num_workers=0)
        loader_list.append(B_ld)

    # Leaving the dataset folder:
    os.chdir('../../../../')
    # Reentering the report folder:
    os.chdir('.//Reports/' + report_name)

    # Looping twice, for the last and best epochs.
    for times in range(2):
        if times == 0:
            pass
        else:
            if epoch is None:
                with open('base_model.txt', 'rb') as f:
                    Model = pickle.load(f)
                dir_name = '_Final'
            else:
                os.chdir('.//Epochs')
                Model.load_state_dict(
                    th.load('Epoch_' + str(epoch) + '.txt', map_location='cpu')
                )
                os.chdir('../')
                dir_name = '_Epoch_' + str(epoch)

        Model.to(device)

        for loader in loader_list:

            # Battery's file name
            file_name = loader.dataset.batteries[0]

            if file_name in train:
                title = file_name + ' (used in training)'
                print(title)
            else:
                title = file_name
                print(title)

            simulate(
                loader, Model, title=title, report=report_bool, block=False,
                file_name=file_name.split('.')[0], plot_bool=plot_bool,
                dir_name=dir_name, device=device, denormalize=denormalize_bool
            )

        if analysis:
            Analysis(
                loader_list, Model, train, device=device, file_name=dir_name,
                base_capacity=base_capacity, denormalize=denormalize_bool
            )

    # Leaving the folder
    os.chdir('../../')


def Load_Save_v2(report_name, dataset_name, fixed_len=150, time_step=None,
                 analysis=True, device='cpu', norm_test='dataset', Model=None,
                 offset_analysis=False, cycle_lim=1e6, denormalize_bool=True,
                 base_capacity=1, threshold=0.1):

    """Function for running analysing a model on different datasets.

    The function takes a ``report_name`` as input, and accesses the report to
    find the desired model. With ``dataset_name``, it access the correct folder
    for the dataset to test.

    This function is different from :func:`~Load_Save.Load_Save` because it can
    apply a model to datasets different from the one it was trained on. The user
    can also choose how the standardisation parameters will be calculated for
    this dataset through the ``norm_test`` variable , although this won't change
    much for :std:doc:`LSTMs with normalisation <LSTMwithNorm>`, since in their
    case the normalisation is done inside of the model and its parameters won't
    be affected.

    .. note::
        How to proceed when a ``ModuleNotFoundError`` is raised:
        This is caused when a model trained with an older module tries to be
        loaded. The older and newer models are compatible (the only thing that
        changed between modules was only the ``Torch_Dataset_LSTM`` function).

        In order to make this work, you'll have to open the `TEST_Report.txt`
        file in the desired report folder. It will contain all of the
        information needed to initialize a similar model the parameters of which
        can be replaced with the ones saved in the report.

        In the `TEST_Report.txt` file, you'll find something like this:

        .. code-block:: text

            MODEL
              Model name: LSTM
              Version: 4
              Dataset Format: You et al. [56]
              Delta: 10
              Hidden layer dimension: 50
              Pooling_layer: bi_mean
              Dropout prob.: 0
              Number of linear layers: 1

        Define a model of the same version:

        .. code-block::

            model = LSTM_v4(Delta, Hidden layer dimension,
                            pooling_layer=Pooling_layer, drop_out=Dropout prob.,
                            version=Version, lin_layers=Number of linear layers,
                            lin_dropout=0)

        (If ``lin_dropout > 0``, an extra entry will be present in the report).
        For versions 1 through 3, call :class:`LSTM_v1` instead.
        Then, just call this function again with variable ``Model=model``

    Parameters
    ----------
    report_name : str
        The name of the report folder inside the "Reports" folder.
    dataset_name : {'NASA', 'MIT', 'Tarascon Coin'}
        The name of the dataset to test the model on.
    fixed_len : int, default = 150
        The final length of the resampled charge curve data (in number of
        points). See ``Torch_Dataset_LSTM`` in `LSTM.py` for more information.

    time_step : float, optional
        The desired time step for the resampling procedure.
        If ``None`` is given, than a time step will be automatically generated
        in function of the given ``norm_test`` variable.

    analysis : bool, default = True
        Whether or not to run a quantitative analysis of the dataset.
        See ``Analysis`` function in `Load_Save` for more information.

    device : torch.device or str, default = 'cpu'
        In which device the model and evaluation should be done.
        Can either be 'cpu' or a cuda ('cuda:0', 'cuda:1', ...).

    norm_test : str or list(str)
        Informs how to calculate the normalization information to be used for
        the data. It can be:

            "Model" : the model normalization will be used
            "dataset" : the whole dataset will be used
            list(str) : the batteries in the list

                Example: ['B0001', 'B0005', 'B0025']
                Batteries 1, 5 and 25 from the dataset will be used.

        If ``time_step`` is ``None``, then the time_step will also be calculated
        automatically for these batteries.

    Model : torch.nn.Module, optional
        A model to use as a base for loading the report's parameters. This is
        only necessary when trying to load a Model trained with an older module.
        Check this function's note for more information on how this should be
        initialized.
    offset_analysis : bool, optional
        Whether or not to add an offset to the model's bias term in order to
        decrease the offset error.
    cycle_lim : int, optional
        The greatest cycle number to take into account for the dataset creation.
        Default equals ``1e6`` meaning that no cycles will be ignored.

    Raises
    ------
    NameError
        When an unknown ``dataset_name``, model name or ``norm_test`` is given.
    ModuleNotFoundError
        When an older module tries to be imported.
    AttributeError
        When the model does not have attribute ``variant``.
    """

    # Opening the report folder:
    os.chdir('.//Reports/' + report_name)
    if Model is None:
        try:
            with open('best_model.txt', 'rb') as f:
                Model = pickle.load(f)
        except ModuleNotFoundError:
            raise ModuleNotFoundError(
                "It seems that you've tried loading a" +
                " model trained with an old module. The versions are compatible, " +
                "check this function's (Load_Save_v2) docstring on how to proceed" +
                "to make this work.")
    else:
        state_dict = th.load('best_model_dict.txt')
        Model.load_state_dict(state_dict)
    # Leaving the report folder:
    os.chdir('../../')

    # Picking the dataset function:
    try:
        if Model.variant == 'chemistry' or Model.variant == 'tech':
            dataset_fn = chem.Torch_Dataset_LSTM
        elif Model.variant == 'normal':
            dataset_fn = Torch_Dataset_LSTM
        elif Model.variant == 'with norm':
            dataset_fn = wn.Torch_Dataset_LSTM
            print(dataset_fn)
        else:
            raise NameError(
                "Model type not yet supported: " + str(Model.model_name))
    except AttributeError:
        raise AttributeError(
            "Model does not have ``variant`` attribute,  call this function"
            " from Old_Load_Save.py instead"
        )

    Model = Model.to(device)

    # Entering the dataset folder:
    dataset_dict = main.path_dict
    os.chdir(dataset_dict[dataset_name])

    files = os.listdir()  # All files in the folder
    file_list = []  # List with the valid files

    for file_name in files:
        match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
        # the battery files
        if match == None:
            continue
        else:
            file_list.append(file_name)
    file_list.sort()

    complete_list = ['B' + str(i).zfill(4) + '.txt' for i in range(200)]

    if dataset_name == 'NASA':
        for battery in main.ignore_dict['NASA']:
            complete_list.remove(battery)
        # base_capacity = 2.1
    elif (dataset_name == 'MIT'):
        # base_capacity = 1.1
        pass
    elif dataset_name == 'Tarascon Coin':
        complete_list.remove('B0007.txt')
        complete_list.remove('B0008.txt')
        complete_list.remove('B0019.txt')
        complete_list.remove('B0023.txt')
        # base_capacity = 0.1284
    elif dataset_name == 'MXene':
        complete_list.append('B0063.txt')
        complete_list.append('B1821.txt')
        complete_list.append('B1851.txt')
        complete_list.append('B1892.txt')
        complete_list.append('B1591.txt')
        complete_list.append('B1592.txt')
    else:
        raise NameError("Unknown Dataset")

    battery_list = []
    for file_name in file_list:
        if file_name in complete_list:
            with open(file_name, 'rb') as f:
                Battery = pickle.load(f)
            battery_list.append(Battery)

    DELTA = Model.delta
    LSTM_version = Model.version

    if norm_test == 'Model':
        norm = Model.norm
        assert time_step is not None, "Can't use time_step = None with " \
                                      "norm_test = 'Model'."
        train = []
    elif norm_test == 'dataset' or isinstance(norm_test, list):

        if norm_test == 'dataset':
            ds = battery_list
        else:
            ds = [battery for battery in battery_list
                  if battery.Name in norm_test]

        train_dataset = dataset_fn(
            ds, DELTA, LSTM_version, print_bool=True, time_step=time_step,
            fixed_len=fixed_len, cycle_lim=cycle_lim, threshold=threshold
        )

        norm = train_dataset.norm
        time_step = train_dataset.time_step
        train = train_dataset.batteries
        if offset_analysis:
            Model = offset_calculator(Model, train_dataset, device=device)

    elif isinstance(norm_test, tuple):
        norm = norm_test
        train = []
    else:
        raise NameError("Unsupported ``norm_test``: " + str(norm_test))

    # print(norm)

    os.chdir('../../../../')
    os.chdir('.//Reports/' + report_name)
    loader_list = []
    for battery in battery_list:
        dataset = dataset_fn([battery], DELTA, LSTM_version,
                             print_bool=True, time_step=time_step,
                             fixed_len=fixed_len, norm=norm, threshold=threshold)

        loader = DataLoader(dataset=dataset, batch_size=len(dataset),
                            shuffle=False)
        loader_list.append(loader)

        simulate(loader, Model, title=battery.Name, report=True, block=False,
                 file_name=battery.Name, plot_bool=False, dir_name=dataset_name,
                 device=device, denormalize=denormalize_bool)

    if analysis:
        Analysis(loader_list, Model, train, device=device,
                 file_name='_' + dataset_name, base_capacity=base_capacity,
                 denormalize=denormalize_bool)

    os.chdir('../../')


def Analysis(battery_loader_list, model, train, device='cpu', file_name='',
             base_capacity=1, denormalize=True):
    """
    Analyses the model's performance over the different batteries.

    Calculates the mean and maximum absolute error, mean and maximum relative
    error, and the root mean squared error (RMSE).

    A report will create a text file with the results.

    Parameters
    ----------
    battery_loader_list : list[torch.utils.data.DataLoader]
        The battery's DataLoader loader.
    model : torch.nn.Module
        The model to use for the analysis.
    device : torch.device or str, default = 'cpu'
        In which device the model and evaluation should be done.
        Can either be 'cpu' or a cuda ('cuda:0', 'cuda:1', ...).
    file_name : str
        A string to add to the Analysis file name. The final name will be
        'Analysis'+file_name+'.txt'.
    base_capacity : float, default = 1
        The battery's maximum capacity value (at its beginning of life).

    Raises
    ------
    NotImplementedError
        If the normalization type present in battery_loader.dataset.norm is not
        supported.
    """

    analysis_file = open('Analysis' + file_name + '.txt', 'w')

    model = model.to(device)

    # L1 Loss function:
    loss_fn = nn.L1Loss(reduction='none')
    los2_fn = nn.MSELoss()

    # "Simulation"
    with th.no_grad():
        model.eval()
        for battery_loader in battery_loader_list:
            output_list = []
            labels_list = []
            for data, labels in battery_loader:
                data = data.to(device)
                labels = labels.to(device)
                output = model.forward(data)

                # The following lines are more general (any batch_size)
                output_list.append(output)
                labels_list.append(labels)

            # Turning them back to th.Tensors
            output = th.stack(output_list)
            labels = th.stack(labels_list)

            # Denormalization:
            norm = battery_loader.dataset.norm
            if denormalize:
                if norm[4] == 'zscore':
                    output = output * norm[3].to(device) + norm[2].to(device)
                    labels = labels * norm[3].to(device) + norm[2].to(device)
                elif norm[4] == 'minmax':
                    output = output * (norm[3].to(device) - norm[2].to(device))\
                             + norm[2].to(device)
                    labels = labels * (norm[3].to(device) - norm[2].to(device))\
                             + norm[2].to(device)
                else:
                    raise NotImplementedError(
                        norm[4] + ' normalization type not supported'
                    )

            # Absolute error:
            abs_loss = loss_fn(output, labels)
            mean_abs = th.mean(abs_loss)
            amax_abs = th.amax(abs_loss)

            # Relative error
            rel_loss = abs_loss / abs(labels)
            mean_rel = th.mean(rel_loss)
            amax_rel = th.amax(rel_loss)

            # Relative to the initial capacity value
            mean_init = mean_abs / base_capacity
            amax_init = amax_abs / base_capacity

            # RMSE
            RMSE = th.sqrt(los2_fn(output, labels))

            battery_name = str(battery_loader.dataset.batteries[0])
            if battery_name in train:
                battery_name += ' (training)'

            analysis_file.write(
                f"Battery {battery_name}\n\n"
                f"    Mean and Max Absolute Error: "
                f"{str(mean_abs.item())[:6]}, "
                f"{str(amax_abs.item())[:6]}\n"
                f"    Mean and Max Relative Error: "
                f"{str(mean_rel.item()*100)[:4]}%, "
                f"{str(amax_rel.item()*100)[:4]}%\n"
                f"    Relative to the BoL capacity: "
                f"{str(mean_init.item()*100)[:4]}%, "
                f"{str(amax_init.item()*100)[:4]}%\n"
                f"    RMSE: "
                f"{str(RMSE.item())[:6]}\n\n"
            )

    analysis_file.close()


def offset_calculator(Model, dataset, device='cpu'):
    """Calculates the mean offset between prediction and experimental values.

    This function will automatically update the model's bias parameter in order
    to decrease the offset.

    .. note::
        Hasn't been used in a long time.

    Parameters
    ----------
    Model : torch.nn.Module
        The Model to be analyzed and updated.
    dataset : torch.utils.data.Dataset
        The dataset to get the offset from.

    Returns
    -------
    The model with updated bias parameter.
    """
    loader = DataLoader(dataset=dataset, batch_size=len(dataset), shuffle=False)

    for inputs, labels in loader:
        inputs = inputs.to(device)
        labels = labels.to(device)
        output = Model(inputs)
        bias_tensor = labels - output

    bias_value = th.mean(bias_tensor, dim=0)
    with th.no_grad():

        if isinstance(Model.lin1, nn.Sequential):
            # The model has more than 1 linear layer
            Model.lin1[-1].bias += bias_value
        else:
            Model.lin1.bias += bias_value

    return Model

