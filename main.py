dataset_dict = {
    0: 'NASA',
    1: 'MIT',
    2: 'Tarascon Coin',
    3: 'MXene'
}

default_time_step_dict = {
    'NASA': 30.,
    'MIT': 3.,
    'Tarascon Coin': 30.,
    'MXene': 0.039,
}

path_dict = {
    'NASA': './/Datasets/NASA/B. Saha and K Goebel/Separate_pickles',
    'MIT': './/Datasets/Severson et al./2. 2017-06-30/Clean Dataset',
    # 'MIT': './/Datasets/Severson et al./Cleaned_v2/2. 2017-06-30',
    'Tarascon Coin': './/Datasets/Tarascon/Coins/Clean_Batteries',
    'MXene': './/Datasets/MXene (V_nom=1)/Batteries/Antonio-ML',
}

from LSTM import *
import LSTM_chemistry as chem
import LSTM_with_norm as wn
dataset_function_dict = {
    'normal': Torch_Dataset_LSTM,
    'chemistry': chem.Torch_Dataset_LSTM,
    'with norm': wn.Torch_Dataset_LSTM
}

# Batteries to exclude from training and validation datasets:
ignore_dict = {
    'NASA': ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt'],
    'MIT': ['B'+str(i).zfill(4)+'.txt' for i in range(15, 48)],
    # There are no problems with these ^ batteries, it's just that MIT's dataset
    # is huge, so I don't include all of the batteries in the validation dataset
    'Tarascon Coin': ['B0008.txt', 'B0019.txt', 'B0023.txt'],
    'MXene': [],
}

# Batteries to include in the training dataset:
train_dict = {
    'NASA': ['B0006.txt',
             'B0030.txt',
             'B0038.txt',
             'B0039.txt',
             'B0041.txt',
             'B0045.txt',
             'B0055.txt'],
    'MIT': ['B0001.txt'],
    'Tarascon Coin': ['B0001.txt', 'B0003.txt', 'B0005.txt',
                      'B0006.txt', 'B0012.txt', 'B0013.txt',
                      'B0014.txt', 'B0018.txt', 'B0020.txt',
                      'B0021.txt'],
    'MXene': ['B0170.txt', 'B1821.txt', 'B1851.txt']
}

palette = {
    "NASA": 'blueviolet',
    "MIT": 'springgreen',
    "Tarascon Coin": 'red',
    'MXene': 'gold'
}