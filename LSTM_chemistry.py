from Cleaning import *
from Training import *
from torch.utils.data import Dataset, DataLoader
import torch.nn.functional as F
import torch.nn as nn
import re
from LSTM_Base import LSTM_Base, Dataset_Base

class LSTM_v1(LSTM_Base):
    """LSTM type neural net for Battery SOH estimation.

    This model acts just like :class:`LSTM.LSTM_v1`, but also takes the
    dataset's chemistry as an input in the regression layer (much like
    :class:`LSTM.LSTM_v4` takes the ambient temperature as input).

    A Li-ion dataset will have an input of 0, whilst a Na-ion dataset will have
    and input of 1.

    For more information about the model's architecture, check the documentation
    for :class:`LSTM.LSTM_v1`.

    Parameters
    ----------
    delta : int
        The size of the sliding window used to generate the x_i's.
    hidden_dim : int
        Hidden dimension of the LSTM (also its main output dimension).
    pooling_layer : {'last', 'mean', 'bi_mean"}
        Decides the type of pooling layer:
        - 'last' (only take the last result),
        - 'mean' (take the mean of the results) and
        - 'bi_mean' (take the mean of the product).
    drop_out : float
        The probability of the dropout layer that comes right after the LSTM.
        Must be a number between 0 and 1.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.

        This isn't a necessary parameter and is not used in the model in any
        way. It's just useful for documentation purposes and for normalizing
        test and validation sets.

    version : int, default = 1
        The model version to be used.

        * ``version`` = 1 corresponds to the model created by the article cited.
        * ``version`` = 2 corresponds to the same model, but adapted to also
          take the charge cycle's temperature data.
        * ``version`` = 3 is similar to version 1, but takes the ambient
          temperature as an extra inputs.

    lin_layers : int, default = 1
        The number of linear regression layers. As of now, cannot be greater
        than two.
    lin_dropout : float, optional
        The value for the dropout layer that comes after the first linear layer
        in the regression step. Will only be added if ``lin_layers`` > 1.

    Raises
    ------
    AssertionError
        If the version in different than 1, 2 or 3.
        If an unsupported pooling_layer is informed.

    Examples
    --------
    >>> from torch.utils.data import Dataset
    >>> from LSTM import *
    >>> delta = 10
    >>> hidden_dim = 20
    >>> version = 2
    >>> model = LSTM_v1(delta, hidden_dim, pooling_layer='last', version=version)
    >>> with open('B0005.txt', 'rb') as f:
    ...     Battery = pickle.load(f)
    ...
    >>> train_dataset = Torch_Dataset_LSTM([Battery], delta, version, fixed_len=150)
    >>> estimated_capacity = model(train_dataset.inputs[0])
    >>> loss_function = th.nn.MSELoss()
    >>> loss = loss_function(estimated_capacity, train_dataset.output[0])
    2.2862
    """
    def __init__(self, delta, hidden_dim, pooling_layer='bi_mean', drop_out=0,
                 norm=None, version=1, lin_layers=1, lin_dropout=0):

        super().__init__()

        assert version in [1, 2, 3], \
            "Unsupported version. For versions 4 and 5, call LSTM_v4 instead."

        # Model information:
        self.version = version                      # The kind of model
        self.variant = 'chemistry'
        self.pooling_layer = pooling_layer          # The type of pooling layer
        # Normalization information:
        self.norm = norm                            # The normalization used

        # Important dimensions
        self.delta = delta                          # The sliding window size
        self.hidden_dim = hidden_dim                # Hidden dimension (LSTM)

        # Input Dimension (number of input variables)
        if version == 1:
            self.inputs_dim = 2 * delta             # (I, V) * delta
        elif version == 2:
            self.inputs_dim = 3 * delta             # (I, V, T) * delta
        elif version == 3:
            self.inputs_dim = 2 * delta + 1         # (I, V) * delta + T_amb

        self.output_dim = 1

        self.dropout_value = drop_out

        assert (pooling_layer == 'last') or (pooling_layer == 'mean') or\
               (pooling_layer == 'bi_mean'), "pooling_layer"\
               " either 'last', 'mean' or 'bi_mean"

        # If the pooling layer requires the LSTM to be of bidirectional type
        if pooling_layer == 'bi_mean':
            bi_bool = True
        else:
            bi_bool = False

        # Definition of the LSTM itself
        self.lstm = nn.LSTM(self.inputs_dim, hidden_dim, batch_first=True,
                            bidirectional=bi_bool)

        # The linear layer that maps from hidden state space to tag space
        if lin_layers == 1:
            self.lin1 = nn.Linear(hidden_dim+1, self.output_dim)
            self.nb_layers = 1
        else:
            self.nb_layers = 2
            self.lin0 = nn.Linear(hidden_dim+1, hidden_dim//2)
            self.actf = nn.LeakyReLU()
            self.lin2 = nn.Linear(hidden_dim//2, self.output_dim)
            # self.lin3 = nn.Linear(hidden_dim//4, self.output_dim)
            self.lin_dropout_fn = nn.Dropout(lin_dropout)
            self.lin1 = nn.Sequential(
                self.lin0,
                self.actf,
                self.lin_dropout_fn,
                self.lin2,
            )

    def forward(self, inputs):

        # Input dimensions: [Batch size, Number of Xis (sequence length), 2*Delta]
        if len(inputs.shape) < 3:   # When Batch size equals 1, the first dimension is ignored,
                                    # but it's necesssary for the LSTM. So we have to reshape the inputs.
            inputs = inputs.view(1, inputs.shape[0], inputs.shape[1])

        tech = inputs[:, 0, -1]  # It doesn't change throughout the cycle
        lstm_inputs = inputs[:, :, :-1]

        lstm_out, _ = self.lstm(lstm_inputs)     # LSTM output
        shp_out = lstm_out.shape            # Its shape ([Batch size, Number of Xis (sequence length), self.hidden_dim])
        lstm_out = F.dropout(lstm_out, self.dropout_value)      # dropout layer

        # Pooling layer, according to the type chosen:
        if self.pooling_layer == 'last':
            pool_out = lstm_out[:, -1, :].view(shp_out[0], self.hidden_dim)         # Only the last result
        elif self.pooling_layer == 'mean':
            pool_out = th.mean(lstm_out, dim=1).view(shp_out[0], self.hidden_dim)   # Mean of the results
        elif self.pooling_layer == 'bi_mean':
            pool_out = th.mean(lstm_out[:, :, :self.hidden_dim]*                    # Mean of the multiplication
                               lstm_out[:, :, self.hidden_dim:], dim=1).view(shp_out[0], self.hidden_dim)
        else:
            raise NameError("Unknown pooling layer type: "+self.pooling_layer)

        # Linear layer
        output = self.lin1(th.cat([pool_out,
                                   tech.view(shp_out[0], 1)], dim=1))

        return output

class LSTM_v4(LSTM_Base):
    """
        LSTM type neural net for Battery SOH estimation.

    This model acts just like :class:`LSTM.LSTM_v4`, but also takes the
    dataset's chemistry as an input in the regression layer.

    A Li-ion dataset will have an input of 0, whilst a Na-ion dataset will have
    and input of 1.

    For more information about the model's architecture, check the documentation
    for :class:`LSTM.LSTM_v4`.

    Parameters
    ----------
    delta : int
        The size of the sliding window used to generate the x_i's.
    hidden_dim : int
        Hidden dimension of the LSTM (also its main output dimension).
    pooling_layer : {'last', 'mean', 'bi_mean"}
        Decides the type of pooling layer:
        - 'last' (only take the last result),
        - 'mean' (take the mean of the results) and
        - 'bi_mean' (take the mean of the product).
    drop_out : float
        The probability of the dropout layer that comes right after the LSTM.
        Must be a number between 0 and 1.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.

        This isn't a necessary parameter and is not used in the model in any
        way. It's just useful for documentation purposes and for normalizing
        test and validation sets.
    version : int, default = 4
        This is a new version being tested It will take the ambient temperature
        as an input directly into the linear layers.
        ``version`` = 4 corresponds to ``LSTM_v1``s version 1, and
        ``version`` = 5 corresponds to ``LSTM_v1``s version 2.
    lin_layers : int, default = 1
        The number of linear regression layers. As of now, cannot be greater
        than two.
    lin_dropout : float, optional
        The value for the dropout layer that comes after the first linear layer
        in the regression step. Will only be added if ``lin_layers > 1``.

    Raises
    ------
    AssertionError
        If the version in different than 4 or 5.
        If an unsupported pooling_layer is informed.

    Examples
    --------
    >>> from torch.utils.data import Dataset
    >>> from LSTM import *
    >>> delta = 10
    >>> hidden_dim = 20
    >>> version = 4
    >>> model = LSTM_v4(delta, hidden_dim, pooling_layer='last', version=version)
    >>> with open('B0005.txt', 'rb') as f:
    ...     Battery = pickle.load(f)
    ...
    >>> train_dataset = Torch_Dataset_LSTM([Battery], delta, version, fixed_len=150)
    >>> estimated_capacity = model(train_dataset.inputs[0])
    >>> loss_function = th.nn.MSELoss()
    >>> loss = loss_function(estimated_capacity, train_dataset.output[0])
    2.2862
    """
    def __init__(self, delta, hidden_dim, pooling_layer='bi_mean', drop_out=0,
                 norm=None, version=4, lin_layers=1, lin_dropout=0):

        super().__init__()

        assert version in [4, 5], "This version is not supported. For versions 1 to 3, call LSTM_v1 instead."

        # Model information:
        self.version = version                      # The kind of model
        self.variant = 'chemistry'
        self.pooling_layer = pooling_layer          # The type of pooling layer
        # Normalization information:
        self.norm = norm                            # The normalization used

        # Important dimensions
        self.delta = delta                          # The sliding window size
        self.hidden_dim = hidden_dim                # Hidden dimension (LSTM)

        # Input Dimension (number of input variables)
        if version == 4:
            self.inputs_dim = 2 * delta         # (I, V) * delta + T_amb
        if version == 5:
            self.inputs_dim = 3 * delta         # (I, V) * delta + T_amb

        self.output_dim = 1

        self.dropout_value = drop_out

        assert (pooling_layer == 'last') or (pooling_layer == 'mean') or\
               (pooling_layer == 'bi_mean'), "pooling_layer either 'last', " \
                                             "'mean' or 'bi_mean"

        # If the pooling layer requires the LSTM to be of bidirectional type
        if pooling_layer == 'bi_mean':
            bi_bool = True
        else:
            bi_bool = False

        # Definition of the LSTM itself
        self.lstm = nn.LSTM(self.inputs_dim, hidden_dim, batch_first=True,
                            bidirectional=bi_bool)

        # The linear layer that maps from hidden state space to tag space
        if lin_layers == 1:
            self.lin1 = nn.Linear(hidden_dim+2, self.output_dim)
            self.nb_layers = 1
        else:
            self.nb_layers = 2
            self.lin0 = nn.Linear(hidden_dim+2, hidden_dim // 2)
            self.actf = nn.LeakyReLU()
            self.lin2 = nn.Linear(hidden_dim // 2, self.output_dim)
            # self.lin3 = nn.Linear(hidden_dim//4, self.output_dim)
            self.lin_dropout_fn = nn.Dropout(lin_dropout)
            self.lin1 = nn.Sequential(
                self.lin0,
                self.actf,
                self.lin_dropout_fn,
                self.lin2,
            )

    def forward(self, inputs):

        # Input dimensions: [Batch size,Number of Xis (sequence length),2*Delta]
        if len(inputs.shape) < 3:
            # When Batch size equals 1, the first dimension is ignored,
            # but it's necesssary for the LSTM. So we have to reshape the inputs.
            inputs = inputs.view(1, inputs.shape[0], inputs.shape[1])

        amb_T = inputs[:, 0, -1]  # It doesn't change throughout the cycle
        tech = inputs[:, 0, -2]  # It doesn't change throughout the cycle
        lstm_inputs = inputs[:, :, :-2]

        lstm_out, _ = self.lstm(lstm_inputs)  # LSTM output
        shp_out = lstm_out.shape              # Its shape
        lstm_out = F.dropout(lstm_out, self.dropout_value)  # dropout layer

        # Pooling layer, according to the type chosen:
        if self.pooling_layer == 'last':
            pool_out = lstm_out[:, -1, :].view(shp_out[0], self.hidden_dim)
        elif self.pooling_layer == 'mean':
            pool_out = th.mean(lstm_out, dim=1).view(shp_out[0],
                                                     self.hidden_dim)
        elif self.pooling_layer == 'bi_mean':
            pool_out = th.mean(lstm_out[:, :, :self.hidden_dim]*
                               lstm_out[:, :, self.hidden_dim:],
                               dim=1).view(shp_out[0], self.hidden_dim)
        else:
            raise NameError("Unknown pooling layer type: "+self.pooling_layer)

        # Linear layer
        output = self.lin1(th.cat([pool_out,
                                   amb_T.view(shp_out[0], 1),
                                   tech.view(shp_out[0], 1)], dim=1))

        return output

class Torch_Dataset_LSTM(Dataset_Base):
    """Creates a pytorch Dataset class that can readily be loaded with torch's
    Loader class for training and validation.

    This class is just like :class:`Torch_Dataset_LSTM` except that it adds the
    battery's chemistry to the inputs - a value indicating if the battery is
    either of type Li-ion (0) or Na-ion (1). Check the other module's
    documentation for more information.

    .. warning::
        This class by itself is not capable of treating two dataset types in one
        go. Rather, it should be used for each dataset separately, and the
        datasets should then be mixed using :class:`Torch_Dataset_Mixer`

    Parameters
    ----------
    Battery_list : list[Clean_Battery]
        List with the Batteries that we want to use for creating the dataset.
    delta : int
        The size of the sliding window used to generate the x_i's.
    model_version : {1, 2, 3, 4, 5}
        An integer indicating which LSTM version to use.
        if ``version`` == 1: then only I and V data will be used.
        elif ``version`` == 2: then I, V and T will be taken into account.
        elif ``version`` == 3: then I, V and amb_T will be taken into account.
        elif ``version`` == 4: then I, V and amb_T will be taken into account,
        but amb_T will enter directly in the last step, the NN.
        elif ``version`` == 5: then I, V, T and amb_T will be taken into account,
        mixing models 2 and 4. Redundant.
    print_bool : bool, default = False
        Whether or not to print some information while the code runs.
        Namely, if we want to print the battery's name and the number of points.
    time_step : int, optional
        The desired time step for the resampling procedure.
        If ``None`` is given, than a time step will be automatically generated
        by the method ``time_step_function``.
    threshold : float, default = 0.1
        The minimum capacity value to be considered as relevant [A.h].
    min_len : int, default = 10
        The minimum amount of points a charge cycle must have to be considered
        as relevant.
    fixed_len : int, optional
        if different than ``None`` (default value), then the charge and discharge
        curves will be limited to ``fixed_len`` points. This will override
        ``min_len``.
    norm_type : {'zscore', 'minmax'}
        A string indicating which kind of normalization to do.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.
        In entered as input, ``norm_type`` will be overridden by norm[4] and the
        given information will be used for the normalization

    adimensionalize : bool, default = True
        Whether or not to make variables dimensionless before adding them to the
        inputs and output lists.

        * The Capacity (output) is normalized by the nominal capacity
          (``Q_nominal``);

        * The Current is normalized by the C rate (``C_rate``) -
          which is the current necessary for charging the battery in one hour
          (theoretically);

        * The Voltage is normalized by the nominal voltage (``V_nominal``),
          the voltage at the plateau in the CV part of the charge curve.

        The values of these parameters are defined inside of each battery class.

    cycle_lim : int, optional
        The greatest cycle number to take into account for the dataset creation.
        Default equals ``1e6`` meaning that no cycles will be ignored.

    Attributes
    ----------
    dataset_name : str
        The name of the dataset ('Dataset_Nasa', for instance).
    batteries : list[str]
        A list with the file name of each battery used.
    dataset_format : str
        A string indicating the type of data format used: 'You et al. [56]'.
    inputs : list[Tensors]
        A list with the input tensors for each cycle.
    output : Tensor
        A tensor with the output capacity tensors for each cycle.

    Raises
    ------
    AssertionError
        If the specified value of ``delta`` is < 1.
        If no batteries are given.
        If the specified version is invalid.
        If ``model_version`` is 2 or 5 and ``self.dataset_name`` is
        ``Clean_Coin_Battery``.
    ValueError
        If the model_version is unknown.

    Examples
    --------
    >>> from torch.utils.data import Dataset
    >>> from LSTM import Torch_Dataset_LSTM
    >>> delta = 10
    >>> version = 2
    >>> with open('B0005.txt', 'rb') as f:
    ...     Battery = pickle.load(f)
    ...
    >>> train_dataset = Torch_Dataset_LSTM([Battery], delta, version,
    ...                                    fixed_len=150, print_bool=True)
    B0005
    165
    """
    def __init__(self, Battery_list, delta, model_version, print_bool=False,
                 time_step=None, threshold=0.1, min_len=10, fixed_len=150,
                 norm_type='zscore', norm=None, adimensionalize=True,
                 cycle_lim=1e6
                 ):

        super().__init__()

        assert delta >= 1, 'delta must be greater or equal to one'
        assert len(Battery_list) > 0, 'No Batteries informed'
        assert model_version in [1, 2, 3, 4, 5], 'Invalid version'

        if fixed_len is None:
            if delta != 1:
                min_len = max(delta - 1, min_len)
                # min_len is the minimum sequence length (number of points) that
                # the charge cycle must have to be taken into account.
            else:
                min_len = 1
        else:
            min_len = fixed_len + delta

        # Dataset related
        self.dataset_name = type(Battery_list[0]).__name__  # The dataset used
        self.batteries = []
        self.adimensionalize = adimensionalize

        for Battery in Battery_list:
            self.batteries.append(Battery.Name+'.txt')

        if self.dataset_name == 'Clean_Coin_Battery':
            assert model_version in [1, 3, 4], "Can't use models 2 or 5 with" \
                "Clean_Coin_Battery. No data on temperature during cycling."
            self.tech = 1.
            new_battery_list = []
            for Battery in Battery_list:
                for cell in Battery.cell:
                    new_battery_list.append(cell)
            Battery_list = new_battery_list
        else:
            self.tech = 0

        self.min_len = min_len
        self.fixed_len = fixed_len
        self.Battery_list = Battery_list

        if time_step is None:
            time_step = self.time_step_function()

        self.dataset_format = 'You et al. [56]'   # The data format used
        # LSTM related
        self.delta = delta                        # The amount of points per xi
        self.model_version = model_version        # The model version
        # Cleaning related:
        self.time_step = time_step                # The time step for resampling
        self.threshold = threshold                # The threshold for cleaning
        self.cycle_lim = cycle_lim

        ent = []        # Inputs list
        sai = []        # Output list

        for Battery in Battery_list:
            try:
                threshold = threshold / (Battery.mass)
            except AttributeError:
                pass
            C_rate = Battery.C_rate
            Q_nominal = Battery.Q_nominal
            V_nominal = Battery.V_nominal

            if print_bool:
                print(Battery.Name)
            for index, cycle_couple in enumerate(Battery.cycle_couple):
                if index > cycle_lim:
                    # If we've gone over the limit number of cycles to test.
                    break
                chr_cycle = cycle_couple[0]
                dis_cycle = cycle_couple[1]

                # print(time_step)
                Data = Resampling_flexible(chr_cycle, variable='Time',
                                           time_step=time_step, min_len=min_len)

                if Data is None:
                    # When there aren't enough points in the cycle, Cycle_t is
                    # returned as None
                    continue
                # Cycle_t = Data[:, 0]

                Cycle_i = Data[:, 1]
                Cycle_v = Data[:, 2]
                Cycle_T = Data[:, 3] # NOT THE TEMPERATURE IF DATASET_TYPE =
                                     # COIN

                if adimensionalize:
                    Cycle_i = Cycle_i/C_rate
                    Cycle_v = Cycle_v/V_nominal

                # Cleaning will take away NaN's, the beginning and the end of
                # the discharge curves (that is, the part before the current
                # reaches the CC value and the part after when the current
                # reaches its cut-off value ``i_cutoff``) and resample the time
                # according to the given time step ``time_step``.

                # Calculating the real number of xis we've got
                if fixed_len is None:
                    Nb = len(Cycle_i) - delta + 1  # Number of xis in this cycle
                else:
                    Nb = fixed_len
                # min_len already take care of the cases where Nb <= 0.

                if self.model_version == 1:
                    XT = np.zeros((Nb, 2 * delta+1))
                    # Regrouped xi info for one whole cycle

                    for t in range(Nb):  # We're going to make one xi for each t
                        xi = np.concatenate([Cycle_i[t:t + delta],  # Current
                                             Cycle_v[t:t + delta],
                                             self.tech*np.ones([1])])  # Voltage
                        # "Add" it to the XT tensor that regroups them.
                        XT[t, :] = xi

                elif self.model_version == 2:
                    XT = np.zeros((Nb, 3 * delta+1))
                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             Cycle_T[t:t + delta],
                                             self.tech*np.ones([1])])# Temperature

                        XT[t, :] = xi

                elif (self.model_version == 3) or (self.model_version == 4):
                    XT = np.zeros((Nb, 2 * delta + 2))

                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             chr_cycle.amb_T*np.ones([1]),
                                             self.tech*np.ones([1])])
                                             # Ambient temperature

                        XT[t, :] = xi

                elif self.model_version == 5:
                    XT = np.zeros((Nb, 3 * delta + 2))

                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             Cycle_T[t:t + delta],
                                             chr_cycle.amb_T*np.ones([1]),
                                             self.tech*np.ones([1])])

                        XT[t, :] = xi
                else:
                    raise ValueError("Unknown version: "+str(self.model_version))

                Capacity = dis_cycle.data.capacity*np.ones(1)

                if Capacity.item() < threshold:
                    # If it's considered to be too close to zero
                    continue

                if adimensionalize:
                    Capacity=Capacity/Q_nominal

                # The XT tensor represents one cycle and is thus one input
                ent.append(th.from_numpy(XT).float())
                # The capacity is one output.
                sai.append(Capacity)

            # The number of inputs and outputs must be the same
            assert len(ent) == len(sai), 'Unequal input and output lengths'

        inputs, output, norm = Normalize(ent, sai, norm_type=norm_type,
                                         norm=norm)

        if print_bool:
            print(len(inputs))

        self.inputs = inputs  # saving the inputs
        self.output = output  # saving the outputs
        self.norm = norm  # saving the norm

        self.path = None
        self.transform = None

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, item):

        ent = self.inputs[item]
        sai = self.output[item]

        return ent, sai

class Torch_Dataset_Mixer(Dataset):
    """Class for mixing different dataset types.

    .. note::
        Notice that the inputs and outputs are normalised prior to this class's
        initialization, and that each dataset that is being mixed will have
        different normalisation parameters. In this sense, this class cannot
        have a ``norm`` attribute and thus attempts to denormalize outputs
        directly from this class will fail. Because of this, when simulating
        results with :func:`Training.simulate`, make sure you call it with
        ``denormalize=False`` so that no ``AttributeError`` is raised.

    .. warning::
        Tests weren't very successful so development has been halted. Missing
        functionalities and attributes may cause ``AttributeError``s in other
        functions.

    Parameters
    ----------
    dataset_list : list[Torch_Dataset_LSTM]
        A list of the different datasets to be used.
    renorm : bool, optional
        Whether or not to renormalize all the data with respect to the whole
        set. Experimental, didn't yield good results.

    """

    def __init__(self, dataset_list, renorm=False):

        super().__init__()
        from Cleaning import Denormalize, Normalize
        self.dataset_list = dataset_list
        self.inputs = []
        self.output = th.Tensor([])
        inputs_list = []
        output_list = []
        for dataset in dataset_list:
            if renorm:
                for tensor in dataset.inputs:
                    inputs_list.append(Denormalize(tensor, dataset.norm, inputs=True))
                output_list = output_list + Denormalize(dataset.output, dataset.norm, inputs=False).tolist()
            else:
                self.inputs = self.inputs + dataset.inputs
                self.output = th.cat([self.output, dataset.output], dim=0)

        if renorm:
            inputs, outputs, norm = Normalize(inputs_list, output_list)
            self.inputs = inputs
            self.output = outputs
            self.norm = norm
        else:
            self.norm = None

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, item):
        ent = self.inputs[item]
        sai = self.output[item]
        return ent, sai

    def report(self):
        string = ''
        for dataset in self.dataset_list:
            string += '\n\n'+str(dataset)+'\n'
            string += dataset.report()
        return string

    def dataset_info(self, name):
        """
        .. deprecated::
            Discontinued
        """
        #TODO
        pass

def norm_adapt(norm, tech=0):
    """Function for adapting a given ``norm`` tuple for LSTM_chemistry dataset.

    This function basically adds an entry to the normalisation parameters
    corresponding to the battery's chemistry. It's useful for when user wants to
    use a ``norm`` calculated for other variants for this one.

    Parameters
    ----------
    norm : tuple
        A norm tuple.
    tech : {0, 1}, default = 0
        An ``int`` indicating which battery chemistry is used (0 is Li-ion and
        1 is Na-ion).

    Returns
    -------
    tuple
        A norm tuple as normal.

    Raises
    ------
    AssertionError
        If the ``tech`` is different than 0 or 1.
    """

    assert tech in [0,1], "Invalid chemistry type: "+str(tech)

    inputs_mean = th.cat([norm[0], th.Tensor([tech])])
    inputs_stdv = th.cat([norm[1], th.Tensor([1])])

    return (inputs_mean, inputs_stdv, norm[2], norm[3], norm[4])