import pandas as pd
import numpy as np
import re

def table_from_report(file_name):
    """Creates an Excel Spreadsheet with model performance from an analysis file

    The spreadsheet is saved in the respective report folder with the name of
    the analysis file.

    .. _Figure 0:

    .. figure:: Images/table_example.png
        :figwidth: 50 %

        Figure 0: Example of a table created with this function.

    Parameters
    ----------
    file_name : str
        Name of the Analysis file ("Analysis_Best.txt", "Analysis_MIT.txt" etc.)
    """
    try:
        file = open(file_name, 'r')
        df = pd.DataFrame()

        line_list = file.readlines()
        index = 0
        train_list = []
        while True:
            # b_index = line_list[index].find('B00')
            b_index = re.search('B\d{4}', line_list[index])
            idx1, idx2 = b_index.span()
            battery_number = int(line_list[index][idx1+1:idx2])
            print(battery_number)
            if line_list[index].find('training') != -1:
                train_str = 'Training'
            else:
                train_str = 'Validation'
            print(train_str)

            # Mean and Max Relative Error:
            error_index = line_list[index + 3].find(':')
            print(line_list[index + 3][error_index + 2:error_index + 6])
            mean_rel_error = float(
                line_list[index + 3][error_index + 2:error_index + 6]
            ) * 1e-2
            print(line_list[index + 3][error_index + 9:error_index + 13])
            max_rel_error = float(
                line_list[index + 3][error_index + 9:error_index + 13]
            ) * 1e-2

            # Mean and Max Absolute Error:
            error_index = line_list[index+4].find(':')
            print(line_list[index + 4][error_index + 2:error_index + 6])
            mean_error = float(
                line_list[index + 4][error_index + 2:error_index + 6]
            ) * 1e-2
            print(line_list[index + 4][error_index + 9:error_index + 13])
            max_error = float(
                line_list[index + 4][error_index + 9:error_index + 13]
            ) * 1e-2

            df_2 = pd.DataFrame(
                data=np.array([[battery_number,
                                mean_error, max_error,
                                mean_rel_error, max_rel_error]]),
                columns=['Battery Number',
                         'Mean error', 'Max error',
                         'Mean Rel. Error', 'Max Rel. Error']
            )

            train_list.append(train_str)

            df = df.append(df_2)

            index += 7
            if index >= len(line_list):
                break

    finally:
        file.close()
        df['Set'] = np.array(train_list).reshape(len(train_list), 1)
        with pd.ExcelWriter('Report'+file_name+'.xlsx') as writer:
            df.to_excel(writer, sheet_name=file_name)