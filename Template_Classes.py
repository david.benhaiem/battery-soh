import math
from Cleaning import *
import pandas as pd
import numpy as np
import os

class Raw_Dataset:
    """Class for saving the dataset raw data as is.

    .. note::
        The following are required attributes.

    Required Attributes
    -------------------
    Name : str
        Battery name ("B000X")
    cycle : list(Raw_Dataset.Raw_Cycle)
        List of the cycle objects

    """
    def __init__(self, raw_file, battery_number=None):

        # Naming:
        if battery_number is None:
            # If 'raw_file' is already in the standard B0002 form
            self.Name = raw_file.split('.')[0]
        else:
            # You can number the battery yourself before calling the function
            self.Name = 'B'+str(battery_number).zfill(4)


        # Line for opening and exploring the raw_file
        # This will depend on the file type etc.
        # example for '.mat' files:

        import scipy.io
        raw_data = scipy.io.loadmat(raw_file)

        # Then you'll have to work from there how to get the cycles etc.
        # Since this class is only for the raw data, you can customize
        # attributes etc. however you want. Only the Clean Battery has to follow
        # some rules to be compatible with other code.

        self.cycle = []

        # This structure will depend on your file
        for index, cycle in enumerate(raw_data):
            self.cycle.append(self.Raw_Cycle(cycle, index))
            # Here, I'm defining the cycle and data classes inside the class
            # This is better practice, I should have known better for the older
            # codes...

        pass

    class Raw_Cycle:
        """Class for storing Cycle data from raw battery data

        In some datasets, charge and discharge data come together in one file,
        there's no problem with that, you can save them as one single cycle
        here (not for the clean class).
        Check :py:mod:`MIT_Classes` to see how this is handled there.

        .. note::
            The following are required attributes.

        Required Attributes
        -------------------
        number : int
            The cycle number.
        type : {'charge', 'discharge'}
            The cycle type.
        amb_T : float
            The ambient temperature in which the battery was cycled [ºC]
        data : list(Raw_Dataset.Raw_Cycle.Raw_Data)
            List with the actual data objects

        """
        def __init__(self, cycle, index):

            # From now on, I'll pretend cycle is like a dictionary
            # Of course, the rest of the code will depend on how this object is
            # structured


            if cycle['number'] is not None:
                # If the cycle contains its number:
                self.number = cycle['number']
            else:
                # We'll give it an index.
                self.number = index

            self.type = cycle['type']
            self.amb_T = float(cycle['ambient_temperature'])

            self.data = self.Raw_Data(cycle['data'], self.type)

        class Raw_Data:
            """Class for storing the actual raw data.

            .. note::
                The following are required attributes.

            Required Attributes
            -------------------
            time : array like
                Time array [s]
            meas_i : array like
                Measured Current array [A]
            meas_v : array like
                Measured Voltage array [V]
            capacity : float
                Capacity value for the charge [A.h] (for discharge cycles).

            .. note::
                The following are recommended attributes for when the
                corresponding data is available.

            Recommended Attributes
            ----------------------
            meas_t : array like
                Measured temperature [ºC]
            meas_q : array like
                Measured capacity [A.h]

            """

            def __init__(self, data, type='discharge'):

                self.time = data['time']
                self.meas_i = data['measured_current']
                self.meas_v = data['measured_voltage']
                self.meas_t = data['measured_temperature']
                if type == 'discharge':
                    self.capacity = data['capacity']

class Clean_Dataset:
    """Class for cleaning and storing clean data

    .. note::
        The following are required attributes.

    Required Attributes
    -------------------
    Name : str
        Battery name ("B00XX").
    capacity_curve : array like
        A 2D array where the first column is the cycle number and the second is
        the capacity value [A.h] associated to it.
    cycle_couple : list(tuple(Clean_Dataset.Clean_Dataset_Cycle))
        A list with the cleaned charge and discharge cycles (respectively) that
        are associated one with the other (the charge cycle is always the one
        that comes immediately after the discharge cycle).
    C_rate : float
        The C rate associated to the battery for nondimensionning the
        current [A].
    Q_nominal : float
        The battery's nominal capacity (for nondimensionning the SOH) [A.h].
    V_nominal : float
        The battery's nominal voltage (for nondimensionning the voltage) [V].


    .. note::
        The following are recommended attributes.

        This has the purpose of simplifying things, but you'll still have to
        define them in the Dataset classes individually. That's why it's just
        recommended.

    Recommended Attributes
    ----------------------
    couples : list(tuple(int))
        When the charge and discharge data are in separate cycles, we need to
        first find the related couples. This will save their numbers so they can
        later be called.

    """

    def __init__(self, Battery, i_cutoff=2e-2, min_len=1, threshold=0.1,
                 z_lim=3):
        """

        Parameters
        ----------
        Battery : Raw_Dataset
            The Raw Battery we are going to clean and store here
        i_cutoff : float, default = 2e-2
            The current cut-off value to be used to determine the end of the
            charge cycles [A].

        min_len : int, default = 1
            The minimum amount of points a charge cycle must have to be
            considered as relevant. Equal to one by default. Something similar
            is done during the resampling stages in the dataset creation.

        threshold : float, default = 0.1
            the minimum capacity value to be considered as relevant [A.h].
        z_lim : float
            The maximum zscore allowed when searching for outliers.
        """

        self.Name = Battery.Name
        self.capacity_curve = []
        self.cycle_couple = []

        # if the cycles are already separated in charge and discharge cycles:
        # self.couples = []
        #
        # Finding the couples:
        #
        # dis_nb = -1
        # for index, Cycle in enumerate(Battery.cycle):
        #     if Cycle.type == 'discharge':
        #             dis_nb = index
        #     elif Cycle.type == 'charge':
        #         if dis_nb != -1:
        #             chr_nb = index
        #             self.couples.append((chr_nb, dis_nb))
        #             dis_nb = -1
        #
        # for couple in self.couples:
        #   chr_cycle = Battery.cycle[couple[0]]
        #   dis_cycle = Battery.cycle[couple[1]]
        #   chr_cycle, dis_cycle = self.separate_and_clean(chr_cycle, dis_cycle)
        #   # continue from line 237 ('if chr_cycle is None:'...)
        #

        # if the charge and discharge cycles are stuck in one single cycle
        # entry:

        for cycle in Battery.cycle:
            chr_clean, dis_clean = self.separate_and_clean(cycle, i_cutoff,
                                                           min_len, threshold)

            if (chr_clean is None) or (dis_clean is None):
                continue


            # If cycles are in order discharge - charge:
            self.cycle_couple.append((chr_clean, dis_clean))
            # else: #(we save this discharge cycle and wait for the next)
            # if len(self.cycle_couple) == 0:
            #     old_dis_clean = dis_clean
            #     self.cycle_couple.append('Wait') #
            #     continue
            # else:
            #     # Saving the cycle couple
            #     # print(chr_clean.number, old_dis_clean.number)
            #     self.cycle_couple.append((chr_clean, old_dis_clean))
            #     old_dis_clean = dis_clean
            #
        # self.cycle_couple.pop(0)  # Take out the 'Wait' entry

        # Creating the cycle couple attribute:
        for index, cycle_couple in enumerate(self.cycle_couple):
            capacity_point = (index, cycle_couple[1].data.capacity)
            self.capacity_curve.append(capacity_point)

        # Searching for outliers in the capacity data
        cycle_couple, capacity_curve = Outliers(
            self.cycle_couple, self.capacity_curve, z_lim=z_lim
        )

        self.cycle_couple = cycle_couple
        self.capacity_curve = capacity_curve

        self.raw_couples = self.couples     # If it makes sense in this dataset
        self.couples = []
        for cycle_couple in self.cycle_couple:
            chr_nb = cycle_couple[0].number
            dis_nb = cycle_couple[1].number
            self.couples.append((chr_nb, dis_nb))


    def separate_and_clean(self, cycle, i_cutoff, min_len, threshold):
        """Separate cycles (if needed) and cleans them.

        I recommend looking at
        :func:`Coin_Classes.Clean_Cell.separate_coin_cycles` and
        :func:`Cleaning.Input_outliers_v3`.

        """
        # Do something that separates the cycles (if needed)

        chr_cycle = 0
        dis_cycle = 0
        problem = True

        # Then clean them (check other datasets for their cleaning methods).
        
        # Then call the clean cycle class
        chr_clean = self.Clean_Dataset_Cycle(chr_cycle, i_cutoff, min_len)
        dis_clean = self.Clean_Dataset_Cycle(dis_cycle, i_cutoff, min_len,
                                             threshold)
        # If something goes wrong:
        if problem:
            return None, None

        return chr_clean, dis_clean


    class Clean_Dataset_Cycle:
        """Creates a clean cycle class

        """
        def __init__(self, cycle, i_cutoff, min_len, threshold=0.1):

            self.number = cycle.number
            self.type = cycle.type
            self.amb_T = cycle.amb_T
            self.data = self.Clean_Data_Class(cycle.data)
            pass

        #def clean_cycle(self):
        #    pass

        class Clean_Data_Class:
            def __init__(self, data):
                self.time = data.time
                self.meas_i = data.meas_i
                self.meas_v = data.meas_v
                try:
                    self.capacity = data.capacity
                except AttributeError:
                    pass





