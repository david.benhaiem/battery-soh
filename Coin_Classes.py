import math
from Cleaning import *
import pandas as pd
import numpy as np
import os

class Dataset_Coin:
    """Dataset for storing the raw coin-type Na-ion battery data provided by
    Jean-Marie Tarascon and Sathiya Mariyappan.

    Each dataset contains information for one set of experiments, which usually
    contain 2 cells, the data of which are usually separated in two parts. The
    first part contains the first 10 charge-discharge cycles (formation step),
    and the second usually contains a rest period (of 1 week) and the rest of
    the charge-discharge cycles. In a few cases, the rest period may show up in
    the first part instead of the first, or in a third part.

    The data for each experiment set is in a folder with the
    respective battery number, in '/Datasets/Tarascon/Coins/Raw_Data' . The
    files are named as follows:
    'Cell' + cell number + '_' + part number + '.txt'

    .. note::
        The console must already be in the correct directory upon calling of the
        function (/Datasets/Tarascon/Coins/Raw_Data).

    The data contains time [s], voltage [V], current [I] and capacity [A.h]
    measurements. The capacity should, however, be normalized by the sodium mass,
    which is given in the excel spreadsheet present in the same directory:
    'Information regarding cells.xlsx'. The code will automatically read it and
    save all of the information contained.

    Parameters
    ----------
    battery_number : int
        The battery number, so the right directory can be opened

    Attributes
    ----------
    number : int
        The battery number informed upon initialization.
    Name : str
        The battery's name: 'B'+str(battery_number).zfill(4).
        For battery_number = 1: Name = B0001; 12: Name = B0012 etc.
    electrode_batch : str
        Information about the thickness of the coating: 'High Energy' will
        relate to thick coating, while 'High Power' will relate to thin.
    amb_T : float
        Temperature of the experiment. Usually (if not always) 55ºC.
    chemistry : str
        Information on the chemistry of the battery. The actual chemicals used 
        are hidden because of secrecy reasons.
    additive : str
        Information on the additives used (if any).
    NVPF_mass_1 : float
        The mass [mg] of the NVPF electrode used for the first cell.
    NVPF_mass_2 : float or str
        The mass [mg] of the NVPF electrode used for the second cell or '-' when
        not applicable.
    cell : list(Coin_Cell)
        A list of Coin_Cell objects containing information and data of the
        specific cells.
    """
    def __init__(self, battery_number):

        self.number = battery_number
        self.Name = 'B'+str(battery_number).zfill(4)

        # Information from the excel spreadsheet:
        excel_df = pd.read_excel('Information regarding cells.xlsx')
        row = excel_df[excel_df['FOLDER Name '].eq(battery_number)]
        np_row = row.to_numpy()
        self.electrode_batch = np_row[0,1]
        self.amb_T = float(np_row[0,2].split(np_row[0,2][-2])[0])
        self.chemistry = np_row[0,3]
        self.additive = np_row[0,4]
        self.NVPF_mass_1 = np_row[0,5]
        self.NVPF_mass_2 = np_row[0,6]

        # The actual charge/discharge data.
        os.chdir('.//'+str(battery_number))
        self.cell = []
        for cell in range(1, 3):
            cell_file = 'Cell'+str(cell)+'_'
            self.cell.append(Coin_Cell(cell_file, cell,
                                       getattr(self, 'NVPF_mass_'+str(cell))))
        os.chdir('../')


        # Reading the actual data:

class Coin_Cell:
    """Class for storing the data for a single coin-like cell of Na-ion data.

    This follows from the :class:`Dataset_Coin` class.

    Parameters
    ----------
    cell_file : str
        The first half of the file's name. The second half will be added later
        according to the part number. The ``cell_file`` looks something like:
        *'Cell1_'*.
    cell_number : int
        The number of the cell.
    mass : float
        The mass of the cell [mg].

    Attributes
    ----------
    number : int
        The cell number.
    mass : float
        The mass of the cell [mg].
    part : list(Coin_Part)
        A list of Coin_Part objects, containing the different parts of the data
        (from 1 to 3 entries).
    """
    def __init__(self, cell_file, cell_number, mass):

        self.number = cell_number
        self.mass = mass
        self.part = []
        for part in range(1, 4):
            part_file = cell_file + str(part) + '.txt'
            try:
                self.part.append(Coin_Part(part_file))
            except FileNotFoundError:
                if (part == 1) or (part == 2):
                    self.part.append(None)
                elif part == 3:
                    break

class Coin_Part:
    """Class for storing the data contained in each '.txt' cell part file.

    This class follows from the Coin_Cell class.

    Parameters
    ----------
    part_file : str
        The final file_name for the part file.

    Attributes
    ----------
    time : np.ndarray
        The time stamps [s].
    meas_v : np.ndarray
        The measured voltage [V].
    meas_i : np.ndarray
        The measured current [mA].
    meas_q : np.ndarray
        The measured capacity [A.h].
    """
    def __init__(self, part_file):

        df = pd.read_csv(part_file, delimiter="\t")
        if len(df.columns) > 4:
            df = df.drop(df.columns[-1], axis=1)
        df = df.to_numpy()
        self.time = df[:, 0:1]
        self.meas_v = df[:, 1:2]
        self.meas_i = df[:, 2:3]
        self.meas_q = df[:, 3:4]

class Clean_Coin_Battery:
    """Class for storing the cleaned data from a coin-type battery provided by
    Jean-Marie Tarascon and Sathiya Mariyappan.

    This will take a Dataset_Coin object, save its cells in the same way
    (attribute cell, which is a list of class:`Clean_Cell` objects this time)
    but separate the cycles contained in each part in different objects
    (specifically class:`Coin_Cycle` objects) - for more information see the
    documentation for the class:`Clean_Cell` class. As in the
    class:`Nasa_classes.Clean_Battery` class, the cycles are then separated into
    couples of charge and discharge cycles and saved in attribute
    ``cycle_couples``

    .. note::
        Many attributes are directly inherited from the ``Dataset_Coin`` class
        given as input to the initialization. Because of this, they aren't
        described in this documentation, only in the ``Dataset_Coin``'s. These
        attributes are: ``Name``, ``number``, ``electrode_batch``, ``amb_T``,
        ``chemistry``, ``additive``, ``NVPF_mass_1``, ``NVPF_mass_2``.

    Parameters
    ----------
    Battery : Dataset_Coin
        The Dataset_Coin object from which we'll be extracting and treating the
        data.
    threshold : float, default = 0.1
        The minimum value of capacity [A.h, before normalization per mass] a
        cycle must have for it to be considered as relevant. Otherwise, the
        cycle is discarded.

    Attributes
    ----------
    cell : list(Clean_Cell)
        List with the Clean_Cell objects containing information about each cell.
    """
    def __init__(self, Battery, threshold=0.1):

        # Base information:
        self.Name = Battery.Name
        self.number = Battery.number
        self.electrode_batch = Battery.electrode_batch
        self.amb_T = Battery.amb_T
        self.chemistry = Battery.chemistry
        self.additive = Battery.additive
        self.NVPF_mass_1 = Battery.NVPF_mass_1
        self.NVPF_mass_2 = Battery.NVPF_mass_2

        # Now for the cells and cycles:
        self.cell = []
        for cell in Battery.cell:
            self.cell.append(Clean_Cell(cell, self.Name, amb_T=self.amb_T,
                                        threshold=threshold))

            if len(self.cell[-1].cycle) == 0:
                self.cell.pop(-1)

class Clean_Cell:
    """Class that stores the cleaned data for each coin-type cell.

    This follows from the :class:`Clean_Coin_Battery` class.

    .. note ::
        Some of this class's attributes are directly inherited from the
        :class:`Coin_Cell` class which is used as a base for the cleaning and thus
        won't be cited in this documentation. These attributes are: ``number``,
        ``mass``.

    Parameters
    ----------
    cell : Coin_Cell
        The Coin_Cell object used as a base.
    Name : str
        The battery's name. This isn't a intrinsically necessary information to
        have in this class, but it is defined because dataset creation classes
        may call for a battery's ``Name`` attribute, so this avoids unnecessary
        errors/warnings.
    amb_T : float, default = 55.
        The ambient temperature where the experiments where held.
    threshold : float, default = 0.1
        The minimum value of capacity [A.h, before normalization per mass] a
        cycle must have for it to be considered as relevant. Otherwise, the
        cycle is discarded.

    Attributes
    ----------
    separation : int
        The cycle number that separates the first part of the data from the
        second. This is mostly for documenting than anything.
    cycle : list[Coin_Cycle]
        A list with all of the tested cycles. See ``Coin_Cycle`` and
        ``clean_coin_cycle`` (in `Cleaning.py`) documentations for details on
        the class structure and on how the cycles are treated.
    raw_couples : list[tuple(int)]
        The number of the charge and discharge cycles in each couple.
        Example:
        self.couples[k] = (charge cycle number, discharge cycle number)
    couples : list[tuple(int)]
        Exactly like the ``raw_couples`` attribute, but without the entries that
        are judged as abnormal or that have missing datapoints or that are too
        short etc.
    cycle_couple : list[tuple(Clean_Cycle)]
        The Coin_Cycle objects corresponding to each couple.
        self.cycle_couple[k] = (charge Coin_Cycle, discharge Coin_Cycle)
    capacity_curve : np.ndarray
        Array with the capacity value for each discharge cycle. The first
        column corresponds to the cycle's number and the second one to the
        capacity value (normalized by the electrode's mass) [A.h/g].
    C_rate : float
        The C rate associated to the battery for nondimensionning the
        current [A].
    Q_nominal : float
        The battery's nominal capacity (for nondimensionning the SOH) [A.h].
    V_nominal : float
        The battery's nominal voltage (for nondimensionning the voltage) [V].

    """
    def __init__(self, cell, Name, amb_T=55., threshold=0.1):

        self.C_rate = 0.1284*cell.mass
        self.Q_nominal = 0.1284
        self.V_nominal = 4.3
        self.cycle = []
        self.mass = cell.mass
        self.Name = Name

        threshold = threshold/cell.mass
        last_time = 0   # To maintain battery history
        for part_nb in range(len(cell.part)):
            part = cell.part[part_nb]
            if part_nb == 1:
                self.separation = len(self.cycle)
                if self.separation == 0:  # First part was None
                    # We take the first cycles into account...
                    cycle_number = 21
                else:
                    cycle_number = self.separation
            else:
                cycle_number = 0

            if part is not None:

                data = np.concatenate([part.meas_v, part.meas_i,
                                       part.meas_q], axis=1)

                # if time is not None:
                self.separate_coin_cycles(part.time, data, amb_T=amb_T,
                                          cycle_number=cycle_number,
                                          last_time=last_time)

                last_time = part.time[-1]

        dis_nb = -1
        self.couples = []
        self.cycle_couple = []
        self.capacity_curve = []
        for idx, cycle in enumerate(self.cycle):
            if cycle.type == 'discharge':
                dis_nb = idx
            elif cycle.type == 'charge':
                if dis_nb != -1:
                    chr_nb = idx
                    self.couples.append((chr_nb, dis_nb))
                    dis_nb = -1

        # print(self.couples)
        for couple in self.couples: # For each charge/discharge couple:
            # Saving the cycles
            chr_cycle = self.cycle[couple[0]]    # The charge cycle
            dis_cycle = self.cycle[couple[1]]    # The discharge cycle

            # For the discharge cycle first:
            if math.isnan(dis_cycle.data.capacity): # if the capacity is nan:
                continue    # skip this couple
            elif dis_cycle.data.capacity < threshold:
                continue

            self.cycle_couple.append((chr_cycle, dis_cycle))  # Saving the cycle couple

        for i in range(len(self.cycle_couple)):
            capacity_point = (i, self.cycle_couple[i][1].data.capacity)     # Defining the capacity point
            self.capacity_curve.append(capacity_point)                      # Saving the capacity point

        self.capacity_curve = np.array(self.capacity_curve)

        # Searching for outliers in the capacity data
        if len(self.capacity_curve) != 0:
            cycle_couple, capacity_curve = Outliers(self.cycle_couple,
                                                    self.capacity_curve)
            self.cycle_couple = cycle_couple
            self.capacity_curve = capacity_curve
        else:
            pass

        self.raw_couples = self.couples
        self.couples = []

        for cycle_couple in self.cycle_couple:
            chr_nb = cycle_couple[0].number
            dis_nb = cycle_couple[1].number
            self.couples.append((chr_nb, dis_nb))


    def separate_coin_cycles(self, time, data, amb_T=55., cycle_number=0,
                             last_time=0, min_len=50):

        """Function for separating the cycles from the
        :class:`Coin_Part`'s raw data.

        The dataset's raw data comes with all of the cycles concatenated.
        This function will separate them from each other and add them to
        ``Clean_Cell``'s ``cycle`` attribute (list of :class:`Coin_Cycle`
        objects).

        The function starts by seeing when the first cycle starts (Current ≠ 0).
        In some cases, the file being read corresponds solely to the rest
        period, in these cases the current = 0 all the way so the function
        prints "I = 0" and quits by returning ``None``.

        A `while` loop is then used to check when the current changes sign
        (switching from charge, I > 0, to discharge, I < 0, and vice-versa).
        The index of the following cycle is saved in ``final_idx``. Only data
        before ``final_idx`` is then treated for the current cycle.
        If the current no longer changes sign (the last cycle is reached), an
        ``IndexError`` is raised, which leads the ``try`` statement another way
        in order to later stop the loop.

        Knowing the index window of the cycle, a moving window outlier detection
        is done through ``pandas``'s ``rolling(...)`` method. The window and
        threshold were *hand-picked* to eliminate big outliers and avoid
        eliminating curve detail. A interquantile range method was also tested
        but yielded worse results.

        Finally, if the last few points of the cycle have ``Current = 0``, these
        points are taken away. This happens in some cases because the only way
        one cycle is separated from the other is by checking when the current
        changes sign.

        Finally, if the cycle is too short (< ``min_len``) it is discarded (not
        added to the ``self.cycle`` list).

        Parameters
        ----------
        time : np.ndarray
            Time stamps [s].
        data : np.ndarray
            The rest of the data (voltage [V], current [mA] and capacity [A.h])
            concatenated in one array (features in columns, axis=1).
        amb_T : float, default = 55.
            The ambient temperature [ºC].
        cycle_number : int, default = 0
            The cycle number.
        last_time : float, default = 0
            Last time stamp from the last battery *part* cleaned.

            In some cases, the time stamps from one *part* to the next are reset
            to zero (example: from the *part* before the rest and the one right
            after).

            Usually only ∆t is important for analyses, but having absolute
            information in better and might come in handy in the future.
        min_len : int, default = 50
            The minimum length (number of points) the cycle must have for it to
            be considered relevant.
        """

        mass = self.mass

        if time[0] < last_time:
            time += last_time

        Current = data[:, 1:2]

        # Checking where the cycles begin (I ≠ 0)
        try:
            idx_begin = np.where(Current != 0)[0][0]
        except IndexError:
            print('I = 0')
            return None

        data = data[idx_begin:, :]
        time = time[idx_begin:, :]
        Current = Current[idx_begin:, :]

        start_idx = 0
        run = True

        while run:
            try:

                # Checking when the current changes sign:
                final_idx = np.where(
                    np.sign(Current[start_idx:, 0]) == (-1) * np.sign(
                        Current[start_idx, 0]
                    )
                )[0][0]

                final_idx = final_idx + start_idx
            except IndexError:
                # We are in the last cycle
                final_idx = len(Current)
                run = False  # Will stop the while loop.

            cycle_data = np.concatenate([time[start_idx: final_idx, :],
                                         data[start_idx: final_idx, :]],
                                        axis=1)

            cycle_data, _ = Input_outliers_v3(cycle_data, feature_index=1,
                                              window=100)

            if cycle_data[-1, 2] == 0:
                idx = np.where(cycle_data[:, 2:3] == 0)[0][0]
                cycle_data = cycle_data[:idx, :]

            start_idx = final_idx

            if len(cycle_data) >= min_len:
                cycle_class = Coin_Cycle(cycle_data, cycle_number, mass,
                                         amb_T=amb_T)
                self.cycle.append(cycle_class)
            else:
                pass
            cycle_number += 1

    def plot_capacity_curve(self):
        q = np.stack(self.capacity_curve)
        plt.plot(q[:, 0], q[:, 1], label='Discharge Capacity')
        plt.xlabel("Cycle Number")
        plt.ylabel("Capacity (mA.h)")
        plt.title(f"Capacity Curve: {self.Name}")
        plt.legend()
        plt.show()

class Coin_Cycle:
    """Class for storing information about each cycle of Na-ion battery dataset.

    Parameters
    ----------
    data : np.ndarray
        Array containing all of the important data (voltage [V], current [mA] and
        capacity [A.h]) concatenated in one array (features in columns, axis=1).
    number : int
        The cycle number.
    mass : float
        The cell's mass [mg].
    amb_T : float, default = 55.
        The ambient temperature where the experiments where held.

    Attributes
    ----------
    number : int
        The cycle number
    amb_T : float
        The ambient temperature where the experiments where held.
    data : Coin_Data
        Class with the actual information.
    """
    def __init__(self, data, number, mass, amb_T = 55.):

        self.number = number
        if np.sign(data[0, 2]) > 0:
            self.type = 'charge'
        else:
            self.type = 'discharge'
        self.amb_T = amb_T
        self.data = Coin_Data(data, mass)

class Coin_Data:
    """Class for storing information about each cycle of Na-ion battery dataset.

    Parameters
    ----------
    data : np.ndarray
        Array containing all of the important data (voltage [V], current [A] and
        capacity [A.h]) concatenated in one array (features in columns, axis=1).
    mass : float
        The cell's mass [mg].

    Attributes
    ----------
    meas_v : ndarray[floats]
        Battery terminal voltage [V].
    meas_i : ndarray[floats]
        Battery output current [mA].
    meas_q : ndarray[floats]
        Battery capacity variatioon over time, normalized by the mass) [A.h/mg].
    time : ndarray[floats]
        Time vector for the cycle [s].
    capacity : float
        The capacity value normalized by the mass[A.h/mg].
    """
    def __init__(self, data, mass):

        self.time = data[:, 0:1]
        self.meas_v = data[:, 1:2]
        self.meas_i = data[:, 2:3]
        self.meas_q = data[:, 3:4]/mass
        self.capacity = data[-1, 3].item()/mass


########################################################################################



if __name__ == '__main__':
    import pickle

    # For treating the raw data:
    # os.chdir('.//Datasets/Tarascon/Coins/Raw_Data')
    # for i in range(1, 7):
    #     print(i)
    #     Battery = Dataset_Coin(i)
    #     with open(Battery.Name+'.txt', 'wb') as f:
    #         pickle.dump(Battery, f)
    #     Clean_Battery = Clean_Coin_Battery(Battery)
    #     print(Clean_Battery.Name)
    #     os.chdir('../')
    #     os.chdir('.//Clean_Batteries')
    #     with open(Clean_Battery.Name+'.txt', 'wb') as f:
    #         pickle.dump(Clean_Battery, f)
    #
    #     os.chdir('../')
    #     os.chdir('.//Raw_Data')

    ##
    files = os.listdir()
    import re

    file_list = []  # List with the valid files

    for file_name in files:
        match = re.search(r'B\d{4}.txt', file_name)
        # Only take into account the battery files
        if match is None:
            continue
        else:
            file_list.append(file_name)
    file_list.sort()

    for file_name in file_list:
        if file_name == 'B0007.txt':
            continue

        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)
        print(Battery.Name)
        Clean_Battery = Clean_Coin_Battery(Battery)
        os.chdir('../')
        os.chdir('.//Clean_Batteries')
        with open(Clean_Battery.Name+'.txt', 'wb') as f:
            pickle.dump(Clean_Battery, f)

        os.chdir('../')
        os.chdir('.//Raw_Data')
