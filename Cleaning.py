import matplotlib.pyplot as plt
import torch as th
import pandas as pd
from PyAstronomy import pyasl
from scipy import stats
import numpy as np

def Input_outliers_v3(Data, feature_index=0, window=50):
    """Identifies and takes away the outliers present in the
    dataset.

    This is a third version of the previous ``Input_outliers`` function
    (now discontinued). It is more general in the sense that it takes as input
    a whole dataset matrix instead of the separate vectors and can thus study
    differently sized data.

    Most of the work is still done by the function :func:`Outliers_idx`.

    Parameters
    ----------
    Data : ndarray[float]
        The cycle's data. Each column must be a different feature.
    feature_index : int, default = 1
        The index associated to the feature used for the outlier
        detection (column of the ``Data`` matrix).
    window : int, default = 50
        Size of the sliding window used for the outlier detection.

    Returns
    -------
    An np.ndarray of the ``New_Data``, without the outliers, and a boolean
    np.ndarray ``outlier_array`` with information about where the outliers are
    located.
    """

    features = Data.shape[1]
    df = pd.DataFrame(data=Data, columns=[str(i) for i in range(features)])

    # Outlier detection
    threshold = 3
    df['median'] = df[str(feature_index)].rolling(window, center=True).median()
    df['std'] = df[str(feature_index)].rolling(window, center=True).std()
    df = df.fillna(method='bfill')
    df = df.fillna(method='ffill')
    difference = np.abs(df[str(feature_index)] - df['median']) / df['std']
    outlier_idx = (difference > threshold)
    df = df[~outlier_idx]

    df.drop(['median', 'std'], axis=1, inplace=True)
    cycle_data = df.to_numpy()

    return cycle_data, outlier_idx

def Outliers(cycle_couple, capacity_curve, z_lim=3):
    """
    Removes outliers from the battery's output capacity data.

    As of know, it removes outliers based on moving window averages (generalized
    Extreme Studentized Deviate (ESD) test - implemented through PyAstronomy's
    pyasl.pointDistGESD) and on zscore (by taking away any remaining points with
    a z-score > z_lim). Where z_score_i = x_i - mean(x)/std(x).

    This function is called by the class :class:`Nasa_Classes.Clean_Battery`.

    Parameters
    ----------
    cycle_couple : list[tuple(Clean_Cycle)]
        The list of tuples associated to each cycle couple. It's a necessary
        input because it will be updated as we remove outliers.
    capacity_curve : list[tuple(float)]
        List of tuples ("capacity points"). Each tuple corresponds to one
        capacity point. The first entry is the cycle_couple index associated to
        the capacity value - which is the second entry.
    z_lim : float, default = 3
        The maximum zscore allowed when searching for outliers.

    Returns
    -------
    cycle_couple : list[tuple(Clean_Cycle)]
        The list of tuples associated to cycle couple after outlier removal.
    capacity_curve : ndarray
        A 2D array containing in the cycle_couple index in the first column and
        the associated capacity value in the second.
    """

    # Turning the output list into a numpy array:
    capacity_curve = np.array(capacity_curve)

    # Taking away local outliers (based on moving windows)
    outlier_index = Outliers_idx(capacity_curve[:, 1], force=True)
    cycle_numbers = list(capacity_curve[outlier_index, 0])
    cycle_numbers.sort(reverse=True)

    # Only the non_outliers:
    for i in cycle_numbers:
        # Updating the cycle_couple (taking away the outliers)
        cycle_couple.pop(int(i))

    # Updating the capacity curve (important because of the change in indexing!)
    capacity_curve = []
    for i in range(len(cycle_couple)):
        capacity_point = (i, cycle_couple[i][1].data.capacity)
        capacity_curve.append(capacity_point)
    capacity_curve = np.array(capacity_curve)

    # Taking away bigger outliers:
    # We check which points have a z_score < z_lim
    bool_array = (np.abs(stats.zscore(capacity_curve[:, 1:2])) < z_lim)
    outlier_array = bool_array.all(axis=1)
    # Outlier indexes:
    outlier_index = np.flip(np.where(outlier_array == False)[0])

    while len(outlier_index) > 0:
        cycle_numbers = list(capacity_curve[outlier_index, 0])
        cycle_numbers.sort(reverse=True)
        # Removing outliers:
        for i in cycle_numbers:
            cycle_couple.pop(int(i))

        # Recreating the capacity curve list with the new numbering:
        capacity_curve = []
        for i in range(len(cycle_couple)):
            capacity_point = (i, cycle_couple[i][1].data.capacity)
            capacity_curve.append(capacity_point)

        # Restarting to see how it goes:
        capacity_curve = np.array(capacity_curve)
        bool_array = (np.abs(stats.zscore(capacity_curve[:, 1:2])) < z_lim)
        outlier_array = bool_array.all(axis=1)
        # Outlier indexes:
        outlier_index = np.flip(np.where(outlier_array == False)[0])

    return cycle_couple, capacity_curve

def Outliers_idx(inputs, force=False, outlier_lim=20):
    """
    Identifies outliers in a data array and outputs their indexes.

    As of know, it only removes outliers based on moving window averages
    (generalized Extreme Studentized Deviate (ESD) test - implemented through
    PyAstronomy's `pyasl.pointDistGESD <https://pyastronomy.readthedocs.io/en/latest/pyaslDoc/aslDoc/outlier.html#PyAstronomy.pyasl.pointDistGESD>`_).

    Parameters
    ----------
    inputs : ndarray
        The array of data we want to find the outliers of.
    force : bool, optional
        Whether or not to let the data pass (don't take away any outliers) when
        ZeroDivisionError is raised during outlier identification.

        This error may be raised when the array is completely constant (but not
        always, apparently...) - depending on what we're studying, it may make
        sense to discard the data (force = False) and in others not (force =
        True).
    outlier_lim : int, default = 20
        The maximum amount of outliers picked per pass. Check maxOLs argument in
        PyAstronomy's pyasl.pointDistGESD documentation (linked above).

    Returns
    -------
    list[ints]
        A list containing the index of each outlier.
    """

    try:
        # Finding the local outliers (based on moving windows)
        r = pyasl.pointDistGESD(inputs, outlier_lim, alpha=0.01)
    except ZeroDivisionError:
        r = [0, 1]
        if force:
            r[1] = []   # No outliers
        else:
            r[1] = [i for i in range(len(inputs))]  # All outliers

    return r[1]

def Resampling_flexible(Cycle, variable='Voltage', time_step=0.01, min_var=None,
                        max_var=None, min_len=1, nan_lim=1, time_diff_lim=None):
    """
    This function resamples a cycle's data to a desired step, while also
    keeping an eye for NaNs and correcting them when necessary.

    It can resample data based on variables other than time.
    In particular, it's used for resampling capacity data in function of voltage
    for the Severson dataset creation (Torch_Dataset_SEV in FFNN.py
    (note (2022): FFNN.py was discontinued). In it, features are extracted from
    the difference between two Q(V) curves.

    .. note::
        If the cycle is deemed as too short or abnormal, then all of the arrays
        are returned as ``None``.

    Parameters
    ----------
    Cycle : Clean Cycle object
        A cycle_like object containing time, current, voltage and temperature
        data (for a Clean_Cycle class), or time, current, voltage, temperature
        and capacity data (for Clean_MIT_Cycle classes).

        Units: [s], [A], [V], [ºC] and [A.h] (when applicable) respectively.

    variable : str, default = 'Voltage'
        The desired variable to use as a base for the resampling.
        Can either be 'Time', 'Voltage', 'Current', 'Temperature' or 'Capacity'.
    time_step : float, default = 0.01
        The desired step for resampling. The unit will depend on the variable
        but it has to follow the input convention.
    min_var : float, default = 2.
        The lower bound of the resampling.
    max_var : float, default = 4.
        The higher bound of the resampling.
    min_len : int, default = 1
        The minimum length (number of points) the cycle must have at the end of
        the resampling for it to be considered relevant.
    nan_lim : int, default = 1
        The amount of NaNs the resampled vector must have to print a warning and
        return None.
    time_diff_lim : float, optional
        The biggest time jump the raw dataset can have before resampling.

        If the dataset has a time jump that is too big, resampling is
        compromised and the data cannot be used (simple interpolation can't
        reproduce the missing data), returns ``None``.
        If no value is suggested, it defaults to 10 times the ``time_step``
        size.

    Returns
    -------
    An np.ndarray with the resampled data.

    Raises
    ------
    AssertionError
        If Cycle.type == 'impedance' (when the input Cycle is a Clean_Cycle).
        If min_len < 1.
    NameError
        If the dataset type is unknown.
    """
    Dataset_type = type(Cycle).__name__

    if Dataset_type == 'Clean_Cycle':
        assert Cycle.type != 'impedance', 'Only charge and discharge cycles are allowed.'
    assert min_len >= 1, 'min_len must be greater or equal to 1'

    if time_diff_lim is None:
        time_diff_lim = 10*time_step

    # Numpy arrays
    Time = Cycle.data.time  # Time data

    # Checking right away
    if len(Time) < min_len:
        return None

    Current = Cycle.data.meas_i  # Current data
    Voltage = Cycle.data.meas_v  # Voltage data

    if Dataset_type == 'Clean_MIT_Cycle':
        Temperature = Cycle.data.meas_t  # Temperature data
        if Cycle.type == 'charge':
            Capacity = Cycle.data.chr_capacity
        else:
            Capacity = Cycle.data.dis_capacity
        # Concatenating the time and the data
        Cat_Data = np.concatenate((Time, Current, Voltage,
                                   Temperature, Capacity), axis=1)  # Cat stands for concatenated
        features = ['Time', 'Current', 'Voltage', 'Temperature', 'Capacity']
    elif Dataset_type == 'Clean_Cycle':
        Temperature = Cycle.data.meas_t  # Temperature data
        Cat_Data = np.concatenate((Time, Current, Voltage,
                                   Temperature), axis = 1)
        features = ['Time', 'Current', 'Voltage', 'Temperature']
    elif Dataset_type == 'Coin_Cycle':
        Capacity = Cycle.data.meas_q
        Cat_Data = np.concatenate((Time, Current, Voltage, Capacity),
                                  axis=1)  # Cat stands for concatenated
        features = ['Time', 'Current', 'Voltage', 'Capacity']
    elif Dataset_type == 'Clean_MXene_Cycle':
        Capacity = Cycle.data.meas_q
        Cat_Data = np.concatenate((Time, Current, Voltage, Capacity),
                                  axis=1)  # Cat stands for concatenated
        features = ['Time', 'Current', 'Voltage', 'Capacity']
    else:
        # In case new classes are implemented:
        raise NameError("Unknown dataset type: "+Dataset_type)

    # Creating a data frame
    df = pd.DataFrame(data=Cat_Data,
                      columns=features)
    df.set_index(variable, inplace=True)

    # Taking any NaN's away:
    df = df.dropna()

    # Taking away any duplicated values:
    df = df[~df.index.duplicated()]

    # Resampling:
    if min_var is None:
        min_var = df.index.min()
    if max_var is None:
        max_var = df.index.max()

    resampled_variable = np.linspace(min_var, max_var,
                                     int((max_var-min_var)/time_step))

    df = df.reindex(df.index.union(
        resampled_variable)).interpolate('values').loc[resampled_variable]
    df.index.name = variable

    # Checking for NaNs created during resampling:
    if df.isnull().values.any():        # If there's any

        idx_list = df.index.tolist()
        nan_idx0 = np.unique(np.where(df.isnull())[0]).tolist()     # The lines
        nan_idx0.sort(reverse=True)     # reverse order

        # Warning message is printed if too many NaNs
        if len(nan_idx0) > nan_lim:
            print("Warning, ", len(nan_idx0),
                  " NaNs in this Cycle after resampling")

            return None

        # Find the groups of NaNs
        groups = ranges(nan_idx0)   # ranges is a function, see below.
        groups.sort(reverse=True)   # reverse order

        for tuple in groups:
            i, j = tuple
            if i == 0 or j == len(df) - 1:
                # If the first or last lines are NaN, then we have to
                # take away all of the NaN points in this group because no
                # simple interpolation is possible
                df.drop(df.index[[k for k in range(i, j+1)]], inplace=True)
            else:
                for k in range(i, j+1):
                    # mean interpolation
                    df.loc[idx_list[k]] = (df.loc[idx_list[k-1]] + df.loc[idx_list[j+1]])/2

    # if there are still any NaNs:
    if df.isnull().values.any():
        print("Persistent NaNs.")
        print('Cycle number: ', Cycle.number)
        return None

    # Saving the new data
    df = df.reset_index(level=[variable])
    Data = df.to_numpy()

    if len(Data) < min_len:
        return None

    return Data

def ranges(nums):
    """
    Finds groups of consecutive numbers in a list and outputs a tuple with the
    first and last values of each group.

    This function was taken from `this StackOverflow question
    <https://stackoverflow.com/questions/2361945/detecting-consecutive-integers-in-a-list>`_

    Parameters
    ----------
    nums : list[int]
        The list with the integers.

    Returns
    -------
    list[tuple(int,int)]
        A list containing the tuples.

    Examples
    --------
    >>> A_list = [1, 2, 3, 4, 7, 9, 10]
    >>> ranges(A_list)
    [(1,4), (7,7), (9,10)]
    """
    nums = sorted(set(nums))
    gaps = [[s, e] for s, e in zip(nums, nums[1:]) if s + 1 < e]
    edges = iter(nums[:1] + sum(gaps, []) + nums[-1:])
    return list(zip(edges, edges))

def Normalize(inputs, output, norm_type='zscore', norm=None, concatenate=False):
    """
    Normalizes both the inputs and outputs based either on z-score or on min-max
    normalization.

    All of the necessary normalization information is stored in the `norm` tuple
    as follows:

    .. code-block:: text

        If norm_type == 'zscore':
            norm = (inputs' mean, inputs' standard deviation,
                    outputs' mean, outputs' standard deviation, 'zscore')
        elif norm_type == 'minmax':
            norm = (min(inputs), max(inputs),
                    min(output), max(output), 'minmax')

    This normalization information can then be used as an input for this
    function for normalizing other datasets.

    .. note::
        If a ``norm`` tuple is specified, the specified ``norm_type`` will be
        overridden by the one in ``norm[4]``.

    The normalization procedure does take into account cases where the standard
    deviation or the difference max - min equals zero (constant inputs or
    outputs). The standard deviation and (max - min) values may be overridden to
    1 in order to avoid division by zero. In some cases, an AssertionError may
    be raised.

    Parameters
    ----------
    inputs : list[Tensor]
        The list containing the input tensors.
    output : list[float]
        The list containing the output capacity values.
    norm_type : {'zscore', 'minmax'}
        A string indicating which kind of normalization to do.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.
        In entered as input, `norm_type` will be overridden by norm[4] and the
        given information will be used for the normalization
    concatenate : bool
        Whether or not to return the normalized concatenated inputs
        instead of normalizing each input separately and returning a list.

    Returns
    -------
    inputs : list[Tensor] or Tensor
        The normalized inputs. If concatenate == True, then it's a simple Tensor,
        otherwise, it's a list of tensors.
    output_stack: Tensor
        The normalized outputs.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string)
        The normalization information used for normalizing the inputs and the
        outputs.

    Raises
    ------
    AssertionError
        When all of the inputs or all of the outputs are constant.
        When normalization type is not supported
    """

    # Stacking inputs. This can be done for calculating the normalization
    # information but the actual normalization may have to be done case by case
    # (concatenate = False).
    inputs_stack = th.cat(inputs, dim=0)
    # Stacking outputs.
    output_stack = th.FloatTensor(output)

    if norm is not None:        # If a norm tuple has been specified
        # The norm_type is overridden by the one contained in the norm.
        norm_type = norm[4]

    assert norm_type in ['zscore', 'minmax'], 'Normalization type not supported'

    if norm_type == 'zscore':
        if norm is None:        # If no norm tuple has been specified

            # Checking if there are entries for which the standard deviation
            # is zero:
            std_in = th.std(inputs_stack, dim=0)    # inputs
            bool_std = (std_in == 0)                # if yes, then:
            std_in[bool_std] = 1
            # Those entries are set to 1, in order to avoid division by zero.

            std_out = th.std(output_stack, dim=0)   # outputs
            bool_std = (std_out == 0)               # Analogously
            std_out[bool_std] = 1

            # creation of the norm tuple:
            norm = (th.mean(inputs_stack, dim=0), std_in,
                    th.mean(output_stack, dim=0), std_out, norm_type)

        # output normalization
        output_stack = (output_stack - norm[2])/norm[3]

        if concatenate:     # If we can concatenate, then:
            # the inputs are overridden by the normalized input stack
            inputs_2 = (inputs_stack - norm[0]) / norm[1]
        else:
            inputs_2 = []
            for i in range(len(inputs)):
                # This is necessary for the LSTM if the sequences are of
                # different lengths.
                inputs_2.append((inputs[i] - norm[0]) / norm[1])

    elif norm_type == 'minmax':

        if norm is None:  # no norm tuple has been specified.
            min_in = th.amin(inputs_stack, dim=0)
            max_in = th.amax(inputs_stack, dim=0)
            min_out = th.amin(output_stack, dim=0)
            max_out = th.amax(output_stack, dim=0)

            # asserting that the inputs are not all constant.
            assert ~(min_in == max_in).all(), "Constant inputs"
            # asserting that the output is not constant
            assert ~(min_out == max_out), "Constant output"

            bool_minmax = (min_in == max_in)    # Some inputs can, however, be constant. This is specially true for
            # min_in[bool_minmax] = 1           # the k-means approach (now discontinued), where some batteries may have 0 points at some
            max_in[bool_minmax] += 1            # clusters. In that case, (max - min) is set to 1 to avoid division by 0

            norm = (min_in, max_in,
                    min_out, max_out, norm_type)    # creation of the norm tuple

        output_stack = (output_stack - norm[2])/(norm[3] - norm[2])     # output normalization

        if concatenate:     # Analogous to the z-score normalization
            inputs_2 = (inputs_stack - norm[0]) / (norm[1] - norm[0])
        else:
            inputs_2 = []
            for i in range(len(inputs)):
                inputs_2.append((inputs[i] - norm[0])/(norm[1] - norm[0]))

    return inputs_2, output_stack, norm

def Denormalize(tensor, norm, inputs=False):
    """Function for "denormalizing" input or output data.

    Parameters
    ----------
    tensor : torch.Tensor
        The tensor with the information to be denormalized
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string)
        The normalization information used for normalizing the inputs and the
        outputs.
    inputs : bool, default = False
        Whether or not the given tensor is an input or an output type.
        This is necessary because they'll use different normalization
        information.

    Returns
    -------
    th.Tensor
        The denormalized tensor.
    """

    assert norm is not None, "'norm' tuple is None."
    tensor = tensor.to('cpu')

    # For inputs
    if inputs:
        if norm[4] == 'zscore':
            tensor = (tensor * norm[1].cpu()) + norm[0].cpu()
        elif norm[4] == 'minmax':
            tensor = (tensor * (norm[1].cpu() - norm[0].cpu())) + norm[0].cpu()
        else:
            raise AssertionError(norm[4], ' normalization not supported')

    # For outputs!
    else:
        if norm[4] == 'zscore':
            tensor = (tensor * norm[3].cpu()) + norm[2].cpu()
        elif norm[4] == 'minmax':
            tensor = (tensor * (norm[3].cpu() - norm[2].cpu())) + norm[2].cpu()
        else:
            raise AssertionError(norm[4], ' normalization not supported')

    return tensor