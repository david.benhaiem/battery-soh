import numpy as np
from Cleaning import *
import math
import h5py

class Dataset_MIT:
    """
    Severson et al. dataset from "Data-driven prediction of battery cycle
    life before capacity degradation" (check note for full reference).

    .. note::
        Full reference: Severson, K.A., Attia, P.M., Jin, N. et al.
        Data-driven prediction of battery cycle life before capacity
        degradation. Nat Energy 4, 383–391 (2019).
        <https://doi.org/10.1038/s41560-019-0356-8>

    .. note::
        Thorough information about each dataset can be found at:
        <https://data.matr.io/1/projects/5c48dd2bc625d700019f3204>

    Parameters:
    -----------
    f : HDF5 File
        The HDF5 File (h5py.File) object corresponding to the file itself.
        It is necessary since it's called during the cycle creation.
    battery_number : int
        The battery number.
    print_bool : bool
        Toggles some information printing on and off (number of cycles, etc.).

    Attributes
    ----------
    Name : str
        The battery's name. It is created based on the battery number.
        The number is padded with zeros in order to contain 4 digits.
        If battery_number = 12, then the battery Name will be B0012.
    cycle_life : float
        The battery's cycle life (based on 20% nominal capacity degradation -
        going from 1.10 A.h to 0.88 A.h).
    policy : str
        A string containing information about the charge cycle.

        It's written in the format "C1(Q1)-C2". C1 is the first C rate used
        for charging, Q1 is the SOC where this first charging step stops and
        C2 is the second C rate used for charging.
    cycle : list(MIT_Cycle)
        List containing the cycling information. See MIT_Cycle class.

    Examples
    --------
    >>> f = h5py.File('2017-06-30_batchdata_updated_struct_errorcorrect.mat')
    >>> battery_number = 24
    >>> B = Dataset_MIT(f, battery_number, print_bool=True)
    >>> B.policy
    '4.8C(80%)-4.8C' # in this case the 1st and second charges are at the same
                     # C rate.
    >>> B.Name
    'B0024'
    >>> B.cycle_life
    495.0
    >>> len(B.cycle)
    524

    """
    def __init__(self, f, battery_number, print_bool=False):

        # Battery name:
        self.Name = 'B'+str(battery_number).zfill(4)

        # Cycle_life:
        cycle_life_ref = f['batch']['cycle_life'][battery_number, 0]
        cycle_life = f[cycle_life_ref]
        self.cycle_life = cycle_life[0,0] # float

        # Policy:
        policy_ref = f['batch']['policy_readable'][battery_number, 0]
        policy = f[policy_ref]
        policy_str = ''
        for i in range(len(policy)):
            policy_str += policy[i, 0].tobytes()[::2].decode()
        self.policy = policy_str

        # Cycling data:
        self.cycle = []

        # Summary (will be called for each cycle):
        summary_ref = f['batch']['summary'][battery_number, 0]
        summary = f[summary_ref]

        # Cycling data (will be called for each cycle):
        battery_ref = f['batch']['cycles'][battery_number, 0]
        battery = f[battery_ref]

        if print_bool:
            print('Number of cycles: ', len(battery['I']))

        for cycle_nb in range(len(battery['I'])):
            # print(Cycle_nb)
            Cycle = MIT_Cycle(f, battery, summary, cycle_nb)
            self.cycle.append(Cycle)

class MIT_Cycle:
    """
    A Cycle class for the Severson dataset.

    Parameters
    ----------
    f : HDF5 File
        The HDF5 File (h5py.File) object corresponding to the file itself.
    battery : HDF5 group
        The group containing the cycling information for each cycle.
    summary : HDF5 group
        The group containing the summary information for each cycle.
    Cycle_nb : int
        The Cycle's number.

    Attributes
    ----------
    number : int
        The cycle's number.
    amb_T : int
        The cycle's ambient temperature. They were all done at 30ºC.
        This is saved to try to agree as much as possible with the Nasa_Cycle
        class as well as to document this detail.
    summary_keys : list
        A list containing the name of the following attributes:

        IR : float
            Measured internal resistance.
        QCharge : float
            The charge cycle's capacity.
        QDischarge : float
            The discharge cycle's capacity.
        Tavg : float
            The cycle's average temperature.
        Tmax : float
            The cycle's maximum temperature.
        Tmin : float
            The cycle's minimum temperature.
        chargetime : float
            The charge cycle's duration.
        cycle : float
            The cycle number + 1.

    battery_keys : list
        A list containing the name of the variables stored in the battery group.
    data : MIT_Data
        The Severson_Data class with all of the important data.
    """
    def __init__(self, f, battery, summary, Cycle_nb):

        # Cycle number:
        self.number = Cycle_nb
        # Ambient temperature (is the same for all of the dataset):
        self.amb_T = 30

        # Summary information:
        keys = summary.keys()
        self.summary_keys = list(keys)  # So that we can always check the
                                        # attributes' names.
        # keys = ['IR', 'QCharge', 'QDischarge', 'Tavg', 'Tmax', 'Tmin',
        # 'chargetime', 'cycle']

        for key in keys:
            setattr(self, key, summary[key][0, Cycle_nb])

        # Battery information
        keys = battery.keys()
        self.battery_keys = list(keys)
        battery_dict = {}
        for key in keys:
            key_ref = battery[key][Cycle_nb, 0]
            key_dataset = f[key_ref]
            key_array = key_dataset[0, :-1]
            # Will hold the information for all of the data, temporarily:
            battery_dict[key] = key_array.reshape([len(key_array), 1])

        self.data = MIT_Data(battery_dict, self.QDischarge)  # The actual data

class MIT_Data:
    """
    Data class for the Severson dataset.

    Parameters
    ----------
    battery_dict : dict
        Dictionary containing all of the data arrays.
    capacity : float
        The battery's calculated discharge capacity at the end of the cycle.

    Attributes
    ----------
    time : ndarray[floats]
        Time vector for the cycle [min].
    meas_v : ndarray[floats]
        Battery terminal voltage [V].
    meas_i : ndarray[floats]
        Battery output current [A].
    meas_t : ndarray[floats]
        Battery temperature [ºC].
    capacity : float
        The battery's calculated discharge capacity at the end of the cycle [A.h].
    dis_capacity : ndarray[floats]
        The battery discharge capacity evolution during cycling [A.h].
    chr_capacity : ndarray[floats]
        The battery charge capacity during cycling [A.h].
    dis_dQdV : ndarray[floats]
        The differential capacity curve calculated from the discharge data.
    Qdlin : ndarray[floats]
        Linearly interpolated capacity information (?).
    Tdlin : ndarray[floats]
        Linearly interpolated temperature information (?).
    """
    # def __init__(self, Data, Type, Qd = None):
    def __init__(self, battery_dict, capacity):

        # Cycle information is also present in the Nasa dataset:
        self.time = battery_dict['t']
        self.meas_i = battery_dict['I']
        self.meas_v = battery_dict['V']
        self.meas_t = battery_dict['T']
        self.capacity = capacity

        # Information during cycling that will likely be unused:
        self.dis_capacity = battery_dict['Qd']
        self.chr_capacity = battery_dict['Qc']
        self.dis_dQdV = battery_dict['discharge_dQdV']
        self.Qdlin = battery_dict['Qdlin']
        self.Tdlin = battery_dict['Tdlin']

class Clean_MIT_Battery:
    """Class that holds the cleaned cycle couples and their data for one Battery.

    It takes as a base one Dataset_Severson class (one Battery from the Severson
    Dataset). From it, it extracts the cycle couples and cleans each one
    of them, excluding those that present abnormalities.

    It cleans the discharge cycle by taking away the first few points before the
    actual cycle begins and the last ones after the current has reached its
    cut-off point (< ``i_cutoff``).

    Couples are excluded in this process when:
        The discharge curve's capacity value is nan, or it's below the
        ``threshold`` value.
        When the charge cycle is too short (< ``min_len``).
    The cycle couples that weren't excluded then pass by an outlier detection
    based on the capacity value. Outliers are detected by sliding windows and
    z-score values (mainly sliding windows, small ``z_lim`` values yielded bad
    results, incorrectly excluding large amounts of the dataset).

    Parameters
    ----------
    Battery : Dataset_MIT
        The battery we want to create the class for.
    threshold : float, default = 0.1
        the minimum capacity value to be considered as relevant [A.h].
    i_cutoff : float, default = 1.1/50
        The current cut-off value to be used to determine the end of the charge
        cycles [A].
    min_len : int, default = 1
        The minimum amount of points a charge cycle must have to be considered
        as relevant. Equal to one by default. Something similar is done during
        the resampling stages in the dataset creation.
    zlim : float, default = 6
        The maximum zscore allowed when searching for outliers.

    Attributes
    ----------
    Name : string
        The battery's name.
    cycle_life : float
        The battery's cycle life (based on 20% nominal capacity degradation -
        going from 1.10 A.h to 0.88 A.h).
    policy : str
        A string containing information about the charge cycle.
        Check ``Dataset_Severson``'s documentation for more information.
    capacity_curve : ndarray[float, float]
        A 2D array where the first column corresponds to the cycle couple number
        and the second to the associated capacity value [[], A.h].
    cycle_couple : list[tuple(Clean_MIT_Cycle)]
        The Clean_Cycle objects corresponding to each couple.

        .. code-block::

            self.cycle_couple[k] = (charge Clean_MIT_Cycle,
                                    discharge Clean_SEV_Cycle)

    C_rate : float
        The C rate associated to the battery for nondimensionning the
        current [A].
    Q_nominal : float
        The battery's nominal capacity (for nondimensionning the SOH) [A.h].
    V_nominal : float
        The battery's nominal voltage (for nondimensionning the voltage) [V].

    """
    def __init__(self, Battery, threshold=0.1, i_cutoff=1.1/50, min_len=1,
                 zlim=6, print_bool=True):

        self.C_rate = 1.1
        self.V_nominal = 3.6
        self.Q_nominal = 1.1
        self.Name = Battery.Name
        self.cycle_life = Battery.cycle_life
        self.policy = Battery.policy
        self.capacity_curve = []
        self.cycle_couple = []

        if print_bool:
            print(len(Battery.cycle))

        for cycle in Battery.cycle: # For each charge/discharge couple:
            # Here, a cycle corresponds to a cycle couple
            # For the discharge cycle first:
            if math.isnan(cycle.data.capacity): # if the capacity is nan:
                print('Cycle number ', cycle.number, ' NaN')
                continue    # skip this couple
            elif cycle.data.capacity < threshold:
                print('Cycle number ', cycle.number, ' has capacity = ',
                      cycle.data.capacity, ' < threshold = ', threshold)
                continue

            chr_data, dis_data = self.clean_MIT_cycle_method(cycle,
                                                             i_cutoff=i_cutoff,
                                                             min_len=min_len)

            if chr_data is None:
                # Cycle is too short.
                print('Cycle number ', cycle.number, ' too short')
                continue

            chr_clean = Clean_MIT_Cycle(cycle, chr_data, 'charge')
            dis_clean = Clean_MIT_Cycle(cycle, dis_data, 'discharge')

            if len(self.cycle_couple) == 0:
                old_dis_clean = dis_clean
                self.cycle_couple.append(None)
                continue
            else:
                # Saving the cycle couple
                self.cycle_couple.append((chr_clean, old_dis_clean))
                old_dis_clean = dis_clean

        self.cycle_couple.pop(0)

        for i in range(len(self.cycle_couple)):
            capacity_point = (i, self.cycle_couple[i][1].data.capacity)     # Defining the capacity point
            self.capacity_curve.append(capacity_point)                      # Saving the capacity point

        # Searching for outliers in the capacity data
        cycle_couple, capacity_curve = Outliers(self.cycle_couple,
                                                self.capacity_curve, z_lim=zlim)

        self.cycle_couple = cycle_couple
        self.capacity_curve = capacity_curve

    def clean_MIT_cycle_method(self, Cycle, i_cutoff=1.1/50, min_len=1):
        """
        Function for cleaning current, voltage and temperature data for a given
        cycle from the Severson et al. Dataset (full reference in note).

        The cleaning is done by removing outliers and then the last points of the
        cycle, when the current is being adjusted. The function will also separate
        the charge cycle from the discharge cycle, which come together in the
        dataset.

        This function is called upon initialization of a Clean_MIT_Battery class
        (see MIT_Classes.py for more information).

        .. note::
            This function uses the convention where the discharge current is
            negative and the charge current positive.
            If the cycle is deemed as too short or abnormal, then all of the arrays
            are returned as None.

        .. note::
            Full reference: Severson, K.A., Attia, P.M., Jin, N. et al.
            Data-driven prediction of battery cycle life before capacity
            degradation. Nat Energy 4, 383–391 (2019).
            <https://doi.org/10.1038/s41560-019-0356-8>

        Parameters
        ----------
        Cycle : MIT_Cycle
            The Cycle we want to clean.
        i_cutoff : float, default = 1.1/50
            The current cut-off value to be used to determine the end of the
            discharge cycles [A].
        min_len : int, default = 1
            The minimum amount of points a charge cycle must have to be considered
            as relevant. Equal to one by default. Something similar is done during
            the resampling stages in the dataset creation.

        Returns
        -------
        Two tuples (ch_data, ds_data), each one containing the cleaned data for
        the charge and discharge cycle, respectively.

        ch_data = (time, current, voltage, temperature, capacity) (analogous for the
        discharge). Each entry is an np.ndarray.

        When one of the two cycles is too short (len(current) < min_len), the cycle
        is scraped and the according tuple is returned as `None`.

        Raises
        ------
        AssertionError
            If `min_len` < 1.
        """

        assert min_len >= 1, 'min_len must be greater or equal to 1'

        # Numpy arrays
        Cycle_t = Cycle.data.time  # Time data
        Cycle_i = Cycle.data.meas_i  # Current data
        Cycle_v = Cycle.data.meas_v  # Voltage data
        Cycle_T = Cycle.data.meas_t  # Temperature data
        Qd = Cycle.data.dis_capacity
        Qc = Cycle.data.chr_capacity

        # Outliers
        Data = np.concatenate([Cycle_t, Cycle_i, Cycle_v, Cycle_T, Qd, Qc],
                              axis=1)

        Data, outlier_array = Input_outliers_v3(Data, feature_index=2)
        Cycle_t = Data[:, 0:1]
        Cycle_i = Data[:, 1:2]
        Data = Data[:, 2:]

        # Rest period:
        try:
            idx_chr_time = np.where(Cycle_t > 10)[0][0]

            if Cycle_i[idx_chr_time, 0] > 0:
                idx_chr_time = np.where(Cycle_i[idx_chr_time:,:] == 0
                                        )[0][0]+idx_chr_time
            # 1C charge time:
            idx_1c_time = np.where(Cycle_i[idx_chr_time:, :] > 0
                                   )[0][0]+idx_chr_time

            if Cycle_i[idx_1c_time, 0] < 0.9*np.amax(Cycle_i[idx_1c_time:, 0]):
                idx_1c_time = np.where(Cycle_i[idx_1c_time:, :] > 0.9*np.amax(
                    Cycle_i[idx_1c_time:, 0]
                )
                                       )[0][0] + idx_1c_time
        except IndexError:
            return None, None

        # Finding the index where the discharge starts:
        idx = np.where(Cycle_i[idx_1c_time:, :] < 0)[0][0]+ idx_1c_time

        # Here we cut off the last bit of the curve where the current is yet again
        # lifted

        if Cycle_i[-1, 0] >= -i_cutoff:  # Analogous to the charge cycle.
            # Notice that we take away the first 5 points in the analysis just to
            # make sure that an initial transient point won't cause any trouble
            idx_ds = np.where(Cycle_i[idx + 5:, :] >= -i_cutoff)[0][0] + idx + 5
        else:
            idx_ds = len(Cycle_i)

        # Taking only the relevant data
        # Separating the vectors:

        ch_data = np.concatenate([Cycle_t[idx_1c_time:idx, :],
                                  Cycle_i[idx_1c_time:idx, :],
                                  Data[idx_1c_time:idx, :2],
                                  Data[idx_1c_time:idx, -1:]], axis=1)

        ds_data = np.concatenate([Cycle_t[idx:idx_ds, :], Cycle_i[idx:idx_ds, :],
                                  Data[idx:idx_ds, :2], Data[idx:idx_ds, -1:]],
                                 axis=1)

        # Checking if there are any NaNs:
        if np.any(np.isnan(np.reshape(Cycle_i, -1))).item():
            print("NaNs in Cycle_i")

        if (len(ch_data) < min_len) or (len(ds_data) < min_len):
            # If the Cycle is too short, than we'll return None.
            return None, None
        else:
            return ch_data, ds_data

class Clean_MIT_Cycle:
    """
    Class structured similarly to a Severson_Cycle, but only containing the
    cleaned data that is relevant for analysis.

    Parameters
    ----------
    cycle : MIT_Cycle
        The Severson_Cycle object corresponding to this cycle.
    data : ndarray[floats]
        The array containing all of the data.
        Each column corresponds to a different variable, in the following
        order: [time, current, voltage, temperature, capacity].
        Units: [s], [A], [V], [ºC] and [A.h] respectively.
    type : {'charge', 'discharge'}
        A string expressing which kind of cycle it corresponds to. Can either
        be 'charge' or 'discharge'.

    Attributes
    ----------
    type : {'charge', 'discharge'}
        The type of cycle.
    amb_T : int
        The ambient temperature during measurements [ºC].
        It's always 30ºC in this case.
    summary_keys : list(str)
        A list containing the name of the following attributes:

        IR : float
            Measured internal resistance.
        QCharge : float
            The charge cycle's capacity.
        QDischarge : float
            The discharge cycle's capacity.
        Tavg : float
            The cycle's average temperature.
        Tmax : float
            The cycle's maximum temperature.
        Tmin : float
            The cycle's minimum temperature.
        chargetime : float
            The charge cycle's duration.
        cycle : float
            The cycle number + 1.

    battery_keys : list
        A list containing the name of the variables stored in the battery group.
    data : Clean_MIT_Data
        Clean_Sev_Data class containing all of the actual data.
        See Clean_Sev_Data for more information.
    """
    def __init__(self, cycle, data, type):

        self.type = type
        self.number = cycle.number
        self.amb_T = cycle.amb_T
        self.summary_keys = cycle.summary_keys

        for key in self.summary_keys:
            setattr(self, key, getattr(cycle, key))

        self.battery_keys = cycle.battery_keys

        self.data = Clean_MIT_Data(cycle, data, type)

class Clean_MIT_Data:
    """
    Data class for the cleaned Severson data.

    Parameters
    ----------
    cycle : MIT_Cycle
        The Severson_Cycle object corresponding to this cycle.
    data : ndarray[floats]
        The array containing all of the data.
        Each column corresponds to a different variable, in the following
        order: [time, current, voltage, temperature, capacity].
        Units: [s], [A], [V], [ºC] and [A.h] respectively.
    type : {'charge', 'discharge'}
        A string expressing which kind of cycle it corresponds to. Can either
        be 'charge' or 'discharge'.

    Attributes
    ----------
    time : ndarray[floats]
        Time vector for the cycle [min].
    meas_v : ndarray[floats]
        Battery terminal voltage [V].
    meas_i : ndarray[floats]
        Battery output current [A].
    meas_t : ndarray[floats]
        Battery temperature [ºC].

    if type == 'charge':
        chr_capacity : ndarray[floats]
            The battery charge capacity during cycling [A.h].
    elif type == 'discharge':
        dis_capacity : ndarray[floats]
            The battery discharge capacity evolution during cycling [A.h].
        dis_dQdV : ndarray[floats]
            The differential capacity curve calculated from the discharge data.
        capacity : float
            The battery's calculated discharge capacity at the end of the cycle
            [A.h].

    Raises
    ------
    Exception
        If the type isn't 'charge' nor 'discharge'.
    """
    def __init__(self, cycle, data, type):

        self.time = data[:, 0:1]*60
        self.meas_i = data[:, 1:2]
        self.meas_v = data[:, 2:3]
        self.meas_t = data[:, 3:4]
        if type == 'charge':
            self.chr_capacity = data[:, 4:5]
        elif type == 'discharge':
            self.dis_capacity = data[:, 4:5]
            self.dis_dQdV = data[:, 5:6]
            self.capacity = cycle.data.capacity
        else:
            raise Exception("Unknown type: " + str(type))

########################################################################################


if __name__ == '__main__':
    import os
    import pickle
    import re

    os.chdir('.//Datasets/Severson et al./Raw Dataset')
    os.chdir('.//2. 2017-06-30')

    # Creating the raw batteries
    f = h5py.File('2017-06-30_batchdata_updated_struct_errorcorrect.mat')

    for battery_number in range(len(f['batch']['cycle_life'])):
        print(battery_number)
        f = h5py.File('2017-06-30_batchdata_updated_struct_errorcorrect.mat')
        B = Dataset_MIT(f, battery_number, print_bool=True)

        with open('B' + str(battery_number) + '_Dataset_Severson.txt',
                  'wb') as f_2:
            pickle.dump(B, f_2)

    # Creating the cleaned datasets:
    files = os.listdir()  # All files in the folder
    file_list = []  # List with the valid files
    for file_name in files:
        match = re.search(r'B\d{1}', file_name)  # Only take into acount
        # the battery files
        if match == None:
            continue
        else:
            file_list.append(file_name)
    file_list.sort()

    for file_name in file_list:
        with open(file_name, 'rb') as f:
            B = pickle.load(f)

        print(B.Name)
        Battery = Clean_MIT_Battery(B)

        os.chdir('../../')
        os.chdir('.//Cleaned_v2/2. 2017-06-30')
        with open(Battery.Name + '.txt', 'wb') as f:
            pickle.dump(Battery, f)
        os.chdir('../../')
        os.chdir('.//Raw Dataset/2. 2017-06-30')