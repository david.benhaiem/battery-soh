import os
import numpy as np
from datetime import datetime
import scipy.io
import pickle
import re
import math
from Cleaning import *



class Raw_Dataset:
    """Class for saving the dataset raw data as is.

    .. note::
        The following are required attributes.

    Attributes
    ----------
    Name : str
        Battery name ("B000X")
    cycle : list(Raw_Dataset.Raw_Cycle)
        List of the cycle objects
    amb_T : float
        The temperature the battery was cycled on
    capacity_curve : numpy.array
        A 2D array where the first column is the cycle number and the second is
        the capacity value [mA.h] associated to it.

    Methods
    -------
    plot_capacity_curve
        Plots the capacity curve.
    """

    T_dict = {
        "146": 70.,  # degrés C
        "149": 70.,  # degrés C
        # "159_1": 40,  # degrés C
        # "159_2": 40,  # degrés C
        "159": 40.,  # degrés C
        "170": 40.,  # degrés C
        "175": 25.,  # degrés C
        "178": 25.,  # degrés C
        "182A": 25.,  # degrés C
        "185A": 70.,  # degrés C
        "189B": 70.,  # degrés C
        "6C": 70.,  # degrés C
    }

    def __init__(self, raw_file):

        # Naming:
        data = raw_file.split('-')
        number = data[2]

        T = Raw_Dataset.T_dict[data[2]]
        self.amb_T = T

        if data[2][-1].isalpha():
            alph_dict = {'A': '1', 'B': '2', 'C': '3'}
            number = data[2][:-1]+alph_dict[data[2][-1]]

        if data[3].isnumeric():
            number += data[3]

        Name = f'B{number.zfill(4)}'
        self.Name = Name

        # Getting the actual data:
        df = pd.read_table(raw_file, sep='\s+')
        df.columns = list(df.columns[1:]) + ['Delete']
        df.drop('Delete', inplace=True, axis=1)

        # Saving the cycles:
        self.cycle = []
        for idx in range(0, int(max(df['Cyclenumber'])) + 1):

            cyclepair = df[df['Cyclenumber'] == idx]

            charge = cyclepair[cyclepair['<I>(mA)'] > 0]
            if len(charge) > 0:
                self.cycle.append(
                    self.Raw_Cycle(charge, 'charge', idx, T))

            discharge = cyclepair[cyclepair['<I>(mA)'] < 0]
            if len(discharge) > 0:
                self.cycle.append(
                    self.Raw_Cycle(discharge, 'discharge', idx, T))

        data = []
        for cycle in self.cycle:
            if cycle.type == 'discharge':
                data.append((cycle.number, cycle.data.capacity))

        self.capacity_curve = np.stack(data)

    def plot_capacity_curve(self):
        q = self.capacity_curve
        plt.plot(q[:,0], q[:,1], label = 'Discharge Capacity')
        plt.xlabel("Cycle Number")
        plt.ylabel("Capacity (mA.h)")
        plt.title(f"Capacity Curve: {self.Name}")
        plt.legend()
        plt.show()

    class Raw_Cycle:
        """Class for storing Cycle data from raw battery data

        Attributes
        ----------
        number : int
            The cycle number.
        type : {'charge', 'discharge'}
            The cycle type.
        amb_T : float
            The ambient temperature in which the battery was cycled [ºC]
        data : list(Raw_Dataset.Raw_Cycle.Raw_Data)
            List with the actual data objects

        Methods
        -------
        plot
            Plots the cycle's data.
        """

        def __init__(self, cycle, type, index, T):

            self.number = index
            self.type = type
            self.amb_T = float(T)
            self.data = self.Raw_Data(cycle, type)

        def plot(self):
            plt.plot(self.data.time, self.data.meas_i, label='Current (mA)')
            plt.plot(self.data.time, self.data.meas_v, label='Voltage (V)')
            plt.plot(self.data.time, 1e2*self.data.meas_q, label='Capacity (1e-2*mA.h)')
            plt.title(f'Cycle {self.number} - {self.type}')
            plt.legend()
            plt.show()

        class Raw_Data:
            """Class for storing the actual raw data.

            Attributes
            ----------
            time : array like
                Time array [s]
            meas_i : array like
                Measured Current array [mA]
            meas_v : array like
                Measured Voltage array [V]
            meas_q : array like
                Measured capacity [mA.h]
            capacity : float
                Capacity value for the charge [mA.h] (for discharge cycles).
            """

            def __init__(self, data, type):

                self.time = data['time(s)'].to_numpy()
                self.meas_i = data['<I>(mA)'].to_numpy()
                self.meas_v = data['E(V)'].to_numpy()
                # self.meas_t = data['measured_temperature']
                self.meas_q = data['Qdischarge/charge(mA.h)'].to_numpy()
                if type == 'discharge':
                    self.capacity = -1*float(self.meas_q[-1])   # [mA.h]

class Clean_Dataset:
    """Class for cleaning and storing clean data

    .. note::
        The following are required attributes.

    Required Attributes
    -------------------
    Name : str
        Battery name ("B00XX").
    capacity_curve : array like
        A 2D array where the first column is the cycle number and the second is
        the capacity value [A.h] associated to it.
    cycle_couple : list(tuple(Clean_Dataset.Clean_Dataset_Cycle))
        A list with the cleaned charge and discharge cycles (respectively) that
        are associated one with the other (the charge cycle is always the one
        that comes immediately after the discharge cycle).
    C_rate : float
        The C rate associated to the battery for nondimensionning the
        current [A].
    Q_nominal : float
        The battery's nominal capacity (for nondimensionning the SOH) [A.h].
    V_nominal : float
        The battery's nominal voltage (for nondimensionning the voltage) [V].


    .. note::
        The following are recommended attributes.

        This has the purpose of simplifying things, but you'll still have to
        define them in the Dataset classes individually. That's why it's just
        recommended.

    Recommended Attributes
    ----------------------
    couples : list(tuple(int))
        When the charge and discharge data are in separate cycles, we need to
        first find the related couples. This will save their numbers so they can
        later be called.

    """

    mass_dict = {   # ug
        "0146": 29.,
        "0149": 27.,
        "1591": 34.,
        "1592": 71.,
        "0170": 27.33,
        "0175": 27.,
        "0178": 25.,
        "1821": 24.5,
        "1851": 26.,
        "1892": 23.33,
        "0063": 22.,
    }

    def __init__(self, Battery, save_dis_cycle=False, threshold=0.0001, z_lim=3):
        """

        Parameters
        ----------
        Battery : Raw_Dataset
            The Raw Battery we are going to clean and store here
        i_cutoff : float, default = 2e-2
            The current cut-off value to be used to determine the end of the
            charge cycles [A].

        min_len : int, default = 1
            The minimum amount of points a charge cycle must have to be
            considered as relevant. Equal to one by default. Something similar
            is done during the resampling stages in the dataset creation.

        threshold : float, default = 0.1
            the minimum capacity value to be considered as relevant [mA.h].
        z_lim : float
            The maximum zscore allowed when searching for outliers.
        """

        self.Name = Battery.Name
        self.capacity_curve = []
        self.couples = []
        self.cycle_couple = []
        self.mass = Clean_Dataset.mass_dict[Battery.Name[1:]]

        # The cycles are already separated in charge and discharge cycles:
        # Finding the couples:
        dis_nb = -1
        for index, Cycle in enumerate(Battery.cycle):
            if Cycle.type == 'discharge':
                    dis_nb = index
            elif Cycle.type == 'charge':
                if dis_nb != -1:
                    chr_nb = index
                    self.couples.append((chr_nb, dis_nb))
                    dis_nb = -1

        for couple in self.couples:
            chr_cycle = Battery.cycle[couple[0]] # cycle object
            dis_cycle = Battery.cycle[couple[1]] # cycle object

            if math.isnan(dis_cycle.data.capacity):
                continue
            elif dis_cycle.data.capacity < threshold:
                continue

            chr_clean = self.clean_cycle_method(chr_cycle, self.mass)
            dis_clean = self.clean_cycle_method(dis_cycle, self.mass)

            if (chr_clean is None) or (dis_clean is None):
                continue

            if not save_dis_cycle:
                dis_clean = self.Test(dis_clean)

            self.cycle_couple.append((chr_clean, dis_clean))

        # Creating the cycle couple attribute:
        for index, cycle_couple in enumerate(self.cycle_couple):
            capacity_point = (index, cycle_couple[1].data.capacity)
            self.capacity_curve.append(capacity_point)

        # Searching for outliers in the capacity data
        cycle_couple, capacity_curve = Outliers(
            self.cycle_couple, self.capacity_curve, z_lim=z_lim
        )

        self.cycle_couple = cycle_couple
        self.capacity_curve = capacity_curve

        self.raw_couples = self.couples  # If it makes sense in this dataset
        self.couples = []
        for cycle_couple in self.cycle_couple:
            chr_nb = cycle_couple[0].number
            dis_nb = cycle_couple[1].number
            self.couples.append((chr_nb, dis_nb))

        # self.Q_nominal = max(np.stack(self.capacity_curve)[:, 1])
        # v_vals = []
        # c_vals = []
        # for cycle_couple in self.cycle_couple:
        #     cycle = cycle_couple[0]
        #     c_vals.append(float(np.mean(cycle.data.meas_i)))
        #     v_vals.append(max(cycle.data.meas_v))
        #
        # self.C_rate = float(abs(np.mean(c_vals)))
        # self.V_nominal = float(abs(np.mean(v_vals)))
        self.Q_nominal = 9.036688361270921e-05
        self.C_rate = 9.036688361270921e-05*self.mass
        self.V_nominal = 1.0

    def plot_capacity_curve(self):
        q = np.stack(self.capacity_curve)
        plt.plot(q[:, 0], q[:, 1], label='Discharge Capacity')
        plt.xlabel("Cycle Number")
        plt.ylabel("Capacity (mA.h)")
        plt.title(f"Capacity Curve: {self.Name}")
        plt.legend()
        plt.show()

    def clean_cycle_method(self, Cycle, mass, min_len=1):
        """Separate cycles (if needed) and cleans them.

        I recommend looking at
        :func:`Coin_Classes.Clean_Cell.separate_coin_cycles` and
        :func:`Cleaning.Input_outliers_v3`.

        """
        # Numpy arrays
        Cycle_t = Cycle.data.time  # Time data
        Cycle_i = Cycle.data.meas_i  # Current data
        Cycle_v = Cycle.data.meas_v  # Voltage data
        Cycle_T = Cycle.data.meas_q  # Temperature data

        Cycle_t = Cycle_t.reshape((len(Cycle_t), 1))
        Cycle_i = Cycle_i.reshape((len(Cycle_i), 1))
        Cycle_v = Cycle_v.reshape((len(Cycle_v), 1))
        Cycle_T = Cycle_T.reshape((len(Cycle_T), 1))

        Data = np.concatenate([Cycle_t, Cycle_i, Cycle_v, Cycle_T], axis=1)

        # Taking away outliers that may mess up with the rest of the cleaning:
        Data, out_idx = Input_outliers_v3(Data, feature_index=2, window=50)

        Cycle_t = Data[:, 0:1]
        Cycle_i = Data[:, 1:2]
        Cycle_v = Data[:, 2:3]
        Cycle_T = Data[:, 3:4]

        if Cycle.type == 'charge':
            # Cutting out the beginning of the CC curve
            try:
                idx, _ = np.where(Cycle_i > 0.90 * max(Cycle_i))
                idx_1 = idx[0]  # Taking the very first value that is > 0.9*max(Cycle_i)
                idx_2 = idx[-1]  # Taking the very first value that is > 0.9*max(Cycle_i)
                # We check where the current is > 0.9*max(i), then we take away
                # everything that comes before it, which corresponds to the part
                # where the current hasn't yet stabilized at the CC value.
            except:
                # Just in case, I don't think this is even possible.
                return None
        elif Cycle.type == 'discharge':
            # We start by taking away the first few points, where the current
            # hasn't yet reached its max (min because it's negative) value.
            try:  # Analogous to the charge cycle.
                idx, _ = np.where(Cycle_i < 0.90 * min(Cycle_i))
                idx_1 = idx[0]
                idx_2 = idx[-1]
            except:
                # Just in case, I don't think this is even possible.
                return None

        # Taking only the relevant data
        Cycle_t = Cycle_t[idx_1: idx_2, :]
        Cycle_i = Cycle_i[idx_1: idx_2, :]
        Cycle_v = Cycle_v[idx_1: idx_2, :]
        Cycle_T = Cycle_T[idx_1: idx_2, :]

        # Checking if there are any NaNs and taking them away.
        if np.any(np.isnan(np.reshape(Cycle_i, -1))).item():
            nan_t = ~np.isnan(Cycle_t)
            nan_i = ~np.isnan(Cycle_i)
            nan_v = ~np.isnan(Cycle_v)
            nan_T = ~np.isnan(Cycle_T)
            isnt_nan = nan_t & nan_i & nan_T & nan_v

            Cycle_t = Cycle_t[isnt_nan]
            Cycle_i = Cycle_i[isnt_nan]
            Cycle_v = Cycle_v[isnt_nan]
            Cycle_T = Cycle_T[isnt_nan]

            Cycle_t = Cycle_t.reshape((len(Cycle_t), 1))
            Cycle_i = Cycle_i.reshape((len(Cycle_i), 1))
            Cycle_v = Cycle_v.reshape((len(Cycle_v), 1))
            Cycle_T = Cycle_T.reshape((len(Cycle_T), 1))

        if len(Cycle_t) < min_len:  # If the Cycle is too short, than we'll skip
            return None

        cycle_data = np.concatenate([Cycle_t, Cycle_i, Cycle_v, Cycle_T], axis=1)

        # Then call the clean cycle class
        clean_cycle = self.Clean_MXene_Cycle(Cycle, cycle_data, mass)

        return clean_cycle

    class Clean_MXene_Cycle:
        """Creates a clean cycle class

        """

        def __init__(self, cycle, cycle_data, mass):

            self.number = cycle.number
            self.type = cycle.type
            self.amb_T = cycle.amb_T
            if cycle.type == 'discharge':
                self.data = self.Clean_Data_Class(cycle_data, mass, cycle.data.capacity)
            else:
                self.data = self.Clean_Data_Class(cycle_data, mass)

        def plot(self):
            plt.plot(self.data.time, self.data.meas_i, label='Current (mA)')
            plt.plot(self.data.time, self.data.meas_v, label='Voltage (V)')
            plt.plot(self.data.time, 1e2*self.data.meas_q, label='Capacity (1e-2*mA.h)')
            plt.title(f'Cycle {self.number} - {self.type}')
            plt.legend()
            plt.show()

        class Clean_Data_Class:
            def __init__(self, data, mass, capacity=None):
                self.time = data[:, 0:1]
                self.meas_i = data[:, 1:2]
                self.meas_v = data[:, 2:3]
                self.meas_q = data[:, 3:4]/mass
                if capacity is not None:
                    self.capacity = capacity/mass
                else:
                    self.capacity = None

    class Test:

        def __init__(self, cycle):
            self.number = cycle.number
            self.data = self.Capacity(cycle.data.capacity)

        class Capacity:

            def __init__(self, capacity):
                self.capacity = capacity



