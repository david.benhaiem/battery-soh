import os
import numpy as np
from datetime import datetime
import scipy.io
import pickle
import re
import math
from Cleaning import *

class Dataset_Nasa:
    """
    Nasa Dataset class. This is based on the "Battery Data Set" by B. Saha and
    K. Goebel (check note for full reference).

    One ``Dataset_Nasa`` object corresponds to one battery/one '.mat' file.

    This class has attribute :py:attr:`~cycle` which is a list with every cycle
    (:class:`~Nasa_Classes.Nasa_Cycle` objects), which in turn have a
    :py:attr:`~data` attribute (:class:`~Nasa_Classes.Nasa_Data` object) with
    the measured data.

    That way, to reach into measured data:

    >>> A = Dataset_Nasa('B0001.txt')
    >>> measured_current = Dataset_Nasa.cycle[0].data.meas_i
    >>> measured_voltage = Dataset_Nasa.cycle[0].data.meas_v

    .. note::
        Full reference: B. Saha and K. Goebel (2007). "Battery Data Set",
        NASA Ames Prognostics Data Repository
        <https://ti.arc.nasa.gov/tech/dash/groups/pcoe/prognostic-data-repository/#battery>,
        NASA Ames Research Center, Moffett Field, CA

    Parameters
    ----------
    file_name : string
        The name of the .mat file, including the extension.

    Attributes
    ----------
    Name : str
        The battery ID (example: 'B0005').
    cycle : list[Nasa_Cycle]
        List of the cycles tested.
        See :class:`~Nasa_Classes.Nasa_Cycle` and
        :class:`~Nasa_Classes.Nasa_Data` classes for more information.
    """
    def __init__(self, file_name):

        Mat  = scipy.io.loadmat(file_name)          # Loading the .mat file
        Name = file_name.split('.')[0]      # Just taking the name of the battery
        shp  = Mat[Name]['cycle'][0, 0].shape       # Shape of the cycle matrix

        # Reshaping from (1,N) to (N,) for practical reasons, saving it as cycle
        cycle = np.reshape(Mat[Name]['cycle'][0, 0], shp[1])

        # Initializing the self.cycle list
        self.cycle = []

        for i in range(len(cycle)):
            c_i = Nasa_Cycle(cycle[i], i)     # Go to Nasa_Cycle class.
            self.cycle.append(c_i)

        # Other information present in the .mat file, just in case:
        self.header  = Mat['__header__']
        self.version = Mat['__version__']
        self.globals = Mat['__globals__']
        # Let's also save its name:
        self.Name = Name

class Nasa_Cycle:
    """Nasa Cycle class. This includes all of the cycle's information.

    This class is called upon initialization of the :class:`~Dataset_Nasa`
    class.

    Parameters
    ----------
    CYCLE : dict
        A dictionary containing all of the cycle's information except its number
    CYCLE_NB : int
        The cycle's number.

    Attributes
    ----------
        number : int
            The cycle number.
        type : {'charge', 'discharge', 'impedance'}
            The cycle type.
        amb_T : int
            The ambient temperature during measurements [ºC].
            (The raw data was already an integer, I just kept it this way)
        time : [datetime.datetime]
            The date and time of the experiment [datetime.datetime]
        unix_time : float
            The date and time of the experiment expressed in unix time [s]
        data : Nasa_Data
            Nasa_Data object containing all of the actual data.
            See Nasa_Data for more information.
    """
    def __init__(self, CYCLE, CYCLE_NB):

        self.number = CYCLE_NB
        self.type   = str(CYCLE['type'][0])                      # The type of cycle (charge, discharge or impedance)
        self.amb_T  = int(CYCLE['ambient_temperature'][0,0])     # The ambient temperature of the measurements

        # Time conversion for datetime.datetime object and UNIX
        Y = str(int(CYCLE['time'][0, 0]))
        m = str(int(CYCLE['time'][0, 1]))
        d = str(int(CYCLE['time'][0, 2]))
        H = str(int(CYCLE['time'][0, 3]))
        M = str(int(CYCLE['time'][0, 4]))
        S = str(int(CYCLE['time'][0, 5]))
        time_str = Y + '/' + m + '/' + d + ' ' + H + ':' + M + ':' + S

        self.time = datetime.strptime(time_str, '%Y/%m/%d %H:%M:%S')    # Time as datetime object
        self.unix_time = self.time.timestamp()                          # Time in UNIX format (seconds since 1970)

        #print(self.time) # for bugfixing
        self.data = Nasa_Data(CYCLE['data'], self.type)                 # The actual data, see Nasa_Data class

class Nasa_Data:
    """
    Nasa Data class. Includes all of the actually measured data.

    The attributes of this class depend on the cycle type ('charge',
    'discharge' or 'impedance'). That's the reason why this is a required input.

     This class is called directly upon initialization of the
     :class:`~Nasa_Cycle` class.

    Parameters
    ----------
    Data : dict
        Dictionary containing all of the data.
    Type : {'charge', 'discharge', 'impedance'}
        The cycle type.

    Attributes
    ----------
    sense_i : ndarray[float]
        Current in sens branch [A] (only for impedance cycles).
    battery_i : ndarray[float]
        Current in battery branch [A] (only for impedance cycles).
    ratio_i : ndarray[float]
        Ratio of the above currents [] (only for impedance cycles).
    battery_z : ndarray[float]
        Battery Impedance computed from raw data [Ohms]
        (only for impedance cycles).

    rectified_z : ndarray[float]
        Calibrated and smoothed battery impedance [Ohms]
        (only for impedance cycles).

    Re : float or complex
        Estimated electrolyte resistance [Ohms] (only for impedance cycles).
    Rct : float
        Estimated charge transfer resistance [Ohms] (only for impedance cycles).
    time : ndarray[float]
        Time vector for the cycle [s] (for charge and discharge cycles).
    meas_v : ndarray[float]
        Battery terminal voltage [V] (for charge and discharge cycles).
    meas_i : ndarray[float]
        Battery output current [A] (for charge and discharge cycles).
    meas_t : ndarray[float]
        Battery temperature [ºC] (for charge and discharge cycles).
    charge_i : ndarray[float]
        Current measured at charger [A] (only for charge cycles).
    charge_v : ndarray[float]
        Voltage measured at charger [V] (only for charge cycles).
    load_i : ndarray[float]
        Current measured at load [A] (only for discharge cycles).
    load_v : ndarray[float]
        Voltage measured at load [V] (only for discharge cycles).
    capacity : float
        Battery capacity for discharge till 2.7V [A.h]
        (only for discharge cycles).


    """
    def __init__(self, Data, Type):

        if Type == 'impedance':
            # In this case we have as attributes:
            # Sense_current
            self.sense_i = np.transpose(Data['Sense_current'][0,0])
            # Battery_current
            self.battery_i = np.transpose(Data['Battery_current'][0,0])
            # Current_ratio
            self.ratio_i = np.transpose(Data['Current_ratio'][0,0])
            # Battery_impedance
            self.battery_z = Data['Battery_impedance'][0,0]
            # Rectified_Impedance
            self.rectified_z = Data['Rectified_Impedance'][0,0]
            # Re  (estimated electrolyte resistance in Ohms)
            self.Re = Data['Re'][0,0][0,0].item()
            # Rct (estimated charge transfer resistance in Ohms)
            self.Rct = Data['Rct'][0,0][0,0].item()

        else:   # If the type isn't impedance, then it's either charge or discharge which have a lot in common:

            # Voltage_measured
            self.meas_v = np.transpose(Data['Voltage_measured'][0,0])
            # Current_measured
            self.meas_i = np.transpose(Data['Current_measured'][0,0])
            # Temperature_measured
            self.meas_t = np.transpose(Data['Temperature_measured'][0,0])
            # Time
            self.time = np.transpose(Data['Time'][0, 0])

            if Type == 'charge':
                # Current_charge
                self.charge_i = np.transpose(Data['Current_charge'][0,0])
                # Voltage_charge
                self.charge_v = np.transpose(Data['Voltage_charge'][0,0])
            else:
                # Current_load
                self.load_i = np.transpose(Data['Current_load'][0,0])
                # Voltage_load
                self.load_v = np.transpose(Data['Voltage_load'][0,0])
                # Capacity
                # The following 'try:' statement is necessary because
                # some files have missing Capacity data
                try:
                    self.capacity = float(Data['Capacity'][0,0][0,0])
                except IndexError:
                    print('No Capacity value')
                    # maybe not the best way of dealing with this
                    self.capacity = float('nan')

class Clean_Battery:
    """
    Class that holds the cleaned cycle couples and their data for one Battery.

    It takes as a base one Dataset_Nasa class (one Battery from the Nasa
    Dataset). From it, it extracts the cycle couples associated to the model's
    inputs and outputs (first a discharge cycle, related to the capacity values,
    and then the charge cycle that comes immediately after for the inputs).
    Then, it cleans each one of the selected cycles excluding the couples that
    present abnormalities.

    It cleans the charge cycle by taking away the first few points before the
    actual cycle begins and the last ones after the current has reached its
    cut-off point (< `i_cutoff`).

    Couples are excluded in this process when:
        The discharge curve's capacity value is nan, or it's below the
        `threshold` value.
        When the charge cycle is too short (< `min_len`).
    The cycle couples that weren't excluded then pass by an outlier detection
    based on the capacity value. Ouliers are detected by sliding windows and
    z-score values.

    The remaining, "valid" cycle couples are saved in the attribute `couples`.
    No resampling is done at this stage, only during dataset creation for model
    training/validation.

    Parameters
    ----------
    Battery : Dataset_Nasa
        The battery we want to create the class for.
    i_cutoff : float, default = 2e-2
        The current cut-off value to be used to determine the end of the charge
        cycles [A].
    min_len : int, default = 1
        The minimum amount of points a charge cycle must have to be considered
        as relevant. Equal to one by default. Something similar is done during
        the resampling stages in the dataset creation.
    threshold : float, default = 0.1
        the minimum capacity value to be considered as relevant [A.h].
    z_lim : float
        The maximum zscore allowed when searching for outliers.

    Attributes
    ----------
    Name : string
        The battery's name.
    capacity_curve : ndarray[float, float]
        A 2D array where the first column corresponds to the cycle couple number
        and the second to the associated capacity value [[], A.h].
    raw_couples : list[tuple(int)]
        The number of the charge and discharge cycles in each couple, based on
        the Dataset_Nasa's cycle numbers.

        Example:
        self.couples[k] = (charge cycle number, discharge cycle number)
        Battery.cycle[charge cycle number] = Nasa_Cycle object corresponding to
        the couple's charge cycle. Analogous for the discharge cycle.
    couples : list[tuple(int)]
        Exactly like the ``raw_couples`` attribute, but without the entries that
        are judged as abnormal or that have missing datapoints or that are too
        short etc..
    cycle_couple : list[tuple(Clean_Cycle)]
        The Clean_Cycle objects corresponding to each couple.
        self.cycle_couple[k] = (charge Clean_Cycle, discharge Clean_Cycle)
    capacity_curve : np.ndarray
        Array with the capacity value for each discharge cycle. The first
        column corresponds to the cycle's number and the second one to the
        capacity value [A.h].
    C_rate : float
        The C rate associated to the battery for nondimensionning the
        current [A].
    Q_nominal : float
        The battery's nominal capacity (for nondimensionning the SOH) [A.h].
    V_nominal : float
        The battery's nominal voltage (for nondimensionning the voltage) [V].
    """
    def __init__(self, Battery, i_cutoff=2e-2, min_len=1, threshold=0.1,
                 z_lim=3):

        self.C_rate = 2.1
        self.V_nominal = 4.2
        self.Q_nominal = 2.1
        self.Name = Battery.Name
        self.capacity_curve = []
        self.couples = []
        self.cycle_couple = []


        for i in range(len(Battery.cycle)):
            assert Battery.cycle[i].number == i

        # The charge cycle should come right after the discharge one
        dis_nb = -1
        for Cycle in Battery.cycle:
            if Cycle.type == 'discharge':
                    dis_nb = Cycle.number
            elif Cycle.type == 'charge':
                if dis_nb != -1:
                    chr_nb = Cycle.number
                    self.couples.append((chr_nb, dis_nb))
                    dis_nb = -1

        for couple in self.couples: # For each charge/discharge couple:
            # Saving the cycles
            chr_cycle = Battery.cycle[couple[0]]    # The charge cycle
            dis_cycle = Battery.cycle[couple[1]]    # The discharge cycle

            # For the discharge cycle first:
            if math.isnan(dis_cycle.data.capacity): # if the capacity is nan:
                continue    # skip this couple
            elif dis_cycle.data.capacity < threshold:
                continue

            t, i, v, T = self.clean_cycle_method(dis_cycle, i_cutoff=i_cutoff,
                                                 min_len=min_len)
                                                    # cleaning the data
            if t is None:
                continue
            dis_data = np.concatenate([t, i, v, T], axis=1)
            # Creating the Clean_Cycle class for this discharge cycle
            dis_clean = Clean_Cycle(dis_cycle, dis_data)

            ##

            # Now for the charge cycle:
            t, i, v, T = self.clean_cycle_method(chr_cycle, i_cutoff=i_cutoff,
                                                 min_len=min_len)
            if t is None:
                continue

            chr_data = np.concatenate([t, i, v, T], axis=1)
            chr_clean = Clean_Cycle(chr_cycle, chr_data)

            # Saving the cycle couple
            self.cycle_couple.append((chr_clean, dis_clean))

        for i in range(len(self.cycle_couple)):
            # Defining the capacity point
            capacity_point = (i, self.cycle_couple[i][1].data.capacity)
            # Saving the capacity point
            self.capacity_curve.append(capacity_point)

        # Searching for outliers in the capacity data
        cycle_couple, capacity_curve = Outliers(
            self.cycle_couple, self.capacity_curve, z_lim=z_lim
        )

        self.cycle_couple = cycle_couple
        self.capacity_curve = capacity_curve

        self.raw_couples = self.couples
        self.couples = []
        for cycle_couple in self.cycle_couple:
            chr_nb = cycle_couple[0].number
            dis_nb = cycle_couple[1].number
            self.couples.append((chr_nb, dis_nb))

    def clean_cycle_method(self, Cycle, i_cutoff=2e-2, min_len=1):
        """
        Function for cleaning current, voltage and temperature data for a given
        cycle.

        The cleaning is done by removing outliers and then the first few and
        last points of the cycle, when the current is being adjusted.
        Impedance cycles are not supported, only charge and discharge.

        This function is called upon initialization of a Clean_Battery class
        (see Nasa_Classes.py for more information).

        .. note::
            This function uses the convention where the discharge current is
            negative and the charge current positive.
            If the cycle is deemed as too short or abnormal, then all of the
            arrays are returned as None.

        .. warning::
            This discharge cycle cleaning only works for constant current (CC)
            discharge cycles. This must be adapted for more complex discharge
            curves if they are to be used in the future.

        Parameters
        ----------
        Cycle : Nasa_Cycle
            The Cycle we want to clean.
        i_cutoff : float, default = 2e-2
            The current cut-off value to be used to determine the end of the
            charge cycles [A].

        min_len : int, default = 1
            The minimum amount of points a charge cycle must have to be
            considered as relevant. Equal to one by default. Something similar
            is done during the resampling stages in the dataset creation.

        Returns
        -------
        Cycle_t : ndarray[float]
            The cycle's time stamps after cleaning [s].
        Cycle_i : ndarray[float]
            The cycle's current values after cleaning [A].
        Cycle_v : ndarray[float]
            The cycle's voltage values after cleaning [V].
        Cycle_T : ndarray[float]
            The cycle's temperature after cleaning [ºC].

        Raises
        ------
        AssertionError
            If `Cycle.type` == 'impedance'.
            If `min_len` < 1.
        """

        assert Cycle.type != 'impedance',\
            'Only charge and discharge cycles are allowed.'
        assert min_len >= 1, 'min_len must be greater or equal to 1'

        # Numpy arrays
        Cycle_t = Cycle.data.time  # Time data
        Cycle_i = Cycle.data.meas_i  # Current data
        Cycle_v = Cycle.data.meas_v  # Voltage data
        Cycle_T = Cycle.data.meas_t  # Temperature data

        Data = np.concatenate([Cycle_t, Cycle_i, Cycle_v, Cycle_T], axis=1)

        # Taking away outliers that may mess up with the rest of the cleaning:
        Data, out_idx = Input_outliers_v3(Data, feature_index=2, window=50)

        Cycle_t = Data[:, 0:1]
        Cycle_i = Data[:, 1:2]
        Cycle_v = Data[:, 2:3]
        Cycle_T = Data[:, 3:4]

        if Cycle.type == 'charge':
            # Cutting out the beginning of the CCCV curve
            try:
                idx, _ = np.where(Cycle_i > 0.90 * max(Cycle_i))
                idx_1 = idx[
                    0]  # Taking the very first value that is > 0.9*max(Cycle_i)
                # We check where the current is > 0.9*max(i), then we take away
                # everything that comes before it, which corresponds to the part
                # where the current hasn't yet stabilized at the CC value.
            except:
                # Just in case, I don't think this is even possible.
                return None, None, None, None

            # Now cutting the final part of the charge curve
            if Cycle_i[
                -1, 0] <= i_cutoff:  # If the current reaches the cut-off value:
                idx, _ = np.where(Cycle_i[idx_1:,
                                  :] < i_cutoff)  # Then check when it < than it
                idx_2 = idx[0] + idx_1  # And save the first value

                # Notice that we add idx_1 because we called np.where for
                # Cycle_i[idx_1:, :] and not simply Cycle_i this is because
                # there may be points at the beginning of the curve that satisfy
                # the condition

                # Oh my god these comments are a mess, I'm so sorry

            else:  # If the current never reaches the cut-off value, then:
                idx_2 = len(Cycle_i)  # idx_2 is the last point in Cycle_i

        elif Cycle.type == 'discharge':
            # We start by taking away the first few points, where the current
            # hasn't yet reached its max (min because it's negative) value.
            try:  # Analogous to the charge cycle.
                idx, _ = np.where(Cycle_i < 0.90 * min(Cycle_i))
                idx_1 = idx[0]
            except:
                # Just in case, I don't think this is even possible.
                return None, None, None, None

            # Here we cut off the last bit of the curve where the current is
            # yet again lifted. It has been written in function of a Voltage
            # cut-off instead, since this is more general and should
            # work for more complex discharge cycles (to be studied).
            # It definitely works for harmonic discharge cycles
            if Cycle_v[-1, 0] >= min(Cycle_v):  # Analogous to the charge cycle.
                idx, _ = np.where(Cycle_v[idx_1:, :] <= min(Cycle_v))
                idx_2 = idx[0] + idx_1
            else:
                idx_2 = len(Cycle_v)

        # Taking only the relevant data
        Cycle_t = Cycle_t[idx_1: idx_2, :]
        Cycle_i = Cycle_i[idx_1: idx_2, :]
        Cycle_v = Cycle_v[idx_1: idx_2, :]
        Cycle_T = Cycle_T[idx_1: idx_2, :]

        # Checking if there are any NaNs and taking them away.
        if np.any(np.isnan(np.reshape(Cycle_i, -1))).item():
            nan_t = ~np.isnan(Cycle_t)
            nan_i = ~np.isnan(Cycle_i)
            nan_v = ~np.isnan(Cycle_v)
            nan_T = ~np.isnan(Cycle_T)
            isnt_nan = nan_t & nan_i & nan_T & nan_v

            Cycle_t = Cycle_t[isnt_nan]
            Cycle_i = Cycle_i[isnt_nan]
            Cycle_v = Cycle_v[isnt_nan]
            Cycle_T = Cycle_T[isnt_nan]

            Cycle_t = Cycle_t.reshape((len(Cycle_t), 1))
            Cycle_i = Cycle_i.reshape((len(Cycle_i), 1))
            Cycle_v = Cycle_v.reshape((len(Cycle_v), 1))
            Cycle_T = Cycle_T.reshape((len(Cycle_T), 1))

        if len(Cycle_t) < min_len:  # If the Cycle is too short, than we'll skip
            return None, None, None, None

        return Cycle_t, Cycle_i, Cycle_v, Cycle_T

class Clean_Cycle:
    """
    Class structured just like a Nasa_Cycle, but only containing the relevant
    cleaned data that is relevant for analysis.

    In this regard, this class is incompatible with impedance cycles.

    Parameters
    ----------
    Cycle : Nasa_Cycle
        The Nasa_Cycle object corresponding to this cycle.
    Data : ndarray[floats]
        The array containing all of the data.
        Each column corresponds to a different variable, in the following
        order: Time [s], Current [A], Voltage [V] and Temperature [ºC].

    Attributes
    ----------
    type : {'charge', 'discharge'}
        The type of cycle.
    amb_T : int
        The ambient temperature during measurements [ºC].
        (The raw data was already an integer, I just kept it this way)
    time : datetime.datetime
        The date and time of the experiment.
    unix_time : float
        The date and time of the experiment expressed in unix time [s].
    data : Clean_Data
        Clean_Data class containing all of the actual data.
        See Clean_Data for more information.
    """
    def __init__(self, Cycle, Data):

        self.number = Cycle.number
        self.type = Cycle.type
        self.amb_T = Cycle.amb_T
        self.time = Cycle.time
        self.unix_time = Cycle.unix_time
        if Cycle.type == 'discharge':
            self.data = Clean_Data(Data, Cycle.data.capacity)
        else:
            self.data = Clean_Data(Data)

class Clean_Data:
    """
    Clean_Data class. Stores the cleaned data of one Clean_Cycle

    Parameters
    ----------
    Data : ndarray[floats]
        The array containing all of the data.
        Each column corresponds to a different variable, in the following
        order: Time [s], Current [A], Voltage [V] and Temperature [ºC].
    Capacity : float, optional
        The capacity value.
        This is only necessary for discharge cycles, unit should be [A.h].
        If the cycle is a charge cycle then it defaults to None.

    Attributes
    ----------
    meas_v : ndarray[floats]
        Battery terminal voltage [V].
    meas_i : ndarray[floats]
        Battery output current [A].
    meas_t : ndarray[floats]
        Battery temperature [ºC].
    time : ndarray[floats]
        Time vector for the cycle [s].
    capacity
        For a discharge cycle, the capacity value [A.h], float.
        If it's a charge cycle, then it is None.
    """
    def __init__(self, Data, Capacity=None):
        # Time
        self.time = Data[:, 0:1]
        # Current_measured
        self.meas_i = Data[:, 1:2]
        # Voltage_measured
        self.meas_v = Data[:, 2:3]
        # Temperature_measured
        self.meas_t = Data[:, 3:]
        # Capacity
        self.capacity = Capacity


if __name__ == '__main__':

    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    os.chdir('.//Raw_Batteries (Dataset_Nasa classes)')
    # os.mkdir('Clean_Batteries')

    files = os.listdir()  # All files in the folder
    file_list = []  # List with the valid files
    for file_name in files:
        match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
        # the battery files
        if match == None:
            continue
        else:
            file_list.append(file_name)
    file_list.sort()

    ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt']

    for file_name in file_list:
        if file_name in ignore:
            continue
        print(file_name)
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)
        New_Battery = Clean_Battery(Battery)

        os.chdir('.//Clean_Batteries')
        with open(file_name, 'wb') as f:
            pickle.dump(New_Battery, f)
        os.chdir('../')

    # os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    #
    # # All of Nasa's Datasets:
    # os.chdir('.//Compiled_datasets')
    #
    # import time
    # start = time.time()
    #
    # for i in os.listdir():
    #     if i == '.DS_Store':
    #         continue
    #
    #     print(i)
    #     B = Dataset_Nasa(i)
    #
    #     os.chdir('../Old_Batteries')
    #
    #     with open(B.Name+'.txt', 'wb') as file:
    #         pickle.dump(B, file)
    #
    #     os.chdir('../Compiled_datasets')
    #
    # end = time.time()
    # print(end - start)
    # # B0052.mat and B0050.mat have missing values of Capacity (B0052.mat in particular)
