# Battery-SOH

Repository for the code used for Transferring State of Health Neural Networks 
for different battery chemistries and charging protocols, using renormalization
and Transfer Learning.

You can find the documentation for all functions and classes developed for this
work, as well as instructions on how to use the code, at
http://david.benhaiem.gitlab.io/battery-soh/ (your browser may
issue a safety warning, [which is a problem with the way GitLab
Pages handles usernames with
dots](https://gitlab.com/gitlab-org/gitlab/-/issues/20184#:~:text=This%20is%20a%20known%20limitation,a%20dot%2C%20for%20example%20foo.)).
Alternatively, all static html files are stored at Sphinx/_build/html.

The best way of seeing how to *use* all of the modules is by checking
the "Google Colabs", which are Jupyter notebook files (``'.ipynb'``).

* ``Training_Colab.ipynb``, presents how to train a model with the existing
  datasets.

* ``Normalisation_Training.ipynb ``presents how to create a model that
  integrates normalisation parameters (from scratch or based on old models), and
  how to change and optimize them. Their documentation is
  also a must read.

It is recommended that the user work with the provided colabs, changing as little as needed. Whenever you want to change or add something, check the specific
documentation. In particular if you want to use new datasets or develop new models.