.. Battery SOH documentation master file, created by
   sphinx-quickstart on Fri Jun 11 11:20:42 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Battery SOH's documentation!
=======================================

Here you'll find the documentation for all functions and classes developed during
this project. This page will guide you through the *must read* and where you
can find information about each separate piece of code.

The best way of seeing how to *use* all of these pieces together is by checking
the :std:doc:`Google Colabs <sphinx/Colabs>`, which are notebook files
(``'.ipynb'``). The two most useful are:

* The Training Colab, which presents how to train a model with the existing
  datasets.

* The Normalisation Training Colab presents how to create a model that
  integrates normalisation parameters (from scratch or based on old models), and
  how to change and optimize them. :ref:`Their documentation <lstms_norm>` is
  also a must read.

My recommendation is that you use the provided colabs, changing as little as you
need. Whenever you want to change or add something, check the specific
documentation here. In particular if you want to
:ref:`use new datasets <new_dataset>` or :ref:`develop new models <new_models>`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Google Colabs <rst files/Colabs>
   Useful dictionaries <rst files/main>
   rst files/Battery Classes
   Treatment functions <rst files/Cleaning>
   rst files/Models
   Training and Simulation <rst files/Training>
   Report related <rst files/Report>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
