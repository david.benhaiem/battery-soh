Report related
==============

Here you'll find the documentation for all functions related to the creation of a
report.

A report is created when you train a model using the :func:`Training.Train`.
The report folder is named with the time it was created -
in order to ensure that the user wouldn't create reports with
the same name and overwrite them.

Inside the Train function, the :func:`Report.Report` function is called, which
will write down a text file with a summary of the training and save other model
files in the report folder. Notice that this function relies on the fact that the
classes defined in :std:doc:`Models` all have a ``report()`` method, which
outputs all of the important information needed to recreate it. **If you intend
on developing new models, make sure you add such a method to their classes**.
:ref:`Here <new_models>`, you'll find all you have to do if you want to implement
new models.

Right after training, you'll find the following files in the report folder by
default:

* A ``'TEST_report.txt'`` text file with information about the model, the
  training epochs and the training and validation datasets. This is created
  through the Report function. The file name can be changed by changing the
  ``file_name`` option when calling the :func:`~Training.Train` function.
  You can find an example of what this report looks like :ref:`here <report_ex>`.

* Pickles 'best_model.txt' and 'best_model_dict.txt' which correspond to the
  **BEST** epoch's model and ``state_dict`` (only its parameters), respectively
  (see `PyTorch's documentation  on how to load state_dicts
  <https://pytorch.org/tutorials/beginner/saving_loading_models.html>`_ or
  :ref:`this example <code_example_dict>`).

* Pickles 'base_model.txt' and 'base_model_dict.txt' which correspond to the
  **last** epoch's model and ``state_dict`` (only its parameters), respectively.

* An "Images" folder with an image of the training curve ("TEST_losses.png") for
  the training and validation datasets.

* If the training function was called with ``remove_bool=False``, then there
  will also be a folder called "Epochs" with the model's ``state_dict`` for each
  individual epoch.

* Pickles 'train_info_tuple_.txt' and 'valid_info_tuple_.txt' which are store
  dataset information and are called by the :py:mod:`Load_Save` functions.

If you then call the :func:`~Load_Save.Load_Save` function, folders
"Images_Best" (best epoch model) and "Images_Final" (last epoch model) will be
created with the plots of the model vs data points for each battery - batteries
from the training dataset will be distinguished from the rest in the plot title.
Files "Analysis_Best.txt" and "Analysis_Final.txt" will also be created, with
quantitative information about the model's performance for each battery
(frankly, the most relevant information is on the line "Relative to the BoL
capacity:").

If you then call the :func:`~Load_Save.Load_Save_v2` function, something
similar is done, but the Images folder and Analysis file will include the name of
the dataset picked. **It won't distinguish batteries from the training and
validation datasets**.

**NEW:** You can now also call :func:`Table.table_from_report` on an analysis
file to create an excel spreadsheet with the information contained in it.
**Be aware that it won't distinguish batteries from the training and
validation datasets if the analysis file was created with**
:func:`~Load_Save.Load_Save_v2`.

.. toctree::
  :maxdepth: 2
  :caption: Contents:

Report creation
---------------

Corresponds to the ``Report`` module.

.. automodule:: Report
  :members:

Evaluating models from reports
------------------------------

Corresponds to the ``Load_Save`` and ``Table`` modules.

The :func:`~Load_Save.Load_Save` function is older and should be harder to
adapt to new datasets and models (though not incredibly hard).
:func:`~Load_Save.Load_Save_v2` is newer and should be easier. However, it
doesn't distinguish training and validation batteries in the files it creates.

.. automodule:: Load_Save
  :members:

.. automodule:: Table
  :members:

For older models
----------------

If everything goes right, you won't need to use this.

.. note::
    If you get a ``ModuleNotFoundError`` while using
    :py:func:`Load_Save.Load_Save` or :py:func:`Load_Save.Load_Save_v2`, you can
    still use :py:func:`Load_Save.Load_Save_v2` for analysis. Check its
    documentation for more.

.. toctree::
  Old_Load_Save


.. _dist_plots:

Distribution plots
------------------

Corresponds to the ``DistributionPlots`` module. Contains two functions for
creating histograms and contour plots of the inputs' distribution curves.

.. automodule:: DistributionPlots
  :members: