Regular LSTMs
=============

.. _normal_LSTM_Models:

Models
------

.. autoclass:: LSTM.LSTM_v1
  :members:

.. autoclass:: LSTM.LSTM_v4
  :members:

.. _normal_LSTM_Dataset:

Dataset class
-------------

The datasets created by this class' initialization follow You et al.'s
snapshot approach: I, V data for a charge cycle are used for battery
capacity prediction. A number ``delta`` of sequential :math:`(I, V)` points
(or :math:`(I, V, T)`, depending on the model) are collected and turned into
a 1D vector :math:`x_i`. This window of points slides through time and each
vector :math:`x_i` becomes the sequential inputs of an LSTM.
Here, these :math:`x_i` vectors are made and grouped sequentially in a 2D
tensor called ``XT``. One ``XT`` tensor = one sequence = one charge cycle =
one capacity value.
A list of all ``XT`` s is saved as attribute ``inputs``.

The output for each ``XT`` (each charge cycle) is a capacity value (in A.h) or
the SOH (dimensionless) if parameter ``adimensionalize = True``.
However, capacity values are only measured during discharge cycles. Because
of this, **the capacity values taken as outputs are picked from the
discharge cycle that comes immediately before the charge cycle - this step must
be done during the creation of the cleaned dataset. See**
:std:doc:`Battery Classes <Battery Classes>` **for more information**.

If ``norm`` variable is initialized as ``None`` (default), the dataset will be
automatically normalised according to the given ``norm_type`` (``'zscore'`` by
default), and the calculated normalisation parameters will be stored as
attribute ``norm``. Alternatively, the user can inform a ``norm`` to be used.

.. autoclass:: LSTM.Torch_Dataset_LSTM
  :members:
