Models
======

This Neural net is based on the one proposed by You et al.'s snapshot approach
(see note for full reference). The one proposed by the authors corresponds to
version 1, but many others were developed (a total of 5). Versions 1-3 are
included in the corresponding ``LSTM_v1`` classes, whilst versions 4-5 are
included in the ``LSTM_v4`` classes. Versions 4 and 5 are analogous to versions
1 and 2, but take the ambient temperature as an extra input directly at the
last linear layer. Check their documentation to learn more about the
different versions and how to call them.

.. note::
    Version 4 was the one that performed the best in terms of temperature
    dependency. Version 1 was the base used for non-temperature dependant
    datasets.

With the data in hand, a dataset must be created through the corresponding
Dataset class (``Torch_Dataset_LSTM``), which will adapt and normalize the
inputs and outputs according to the desired model ``version``. To read more
about it check :ref:`Dataset Class <normal_LSTM_Dataset>`. To *train* a
model please check :std:doc:`Training`.

All of the models have the same underlying architecture, presented next.

Architecture
------------

The strucutre is composed of three layers:

#. An LSTM layer that takes in a sequence of :math:`x_i` s as input.

    Together, they (the :math:`x_i` s) represent one charge cycle.
    Each :math:`x_i` is composed of a number :math:`{\Delta}` of sequential
    :math:`(I, V)` or :math:`(I, V, T)` points, spread out in a 1D vector.
    The actual shape of the inputs is thus: ``[batch_size, sequence_length,
    number_of_features]``, and the outputs have shape ``[batch_size,
    sequence_length, hidden_dimension*number_of_LSTMs]``.

    The number of features depends on the model's version (check the classes
    cited previously for more information) If the LSTM is a bidirectional type,
    then the ``number_of_LSTMs`` is equal to two. Otherwise, one.

#. A Pooling layer that treats the LSTM outputs in some way.

    Three possibilities are proposed by the authors for the pooling layer, and
    are implemented here:

    #. taking only the last output
    #. taking the mean of the outputs
    #. using a BiLSTM and taking the mean of the product of the results

    We recommed the latter.

#. A linear layer that gives the final output.

    It's just one linear layer so the number of inputs and outputs is set by
    the LSTM's number of hidden dimensions and its version.

.. note::
    Full reference: G. You, S. Park and D. Oh, "Diagnosis of Electric
    Vehicle Batteries Using Recurrent Neural Networks," in IEEE Transactions
    on Industrial Electronics, vol. 64, no. 6, pp. 4885-4893, June 2017,
    doi: 10.1109/TIE.2017.2674593.

Base Classes
------------

All model and dataset classes are child classes of the base classes defined in
:py:mod:`LSTM_Base`. This module was created just to define methods that
are common to all classes so that they don't have to be repetitively defined
each time.

.. toctree::
    :maxdepth: 1

    Base Classes <LSTM_Base>


.. _lstm_variants:

Variants
--------

More than just the regular (``'normal'``) variant, we have LSTMs that do the
normalisation of the inputs and outputs (LSTMs with normalization) and LSTMs
that also take in the battery chemistry as an input. The class names are the
same so that their use is analogous. More in depth explanations about their
specificities are presented in their pages:

.. note::
    Recommended way of importing (in order to avoid confusion with the class
    names):

    .. code-block::

        >>> from LSTM import *
        >>> import LSTM_with_norm as wn
        >>> import LSTM_chemistry as chem

.. toctree::
    :maxdepth: 1

    Regular LSTMs <LSTM>
    LSTMs with normalization <LSTMwithNorm>
    LSTMs with chemistry <LSTMchem>

.. _new_models:

Developing New Models
---------------------

If you're thinking of developing new models and want to integrate them to the
existing framework developed here, go ahead! In order to assure they'll work
with the other codes, check the :std:doc:`Base Classes's <LSTM_Base>` source
code for the methods defined there, since they'll be called by other functions.
You can redefine them in your own classes if you think it's better.