LSTMs with chemistry
====================

This module (:py:mod:`LSTM_chemistry`) contains the ``'chemistry'`` variants,
which take the battery's chemistry/technology into account as an input variable.
A Li-ion dataset will have an input value of 0, while a Na-ion dataset will have
and input value of 1. This value is introduced into the network in the final
regression layers (just like ``LSTM_v4`` types introduce the ambient temperature
to them).

Because of this, datasets for this model must be created using this module's
:class:`~LSTM_chemistry.Torch_Dataset_LSTM` class. However, it's necessary to
mix batteries of different chemistries in the dataset for the model to be well
trained, which isn't supported by it. The way to proceed is to create a
:class:`~LSTM_chemistry.Torch_Dataset_LSTM` class for each dataset (NASA, MIT,
Coin) and them merge them together through the
:class:`~LSTM_chemistry.Torch_Dataset_Mixer` class.


.. warning::
    Notice that, because of this, each dataset may be normalised independently
    (unless the user specifies the same ``norm`` for each dataset), and the final
    :class:`~LSTM_chemistry.Torch_Dataset_Mixer` class **won't have a** ``norm``
    **attribute** that you can call.

    Because of this, when calling the :func:`simulate` with a loader
    with this dataset, the user must specify ``denormalize = False``. Function
    :func:`~Load_Save.Load_Save` cannot be used because of this reason, but
    :func:`~Load_Save.Load_Save_v2` can, including with
    ``denormalize_bool=True``, because it treat datasets separately.

    ..
        I actually have check if Load_Save really can't be used though,
        actually.

Check the Google Colab/Notebook file "LSTM Chemistry Training.ipynb" for a
step-by-step of how to train a model.

.. _LSTM_chemistry_Models:

Models
------

.. autoclass:: LSTM_chemistry.LSTM_v1
  :members:

.. autoclass:: LSTM_chemistry.LSTM_v4
  :members:

.. _LSTM_chemistry_Dataset:

Dataset Classes
---------------

.. autoclass:: LSTM_chemistry.Torch_Dataset_LSTM
  :members:

.. autoclass:: LSTM_chemistry.Torch_Dataset_Mixer
  :members:

.. autofunction:: LSTM_chemistry.norm_adapt

