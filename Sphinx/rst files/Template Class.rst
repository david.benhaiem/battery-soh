Template Classes
================

Corresponds to the ``Template_Classes`` module.

Notice that for each new dataset integrated, you'll have to add the respective
values of ``C_nominal``, ``V_nominal`` and ``Q_nominal`` to the
:class:`~LSTM.Torch_Dataset_LSTM` class for each of the :py:mod:`LSTM`,
:py:mod:`LSTM_chemistry` and :py:mod:`LSTM_with_norm` modules
(:std:doc:`Models`).

.. automodule:: Template_Classes
    :members: