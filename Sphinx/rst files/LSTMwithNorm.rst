.. _lstms_norm:

LSTMs with normalization
========================

This module (:py:mod:`LSTM_with_norm`) contains the models that include the
normalisation parameters (``norm`` tuple) as optimizable parameters, that can be
trained with or without the rest of the neural network.

Methods :func:`~LSTM_with_norm.LSTM_v1.enable_training` and
:func:`~LSTM_with_norm.LSTM_v1.disable_training` can be used to
enable and disable training of the neural network's parameters.

.. note::
    As of now, there isn't a method for disabling/re-enabling training of the
    normalisation parameters (since they are the whole point of these models).
    :ref:`Here's <norm_train>` a code that does just that in case you need it.

Usually, these models are used exclusively to optimize the normalisation
parameters. Optimizing them at the same time as the neural network has not been
attempted.

In order to get the network's parameters from another model, first initialize
the desired model (``norm`` can be initialized as ``None``, though it won't work
until you define an actual value) then call the :func:`from_model` method with
the old model you want to get the NN parameters of.

Models from this module also have a :func:`change_norm` method, where the user
can directly input a norm, facilitating the process of alternating between
norms.

.. warning::
    After training a model's normalisation parameters, make sure to call the
    ``update_norm()`` method. It ensures that the saved ``norm`` attribute is
    updated. This is necessary because the parameters updated during training
    are stored in other attributes.

    This will not affect the model's performance, it's just useful for when
    calling the model's normalisation parameters in the standard ``norm`` form
    through the ``norm`` attribute. **Not updating the attribute may result in
    silent errors in other functions or when calling it in the future**.

Example of training an LSTM with norm:

.. code-block::

    >>> from LSTM import *
    >>> import LSTM_with_norm as wn
    >>> from Training import *
    >>> from MIT_Classes import *
    >>> import main
    >>> # Model version 1 trained with Tarascon's Coin dataset:
    >>> report_name = '2021-08-04 13_53_33.071482-coinv1'
    >>> # getting the model from the report:
    >>> os.chdir('.//Reports/'+report_name)
    >>> with open('best_model.txt', 'rb') as f:
    ...     base_model = pickle.load(f)
    ...
    >>> os.chdir('../../')
    >>> # Defining an "Empty" model (the inputs don't matter because it will)
    ... # be reinitialized
    ...
    >>> Model = wn.LSTM_v1(10, 10)
    >>> Model.from_model(base_model)
    >>> # Skipping dataset creation (we're using MIT dataset)
    ...
    >>> Model.change_norm(train_dataset.norm) # Using the training dataset norm
    >>> Model = Model.to(device) # if the device is a GPU
    >>> Nb_Epochs = 1
    >>> Learning_Rate = 5e-6
    >>> _,_,_,date = Train(train, valid_loader, Model, Nb_Epochs)
    1 . Training loss:  tensor(7.3074e-05)
    1 . Validation loss:  tensor(0.0003)
    Best validation error: 1
    >>> Model.update_norm() # Making sure the Model.norm attribute is updated

Check the Google Colab/Notebook file "Normalisation Training.ipynb" for a
step-by-step of how to train a model.

.. _LSTM_with_norm_Models:

Models
------

.. autoclass:: LSTM_with_norm.LSTM_v1
  :members:

.. autoclass:: LSTM_with_norm.LSTM_v4
  :members:

.. _LSTM_with_norm_Dataset:

Dataset Class
-------------

.. autoclass:: LSTM_with_norm.Torch_Dataset_LSTM
  :members: