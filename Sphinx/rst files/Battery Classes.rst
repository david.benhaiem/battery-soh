Battery Classes
===============

Each one of the modules cited next contain two sets of classes that aim to store
the cycling data of each dataset. The first set aims to open the raw data files
provided by the authors and store all of the information as is in a python
object. The second set of classes aim to clean and select data that is
proper for model training and validation, as well as storing them in a way that
the dataset creation classes can access them. They take as input the
corresponding raw battery class.

Usually they are structured something like this (**fake class names, class
names depend on the dataset**):

.. code-block::

    # Raw data
    dataset_class = Raw_Dataset('raw_data.mat')
    list_of_cycles = dataset_class.cycle
    cycle_class = dataset_class.cycle[0]    # Instance of Raw_Cycle, for cycle 0
    data_class = cycle_class.data           # Instance of Raw_Data
    voltage = data_class.meas_v             # measured voltage for cycle 0

    # Clean data
    dataset_class = Clean_Dataset(Raw_Dataset)
    list_cycle_couples = dataset_class.cycle_couple # List of tuples
    # Associated charge and discharge cycles (Clean_Cycle classes):
    charge_cycle, discharge_cycle = list_cycle_couples[0]
    data_class = charge_cycle.data          # Clean_Data classes
    voltage = data_class.meas_v

All source code has examples on how the classes should be called/initialized
for treating the data (after the ``if __name__ == '__main__':`` part).

For new datasets, raw battery classes will invariably have to be created
according to the way the data is given (type of file etc.). New Clean Battery
Classes should probably be created for each new
dataset (though the framework is similar and can mostly be reused, with few
adaptations for the small differences between datasets).

The dataset with the most different structure is the Na-ion dataset, because
batteries are split into cells.

.. toctree::
    :maxdepth: 1

    Nasa dataset <NASA>
    MIT dataset <MIT>
    Na-ion dataset <COIN>

.. _new_dataset:

Working with new Datasets
-------------------------

If you wish to work with new datasets, you'll have to create new raw and clean
dataset classes each time, but you can use the template
:std:doc:`Template_Classes.py <Template Class>` to have an idea on how to do it
(it's easier to look directly at its source code, I think, and also look at
the other dataset classes).

Keep in mind that, for easier integration with other codes, you'll have to
update:

* The ``main.py`` dictionaries (:std:doc:`Useful Dictionaries <main>`).

* A few statements in :func:`~Load_Save.Load_Save` and
  :func:`~Load_Save.Load_Save_v2`.

* Probably a few ``assertion`` s here and there (like in
  :func:`~DistributionPlots.dataset_joint_plot`).

* The path from the main folder to the clean batteries folder must have exactly
  four steps, so that the codes can come back to the main project folder through
  ``os.chdir('../../../../')``.

* Name the clean battery pickles as "B000X.txt". Examples: B0001.txt, B0020.txt,
  B0134.txt etc. This standard notation is important to be recognized by codes.
  It can be created by: ``"B"+str(i).zfill(4)+".txt"``, with ``i`` being the
  battery number.