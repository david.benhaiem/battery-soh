Base Classes
============

Corresponds to the ``LSTM_Base`` module. It contains the parent classes of the
models and dataset classes defined in the different LSTM modules.

These classes are just defined here to avoid repetitive redefinition of certain
class methods defined for every model. More attributes could have been defined
here as well but I prefered to keep it simple.

.. automodule:: LSTM_Base
    :members: