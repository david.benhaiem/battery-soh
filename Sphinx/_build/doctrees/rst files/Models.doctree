��qY      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Models�h]�h	�Text����Models�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�F/Users/antonio/PycharmProjects/battery-soh/Sphinx/rst files/Models.rst�hKubh	�	paragraph���)��}�(hX<  This Neural net is based on the one proposed by You et al.'s snapshot approach
(see note for full reference). The one proposed by the authors corresponds to
version 1, but many others were developed (a total of 5). Versions 1-3 are
included in the corresponding ``LSTM_v1`` classes, whilst versions 4-5 are
included in the ``LSTM_v4`` classes. Versions 4 and 5 are analogous to versions
1 and 2, but take the ambient temperature as an extra input directly at the
last linear layer. Check their documentation to learn more about the
different versions and how to call them.�h]�(hX  This Neural net is based on the one proposed by You et al.’s snapshot approach
(see note for full reference). The one proposed by the authors corresponds to
version 1, but many others were developed (a total of 5). Versions 1-3 are
included in the corresponding �����}�(hX  This Neural net is based on the one proposed by You et al.'s snapshot approach
(see note for full reference). The one proposed by the authors corresponds to
version 1, but many others were developed (a total of 5). Versions 1-3 are
included in the corresponding �hh.hhhNhNubh	�literal���)��}�(h�``LSTM_v1``�h]�h�LSTM_v1�����}�(hhhh9ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hh.ubh�2 classes, whilst versions 4-5 are
included in the �����}�(h�2 classes, whilst versions 4-5 are
included in the �hh.hhhNhNubh8)��}�(h�``LSTM_v4``�h]�h�LSTM_v4�����}�(hhhhLubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hh.ubh�� classes. Versions 4 and 5 are analogous to versions
1 and 2, but take the ambient temperature as an extra input directly at the
last linear layer. Check their documentation to learn more about the
different versions and how to call them.�����}�(h�� classes. Versions 4 and 5 are analogous to versions
1 and 2, but take the ambient temperature as an extra input directly at the
last linear layer. Check their documentation to learn more about the
different versions and how to call them.�hh.hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhhhubh	�note���)��}�(h��Version 4 was the one that performed the best in terms of temperature
dependency. Version 1 was the base used for non-temperature dependant
datasets.�h]�h-)��}�(h��Version 4 was the one that performed the best in terms of temperature
dependency. Version 1 was the base used for non-temperature dependant
datasets.�h]�h��Version 4 was the one that performed the best in terms of temperature
dependency. Version 1 was the base used for non-temperature dependant
datasets.�����}�(hhmhhkubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhgubah}�(h ]�h"]�h$]�h&]�h(]�uh*hehhhhhh+hNubh-)��}�(hXQ  With the data in hand, a dataset must be created through the corresponding
Dataset class (``Torch_Dataset_LSTM``), which will adapt and normalize the
inputs and outputs according to the desired model ``version``. To read more
about it check :ref:`Dataset Class <normal_LSTM_Dataset>`. To *train* a
model please check :std:doc:`Training`.�h]�(h�ZWith the data in hand, a dataset must be created through the corresponding
Dataset class (�����}�(h�ZWith the data in hand, a dataset must be created through the corresponding
Dataset class (�hhhhhNhNubh8)��}�(h�``Torch_Dataset_LSTM``�h]�h�Torch_Dataset_LSTM�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hhubh�X), which will adapt and normalize the
inputs and outputs according to the desired model �����}�(h�X), which will adapt and normalize the
inputs and outputs according to the desired model �hhhhhNhNubh8)��}�(h�``version``�h]�h�version�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hhubh�. To read more
about it check �����}�(h�. To read more
about it check �hhhhhNhNubh �pending_xref���)��}�(h�*:ref:`Dataset Class <normal_LSTM_Dataset>`�h]�h	�inline���)��}�(hh�h]�h�Dataset Class�����}�(hhhh�ubah}�(h ]�h"]�(�xref��std��std-ref�eh$]�h&]�h(]�uh*h�hh�ubah}�(h ]�h"]�h$]�h&]�h(]��refdoc��rst files/Models��	refdomain�h��reftype��ref��refexplicit���refwarn���	reftarget��normal_lstm_dataset�uh*h�hh+hKhhubh�. To �����}�(h�. To �hhhhhNhNubh	�emphasis���)��}�(h�*train*�h]�h�train�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h�hhubh� a
model please check �����}�(h� a
model please check �hhhhhNhNubh�)��}�(h�:std:doc:`Training`�h]�h�)��}�(hh�h]�h�Training�����}�(hhhh�ubah}�(h ]�h"]�(h��std��std-doc�eh$]�h&]�h(]�uh*h�hh�ubah}�(h ]�h"]�h$]�h&]�h(]��refdoc�h͌	refdomain�h��reftype��doc��refexplicit���refwarn��hӌTraining�uh*h�hh+hKhhubh�.�����}�(h�.�hhhhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhhhubh-)��}�(h�HAll of the models have the same underlying architecture, presented next.�h]�h�HAll of the models have the same underlying architecture, presented next.�����}�(hj  hj  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhhhubh)��}�(hhh]�(h)��}�(h�Architecture�h]�h�Architecture�����}�(hj-  hj+  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj(  hhhh+hKubh-)��}�(h�*The strucutre is composed of three layers:�h]�h�*The strucutre is composed of three layers:�����}�(hj;  hj9  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhj(  hhubh	�enumerated_list���)��}�(hhh]�(h	�	list_item���)��}�(hX�  An LSTM layer that takes in a sequence of :math:`x_i` s as input.

 Together, they (the :math:`x_i` s) represent one charge cycle.
 Each :math:`x_i` is composed of a number :math:`{\Delta}` of sequential
 :math:`(I, V)` or :math:`(I, V, T)` points, spread out in a 1D vector.
 The actual shape of the inputs is thus: ``[batch_size, sequence_length,
 number_of_features]``, and the outputs have shape ``[batch_size,
 sequence_length, hidden_dimension*number_of_LSTMs]``.

 The number of features depends on the model's version (check the classes
 cited previously for more information) If the LSTM is a bidirectional type,
 then the ``number_of_LSTMs`` is equal to two. Otherwise, one.
�h]�(h-)��}�(h�AAn LSTM layer that takes in a sequence of :math:`x_i` s as input.�h]�(h�*An LSTM layer that takes in a sequence of �����}�(h�*An LSTM layer that takes in a sequence of �hjR  ubh	�math���)��}�(h�:math:`x_i`�h]�h�x_i�����}�(hhhj]  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j[  hjR  ubh� s as input.�����}�(h� s as input.�hjR  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhjN  ubh	�block_quote���)��}�(hhh]�(h-)��}�(hX�  Together, they (the :math:`x_i` s) represent one charge cycle.
Each :math:`x_i` is composed of a number :math:`{\Delta}` of sequential
:math:`(I, V)` or :math:`(I, V, T)` points, spread out in a 1D vector.
The actual shape of the inputs is thus: ``[batch_size, sequence_length,
number_of_features]``, and the outputs have shape ``[batch_size,
sequence_length, hidden_dimension*number_of_LSTMs]``.�h]�(h�Together, they (the �����}�(h�Together, they (the �hj{  ubj\  )��}�(h�:math:`x_i`�h]�h�x_i�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j[  hj{  ubh�% s) represent one charge cycle.
Each �����}�(h�% s) represent one charge cycle.
Each �hj{  ubj\  )��}�(h�:math:`x_i`�h]�h�x_i�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j[  hj{  ubh� is composed of a number �����}�(h� is composed of a number �hj{  ubj\  )��}�(h�:math:`{\Delta}`�h]�h�{\Delta}�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j[  hj{  ubh� of sequential
�����}�(h� of sequential
�hj{  ubj\  )��}�(h�:math:`(I, V)`�h]�h�(I, V)�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j[  hj{  ubh� or �����}�(h� or �hj{  ubj\  )��}�(h�:math:`(I, V, T)`�h]�h�	(I, V, T)�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j[  hj{  ubh�L points, spread out in a 1D vector.
The actual shape of the inputs is thus: �����}�(h�L points, spread out in a 1D vector.
The actual shape of the inputs is thus: �hj{  ubh8)��}�(h�5``[batch_size, sequence_length,
number_of_features]``�h]�h�1[batch_size, sequence_length,
number_of_features]�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj{  ubh�, and the outputs have shape �����}�(h�, and the outputs have shape �hj{  ubh8)��}�(h�C``[batch_size,
sequence_length, hidden_dimension*number_of_LSTMs]``�h]�h�?[batch_size,
sequence_length, hidden_dimension*number_of_LSTMs]�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj{  ubh�.�����}�(hj  hj{  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK!hjx  ubh-)��}�(h��The number of features depends on the model's version (check the classes
cited previously for more information) If the LSTM is a bidirectional type,
then the ``number_of_LSTMs`` is equal to two. Otherwise, one.�h]�(h��The number of features depends on the model’s version (check the classes
cited previously for more information) If the LSTM is a bidirectional type,
then the �����}�(h��The number of features depends on the model's version (check the classes
cited previously for more information) If the LSTM is a bidirectional type,
then the �hj  ubh8)��}�(h�``number_of_LSTMs``�h]�h�number_of_LSTMs�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj  ubh�! is equal to two. Otherwise, one.�����}�(h�! is equal to two. Otherwise, one.�hj  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK(hjx  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*jv  hjN  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*jL  hjI  hhhh+hNubjM  )��}�(hXA  A Pooling layer that treats the LSTM outputs in some way.

 Three possibilities are proposed by the authors for the pooling layer, and
 are implemented here:

 #. taking only the last output
 #. taking the mean of the outputs
 #. using a BiLSTM and taking the mean of the product of the results

 We recommed the latter.
�h]�(h-)��}�(h�9A Pooling layer that treats the LSTM outputs in some way.�h]�h�9A Pooling layer that treats the LSTM outputs in some way.�����}�(hjB  hj@  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK,hj<  ubjw  )��}�(hhh]�(h-)��}�(h�`Three possibilities are proposed by the authors for the pooling layer, and
are implemented here:�h]�h�`Three possibilities are proposed by the authors for the pooling layer, and
are implemented here:�����}�(hjS  hjQ  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK.hjN  ubjH  )��}�(hhh]�(jM  )��}�(h�taking only the last output�h]�h-)��}�(hjd  h]�h�taking only the last output�����}�(hjd  hjf  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK1hjb  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*jL  hj_  ubjM  )��}�(h�taking the mean of the outputs�h]�h-)��}�(hj{  h]�h�taking the mean of the outputs�����}�(hj{  hj}  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK2hjy  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*jL  hj_  ubjM  )��}�(h�Ausing a BiLSTM and taking the mean of the product of the results
�h]�h-)��}�(h�@using a BiLSTM and taking the mean of the product of the results�h]�h�@using a BiLSTM and taking the mean of the product of the results�����}�(hj�  hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK3hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*jL  hj_  ubeh}�(h ]�h"]�h$]�h&]�h(]��enumtype��arabic��prefix�h�suffix�j  uh*jG  hjN  ubh-)��}�(h�We recommed the latter.�h]�h�We recommed the latter.�����}�(hj�  hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK5hjN  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*jv  hj<  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*jL  hjI  hhhh+hNubjM  )��}�(h��A linear layer that gives the final output.

 It's just one linear layer so the number of inputs and outputs is set by
 the LSTM's number of hidden dimensions and its version.
�h]�(h-)��}�(h�+A linear layer that gives the final output.�h]�h�+A linear layer that gives the final output.�����}�(hj�  hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK7hj�  ubjw  )��}�(hhh]�h-)��}�(h��It's just one linear layer so the number of inputs and outputs is set by
the LSTM's number of hidden dimensions and its version.�h]�h��It’s just one linear layer so the number of inputs and outputs is set by
the LSTM’s number of hidden dimensions and its version.�����}�(hj�  hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK9hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*jv  hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*jL  hjI  hhhh+hNubeh}�(h ]�h"]�h$]�h&]�h(]�j�  j�  j�  hj�  j  uh*jG  hj(  hhhh+hKubhf)��}�(h��Full reference: G. You, S. Park and D. Oh, "Diagnosis of Electric
Vehicle Batteries Using Recurrent Neural Networks," in IEEE Transactions
on Industrial Electronics, vol. 64, no. 6, pp. 4885-4893, June 2017,
doi: 10.1109/TIE.2017.2674593.�h]�h-)��}�(h��Full reference: G. You, S. Park and D. Oh, "Diagnosis of Electric
Vehicle Batteries Using Recurrent Neural Networks," in IEEE Transactions
on Industrial Electronics, vol. 64, no. 6, pp. 4885-4893, June 2017,
doi: 10.1109/TIE.2017.2674593.�h]�h��Full reference: G. You, S. Park and D. Oh, “Diagnosis of Electric
Vehicle Batteries Using Recurrent Neural Networks,” in IEEE Transactions
on Industrial Electronics, vol. 64, no. 6, pp. 4885-4893, June 2017,
doi: 10.1109/TIE.2017.2674593.�����}�(hj  hj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK=hj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hehj(  hhhh+hNubeh}�(h ]��architecture�ah"]�h$]��architecture�ah&]�h(]�uh*h
hhhhhh+hKubh)��}�(hhh]�(h)��}�(h�Base Classes�h]�h�Base Classes�����}�(hj&  hj$  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj!  hhhh+hKCubh-)��}�(h��All model and dataset classes are child classes of the base classes defined in
:py:mod:`LSTM_Base`. This module was created just to define methods that
are common to all classes so that they don't have to be repetitively defined
each time.�h]�(h�OAll model and dataset classes are child classes of the base classes defined in
�����}�(h�OAll model and dataset classes are child classes of the base classes defined in
�hj2  hhhNhNubh�)��}�(h�:py:mod:`LSTM_Base`�h]�h8)��}�(hj=  h]�h�	LSTM_Base�����}�(hhhj?  ubah}�(h ]�h"]�(h��py��py-mod�eh$]�h&]�h(]�uh*h7hj;  ubah}�(h ]�h"]�h$]�h&]�h(]��refdoc�h͌	refdomain�jI  �reftype��mod��refexplicit���refwarn���	py:module�N�py:class�Nhӌ	LSTM_Base�uh*h�hh+hKEhj2  ubh��. This module was created just to define methods that
are common to all classes so that they don’t have to be repetitively defined
each time.�����}�(h��. This module was created just to define methods that
are common to all classes so that they don't have to be repetitively defined
each time.�hj2  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKEhj!  hhubh	�compound���)��}�(hhh]�h �toctree���)��}�(hhh]�h}�(h ]�h"]�h$]�h&]�h(]�hh͌entries�]��Base Classes��rst files/LSTM_Base���a�includefiles�]�j{  a�maxdepth�K�caption�N�glob���hidden���includehidden���numbered�K �
titlesonly���
rawentries�]�jz  auh*jm  hh+hKJhjj  ubah}�(h ]�h"]��toctree-wrapper�ah$]�h&]�h(]�uh*jh  hj!  hhhh+hNubh	�target���)��}�(h�.. _lstm_variants:�h]�h}�(h ]�h"]�h$]�h&]�h(]��refid��lstm-variants�uh*j�  hKPhj!  hhhh+ubeh}�(h ]��base-classes�ah"]�h$]��base classes�ah&]�h(]�uh*h
hhhhhh+hKCubh)��}�(hhh]�(h)��}�(h�Variants�h]�h�Variants�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hKSubh-)��}�(hX^  More than just the regular (``'normal'``) variant, we have LSTMs that do the
normalisation of the inputs and outputs (LSTMs with normalization) and LSTMs
that also take in the battery chemistry as an input. The class names are the
same so that their use is analogous. More in depth explanations about their
specificities are presented in their pages:�h]�(h�More than just the regular (�����}�(h�More than just the regular (�hj�  hhhNhNubh8)��}�(h�``'normal'``�h]�h�'normal'�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h7hj�  ubhX6  ) variant, we have LSTMs that do the
normalisation of the inputs and outputs (LSTMs with normalization) and LSTMs
that also take in the battery chemistry as an input. The class names are the
same so that their use is analogous. More in depth explanations about their
specificities are presented in their pages:�����}�(hX6  ) variant, we have LSTMs that do the
normalisation of the inputs and outputs (LSTMs with normalization) and LSTMs
that also take in the battery chemistry as an input. The class names are the
same so that their use is analogous. More in depth explanations about their
specificities are presented in their pages:�hj�  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKUhj�  hhubhf)��}�(h��Recommended way of importing (in order to avoid confusion with the class
names):

.. code-block::

    >>> from LSTM import *
    >>> import LSTM_with_norm as wn
    >>> import LSTM_chemistry as chem�h]�(h-)��}�(h�PRecommended way of importing (in order to avoid confusion with the class
names):�h]�h�PRecommended way of importing (in order to avoid confusion with the class
names):�����}�(hj�  hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK\hj�  ubh	�literal_block���)��}�(h�X>>> from LSTM import *
>>> import LSTM_with_norm as wn
>>> import LSTM_chemistry as chem�h]�h�X>>> from LSTM import *
>>> import LSTM_with_norm as wn
>>> import LSTM_chemistry as chem�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]��	xml:space��preserve��force���language��default��highlight_args�}�uh*j�  hh+hK_hj�  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*hehj�  hhhNhNubji  )��}�(hhh]�jn  )��}�(hhh]�h}�(h ]�h"]�h$]�h&]�h(]�hh�jx  ]�(�Regular LSTMs��rst files/LSTM����LSTMs with normalization��rst files/LSTMwithNorm����LSTMs with chemistry��rst files/LSTMchem���ej}  ]�(j  j  j  ej  Kj�  Nj�  �j�  �j�  �j�  K j�  �j�  ]�(j  j  j  euh*jm  hh+hKehj  ubah}�(h ]�h"]�j�  ah$]�h&]�h(]�uh*jh  hj�  hhhh+hNubj�  )��}�(h�.. _new_models:�h]�h}�(h ]�h"]�h$]�h&]�h(]�j�  �
new-models�uh*j�  hKlhj�  hhhh+ubeh}�(h ]�(�variants�j�  eh"]�h$]�(�variants��lstm_variants�eh&]�h(]�uh*h
hhhhhh+hKS�expect_referenced_by_name�}�j6  j�  s�expect_referenced_by_id�}�j�  j�  subh)��}�(hhh]�(h)��}�(h�Developing New Models�h]�h�Developing New Models�����}�(hjB  hj@  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj=  hhhh+hKoubh-)��}�(hX{  If you're thinking of developing new models and want to integrate them to the
existing framework developed here, go ahead! In order to assure they'll work
with the other codes, check the :std:doc:`Base Classes's <LSTM_Base>` source
code for the methods defined there, since they'll be called by other functions.
You can redefine them in your own classes if you think it's better.�h]�(h��If you’re thinking of developing new models and want to integrate them to the
existing framework developed here, go ahead! In order to assure they’ll work
with the other codes, check the �����}�(h��If you're thinking of developing new models and want to integrate them to the
existing framework developed here, go ahead! In order to assure they'll work
with the other codes, check the �hjN  hhhNhNubh�)��}�(h�%:std:doc:`Base Classes's <LSTM_Base>`�h]�h�)��}�(hjY  h]�h�Base Classes’s�����}�(hhhj[  ubah}�(h ]�h"]�(h��std��std-doc�eh$]�h&]�h(]�uh*h�hjW  ubah}�(h ]�h"]�h$]�h&]�h(]��refdoc�h͌	refdomain�je  �reftype��doc��refexplicit���refwarn��hӌ	LSTM_Base�uh*h�hh+hKqhjN  ubh�� source
code for the methods defined there, since they’ll be called by other functions.
You can redefine them in your own classes if you think it’s better.�����}�(h�� source
code for the methods defined there, since they'll be called by other functions.
You can redefine them in your own classes if you think it's better.�hjN  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKqhj=  hhubeh}�(h ]�(�developing-new-models�j/  eh"]�h$]�(�developing new models��
new_models�eh&]�h(]�uh*h
hhhhhh+hKoj9  }�j�  j%  sj;  }�j/  j%  subeh}�(h ]��models�ah"]�h$]��models�ah&]�h(]�uh*h
hhhhhh+hKubah}�(h ]�h"]�h$]�h&]�h(]��source�h+uh*h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h+�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�line_length_limit�J ��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�(j�  ]�j�  aj/  ]�j%  au�nameids�}�(j�  j�  j  j  j�  j�  j6  j�  j5  j2  j�  j/  j�  j�  u�	nametypes�}�(j�  Nj  Nj�  Nj6  �j5  Nj�  �j�  Nuh }�(j�  hj  j(  j�  j!  j�  j�  j2  j�  j/  j=  j�  j=  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]�(h	�system_message���)��}�(hhh]�h-)��}�(hhh]�h�3Hyperlink target "lstm-variants" is not referenced.�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hj  ubah}�(h ]�h"]�h$]�h&]�h(]��level�K�type��INFO��source�h+�line�KPuh*j  ubj  )��}�(hhh]�h-)��}�(hhh]�h�0Hyperlink target "new-models" is not referenced.�����}�(hhhj:  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hj7  ubah}�(h ]�h"]�h$]�h&]�h(]��level�K�type�j4  �source�h+�line�Kluh*j  ube�transformer�N�
decoration�Nhhub.