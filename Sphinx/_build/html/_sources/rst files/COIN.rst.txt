Tarascon's Coin dataset
=======================
Corresponds to the ``Coin_Classes`` module.

Path for the clean batteries:
"Datasets/Tarascon/Coins/Clean_Batteries"
or simply ``import main`` and do ``main.path_dict['Tarascon Coin']``

Batteries are charged at C/5 until the voltage reaches 4,3V. The value of
:math:`Q_nominal` varies slightly for each battery because the electrode mass
used also varies slightly (as well as other chemistry details).
Because of this, we work with specific :math:`Q` values.
The capacity value saved in the :class:`~Coin_Data` class
(Coin_Data.capacity) is already normalized by the mass (A.h/mg). A value of
0.1284 A.h/mg is used for the nominal specific capacity for calculating SOH.
The ``C_rate`` used for nondimensioning the current in the
:class:`LSTM.Torch_Dataset_LSTM` class is also updated according to the mass for
each battery.

You can check extensive information in the "Information regarding cells.xlsx"
spreadsheet found with the raw dataset files.

Tarascon's Coin dataset is structurally different from the others.
Each numbered battery has a different number of independent cells
(which are just duplicates/triplicates etc., separate cells that go through the'
exact same procedure).

Because of this, the class order goes:

Clean Battery > Clean Cell > Clean Cycle > Clean Data.

Instead of the usual

Clean Battery > Clean Cycle > Clean Data.

Each cell's data is divided into different parts.
Experimentally, cells are cycled 10 times, then rest for one week, and then they
are cycled until a certain SOH. Usually, this data is divided into two parts
(he first 10 cycles then the rest period with the rest of the cycles), but this
may vary.

The classes for cleaning and storing the cleaned data already take these
different parts into account, merge them together and separate the different
cycles as necessary.

.. _raw_coin:

Classes for storing the raw data
--------------------------------

.. autoclass:: Coin_Classes.Dataset_Coin
  :members:

.. autoclass:: Coin_Classes.Coin_Cell
  :members:

.. autoclass:: Coin_Classes.Coin_Part
  :members:


.. _clean_coin:


Classes for cleaning and storing cleaned data
---------------------------------------------

.. autoclass:: Coin_Classes.Clean_Coin_Battery
  :members:

.. autoclass:: Coin_Classes.Clean_Cell
  :members:

.. autoclass:: Coin_Classes.Coin_Cycle
  :members:

.. autoclass:: Coin_Classes.Coin_Data
