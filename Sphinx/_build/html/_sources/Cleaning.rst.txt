Treatment functions
===================

Functions used for cleaning :std:doc:`Battery Classes <Battery Classes>`
data or for dataset creation.

.. automodule:: Cleaning
  :members: