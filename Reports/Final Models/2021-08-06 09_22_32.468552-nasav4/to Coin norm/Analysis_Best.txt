Battery B0001.txt (training)

    Mean and Max Absolute Error: 0.2995, 0.4937
    Mean and Max Relative Error: 77.4%, 193.%
    Relative to the BoL capacity: 29.9%, 49.3%
    RMSE: 0.3293

Battery B0002.txt

    Mean and Max Absolute Error: 0.1521, 0.2055
    Mean and Max Relative Error: 22.9%, 32.8%
    Relative to the BoL capacity: 15.2%, 20.5%
    RMSE: 0.1582

Battery B0003.txt

    Mean and Max Absolute Error: 0.0882, 0.1385
    Mean and Max Relative Error: 12.0%, 19.9%
    Relative to the BoL capacity: 8.82%, 13.8%
    RMSE: 0.0946

Battery B0004.txt (training)

    Mean and Max Absolute Error: 0.0453, 0.1824
    Mean and Max Relative Error: 7.21%, 40.0%
    Relative to the BoL capacity: 4.53%, 18.2%
    RMSE: 0.0680

Battery B0005.txt

    Mean and Max Absolute Error: 0.1268, 0.2073
    Mean and Max Relative Error: 19.2%, 37.2%
    Relative to the BoL capacity: 12.6%, 20.7%
    RMSE: 0.1388

Battery B0006.txt

    Mean and Max Absolute Error: 0.1866, 0.4365
    Mean and Max Relative Error: 35.4%, 119.%
    Relative to the BoL capacity: 18.6%, 43.6%
    RMSE: 0.2220

Battery B0009.txt

    Mean and Max Absolute Error: 0.0704, 0.1212
    Mean and Max Relative Error: 9.51%, 17.0%
    Relative to the BoL capacity: 7.04%, 12.1%
    RMSE: 0.0791

Battery B0010.txt

    Mean and Max Absolute Error: 0.0861, 0.1922
    Mean and Max Relative Error: 12.0%, 30.0%
    Relative to the BoL capacity: 8.61%, 19.2%
    RMSE: 0.1005

Battery B0011.txt

    Mean and Max Absolute Error: 0.0519, 0.1055
    Mean and Max Relative Error: 6.73%, 14.5%
    Relative to the BoL capacity: 5.19%, 10.5%
    RMSE: 0.0560

Battery B0012.txt

    Mean and Max Absolute Error: 0.2914, 0.3861
    Mean and Max Relative Error: 72.4%, 76.1%
    Relative to the BoL capacity: 29.1%, 38.6%
    RMSE: 0.3079

Battery B0013.txt

    Mean and Max Absolute Error: 0.3599, 0.4047
    Mean and Max Relative Error: 72.6%, 79.8%
    Relative to the BoL capacity: 35.9%, 40.4%
    RMSE: 0.3609

Battery B0014.txt (training)

    Mean and Max Absolute Error: 0.2483, 0.3910
    Mean and Max Relative Error: 70.1%, 74.4%
    Relative to the BoL capacity: 24.8%, 39.1%
    RMSE: 0.2641

Battery B0015.txt

    Mean and Max Absolute Error: 0.3377, 0.4036
    Mean and Max Relative Error: 66.0%, 80.4%
    Relative to the BoL capacity: 33.7%, 40.3%
    RMSE: 0.3438

Battery B0016.txt (training)

    Mean and Max Absolute Error: 0.3800, 0.4102
    Mean and Max Relative Error: 73.2%, 88.4%
    Relative to the BoL capacity: 38.0%, 41.0%
    RMSE: 0.3807

Battery B0017.txt

    Mean and Max Absolute Error: 0.3852, 0.4089
    Mean and Max Relative Error: 71.0%, 77.0%
    Relative to the BoL capacity: 38.5%, 40.8%
    RMSE: 0.3853

Battery B0018.txt

    Mean and Max Absolute Error: 0.3855, 0.4122
    Mean and Max Relative Error: 82.5%, 90.7%
    Relative to the BoL capacity: 38.5%, 41.2%
    RMSE: 0.3885

Battery B0020.txt

    Mean and Max Absolute Error: 0.3830, 0.3906
    Mean and Max Relative Error: 77.4%, 81.6%
    Relative to the BoL capacity: 38.3%, 39.0%
    RMSE: 0.3830

Battery B0021.txt

    Mean and Max Absolute Error: 0.3635, 0.3860
    Mean and Max Relative Error: 76.3%, 77.8%
    Relative to the BoL capacity: 36.3%, 38.6%
    RMSE: 0.3640

Battery B0022.txt

    Mean and Max Absolute Error: 0.3421, 0.3507
    Mean and Max Relative Error: 77.6%, 81.6%
    Relative to the BoL capacity: 34.2%, 35.0%
    RMSE: 0.3422

Battery B0024.txt

    Mean and Max Absolute Error: 0.3743, 0.3949
    Mean and Max Relative Error: 79.7%, 84.6%
    Relative to the BoL capacity: 37.4%, 39.4%
    RMSE: 0.3747

