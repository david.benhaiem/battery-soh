Battery B0000.txt

    Mean and Max Absolute Error: 0.0084, 0.0187
    Mean and Max Relative Error: 0.93%, 2.14%
    Relative to the BoL capacity: 0.84%, 1.87%
    RMSE: 0.0098

Battery B0001.txt (training)

    Mean and Max Absolute Error: 0.0020, 0.0070
    Mean and Max Relative Error: 0.22%, 0.72%
    Relative to the BoL capacity: 0.20%, 0.70%
    RMSE: 0.0025

Battery B0002.txt

    Mean and Max Absolute Error: 0.0078, 0.0894
    Mean and Max Relative Error: 0.87%, 10.3%
    Relative to the BoL capacity: 0.78%, 8.94%
    RMSE: 0.0123

Battery B0003.txt

    Mean and Max Absolute Error: 0.0082, 0.0148
    Mean and Max Relative Error: 0.90%, 1.70%
    Relative to the BoL capacity: 0.82%, 1.48%
    RMSE: 0.0092

Battery B0004.txt

    Mean and Max Absolute Error: 0.0074, 0.0161
    Mean and Max Relative Error: 0.81%, 1.89%
    Relative to the BoL capacity: 0.74%, 1.61%
    RMSE: 0.0087

Battery B0005.txt

    Mean and Max Absolute Error: 0.0062, 0.1159
    Mean and Max Relative Error: 0.69%, 13.7%
    Relative to the BoL capacity: 0.62%, 11.5%
    RMSE: 0.0104

Battery B0006.txt

    Mean and Max Absolute Error: 0.0080, 0.0328
    Mean and Max Relative Error: 0.85%, 3.50%
    Relative to the BoL capacity: 0.80%, 3.28%
    RMSE: 0.0103

Battery B0007.txt

    Mean and Max Absolute Error: 0.0136, 0.0752
    Mean and Max Relative Error: 1.47%, 7.88%
    Relative to the BoL capacity: 1.36%, 7.52%
    RMSE: 0.0171

Battery B0008.txt

    Mean and Max Absolute Error: 0.0197, 0.0549
    Mean and Max Relative Error: 2.07%, 5.59%
    Relative to the BoL capacity: 1.97%, 5.49%
    RMSE: 0.0223

Battery B0009.txt

    Mean and Max Absolute Error: 0.0200, 0.0326
    Mean and Max Relative Error: 2.11%, 3.37%
    Relative to the BoL capacity: 2.00%, 3.26%
    RMSE: 0.0226

Battery B0010.txt

    Mean and Max Absolute Error: 0.0068, 0.0141
    Mean and Max Relative Error: 0.73%, 1.47%
    Relative to the BoL capacity: 0.68%, 1.41%
    RMSE: 0.0079

Battery B0011.txt

    Mean and Max Absolute Error: 0.0068, 0.0146
    Mean and Max Relative Error: 0.74%, 1.69%
    Relative to the BoL capacity: 0.68%, 1.46%
    RMSE: 0.0080

Battery B0012.txt

    Mean and Max Absolute Error: 0.0076, 0.0157
    Mean and Max Relative Error: 0.83%, 1.64%
    Relative to the BoL capacity: 0.76%, 1.57%
    RMSE: 0.0089

Battery B0013.txt

    Mean and Max Absolute Error: 0.0079, 0.0210
    Mean and Max Relative Error: 0.87%, 2.43%
    Relative to the BoL capacity: 0.79%, 2.10%
    RMSE: 0.0097

Battery B0014.txt

    Mean and Max Absolute Error: 0.0076, 0.0259
    Mean and Max Relative Error: 0.84%, 2.97%
    Relative to the BoL capacity: 0.76%, 2.59%
    RMSE: 0.0104

