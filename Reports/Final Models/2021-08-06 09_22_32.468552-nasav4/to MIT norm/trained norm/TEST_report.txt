MODEL
  Model name: LSTM
  Variant: with norm
  Version: 4
  Dataset Format: You et al. [56]
  Delta: 10
  Hidden layer dimension: 50
  Pooling_layer: bi_mean
  Dropout prob.: 0
  Number of linear layers: 1

TRAINING
  Number of Epochs: 150
  Learning rate: 5e-06
  Batch size: 1
  Epoch with smallest validation loss: 50
    (Epoch number starting at 0)
    Training loss: 1.573727604409214e-05
    Validation loss: 0.00019617773068603128
  Last epoch's loss:
    Training loss:6.389849659171887e-06
    Validation loss: 0.00021364743588492274
  Loss function: MSELoss()
  Optimizer: Adam (
Parameter Group 0
    amsgrad: False
    betas: (0.9, 0.999)
    eps: 1e-08
    lr: 5e-06
    weight_decay: 0
)

TRAINING DATASET
  Batch size: 1
  Dataset Name: Clean_MIT_Battery
  Size of the training set: 130
  Threshold (minimum capacity): 0.1
  Dimensionless variables: True
  Minimum length: 160
  Resampling time step: 2.6253445148468018
  Fixed Length: 150
  Batteries used: ['B0001.txt']
  Normalization information: (tensor([ 0.7405,  0.7375,  0.7345,  0.7314,  0.7283,  0.7252,  0.7220,  0.7188,
         0.7155,  0.7123,  0.9729,  0.9732,  0.9736,  0.9739,  0.9743,  0.9747,
         0.9750,  0.9753,  0.9757,  0.9760, 30.0000]), tensor([0.2915, 0.2933, 0.2952, 0.2970, 0.2988, 0.3006, 0.3024, 0.3041, 0.3058,
        0.3075, 0.0208, 0.0207, 0.0206, 0.0205, 0.0205, 0.0204, 0.0203, 0.0203,
        0.0202, 0.0201, 1.0000]), tensor([0.9173]), tensor([0.0424]), 'zscore')

VALIDATION DATASET
  Batch Size: 7028
  Dataset Name: Clean_MIT_Battery
  Size of the training set: 7028
  Threshold (minimum capacity): 0.1
  Dimensionless variables: True
  Minimum length: 160
  Resampling time step: 2.6253445148468018
  Fixed Length: 150
  Batteries used: ['B0000.txt', 'B0002.txt', 'B0003.txt', 'B0004.txt', 'B0005.txt', 'B0006.txt', 'B0007.txt', 'B0008.txt', 'B0009.txt', 'B0010.txt', 'B0011.txt', 'B0012.txt', 'B0013.txt', 'B0014.txt']
  Normalization information: (tensor([ 0.7405,  0.7375,  0.7345,  0.7314,  0.7283,  0.7252,  0.7220,  0.7188,
         0.7155,  0.7123,  0.9729,  0.9732,  0.9736,  0.9739,  0.9743,  0.9747,
         0.9750,  0.9753,  0.9757,  0.9760, 30.0000]), tensor([0.2915, 0.2933, 0.2952, 0.2970, 0.2988, 0.3006, 0.3024, 0.3041, 0.3058,
        0.3075, 0.0208, 0.0207, 0.0206, 0.0205, 0.0205, 0.0204, 0.0203, 0.0203,
        0.0202, 0.0201, 1.0000]), tensor([0.9173]), tensor([0.0424]), 'zscore')
