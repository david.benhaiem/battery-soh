Battery B0000.txt

    Mean and Max Absolute Error: 0.0076, 0.0152
    Mean and Max Relative Error: 0.83%, 1.60%
    Relative to the BoL capacity: 0.76%, 1.52%
    RMSE: 0.0085

Battery B0001.txt (training)

    Mean and Max Absolute Error: 0.0037, 0.0157
    Mean and Max Relative Error: 0.40%, 1.63%
    Relative to the BoL capacity: 0.37%, 1.57%
    RMSE: 0.0054

Battery B0002.txt

    Mean and Max Absolute Error: 0.0117, 0.0857
    Mean and Max Relative Error: 1.26%, 9.87%
    Relative to the BoL capacity: 1.17%, 8.57%
    RMSE: 0.0142

Battery B0003.txt

    Mean and Max Absolute Error: 0.0062, 0.0111
    Mean and Max Relative Error: 0.68%, 1.17%
    Relative to the BoL capacity: 0.62%, 1.11%
    RMSE: 0.0069

Battery B0004.txt

    Mean and Max Absolute Error: 0.0077, 0.0160
    Mean and Max Relative Error: 0.83%, 1.66%
    Relative to the BoL capacity: 0.77%, 1.60%
    RMSE: 0.0089

Battery B0005.txt

    Mean and Max Absolute Error: 0.0084, 0.1161
    Mean and Max Relative Error: 0.91%, 13.8%
    Relative to the BoL capacity: 0.84%, 11.6%
    RMSE: 0.0112

Battery B0006.txt

    Mean and Max Absolute Error: 0.0075, 0.0366
    Mean and Max Relative Error: 0.81%, 3.92%
    Relative to the BoL capacity: 0.75%, 3.66%
    RMSE: 0.0092

Battery B0007.txt

    Mean and Max Absolute Error: 0.0124, 0.0764
    Mean and Max Relative Error: 1.35%, 8.00%
    Relative to the BoL capacity: 1.24%, 7.64%
    RMSE: 0.0149

Battery B0008.txt

    Mean and Max Absolute Error: 0.0163, 0.0341
    Mean and Max Relative Error: 1.74%, 3.54%
    Relative to the BoL capacity: 1.63%, 3.41%
    RMSE: 0.0179

Battery B0009.txt

    Mean and Max Absolute Error: 0.0160, 0.0253
    Mean and Max Relative Error: 1.71%, 2.67%
    Relative to the BoL capacity: 1.60%, 2.53%
    RMSE: 0.0176

Battery B0010.txt

    Mean and Max Absolute Error: 0.0073, 0.0147
    Mean and Max Relative Error: 0.78%, 1.53%
    Relative to the BoL capacity: 0.73%, 1.47%
    RMSE: 0.0083

Battery B0011.txt

    Mean and Max Absolute Error: 0.0081, 0.0163
    Mean and Max Relative Error: 0.87%, 1.70%
    Relative to the BoL capacity: 0.81%, 1.63%
    RMSE: 0.0093

Battery B0012.txt

    Mean and Max Absolute Error: 0.0075, 0.0428
    Mean and Max Relative Error: 0.81%, 4.44%
    Relative to the BoL capacity: 0.75%, 4.28%
    RMSE: 0.0087

Battery B0013.txt

    Mean and Max Absolute Error: 0.0093, 0.0228
    Mean and Max Relative Error: 1.00%, 2.37%
    Relative to the BoL capacity: 0.93%, 2.28%
    RMSE: 0.0115

Battery B0014.txt

    Mean and Max Absolute Error: 0.0130, 0.0225
    Mean and Max Relative Error: 1.38%, 2.34%
    Relative to the BoL capacity: 1.30%, 2.25%
    RMSE: 0.0150

