Battery B0001.txt (training)

    Mean and Max Absolute Error: 0.0528, 0.1197
    Mean and Max Relative Error: 11.5%, 31.8%
    Relative to the BoL capacity: 5.28%, 11.9%
    RMSE: 0.0673

Battery B0002.txt

    Mean and Max Absolute Error: 0.0193, 0.0647
    Mean and Max Relative Error: 2.81%, 9.11%
    Relative to the BoL capacity: 1.93%, 6.47%
    RMSE: 0.0259

Battery B0003.txt

    Mean and Max Absolute Error: 0.0517, 0.0876
    Mean and Max Relative Error: 7.42%, 12.2%
    Relative to the BoL capacity: 5.17%, 8.76%
    RMSE: 0.0560

Battery B0004.txt (training)

    Mean and Max Absolute Error: 0.2034, 0.4324
    Mean and Max Relative Error: 51.6%, 177.%
    Relative to the BoL capacity: 20.3%, 43.2%
    RMSE: 0.2220

Battery B0005.txt

    Mean and Max Absolute Error: 0.0963, 0.2006
    Mean and Max Relative Error: 17.1%, 56.2%
    Relative to the BoL capacity: 9.63%, 20.0%
    RMSE: 0.1008

Battery B0006.txt

    Mean and Max Absolute Error: 0.0492, 0.0856
    Mean and Max Relative Error: 8.10%, 17.3%
    Relative to the BoL capacity: 4.92%, 8.56%
    RMSE: 0.0542

Battery B0009.txt

    Mean and Max Absolute Error: 0.0852, 0.1100
    Mean and Max Relative Error: 12.5%, 15.6%
    Relative to the BoL capacity: 8.52%, 11.0%
    RMSE: 0.0872

Battery B0010.txt

    Mean and Max Absolute Error: 0.0706, 0.1156
    Mean and Max Relative Error: 10.3%, 16.3%
    Relative to the BoL capacity: 7.06%, 11.5%
    RMSE: 0.0764

Battery B0011.txt

    Mean and Max Absolute Error: 0.0877, 0.1073
    Mean and Max Relative Error: 12.6%, 15.3%
    Relative to the BoL capacity: 8.77%, 10.7%
    RMSE: 0.0893

Battery B0012.txt

    Mean and Max Absolute Error: 0.0430, 0.0721
    Mean and Max Relative Error: 12.1%, 19.1%
    Relative to the BoL capacity: 4.30%, 7.21%
    RMSE: 0.0509

Battery B0013.txt

    Mean and Max Absolute Error: 0.0338, 0.1016
    Mean and Max Relative Error: 9.25%, 35.2%
    Relative to the BoL capacity: 3.38%, 10.1%
    RMSE: 0.0468

Battery B0014.txt (training)

    Mean and Max Absolute Error: 0.0176, 0.0467
    Mean and Max Relative Error: 5.82%, 12.0%
    Relative to the BoL capacity: 1.76%, 4.67%
    RMSE: 0.0211

Battery B0015.txt

    Mean and Max Absolute Error: 0.0700, 0.2470
    Mean and Max Relative Error: 13.1%, 40.4%
    Relative to the BoL capacity: 7.00%, 24.7%
    RMSE: 0.0888

Battery B0016.txt (training)

    Mean and Max Absolute Error: 0.0372, 0.1925
    Mean and Max Relative Error: 12.5%, 104.%
    Relative to the BoL capacity: 3.72%, 19.2%
    RMSE: 0.0615

Battery B0017.txt

    Mean and Max Absolute Error: 0.0259, 0.0871
    Mean and Max Relative Error: 5.53%, 22.2%
    Relative to the BoL capacity: 2.59%, 8.71%
    RMSE: 0.0342

Battery B0018.txt

    Mean and Max Absolute Error: 0.0680, 0.2414
    Mean and Max Relative Error: 34.9%, 204.%
    Relative to the BoL capacity: 6.80%, 24.1%
    RMSE: 0.1009

Battery B0020.txt

    Mean and Max Absolute Error: 0.0395, 0.0655
    Mean and Max Relative Error: 7.20%, 11.7%
    Relative to the BoL capacity: 3.95%, 6.55%
    RMSE: 0.0445

Battery B0021.txt

    Mean and Max Absolute Error: 0.0586, 0.1171
    Mean and Max Relative Error: 11.0%, 21.5%
    Relative to the BoL capacity: 5.86%, 11.7%
    RMSE: 0.0604

Battery B0022.txt

    Mean and Max Absolute Error: 0.0547, 0.0867
    Mean and Max Relative Error: 10.4%, 15.8%
    Relative to the BoL capacity: 5.47%, 8.67%
    RMSE: 0.0622

Battery B0024.txt

    Mean and Max Absolute Error: 0.0506, 0.0877
    Mean and Max Relative Error: 11.9%, 26.9%
    Relative to the BoL capacity: 5.06%, 8.77%
    RMSE: 0.0549

