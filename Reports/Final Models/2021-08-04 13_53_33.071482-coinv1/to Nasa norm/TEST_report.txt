MODEL
  Model name: LSTM
  Variant: with norm
  Version: 1
  Dataset Format: You et al. [56]
  Delta: 10
  Hidden layer dimension: 50
  Pooling_layer: bi_mean
  Dropout prob.: 0
  Number of linear layers: 1

TRAINING
  Number of Epochs: 1
  Learning rate: 1e-100
  Batch size: 1
  Epoch with smallest validation loss: 0
    (Epoch number starting at 0)
    Training loss: 0.01662037894129753
    Validation loss: 0.01966569945216179
  Last epoch's loss:
    Training loss:0.01662037894129753
    Validation loss: 0.01966569945216179
  Loss function: MSELoss()
  Optimizer: Adam (
Parameter Group 0
    amsgrad: False
    betas: (0.9, 0.999)
    eps: 1e-08
    lr: 1e-100
    weight_decay: 0
)

TRAINING DATASET
  Batch size: 1
  Dataset Name: Clean_Battery
  Size of the training set: 483
  Threshold (minimum capacity): 0.1
  Dimensionless variables: True
  Minimum length: 160
  Resampling time step: 31.23419761657715
  Fixed Length: 150
  Batteries used: ['B0006.txt', 'B0030.txt', 'B0038.txt', 'B0039.txt', 'B0041.txt', 'B0045.txt', 'B0055.txt']
  Normalization information: (tensor([0.4417, 0.4377, 0.4338, 0.4299, 0.4259, 0.4221, 0.4183, 0.4145, 0.4107,
        0.4069, 0.9869, 0.9876, 0.9881, 0.9886, 0.9890, 0.9894, 0.9898, 0.9901,
        0.9905, 0.9908]), tensor([0.2395, 0.2398, 0.2400, 0.2402, 0.2404, 0.2406, 0.2408, 0.2410, 0.2411,
        0.2413, 0.0267, 0.0249, 0.0238, 0.0230, 0.0223, 0.0217, 0.0212, 0.0207,
        0.0203, 0.0199]), tensor([0.6211]), tensor([0.1952]), 'zscore')

VALIDATION DATASET
  Batch Size: 1938
  Dataset Name: Clean_Battery
  Size of the training set: 1938
  Threshold (minimum capacity): 0.1
  Dimensionless variables: True
  Minimum length: 160
  Resampling time step: 31.23419761657715
  Fixed Length: 150
  Batteries used: ['B0005.txt', 'B0007.txt', 'B0018.txt', 'B0025.txt', 'B0026.txt', 'B0027.txt', 'B0028.txt', 'B0029.txt', 'B0031.txt', 'B0032.txt', 'B0033.txt', 'B0034.txt', 'B0036.txt', 'B0040.txt', 'B0042.txt', 'B0043.txt', 'B0044.txt', 'B0046.txt', 'B0047.txt', 'B0048.txt', 'B0053.txt', 'B0054.txt', 'B0056.txt']
  Normalization information: (tensor([0.4417, 0.4377, 0.4338, 0.4299, 0.4259, 0.4221, 0.4183, 0.4145, 0.4107,
        0.4069, 0.9869, 0.9876, 0.9881, 0.9886, 0.9890, 0.9894, 0.9898, 0.9901,
        0.9905, 0.9908]), tensor([0.2395, 0.2398, 0.2400, 0.2402, 0.2404, 0.2406, 0.2408, 0.2410, 0.2411,
        0.2413, 0.0267, 0.0249, 0.0238, 0.0230, 0.0223, 0.0217, 0.0212, 0.0207,
        0.0203, 0.0199]), tensor([0.6211]), tensor([0.1952]), 'zscore')
