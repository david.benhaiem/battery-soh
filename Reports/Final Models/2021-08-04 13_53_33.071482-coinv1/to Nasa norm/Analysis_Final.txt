Battery B0005.txt

    Mean and Max Absolute Error: 0.1316, 0.1619
    Mean and Max Relative Error: 21.3%, 24.1%
    Relative to the BoL capacity: 13.1%, 16.1%
    RMSE: 0.1324

Battery B0006.txt (training)

    Mean and Max Absolute Error: 0.1539, 0.2114
    Mean and Max Relative Error: 26.6%, 38.6%
    Relative to the BoL capacity: 15.3%, 21.1%
    RMSE: 0.1552

Battery B0007.txt

    Mean and Max Absolute Error: 0.1351, 0.1692
    Mean and Max Relative Error: 20.8%, 23.1%
    Relative to the BoL capacity: 13.5%, 16.9%
    RMSE: 0.1359

Battery B0018.txt

    Mean and Max Absolute Error: 0.1134, 0.1631
    Mean and Max Relative Error: 17.9%, 22.6%
    Relative to the BoL capacity: 11.3%, 16.3%
    RMSE: 0.1154

Battery B0025.txt

    Mean and Max Absolute Error: 0.1636, 0.1811
    Mean and Max Relative Error: 23.3%, 25.9%
    Relative to the BoL capacity: 16.3%, 18.1%
    RMSE: 0.1638

Battery B0026.txt

    Mean and Max Absolute Error: 0.1532, 0.1672
    Mean and Max Relative Error: 21.7%, 24.0%
    Relative to the BoL capacity: 15.3%, 16.7%
    RMSE: 0.1535

Battery B0027.txt

    Mean and Max Absolute Error: 0.2221, 0.2462
    Mean and Max Relative Error: 34.8%, 39.8%
    Relative to the BoL capacity: 22.2%, 24.6%
    RMSE: 0.2224

Battery B0028.txt

    Mean and Max Absolute Error: 0.1359, 0.1513
    Mean and Max Relative Error: 19.2%, 21.3%
    Relative to the BoL capacity: 13.5%, 15.1%
    RMSE: 0.1362

Battery B0029.txt

    Mean and Max Absolute Error: 0.0835, 0.1041
    Mean and Max Relative Error: 11.2%, 13.4%
    Relative to the BoL capacity: 8.35%, 10.4%
    RMSE: 0.0844

Battery B0030.txt (training)

    Mean and Max Absolute Error: 0.0606, 0.0845
    Mean and Max Relative Error: 8.27%, 11.0%
    Relative to the BoL capacity: 6.06%, 8.45%
    RMSE: 0.0619

Battery B0031.txt

    Mean and Max Absolute Error: 0.0873, 0.1078
    Mean and Max Relative Error: 11.7%, 14.1%
    Relative to the BoL capacity: 8.73%, 10.7%
    RMSE: 0.0883

Battery B0032.txt

    Mean and Max Absolute Error: 0.1020, 0.1277
    Mean and Max Relative Error: 13.9%, 16.5%
    Relative to the BoL capacity: 10.2%, 12.7%
    RMSE: 0.1033

Battery B0033.txt

    Mean and Max Absolute Error: 0.1547, 0.2213
    Mean and Max Relative Error: 28.5%, 40.5%
    Relative to the BoL capacity: 15.4%, 22.1%
    RMSE: 0.1553

Battery B0034.txt

    Mean and Max Absolute Error: 0.1076, 0.1602
    Mean and Max Relative Error: 19.5%, 26.6%
    Relative to the BoL capacity: 10.7%, 16.0%
    RMSE: 0.1084

Battery B0036.txt

    Mean and Max Absolute Error: 0.1580, 0.1945
    Mean and Max Relative Error: 24.2%, 29.5%
    Relative to the BoL capacity: 15.8%, 19.4%
    RMSE: 0.1584

Battery B0038.txt (training)

    Mean and Max Absolute Error: 0.1468, 0.1890
    Mean and Max Relative Error: 22.9%, 28.7%
    Relative to the BoL capacity: 14.6%, 18.9%
    RMSE: 0.1587

Battery B0039.txt (training)

    Mean and Max Absolute Error: 0.1729, 0.3284
    Mean and Max Relative Error: 30.6%, 85.2%
    Relative to the BoL capacity: 17.2%, 32.8%
    RMSE: 0.1752

Battery B0040.txt

    Mean and Max Absolute Error: 0.1187, 0.1383
    Mean and Max Relative Error: 18.9%, 29.2%
    Relative to the BoL capacity: 11.8%, 13.8%
    RMSE: 0.1214

Battery B0041.txt (training)

    Mean and Max Absolute Error: 0.0610, 0.1974
    Mean and Max Relative Error: 15.7%, 51.7%
    Relative to the BoL capacity: 6.10%, 19.7%
    RMSE: 0.0806

Battery B0042.txt

    Mean and Max Absolute Error: 0.1592, 0.2677
    Mean and Max Relative Error: 30.3%, 62.8%
    Relative to the BoL capacity: 15.9%, 26.7%
    RMSE: 0.1705

Battery B0043.txt

    Mean and Max Absolute Error: 0.1650, 0.2725
    Mean and Max Relative Error: 32.9%, 63.4%
    Relative to the BoL capacity: 16.5%, 27.2%
    RMSE: 0.1764

Battery B0044.txt

    Mean and Max Absolute Error: 0.1450, 0.2513
    Mean and Max Relative Error: 28.4%, 55.5%
    Relative to the BoL capacity: 14.5%, 25.1%
    RMSE: 0.1550

Battery B0045.txt (training)

    Mean and Max Absolute Error: 0.0689, 0.1147
    Mean and Max Relative Error: 17.1%, 28.4%
    Relative to the BoL capacity: 6.89%, 11.4%
    RMSE: 0.0755

Battery B0046.txt

    Mean and Max Absolute Error: 0.1786, 0.2575
    Mean and Max Relative Error: 41.5%, 58.8%
    Relative to the BoL capacity: 17.8%, 25.7%
    RMSE: 0.1828

Battery B0047.txt

    Mean and Max Absolute Error: 0.1939, 0.2658
    Mean and Max Relative Error: 48.2%, 63.4%
    Relative to the BoL capacity: 19.3%, 26.5%
    RMSE: 0.1968

Battery B0048.txt

    Mean and Max Absolute Error: 0.1729, 0.2481
    Mean and Max Relative Error: 38.9%, 56.0%
    Relative to the BoL capacity: 17.2%, 24.8%
    RMSE: 0.1764

Battery B0053.txt

    Mean and Max Absolute Error: 0.0845, 0.1364
    Mean and Max Relative Error: 20.4%, 34.6%
    Relative to the BoL capacity: 8.45%, 13.6%
    RMSE: 0.0867

Battery B0054.txt

    Mean and Max Absolute Error: 0.0341, 0.1246
    Mean and Max Relative Error: 8.09%, 31.0%
    Relative to the BoL capacity: 3.41%, 12.4%
    RMSE: 0.0445

Battery B0055.txt (training)

    Mean and Max Absolute Error: 0.0930, 0.1859
    Mean and Max Relative Error: 22.4%, 47.7%
    Relative to the BoL capacity: 9.30%, 18.5%
    RMSE: 0.0969

Battery B0056.txt

    Mean and Max Absolute Error: 0.1121, 0.1909
    Mean and Max Relative Error: 24.9%, 43.7%
    Relative to the BoL capacity: 11.2%, 19.0%
    RMSE: 0.1148

