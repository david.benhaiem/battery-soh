Battery B0001.txt (training)

    Mean and Max Absolute Error: 0.0271, 0.0719
    Mean and Max Relative Error: 7.62%, 24.0%
    Relative to the BoL capacity: 2.71%, 7.19%
    RMSE: 0.0352

Battery B0002.txt

    Mean and Max Absolute Error: 0.0374, 0.1074
    Mean and Max Relative Error: 5.58%, 17.1%
    Relative to the BoL capacity: 3.74%, 10.7%
    RMSE: 0.0424

Battery B0003.txt

    Mean and Max Absolute Error: 0.0237, 0.0716
    Mean and Max Relative Error: 3.24%, 10.3%
    Relative to the BoL capacity: 2.37%, 7.16%
    RMSE: 0.0262

Battery B0004.txt (training)

    Mean and Max Absolute Error: 0.1796, 0.4706
    Mean and Max Relative Error: 27.0%, 69.5%
    Relative to the BoL capacity: 17.9%, 47.0%
    RMSE: 0.2244

Battery B0005.txt

    Mean and Max Absolute Error: 0.0816, 0.2611
    Mean and Max Relative Error: 12.5%, 46.8%
    Relative to the BoL capacity: 8.16%, 26.1%
    RMSE: 0.1001

Battery B0006.txt

    Mean and Max Absolute Error: 0.0213, 0.0605
    Mean and Max Relative Error: 3.81%, 12.2%
    Relative to the BoL capacity: 2.13%, 6.05%
    RMSE: 0.0270

Battery B0009.txt

    Mean and Max Absolute Error: 0.0326, 0.0506
    Mean and Max Relative Error: 4.30%, 7.09%
    Relative to the BoL capacity: 3.26%, 5.06%
    RMSE: 0.0353

Battery B0010.txt

    Mean and Max Absolute Error: 0.0263, 0.0773
    Mean and Max Relative Error: 3.56%, 11.9%
    Relative to the BoL capacity: 2.63%, 7.73%
    RMSE: 0.0319

Battery B0011.txt

    Mean and Max Absolute Error: 0.0208, 0.0441
    Mean and Max Relative Error: 2.64%, 6.06%
    Relative to the BoL capacity: 2.08%, 4.41%
    RMSE: 0.0242

Battery B0012.txt

    Mean and Max Absolute Error: 0.0553, 0.1256
    Mean and Max Relative Error: 22.3%, 64.7%
    Relative to the BoL capacity: 5.53%, 12.5%
    RMSE: 0.0711

Battery B0013.txt

    Mean and Max Absolute Error: 0.0252, 0.0559
    Mean and Max Relative Error: 4.76%, 9.42%
    Relative to the BoL capacity: 2.52%, 5.59%
    RMSE: 0.0304

Battery B0014.txt (training)

    Mean and Max Absolute Error: 0.0802, 0.1230
    Mean and Max Relative Error: 29.9%, 55.9%
    Relative to the BoL capacity: 8.02%, 12.3%
    RMSE: 0.0897

Battery B0015.txt

    Mean and Max Absolute Error: 0.0594, 0.1941
    Mean and Max Relative Error: 12.6%, 53.2%
    Relative to the BoL capacity: 5.94%, 19.4%
    RMSE: 0.0684

Battery B0016.txt (training)

    Mean and Max Absolute Error: 0.0427, 0.1016
    Mean and Max Relative Error: 8.59%, 27.9%
    Relative to the BoL capacity: 4.27%, 10.1%
    RMSE: 0.0462

Battery B0017.txt

    Mean and Max Absolute Error: 0.0425, 0.0634
    Mean and Max Relative Error: 7.82%, 10.5%
    Relative to the BoL capacity: 4.25%, 6.34%
    RMSE: 0.0429

Battery B0018.txt

    Mean and Max Absolute Error: 0.0202, 0.0613
    Mean and Max Relative Error: 4.85%, 17.4%
    Relative to the BoL capacity: 2.02%, 6.13%
    RMSE: 0.0267

Battery B0020.txt

    Mean and Max Absolute Error: 0.0304, 0.0487
    Mean and Max Relative Error: 6.12%, 9.89%
    Relative to the BoL capacity: 3.04%, 4.87%
    RMSE: 0.0322

Battery B0021.txt

    Mean and Max Absolute Error: 0.0477, 0.1021
    Mean and Max Relative Error: 10.2%, 23.9%
    Relative to the BoL capacity: 4.77%, 10.2%
    RMSE: 0.0507

Battery B0022.txt

    Mean and Max Absolute Error: 0.0614, 0.0706
    Mean and Max Relative Error: 13.9%, 15.3%
    Relative to the BoL capacity: 6.14%, 7.06%
    RMSE: 0.0617

Battery B0024.txt

    Mean and Max Absolute Error: 0.0209, 0.0613
    Mean and Max Relative Error: 4.41%, 15.3%
    Relative to the BoL capacity: 2.09%, 6.13%
    RMSE: 0.0264

