from MIT_Classes import *
from torch.utils.data import Dataset, DataLoader
from scipy.stats import kurtosis, skew
from sklearn.cluster import KMeans
from Training import *
import re
import scipy.integrate as integrate

"""DEPRECATED

This module is an early implementation of the kmeans method used by YOU et al.
(https://www.sciencedirect.com/science/article/abs/pii/S0306261916306456)
which was discontinued.
"""

class FFNN_v1(nn.Module):
    """Feed Forward Neural Network for Battery SOH estimation.

    This Neural net is based on the one proposed by You et al.'s* history-based
    approach. See note for full reference.

    The architecture is composed of:
        - A linear layer that takes inputs of shape [batch size, Nb_clusters],
        where Nb_clusters is the number of clusters used in the k-means
        clustering method for dataset creation, and outputs a a tensor of shape
        [batch size, hidden_dim].
            Activation function: Leaky ReLU

        - An optional dropout layer, the probability of which is set through the
         variable `drop_out`.

        - A final linear layer that takes inputs of shape
        [batch size, hidden_dim] and outputs a tensor of shape
        [batch size, output_dim].
            output_dim = 1
            Activation function: Leaky ReLU

    .. note::
        * Full reference: Gae-won You, Sangdo Park, Dukjin Oh, Real-time
        state-of-health estimation for electric vehicle batteries: A data-driven
        approach, Applied Energy, Volume 176, 2016, Pages 92-103,
        ISSN 0306-2619, <https://doi.org/10.1016/j.apenergy.2016.05.051>.

    Parameters
    ----------
    Nb_clusters : int
        The number of k-means clusters used for the dataset creation.
    hidden_dim : int
        The number of hidden nodes. [int]
    output_dim : int
        The number of outputs. [int]
    drop_out : float
        The probability of the Dropout layer. {between 0 and 1} [float]
    norm : tuple
        The normalization information used during the training. [tuple]

        Check function "Normalize" in Cleaning.py for more information.
        This isn't a necessary parameter and is not used in the model in any
        way. It's just useful for documentation purposes and for normalizing
        test and validation sets.

    mean_hist : bool
        Whether or not the "histograms" (inputs) were normalized according to
        the total number of points.

        This is different than the regular input normalization process done by
        the "Normalize" function. Check Torch_Dataset_KMEANS (below) for more.

        This parameter is also not used in the model and isn't necessary. Like
        `norm`, it's just for documentation purposes.
    """
    def __init__(self, Nb_clusters, hidden_dim, output_dim=1,
                 drop_out=0, norm=None, mean_hist=None):

        super().__init__()

        # Model information:
        self.model_name = 'FFNN'                    # The kind of model
        self.version = 1                            # The kind of model
        self.dataset_format = 'You et al. [51]'     # The dataset format it corresponds to
        # Normalization information:
        self.norm = norm                            # The kind of normalization used for the data and its parameters
        self.mean_hist = mean_hist

        # Important dimensions
        self.nb_clusters = Nb_clusters
        self.hidden_dim = hidden_dim
        self.output_dim = output_dim

        # The layers
        self.lin1 = nn.Linear(Nb_clusters, hidden_dim)
        self.lin2 = nn.Linear(hidden_dim, output_dim)
        self.dropout = nn.Dropout(p=drop_out)
        self.act_fn = nn.LeakyReLU()

        self.Model = nn.Sequential(
            self.lin1,
            self.act_fn,
            self.dropout,
            self.lin2,
            self.act_fn
        )

    def forward(self, inputs):

        # Linear layers
        output = self.Model(inputs)

        return output

    def report(self):
        """Creates a little report with all of the information about the class.

        Returns
        -------
            A string with the report information.
        """

        string = "  Model name: " + self.model_name + '\n' + \
            "  Version: " + str(self.version) + '\n' + \
            "  Dataset Format: " + self.dataset_format + '\n' + \
            "  Number of inputs: " + str(self.nb_clusters) + '\n' + \
            "  Hidden layer dimension: " + str(self.hidden_dim) + '\n' + \
            "  Dropout prob.: " + str(self.dropout.p) + '\n'

        return string

class FFNN_Sev(nn.Module):
    """A simple feedforward neural network for predicting battery cycle life.

    This is based on the article by Severson et al., full reference in the note.
    This uses Leaky ReLU as an activation function.

    .. note::
        Full reference: Severson, K.A., Attia, P.M., Jin, N. et al.
        Data-driven prediction of battery cycle life before capacity
        degradation. Nat Energy 4, 383–391 (2019).
        <https://doi.org/10.1038/s41560-019-0356-8>

    Parameters
    ----------
    input_dim : int
        The number of inputs.

        For the "Variance" model proposed in the article, input_dim should be 1,
        for the "Discharge" model, it should be 6 and for the "Full" model it
        should be 9.

    hidden_dim : int
        The size of the hidden layers (when num_layers >= 1).
        All hidden layers have the same size.
    output_dim : int, default = 1
        The number of output dimensions.
    drop_out : float, default = 0
        The dropout probability. The dropout layer is always after the first
        hidden layer.
    version : int, default = 1
        The model version. For the moment, this makes no difference.
    num_layers : int, default = 0
        The number of hidden linear layers.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.

        This isn't a necessary parameter and is not used in the model in any
        way. It's just useful for documentation purposes and for normalizing
        test and validation sets.
    """
    def __init__(self, input_dim, hidden_dim, output_dim=1, drop_out=0,
                 version=1, num_layers=0, norm=None):

        super().__init__()

        # Model information:
        self.model_name = 'FFNN'                    # The kind of model
        self.version = version                      # The model's version
        self.dataset_format = 'Severson et al.'     # The dataset format it
                                                    # corresponds to
        # Normalization information:
        self.norm = norm                            # The kind of normalization used for the data and its parameters

        # Important dimensions
        self.inputs_dim = input_dim
        self.hidden_dim = hidden_dim
        self.output_dim = output_dim

        # The layers
        self.num_layers = num_layers
        self.act_fn = nn.LeakyReLU()
        self.dropout = nn.Dropout(p=drop_out)

        if num_layers == 0:
            self.Model = nn.Sequential(
                nn.Linear(input_dim, output_dim),
            )

        elif num_layers == 1:
            self.lin1 = nn.Linear(input_dim, hidden_dim)
            self.lin2 = nn.Linear(hidden_dim, output_dim)

            self.Model = nn.Sequential(
                self.lin1,
                self.act_fn,
                self.dropout,
                self.lin2,
            )

        else:
            self.lin1 = nn.Linear(input_dim, hidden_dim)
            self.lin2 = nn.Linear(hidden_dim, output_dim)
            lin_list = []

            for i in range(num_layers-1):
                lin_list.append(nn.Linear(hidden_dim, hidden_dim))
                lin_list.append(self.act_fn)

            self.Model = nn.Sequential(
                self.lin1,
                self.act_fn,
                self.dropout,
                *lin_list,
                self.lin2,
            )


    def forward(self, inputs):

        # Linear layers
        output = self.Model(inputs)

        return output

    def report(self):
        """Creates a little report with all of the information about the class.

        Returns
        -------
            A string with the report information.
        """

        string = "  Model name: " + self.model_name + '\n' + \
            "  Version: " + str(self.version) + '\n' + \
            "  Dataset Format: " + self.dataset_format + '\n' + \
            "  Number of inputs: " + str(self.inputs_dim) + '\n' + \
            "  Hidden layer dimension: " + str(self.hidden_dim) + '\n' + \
            "  Dropout prob.: " + str(self.dropout.p) + '\n' + \
            "  Number of linear layers: " + str(self.num_layers) + '\n'

        return string

class Torch_Dataset_KMEANS(Dataset):
    """ Creates a pytorch Dataset class that can readily be loaded with pytorch's Loader class
    for training and validation.

    The datasets created by this class' initialization follow You et al.'s * history-based approach.
    In it, the k-means method is employed to distribute (I, V, T) points into clusters. These points
    come from both charge (I > 0) and discharge curves (I < 0). A Feed Forward Neural Network (FFNN)
    is used for battery SOH estimation. As input, the network receives the amount of (I, V, T) points
    in each cluster (input size = number of clusters) since the battery's Beginning of Life (BoL).
    This cumulative property of the inputs is why this approach is called "history-based".
    In the article, the authors use a FFNN with one hidden layer with as many neurons as the input layer
    (which is the same number as the number of clusters = 80 in their case). They've also used dropout
    for regularization and in order to avoid overfitting.
        * Full reference: Gae-won You, Sangdo Park, Dukjin Oh, Real-time state-of-health estimation for
        electric vehicle batteries: A data-driven approach, Applied Energy, Volume 176, 2016, Pages 92-103,
        ISSN 0306-2619, https://doi.org/10.1016/j.apenergy.2016.05.051.

    This class was made to treat Dataset_Nasa classes and for the FFNN class FFNN_v1 in FFNN.py.
        The network's inputs are the number of points in each cluster, and its output is the battery's
        capacity in A.h.

    Some attributes:
        self.dataset_name   : the name of the dataset (Dataset_Nasa). [string]
        self.dataset_format : a string indicating the type of format used: 'You et al. [51]'. [string]
        self.inputs         : a list with the XT tensors for each cycle. [list of pytorch tensors]
        self.output         : a list with the output capacity tensors for each cycle. [list of pytorch tensors]

    Inputs:
        Battery_list        : list with the Batteries that we want to use for creating the dataset.
                              [list of Dataset_Nasa objects]
        Nb_clusters         : number of clusters for the k-means clustering algorithm. [int]
        kmeans              : The kmeans class if the method was done before-hand (recommended).
                              [sklearn.cluster._kmeans.KMeans object]
        print_bool          : if we want to print some information while the code runs. [bool]
        resampling          : whether or not to resample the data or to use it as is. [bool]
                                Namely, if we want to print the battery's name and the total number of points.
        time_step           : the desired time step for the resampling procedure. [int]
        i_cutoff            : the current cutoff at the end of the charging procedure. [float]
        threshold           : the minimum value of capacity to be taken into account. [float]
                                Values under the threshold will be ignored during the construction
                                of the dataset.
        z_lim               : the limit on the capacity outlier search. [float]
                                If the value of abs(standardized capacity) > z_lim, then the point is
                                considered as an outlier. This will be improved in the future.
        min_len             : minimum sequence length for the cycle to be taken into account. [int]
                                Must be greater or equal to 1.
        fixed_len           : if different than `None` (default value), then the charge and discharge curves
                              will be limited to `fixed_len` points. [int]
                                It makes more sense to use it with resampling = True, but this may come with
                                trouble if the discharge curves are not of CC type. This is something to be
                                improved in both the resampling and data cleaning part.
                                    This will override `min_len`.
        norm                : normalization information to be used for dataset information. [tuple]
                                To find out more, check function "Normalize" in Cleaning.py.
        norm_type           : the type of normalization to be done on the data. [string]
                                As of now, can either be 'zscore' or 'minmax'.
                                    This will be overridden if `norm` is initialized.
        mean_hist           : if the inputs should be normalized by count. [bool]
                                The inputs consist in a vector of length Nb_clusters. Each entry has the number
                                of points in each cluster (integers). If mean_hist = True, then each entry will
                                be divided by the total amount of points. If we don't do this, the neural net
                                might just correlate battery ageing with bigger numbers, and not cluster distribution,
                                which is what we want.
                                    This is something done before the so-called normalization.
        path, transform     : necessary arguments for Pytorch's Dataset class but that are unused. [None]
    """
    def __init__(self, Battery_list, Nb_clusters, kmeans=None, print_bool=False,
                 resampling=False, time_step=30, i_cutoff=2e-2, threshold=0.1,
                 z_lim=3, min_len=10, fixed_len=None,  # cleaning
                 norm=None, norm_type='zscore', mean_hist=False,    # normalization
                 path=None, transform=None,                         # Just necessary arguments for pytorch's Dataset class
                 ):

        super().__init__()

        if isinstance(kmeans, type(None)):
            assert kmeans is not None, 'No kmeans informed'
        else:
            Informed_Nb_clusters = Nb_clusters
            Nb_clusters = kmeans.cluster_centers_.shape[0]
            if Informed_Nb_clusters != Nb_clusters:
                print('The informed number of clusters (', Informed_Nb_clusters,
                      ') was incorrect. ', Nb_clusters, ' will be used.')

        if fixed_len is not None:
            min_len = fixed_len

        # Dataset related
        self.dataset_name = type(Battery_list[0]).__name__      # Basically the dataset used (Nasa,...)
        self.batteries = []
        for Battery in Battery_list:                            # The batteries used in this dataset creation
            self.batteries.append(Battery.Name+'.txt')
        self.dataset_format = 'You et al. [51]'                 # The format of the torch dataset (related to the net)
        # KMeans related
        self.nb_clusters = Nb_clusters                          # The number of kmeans clusters
        self.kmeans = kmeans                                    # The actual kmeans class
        # Cleaning
        self.resampling = resampling                            # Whether resampling has been done or not
        self.time_step = time_step                              # The time step chosen for resampling
        self.i_cutoff = i_cutoff                                # The current cut-off chosen for cleaning
        self.threshold = threshold                              # The threshold chosen for cleaning
        self.z_lim = z_lim                                      # Maximum z-score value to take a point into account
        self.min_len = min_len                                  # Minimum cycle length required to take it into account
        self.fixed_len = fixed_len                              # The fixed length used.
        # Normalization
        self.mean_hist = mean_hist                              # Whether histogram normalization has been done or not
        # the rest of the normalization information is saved at the end

        ent = []                                    # Input list
        sai = []                                    # Output list

        if self.dataset_name == 'Clean_Coin_Battery':
            new_battery_list = []
            for Battery in Battery_list:
                for cell in Battery.cell:
                    new_battery_list.append(cell)

            Battery_list = new_battery_list

        for Battery in Battery_list:
            # History of the inputs for this battery:
            cumulative_input = th.zeros(1, Nb_clusters)
            if print_bool:
                print(Battery.Name)
            for cycle_couple in Battery.cycle_couple:
                chr_cycle = cycle_couple[0]
                dis_cycle = cycle_couple[1]

                if resampling:
                    _, Cycle_i, Cycle_v, Cycle_T = Resampling_v3(chr_cycle, time_step=time_step, min_len=min_len)
                else:
                    # Numpy arrays
                    Cycle_i = chr_cycle.data.meas_i  # Current data
                    Cycle_v = chr_cycle.data.meas_v  # Voltage data
                    Cycle_T = chr_cycle.data.meas_t  # Temperature data

                if Cycle_i is None: # Something has happened and this entry is invalid
                    continue        # continue to the next cycle.

                if fixed_len is not None:
                    # Cycle_t = Cycle_t[:fixed_len]
                    Cycle_i = Cycle_i[:fixed_len]
                    Cycle_v = Cycle_v[:fixed_len]
                    Cycle_T = Cycle_T[:fixed_len]

                # Concatenating the IVT values
                IVT = np.concatenate([np.reshape(Cycle_i, (len(Cycle_i), 1)),
                                      np.reshape(Cycle_v, (len(Cycle_v), 1)),
                                      np.reshape(Cycle_T, (len(Cycle_T), 1))], axis=1)

                # Calculating which cluster center is closer to each point
                cluster_values = th.from_numpy(kmeans.predict(IVT))
                # Calculating the number of points in each cluster.
                inputs = th.bincount(cluster_values, minlength=Nb_clusters)
                # Adding to the history:
                cumulative_input += inputs


                # Saving the capacity value
                Capacity = dis_cycle.data.capacity*np.ones(1)
                # If it's under the threshold:
                if Capacity.item() < threshold:
                    continue    # Continue to the next cycle.

                # Data cleaning:
                if resampling:
                    _, Cycle_i, Cycle_v, Cycle_T = Resampling_v3(dis_cycle, time_step=time_step, min_len=min_len)
                else:
                    # Numpy arrays
                    Cycle_i = dis_cycle.data.meas_i  # Current data
                    Cycle_v = dis_cycle.data.meas_v  # Voltage data
                    Cycle_T = dis_cycle.data.meas_t  # Temperature data

                if Cycle_i is None:
                    continue

                if fixed_len is not None:
                    # Cycle_t = Cycle_t[:fixed_len]
                    Cycle_i = Cycle_i[:fixed_len]
                    Cycle_v = Cycle_v[:fixed_len]
                    Cycle_T = Cycle_T[:fixed_len]

                # Concatenating the IVT values
                IVT = np.concatenate([np.reshape(Cycle_i, (len(Cycle_i), 1)),
                                      np.reshape(Cycle_v, (len(Cycle_v), 1)),
                                      np.reshape(Cycle_T, (len(Cycle_T), 1))], axis=1)

                # Calculating which cluster center is closer to each point
                cluster_values = th.from_numpy(kmeans.predict(IVT))
                # Calculating the number of points in each cluster.
                inputs = th.bincount(cluster_values, minlength=Nb_clusters).view(1, Nb_clusters)
                # Adding to the history:
                cumulative_input += inputs

                # Then:
                if mean_hist:
                    ent.append(th.clone(cumulative_input)/cumulative_input.sum())  # Normalizing by number of points
                else:
                    ent.append(th.clone(cumulative_input))  # The input for this capacity value
                sai.append(Capacity)                        # The capacity is one output

            assert len(ent) == len(sai), 'Unequal input and output lengths'

        if print_bool:
            print(len(ent))

        # Input and output normalization, for the whole dataset:
        inputs, output, norm = Normalize(ent, sai, norm_type=norm_type, norm=norm, concatenate=True)

        self.inputs = inputs    # saving the inputs
        self.output = output    # saving the outputs
        self.norm = norm        # saving the norm

        self.path = path
        self.transform = transform

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, item):

        ent = self.inputs[item]
        sai = self.output[item]

        return ent, sai

    def report(self):
        """Creates a little report with all of the information about the class.

        Returns
        -------
            A string with the report information.
        """
        string = '  Size of the training set: ' + str(len(self.inputs)) + '\n' \
               + '  Threshold (minimum capacity): ' + str(self.threshold) + '\n' \
               + '  Minimum length: ' + str(self.min_len) + '\n' \
               + '  Normalized histograms: ' + str(self.mean_hist) + '\n'
        if self.resampling:
            string += '  Resampling time step: ' + str(self.time_step) + '\n'
        else:
            string += 'Resampling: False' + '\n'
            # + '  Outlier zlim: ' + str(self.z_lim) + '\n'
            # + '  Current cut-off (charge cycle): ' + str(self.i_cutoff) + '\n'
        string += '  Batteries used: ' + str(self.batteries) + '\n' \
                + '  Normalization information: ' + str(self.norm) + '\n'

        return string

    def dataset_info(self, name):
        """Method for creating and information tuple that can be loaded for
        creating a similar dataset in the future.

        Parameters
        ----------
        name : str
            The name of the file where the tuple will be dumped.
        """
        info_tuple = (self.batteries, self.fixed_len, self.time_step,
                      self.resampling, self.norm)

        with open(name+'_info_tuple_.txt', 'wb') as f:
            pickle.dump(info_tuple, f)

class Torch_Dataset_SEV(Dataset):
    """Creates a pytorch Dataset class that can readily be loaded with pytorch's
     Loader class for training and validation.

    The datasets created by this class' initialization follow Severson et al.'s
    approach described in their article (full reference in the note). In it,
    the authors have developed a few different models, all of them based on a
    simple linear framework (here replaced by a FFNN), in order to predict
    battery life. That is, the number of cycles that a battery lasts before its
    capacity fades by 20% - which corresponds to 0.22 A.h in their dataset
    (batteries of 1.1 A.h nominal capacity). Update: Actually they predict
    log(battery life).

    Each of the different models developed in the article have similar
    structure, but different inputs. The model type can be picked through the
    variable ``model_type``, which is an initialization parameter.
    They correspond to the proposed models as follows:

    "Variance" model (``model_type = 1``):
        The model recieves as input solely the log variance of ∆Q(V)100-10.
        In detail: during a cycle's discharge, the cumulative capacity
        (Q = ∫ I dt) and the voltage vary over time. We take this curve defined
        by Q(V) for cycles 10 and 100 and subtract them (this requires a voltage
        sampling step in order to ensure logical comparison).
        The result is an array of ∆Q points, which we then take the variance and
        (then) the log (base 10) of.
    "Discharge" model (``model_type = 2``):
        This model builds on the previous one, taking other features from the
        ∆Q(V)100-10 curve (variance, minimum, skewness, kurtosis), as well as
        the discharge capacity of cycle 2 and the difference between the max
        discharge capacity (before cycle 100) and the discharge capacity of
        cycle 2.
    "Full" model
        Not yet implemented.

    .. note::
        Full reference: Severson, K.A., Attia, P.M., Jin, N. et al.
        Data-driven prediction of battery cycle life before capacity
        degradation. Nat Energy 4, 383–391 (2019).
        <https://doi.org/10.1038/s41560-019-0356-8>

    Parameters
    ----------
    Battery_list : list[Clean_Battery]
        List with the Batteries that we want to use for creating the dataset.
    begin_cycle : int, default = 10
        The cycle number for the first Q(V) curve.
    end_cycle : int, default = 100
        The cycle number for the second Q(V) curve.
    Q_lim : float, default = 0.88
        The capacity threshold for which we want to predict battery life.

        When equal to 0.88, this class will automtically get the cycle number
        through the ``cycle_life`` attribute present in the Clean_MIT_Battery
        class.

        When different than 0.88, this class will search the cycle number for
        which the battery capacity becomes lower than ``Q_lim`` and use it as
        the output.
    time_step : float, default = 0.001
        The VOLTAGE step for resampling [V].
    min_var : float, default = 2.
        The lower bound of the resampling.
    max_var : float, default = 4.
        The higher bound of the resampling.
    lim_cycle : int, optional
        If an integer is given, then the dataset output will be changed to the
        capacity at cycle `lim_cycle`. That is, the model is changed so that it
        predicts the capacity at a given cycle, instead of the cycle where a
        certain capacity value is reached.
      min_len : int, default = 10
        The minimum amount of points a charge cycle must have to be considered
        as relevant.
    model_type : {1, 2}
        An integer indicating which model type to use.
        if ``model_type`` == 1: then the "Variance" model is picked.
        elif ``version`` == 2: then the "Discharge" model is picked.
        elif ``version`` == 3: then the "Full model is picked. (NOT IMPLEMENTED)
    fixed_len : int, optional
        if different than `None` (default value), then the capacity(voltage)
        curves will be limited to `fixed_len` points. This *will* override
        `min_len`.
    time_diff_lim : float, default = 0.1
        The biggest variable jump the raw dataset can have before resampling.

        If the dataset has a value jump that is too big, resampling is
        compromised and the data cannot be used (simple interpolation can't
        reproduce the missing data). The cycle is thenskipped.

    norm_type : {'zscore', 'minmax'}
        A string indicating which kind of normalization to do.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.
        In entered as input, `norm_type` will be overridden by norm[4] and the
        given information will be used for the normalization
    print_bool : bool, default = False
        Whether or not to print some information while the code runs.
        Namely, if we want to print the battery's name and the number of points.

    Raises
    ------
    NameError
        If an unknown dataset type is given. This class currently supports Nasa
        and Severson dataset types (``Clean_Battery`` and ``Clean_MIT_Battery``
        classes).
    NotImplementedError
        If the given model_type is 3.
    Value Error
        If the given model_type is different from 1, 2 or 3.
        If there are no entries (inputs).

    """
    def __init__(self, Battery_list, begin_cycle=10, end_cycle=100, Q_lim=0.88,
                 time_step=0.001, min_var=2., max_var=4., lim_cycle=None,
                 min_len=10,  model_type = 1, fixed_len=None, time_diff_lim=0.1,
                 norm_type='zscore', norm=None, print_bool=False,
                 ):

        super().__init__()

        if fixed_len is not None:
            min_len = fixed_len

        # Dataset related
        self.dataset_name = type(Battery_list[0]).__name__      # Basically the dataset used (Nasa,...)
        self.batteries = []
        for Battery in Battery_list:                            # The batteries used in this dataset creation
            self.batteries.append(Battery.Name+'.txt')
        self.dataset_format = 'Severson et al.'                 # The format of the torch dataset (related to the net)
        # Cleaning
        self.time_step = time_step                              # The time step chosen for resampling
        self.time_diff_lim = time_diff_lim                      # The limit time jump allowed during resampling
        self.min_len = min_len                                  # Minimum cycle length required to take it into account
        self.fixed_len = fixed_len                              # The fixed length used.

        self.model_type = model_type
        self.begin_cycle = begin_cycle
        self.end_cycle = end_cycle
        self.min_var = min_var
        self.max_var = max_var
        self.lim_cycle = lim_cycle
        self.Q_lim = Q_lim

        ent = []                                    # Input list
        sai = []                                    # Output list

        if self.dataset_name == 'Clean_Coin_Battery':
            new_battery_list = []
            for Battery in Battery_list:
                for cell in Battery.cell:
                    new_battery_list.append(cell)

            Battery_list = new_battery_list

        for Battery in Battery_list:
            if print_bool:
                print(Battery.Name)

            Q_list = []
            V_list = []

            # If the number of cycles is too small:
            if len(Battery.cycle_couple) < end_cycle+1:
                print(Battery.Name, ' length = ', len(Battery.cycle_couple),
                      ' < end_cycle = ', end_cycle)
                continue
            elif lim_cycle is not None:
                if len(Battery.cycle_couple) < lim_cycle + 1:
                    print(Battery.Name, ' length = ', len(Battery.cycle_couple),
                          ' < lim_cycle = ', lim_cycle)
                    continue


            if lim_cycle is None:
                # Checking if the capacity value for the cycle end_cycle
                # is still greater than Q_lim:
                end_capacity = Battery.cycle_couple[end_cycle][1].data.capacity
                last_capacity = Battery.cycle_couple[-1][1].data.capacity
                if end_capacity < Q_lim:
                    print(Battery.Name, ': end_capacity = ', end_capacity,
                          ' < Q_lim = ', Q_lim)
                    continue
                elif last_capacity > Q_lim:
                    print(Battery.Name, ': last_capacity = ', last_capacity,
                          ' > Q_lim = ', Q_lim)
                    continue

            for times in range(2):
                if times == 0:
                    nb = begin_cycle
                else:
                    nb = end_cycle

                cycle_couple = Battery.cycle_couple[nb]
                dis_cycle = cycle_couple[1]

                # Data cleaning:
                if self.dataset_name == 'Clean_Battery':
                    # t, I, V, T = Resampling_flexible(dis_cycle, time_step=time_step,
                    Data = Resampling_flexible(dis_cycle, time_step=time_step,
                                               variable='Voltage',
                                               time_diff_lim=time_diff_lim,
                                               min_len=min_len,
                                               min_var=min_var,
                                               max_var=max_var)
                    if Data is None:
                        continue

                    V = Data[:, 0:1]
                    t = Data[:, 1:2]
                    I = Data[:, 2:3]
                    # T = Data[:, 3:4]

                    Q = np.zeros([len(I)])
                    I = I.reshape(-1)
                    t = t.reshape(-1)

                    for i in range(len(I)):
                        Q[i] = -integrate.simps(I[:i + 1], t[:i + 1] / 3600)

                    Q = Q.reshape([len(Q), 1])

                elif self.dataset_name == 'Clean_MIT_Battery':
                    Data = Resampling_flexible(dis_cycle, time_step=time_step,
                                               variable='Voltage',
                                               min_len=min_len,
                                               min_var=min_var,
                                               max_var=max_var)

                    if Data is None:
                        continue


                    V = Data[:, 0:1]
                    # t = Data[:, 1:2]
                    # I = Data[:, 2:3]
                    # T = Data[:, 3:4]
                    Q = Data[:, 4:5]

                else:
                    raise NameError("Unknown Dataset: "+self.dataset_name)

                if fixed_len is not None:
                    V = V[:fixed_len]
                    Q = Q[:fixed_len]

                V_list.append(V)
                Q_list.append(Q)

            # Common values of voltage:
            intersection = np.intersect1d(V_list[0], V_list[1])

            # Indexes of the intersection:
            idx_list_0 = [i for i in range(len(V_list[0])) if
                       V_list[0][i] in intersection] # list of indexes
            idx_list_1 = [i for i in range(len(V_list[1])) if
                       V_list[1][i] in intersection] # list of indexes

            # Only considering the capacity values for the intersection.
            Q_list[0] = Q_list[0][idx_list_0]
            Q_list[1] = Q_list[1][idx_list_1]

            Q_V = Q_list[1] - Q_list[0] # The Q(V) curve.
            # plt.plot(Q_list[0])
            # plt.plot(Q_list[1])

            Min_dQ = np.amin(Q_V)   # min ∆Q
            Var_dQ = np.log(np.var(Q_V))    # var ∆Q
            Skw_dQ = skew(Q_V)      # skewness ∆Q
            Kut_dQ = kurtosis(Q_V)  # kurtosis ∆Q
            Dis_c2 = Battery.cycle_couple[1][1].data.capacity   # Capacity at cycle 2
            Diff_Q = np.amax(Battery.capacity_curve[:, 1]) - Dis_c2 # Max Q - Q_c2

            if lim_cycle is None:
                last_cycle = np.where(Battery.capacity_curve[:,1] < Q_lim)[0][0]
                last_cycle = np.log(Battery.cycle_couple[last_cycle][0].number)
                if (Q_lim == 0.88) and (self.dataset_name=='Clean_MIT_Battery'):
                    last_cycle = np.log(Battery.cycle_life)
            else:
                last_cycle = Battery.cycle_couple[lim_cycle][1].data.capacity

            if model_type == 1:
                entry = th.Tensor([Var_dQ]).view(1)
            elif model_type == 2:
                entry = th.Tensor([Min_dQ, Var_dQ, Skw_dQ,
                                   Kut_dQ, Dis_c2, Diff_Q]).view(6)
            elif model_type == 3:
                raise NotImplementedError("Not implemented.")
            else:
                raise ValueError("Unknown version/model_type: "+str(model_type))

            ent.append(entry)
            sai.append(th.Tensor([last_cycle]).view(1))

            assert len(ent) == len(sai), 'Unequal input and output lengths'

        if print_bool:
            print(len(ent))

        # Input and output normalization, for the whole dataset:
        if len(ent) == 0:
            raise ValueError('No entries')

        inputs, output, norm = Normalize(ent, sai, norm_type=norm_type, norm=norm)
                                         # concatenate=True)

        self.inputs = inputs    # saving the inputs
        self.output = output    # saving the outputs
        self.norm = norm        # saving the norm

        self.path = None
        self.transform = None

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, item):

        ent = self.inputs[item]
        sai = self.output[item]

        return ent, sai

    def report(self):
        """Creates a little report with all of the information about the class.

        Returns
        -------
            A string with the report information.
        """
        string = '  Size of the training set: ' + str(len(self.inputs)) + '\n' \
               + '  Minimum length: ' + str(self.min_len) + '\n' \
               + '  Model type: ' + str(self.model_type) + '\n' \
               + "  ∆Q's first cycle: " + str(self.begin_cycle) + '\n'\
               + "  ∆Q's last cycle: " + str(self.end_cycle) + '\n'\
               + '  Resampling step: ' + str(self.time_step) + '\n'\
               + '  Resampling range: [' + str(self.min_var) + ',' \
                + str(self.max_var) + ']' + '\n'\
               + '  Fixed length: ' + str(self.fixed_len) + '\n'

        if self.lim_cycle is not None:
            string += '  Cycle number for Q prediction: ' + str(self.lim_cycle) + '\n'
        else:
            string += '  Q value for cycle prediction: ' + str(self.Q_lim) + '\n'

        string += '  Batteries used: ' + str(self.batteries) + '\n' \
                + '  Normalization information: ' + str(self.norm) + '\n'

        return string

    def dataset_info(self, name):
        """Method for creating and information tuple that can be loaded for
        creating a similar dataset in the future.

        Parameters
        ----------
        name : str
            The name of the file where the tuple will be dumped.
        """
        info_tuple = (self.batteries, self.begin_cycle, self.end_cycle,
                      self.time_step, self.Q_lim, self.lim_cycle,
                      self.model_type, self.min_var, self.max_var,
                      self.norm)

        with open(name+'_info_tuple_.txt', 'wb') as f:
            pickle.dump(info_tuple, f)

def Resampling_v3(Cycle, time_step=30., min_len=1, nan_lim=50,
                  time_diff_lim=None):
    """
    This function resamples a cycle's data to a desired time step, while also
    keeping an eye for NaNs and correcting them when necessary.

    .. note::
        If the cycle is deemed as too short or abnormal, then all of the arrays
        are returned as `None`.

    Parameters
    ----------
    Cycle : Clean_Cycle or Nasa_Cycle
        A cycle_like object containing time, current, voltage and temperature
        data (in [s], [A], [V] and [ºC], respectively).
    time_step : float, default = 30
        The desired time step for resampling [s].
    min_len : int, default = 1
        The minimum length (number of points) the cycle must have at the end of
        the resampling for it to be considered relevant.
    nan_lim : int, default = 50
        The amount of NaNs the resampled vector must have to print a warning and
        return None.
    time_diff_lim : float, optional
        The biggest time jump the raw dataset can have before resampling.

        If the dataset has a time jump that is too big, resampling is
        compromised and the data cannot be used (simple interpolation can't
        reproduce the missing data), returns ``None``s.
        If no value is suggested, it defaults to 10 times the ``time_step``
        size.

    Returns
    -------
    New_Time : ndarray[float]
        The new, resampled, time tensor [s].
    New_Current : ndarray[float]
        The new, resampled, Current tensor [A].
    New_Voltage : ndarray[float]
        The new, resampled, Voltage tensor [V].
    New_Temperature : ndarray[float]
        The new, resampled, Temperature tensor [ºC].

    Raises
    ------
    AssertionError
        If Cycle.type == 'impedance'.
        If min_len < 1.
    """

    assert Cycle.type != 'impedance', 'Only charge and discharge cycles are allowed.'
    assert min_len >= 1, 'min_len must be greater or equal to 1'

    if time_diff_lim is None:
        time_diff_lim = 5*time_step

    # print(Cycle.number)
    # Numpy arrays
    Time = Cycle.data.time  # Time data

    # Checking right away
    if len(Time) < min_len:
        return None, None, None, None
    Current = Cycle.data.meas_i  # Current data
    Voltage = Cycle.data.meas_v  # Voltage data
    Temperature = Cycle.data.meas_t  # Temperature data

    # Concatenating the time and the data
    Cat_Data = np.concatenate((Time, Current, Voltage, Temperature), axis=1)  # Cat stands for concatenated

    # Creating a data frame
    df = pd.DataFrame(data=Cat_Data,
                      columns=['Time', 'Current', 'Voltage', 'Temperature'])

    # Checking if the time_step is greater than the biggest sampling step:
    df['delta_time'] = df['Time'].diff()  # The time difference between each step

    # This check is done in order to avoid the creation of NaN entries.
    if df['delta_time'].max() > time_diff_lim:
        # If the biggest sample time step is greater than the imposed
        # time_diff_lim
        # print('Big time jump: ', str(df['delta_time'].max()),
        #       '>', str(time_diff_lim), 's.')

        return None, None, None, None

    # Transforming the time data in datetime form:
    df['Time'] = pd.to_datetime(df['Time'] * 1e9)

    df.set_index('Time', inplace=True)
    # Taking any NaN's away:
    # print('df w/ NaNs: ', len(df))
    df = df.dropna()
    # print('df w/o NaNs: ', len(df))
    # plt.clf()
    # plt.plot(df.index, df.Current)
    # plt.plot(df.index, df.Voltage)

    # Resampling
    df = df.resample(str(time_step) + 's').mean()

    # Checking for NaNs created during resampling:
    if df.isnull().values.any():        # If there's any
        idx_list = df.index.tolist()
        nan_idx0 = np.unique(np.where(df.isnull())[0]).tolist() # The lines
        nan_idx0.sort(reverse=True)                             # reverse order

        # Warning message is printed if too many NaNs
        if len(nan_idx0) > nan_lim:
            # print("Warning, ", len(nan_idx0),
            #       " NaNs in this Cycle after resampling")

            return None, None, None, None
            # plt.plot(df.index, df.Current)
            # plt.plot(df.index, df.Voltage)
            # plt.show()

        # Find the groups of NaNs
        groups = ranges(nan_idx0)   # ranges is a function, see below.
        groups.sort(reverse=True)   # reverse order

        for tuple in groups:
            i, j = tuple
            if i == 0 or j == len(df) - 1:
                # If the first or last lines are NaN, then we have to
                # take away all of the NaN points in this group because no
                # simple interpolation is possible
                df.drop(df.index[[k for k in range(i, j+1)]], inplace=True)
            else:
                for k in range(i, j+1):
                    # mean interpolation
                    df.loc[idx_list[k]] = (df.loc[idx_list[k-1]] + df.loc[idx_list[j+1]])/2

    # if there are still any NaNs:
    if df.isnull().values.any():
        print("Persistent NaNs.")
        print('Cycle number: ', Cycle.number)
        return None, None, None, None

    # Saving the new data
    New_Time = (df.index.astype('int64').to_numpy() / 1e9)  # Time (in seconds)
    New_Current = (df['Current'].to_numpy())                # Current
    New_Voltage = (df['Voltage'].to_numpy())                # Voltage
    New_Temperature = (df['Temperature'].to_numpy())        # Temperature

    # If the length is too short:
    if len(New_Time) < min_len:
        return None, None, None, None

    return New_Time, New_Current, New_Voltage, New_Temperature

if __name__ == '__main__':
    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    os.chdir('.//Separate_pickles')
    import time
    Nb_clusters = 80
    HIDDEN_DIM = 50
    DROPOUT = 0
    batch_size = 1
    Nb_Epochs = 120
    Learning_Rate = 1e-4
    norm_type = 'zscore'
    mean_hist = True

    # DO NOT CHANGE:
    resampling = True
    time_step = 30
    fixed_len = None

    # K-MEANS:
    try:  # If a K-means clustering has already been done with
        # that number of clusters:
        with open('kmeans' + str(Nb_clusters) + '.txt', 'rb') as f:
            kmeans = pickle.load(f)
    except FileNotFoundError:  # If not:

        # Opening the complete, clean, IVT data:
        with open('IVT_Data.txt', 'rb') as f:
            IVT = pickle.load(f)
        IVT = IVT.detach().numpy()  # Converting to numpy

        # The actual kmeans clustering:
        print('Running k-means...')
        start = time.time()
        kmeans = KMeans(n_clusters=Nb_clusters).fit(IVT)
        end = time.time()
        print('Finished: ' + str(Nb_clusters) +
              ' cluster k-means in ' + str(end - start) + ' seconds')

        # Saving the class:
        with open('kmeans' + str(Nb_clusters) + '.txt', 'wb') as f:
            pickle.dump(kmeans, f)


    files = os.listdir()  # All files in the folder
    file_list = []  # List with the valid files
    for file_name in files:
        match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
        # the battery files
        if match == None:
            continue
        else:
            file_list.append(file_name)
    file_list.sort()

    # Files to be ignored:
    ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt',
              'B0025.txt', 'B0026.txt', 'B0027.txt', 'B0028.txt',
              'B0029.txt', 'B0030.txt', 'B0031.txt', 'B0032.txt']

    # The big one
    train = ['B0029.txt', 'B0031.txt', 'B0032.txt',  # 43ºC
             'B0038.txt', 'B0039.txt',  # 44ºC et 24ºC
             'B0042.txt', 'B0044.txt',  # 44ºC et 4ºC
             'B0046.txt',  # 4ºC
             'B0055.txt',  # 4ºC
             'B0018.txt']  # 4ºC

    # train = ['B0045.txt', 'B0046.txt',
    #          'B0053.txt', 'B0054.txt']

    # with open('B0033.txt', 'rb') as f:
    #     Battery = pickle.load(f)
    #
    # testn_dataset = Torch_Dataset_KMEANS([Battery], Nb_clusters, kmeans=kmeans, print_bool=True, norm_type='minmax',
    #                                      mean_hist=True)
    #
    # with open('train_dataset_FFNN_zscore.txt', 'rb') as f:
    #     train_dataset, valid_dataset = pickle.load(f)

    # Training files:
    Train_list = []
    for file_name in file_list:
        if file_name in train:
            with open(file_name, 'rb') as f:
                Battery = pickle.load(f)
            Train_list.append(Battery)

    # Creating the dataset:
    train_dataset = Torch_Dataset_KMEANS(Train_list, Nb_clusters, kmeans=kmeans, print_bool=True, norm_type=norm_type,
                                        mean_hist=mean_hist, fixed_len=fixed_len, resampling=resampling, time_step=time_step)

    train_loader = DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True, num_workers=0)

    sim_train_loader = DataLoader(dataset=train_dataset, batch_size=len(train_dataset), shuffle=False, num_workers=0)

    Model = FFNN_v1(Nb_clusters, HIDDEN_DIM, drop_out=DROPOUT, norm=train_dataset.norm)

    simulate(sim_train_loader, Model, block=False)

    # Validation files:
    Valid_list = []
    for file_name in file_list:
        if (file_name not in train) and (file_name not in ignore):
            with open(file_name, 'rb') as f:
                Battery = pickle.load(f)
            Valid_list.append(Battery)

    valid_dataset = Torch_Dataset_KMEANS(Valid_list, Nb_clusters, kmeans=kmeans, print_bool=True,
                                         norm=train_dataset.norm, mean_hist=mean_hist,
                                         fixed_len=fixed_len, resampling=resampling, time_step=time_step)

    valid_loader = DataLoader(dataset=valid_dataset, batch_size=len(valid_dataset), shuffle=False, num_workers=0)

    simulate(valid_loader, Model, block=False)

    os.chdir('../../../../')

    Train_info,_,_,date = Train(train_loader, valid_loader, Model, Nb_Epochs, plot_bool=True, LR=Learning_Rate, report=True,
          sim_loader=sim_train_loader, remove_bool=True)

    simulate(sim_train_loader, Model, block=True)
    simulate(valid_loader, Model, block=True)

    os.chdir('.//Reports/'+str(date))
    load_info = (Train_info.best_epoch.nb + 1, train, ignore, fixed_len, time_step, resampling, mean_hist, train_dataset.norm, kmeans, Nb_clusters)
    with open('load_info.txt', 'wb') as f:
        # best_epoch, train, ignore, fixed_len, time_step, resampling, mean_hist, norm, kmeans = pickle.load(f)
        pickle.dump(load_info, f)
    # os.chdir('../../')

    """
    # Visualizing the input "histograms":
    cluster_names = [str(i) for i in range(Nb_clusters)]
    for k in range(len(Battery_Dataset)):
        plt.bar(cluster_names, Battery_Dataset.inputs[k])
        plt.show(block=False)
        plt.pause(0.1)
        plt.close()
    """
