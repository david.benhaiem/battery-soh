from Cleaning import *
from torch.utils.data import Dataset
import matplotlib.pyplot as plt


class Pytorch_Dataset_LSTM(Dataset):
    """ Creates a pytorch Dataset class that can readily be loaded with pytorch's Loader class
    for training and validation.

    The datasets created by this class' initialization follow You et al.'s * snapshot approach, where
    I, V data for a charge cycle are used for battery capacity prediction. A number ∆ (delta) of
    sequential (I, V) points are collected and turned into a 1D vector x_i. This window of points
    slides through time and each vector x_i becomes the sequential inputs of an LSTM (see LSTM_v1 class
    in NN_Classes.py). Here, these x_i vectors are already grouped in order in a XT 2D tensor and saved
    into the list 'ent'. One XT tensor = One charge cycle = One input.
    The list is later saved as attribute 'inputs' (self.inputs = ent).
        * Full reference: G. You, S. Park and D. Oh, "Diagnosis of Electric Vehicle Batteries
            Using Recurrent Neural Networks," in IEEE Transactions on Industrial Electronics,
            vol. 64, no. 6, pp. 4885-4893, June 2017, doi: 10.1109/TIE.2017.2674593.

    The output for each XT (each charge cycle) is a capacity value (in A.h). However, capacity values are
    only measured during discharge cycles. Because of this, the capacity values taken as outputs are picked
    from the discharge cycle that comes immediately after the charge cycle. When two charge cycles come one
    after the other without any discharge cycle in between, the first one is discarded as an input.
        This last sentence is being thought of, we'll see and check about that.

    Some attributes:
        self.dataset_name   : the name of the dataset (Dataset_Nasa). [string]
        self.dataset_format : a string indicating the type of format used: 'You et al. [56]'. [string]
        self.inputs         : a list with the XT tensors for each cycle. [list of pytorch tensors]
        self.output         : a list with the output capacity tensors for each cycle. [list of pytorch tensors]

    Inputs:
        Battery_list        : list with the Batteries that we want to use for creating the dataset.
                              [list of Dataset_Nasa objects]
        delta               : the size of the sliding window used to generate the x_i's. [int]
        ignore              : list with the name of the files that should be ignored (not used for
                              the dataset generation). [list of strings]
                                It's useful because we already have a complete pickle with all of
                                the batteries. That way we can use this file and just say which
                                batteries to ignore.
        print_bool          : if we want to print some information while the code runs. [bool]
                              Namely, if we want to print the battery's name and the number of inputs.
        time_step           : the desired time step for the resampling procedure. [int]
        i_cutoff            : the current cutoff at the end of the charging procedure. [float]
        threshold            : the minimum value of capacity to be taken into account. [float]
                                Values under the treshold will be ignored during the construction
                                of the dataset.
        path, transform     : necessary arguments for Pytorch's Dataset class but that are unused. [None]
    """
    def __init__(self, Battery_list, delta,
                 ignore = None, print_bool=False,
                 time_step=30, i_cutoff=2e-2,
                 threshold=0.01,
                 path=None, transform=None):                    # Just necessary arguments for pytorch's Dataset class

        super().__init__()

        if isinstance(ignore, type(None)):
            ignore = []

        self.dataset_name = type(Battery_list[0]).__name__      # Basically the dataset used (Nasa,...)
        self.dataset_format = 'You et al. [56]'
        self.delta = delta                                      # The amount of points included per xi
        self.time_step = time_step                              # The time step chosen for resampling
        self.i_cutoff = i_cutoff                                # The current cut-off chosen for cleaning
        self.threshold = threshold                              # The threshold chosen for cleaning

        min_len = delta - 1

        ent = []                                    # Input list
        sai = []                                    # Output list

        # The following commented lines aren't useful now, but in the future they might be
        # (when we have different datasets and formats)

        #if (self.dataset_name == Dataset_Nasa):
        #    if (self.dataset_format == 'You et al. [56]'):

        for Battery in Battery_list:
            if Battery.Name + str('.txt') in ignore:
                continue
            if print_bool:
                print(Battery.Name)

            for Cycle in Battery.cycle:

                if Cycle.type == 'charge':

                    Cycle_t, Cycle_i, Cycle_v, _ = clean(Cycle, time_step=time_step,
                                                         i_cutoff=i_cutoff, min_len=min_len)

                    if Cycle_t is None:
                        continue

                    # Calculating the real number of points we've got
                    Nb = len(Cycle_i) - delta + 1                           # Number of xis in this cycle
                    # min_len already take care of the cases where Nb <= 0.

                    XT = th.zeros(Nb, 2*delta)                              # Regrouped xi info for one whole cycle

                    for t in range(Nb):   # We're going to create one xi for each t
                        xi = th.cat([Cycle_i[t:t+delta],    # Current data
                                     Cycle_v[t:t+delta]])   # Voltage data

                        XT[t, :] = xi                   # "Add" it to the XT tensor that regroups them.

                elif Cycle.type == 'discharge':

                    Capacity = Cycle.data.capacity*th.ones(1)
                    if Capacity.item() < threshold:
                        continue

                    try:
                        ent.append(XT)          # The XT tensor represents one cycle and is thus one input.
                        sai.append(Capacity)    # The capacity is one output.
                    except NameError:           # If this happens, it means that there hasn't been a charge cycle yet.
                        continue

            del XT  # We must delete XT for the next iteration, so that it won't cause problems

        assert len(ent) == len(sai), 'Unequal input and output lengths'

        if print_bool:
            print(len(ent))

        self.inputs = ent
        self.output = sai
        self.path = path
        self.transform = transform

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, item):
        ent = self.inputs[item]
        sai = self.output[item]
        return ent, sai

if __name__ == '__main__':

    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    os.chdir('.//Separate_pickles')

    ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt'] # Files to be ignored.

    with open('B0027.txt', 'rb') as f:
        Battery = pickle.load(f)

    Battery_list = [Battery]
    th_B0029 = Pytorch_Dataset_LSTM(Battery_list, 10)

    out_tsr = np.array(th_B0029.output)
    plt.plot(out_tsr)
    plt.show()

    print('Number of inputs ', len(th_B0029.inputs))     # Number of valid charge cycles
    print('Number of outputs ', len(th_B0029.output))     # Number of capacity outputs (same as the previous)
    print('Size of the sequence for the first cycle: ', len(th_B0029.inputs[0]))