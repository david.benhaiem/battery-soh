from torch.utils.data import DataLoader
from Report import *
from RNN import *
from LSTM import *
from FFNN import *
from Training import *
import re
import os
import torch.nn as nn

def Load_Save(report_name, device='cpu', plot_bool=False, analysis=True,
              epoch=None, dataset_name='Nasa'):
    """
    This function loads the models contained in a report and simulates it's
    performance in all of the available batteries.

    The function reaches into the report directory automatically and loads
    both the best and the last epochs' models. It also reads through the
    load_info.txt file which batteries were used for training and which were
    ignored. It will then evaluate the model in each battery that hasn't been
    ignored and save an image of each result in folders `Images_Best` (for the
    best epoch) and `Images_Final` (for the last epoch) inside of the original
    report folder (`report_name`).

    Parameters
    ----------
    report_name : 'string'
        The name of the report directory. Is usually a date.
    device : torch.device or str, default = 'cpu'
        In which device the model and evaluation should be done.
        Can either be 'cpu' or a cuda ('cuda:0', 'cuda:1', ...).
    plot_bool : bool
        A boolean variable indicating whether or not to plot the battery
        evaluations as they are done.
    analysis : bool
        Whether or not to run a more individual analysis of the different
        batteries.
    """
    os.chdir('.//Reports')
    #report_name = input("Report folder: ")
    report_name = report_name.replace('/', ':')
    os.chdir('.//' + str(report_name))

    with open('best_model.txt', 'rb') as f:
        Model = pickle.load(f)
    dir_name = '_Best'

    if Model.model_name == 'LSTM' or Model.model_name == 'RNN':
        with open('load_info.txt', 'rb') as f:
            best_epoch, train, ignore, fixed_len, time_step, resampling, norm = pickle.load(f)
    else:
        with open('load_info.txt', 'rb') as f:
            best_epoch, train, ignore, fixed_len, time_step, resampling, mean_hist, norm, kmeans, Nb_clusters = pickle.load(f)


    os.chdir('../../')
    # Creating the DataLoaders for each battery:
    if dataset_name == 'Nasa':
        path = './/Datasets/NASA/B. Saha and K Goebel/Separate_pickles'
    elif dataset_name == 'Severson':
        path = './/Datasets/Severson et al./Cleaned/2. 2017-06-30'

    os.chdir(path)

    files = os.listdir()  # All files in the folder
    file_list = []  # List with the valid files
    for file_name in files:
        match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
        # the battery files
        if match == None:
            continue
        else:
            file_list.append(file_name)
    file_list.sort()

    loader_list = []

    for file_name in file_list:
        if file_name in ignore:
            continue
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)

        Battery_list = [Battery]

        if Model.model_name == 'LSTM':
            th_B = Torch_Dataset_LSTM(Battery_list, Model.delta, Model.version,
                                      norm=norm, fixed_len=fixed_len,
                                      time_step=time_step, print_bool=True)

        elif Model.model_name == 'RNN':
            th_B = Torch_Dataset_RNN(Battery_list, Model.delta, Model.version,
                                     norm=norm, fixed_len=fixed_len,
                                     time_step=time_step)
        elif Model.model_name == 'FFNN':
            if dataset_name == 'Nasa':
                th_B = Torch_Dataset_KMEANS(Battery_list, Nb_clusters,
                                            kmeans=kmeans,
                                            norm=norm, time_step=time_step,
                                            resampling=resampling,
                                            mean_hist=mean_hist)
            elif dataset_name == 'Severson':
                th_B = Torch_Dataset_SEV()

        B_ld = DataLoader(dataset=th_B, batch_size=len(th_B), shuffle=False,
                          num_workers=0)

        loader_list.append(B_ld)

    os.chdir('../../../../')
    os.chdir('.//Reports/' + report_name)

    for times in range(2):

        if times == 0:
            pass
        else:
            if epoch is None:
                with open('base_model.txt', 'rb') as f:
                    Model = pickle.load(f)
                dir_name = '_Final'
            else:
                os.chdir('.//Epochs')
                Model.load_state_dict(th.load('Epoch_'+str(epoch)+'.txt',
                                              map_location='cpu'))
                os.chdir('../')
                dir_name = '_Epoch_'+str(epoch)

        Model.to(device)

        for loader in loader_list:

            # Battery's file name
            file_name = loader.dataset.batteries[0]

            if file_name in train:
                title = file_name + ' (used in training)'
                print(title)
            else:
                title = file_name
                print(title)

            simulate(loader, Model, title=title, report=True, block=False,
                     file_name=file_name.split('.')[0], plot_bool=plot_bool,
                     dir_name=dir_name, device=device, denormalize=True)

        if analysis:
            Analysis(loader_list, Model, train, device=device, file_name=dir_name)

    os.chdir('../../')

def Analysis(battery_loader_list, model, train, device='cpu', file_name='',
             base_capacity=2.1):
    """
    Analyses the model's performance over the different batteries.

    Calculates the mean and maximum absolute error, mean and maximum relative
    error, and the root mean squared error (RMSE).

    A report will create a text file with the results.

    Parameters
    ----------
    battery_loader_list : list[torch.utils.data.DataLoader]
        The battery's DataLoader loader.
    model : torch.nn.Module
        The model to use for the analysis.
    device : torch.device or str, default = 'cpu'
        In which device the model and evaluation should be done.
        Can either be 'cpu' or a cuda ('cuda:0', 'cuda:1', ...).
    file_name : str
        A string to add to the Analysis file name. The final name will be
        'Analysis'+file_name+'.txt'.
    base_capacity : float, default = 2.1
        The battery's maximum capacity value (at its beginning of life).

    Raises
    ------
    TypeError
        If the normalization type present in battery_loader.dataset.norm is not
        supported.
    """

    analysis_file = open('Analysis'+file_name+'.txt', 'w')

    model = model.to(device)

    # L1 Loss function:
    loss_fn = nn.L1Loss(reduction='none')
    los2_fn = nn.MSELoss()

    # "Simulation"
    with th.no_grad():
        model.eval()
        for battery_loader in battery_loader_list:
            output_list = []
            labels_list = []
            for data, labels in battery_loader:
                data= data.to(device)
                labels = labels.to(device)
                output = model.forward(data)

                # The following lines are more general (any batch_size)
                output_list.append(output)
                labels_list.append(labels)

            # Turning them back to th.Tensors
            output = th.stack(output_list)
            labels = th.stack(labels_list)

            # Denormalization:
            norm = battery_loader.dataset.norm

            if norm[4] == 'zscore':
                output = output * norm[3].to(device) + norm[2].to(device)
                labels = labels * norm[3].to(device) + norm[2].to(device)
            elif norm[4] == 'minmax':
                output = output * (norm[3].to(device) - norm[2].to(device)) + norm[2].to(device)
                labels = labels * (norm[3].to(device) - norm[2].to(device)) + norm[2].to(device)
            else:
                raise TypeError(norm[4]+' normalization type not supported')

            # Absolute error:
            abs_loss = loss_fn(output, labels)
            mean_abs = th.mean(abs_loss)
            amax_abs = th.amax(abs_loss)

            # Relative error
            rel_loss = abs_loss / output
            mean_rel = th.mean(rel_loss)
            amax_rel = th.amax(rel_loss)

            # Relative to the initial capacity value
            mean_init = mean_abs/base_capacity
            amax_init = amax_abs/base_capacity

            # RMSE
            RMSE = th.sqrt(los2_fn(output, labels))

            battery_name = str(battery_loader.dataset.batteries[0])
            if battery_name in train:
                battery_name += ' (training)'

            analysis_file.write('Battery '+ battery_name
                                +'\n\n'
                                + '    Mean and Max Absolute Error: '
                                + str(mean_abs.item())[:6]+', '+str(amax_abs.item())[:6]
                                +'\n'
                                + '    Mean and Max Relative Error: '
                                + str(mean_rel.item()*100)[:4]+'%, '
                                + str(amax_rel.item()*100)[:4]+'%'
                                + '\n'
                                + '    Relative to the BoL capacity: '
                                + str(mean_init.item() * 100)[:4] + '%, '
                                + str(amax_init.item() * 100)[:4] + '%'
                                + '\n'
                                + '    RMSE: '
                                + str(RMSE.item())[:6]
                                + '\n\n'
                                )

    analysis_file.close()

if __name__ == '__main__':

    os.chdir('.//Reports')
    report_name = input("Report folder: ")
    report_name = report_name.replace('/', ':')
    os.chdir('.//'+str(report_name))

    string = 'A'
    while (string != 'Y' and string != 'N'):
        string = input('Load best epoch (Y/N)? ')

    if string == 'Y':
        with open('best_model.txt', 'rb') as f:
            Model = pickle.load(f)
        dir_name = '_Best'
    else:
        with open('base_model.txt', 'rb') as f:
            Model = pickle.load(f)
        dir_name = '_Final'

    if Model.model_name == 'LSTM' or Model.model_name == 'RNN':
        with open('load_info.txt', 'rb') as f:
            best_epoch, train, ignore, fixed_len, time_step, resampling, norm = pickle.load(f)
    else:
        with open('load_info.txt', 'rb') as f:
            best_epoch, train, ignore, fixed_len, time_step, resampling, mean_hist, norm, kmeans, Nb_clusters = pickle.load(f)

    if string == 'Y':
        print('Best Epoch: ', best_epoch)

    os.chdir('../../')
    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    os.chdir('.//Separate_pickles')

    files = os.listdir()  # All files in the folder
    file_list = []  # List with the valid files
    for file_name in files:
        match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
        # the battery files
        if match == None:
            continue
        else:
            file_list.append(file_name)
    file_list.sort()

    for file_name in file_list:
        if file_name in ignore:
            continue
        if file_name in train:
            title = file_name + ' (used in training)'
            print(title)
        else:
            title = file_name
            print(title)
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)

        Battery_list = [Battery]

        if Model.model_name == 'LSTM':
            th_B = Torch_Dataset_LSTM(Battery_list, Model.delta, Model.version,
                                      norm=norm, fixed_len=fixed_len, time_step=time_step)
        elif Model.model_name == 'RNN':
            th_B = Torch_Dataset_RNN(Battery_list, Model.delta, Model.version,
                                      norm=norm, fixed_len=fixed_len, time_step=time_step)
        elif Model.model_name == 'FFNN':
            th_B = Torch_Dataset_KMEANS(Battery_list, Nb_clusters, kmeans=kmeans,
                                    norm=norm, time_step=time_step, resampling=resampling, mean_hist=mean_hist)

        B_ld = DataLoader(dataset=th_B, batch_size=len(th_B), shuffle=False, num_workers=0)

        os.chdir('../../../../')
        os.chdir('.//Reports/'+report_name)
        simulate(B_ld, Model, title=title, report=True, block=False, file_name=file_name.split('.')[0], plot_bool=False,
                 dir_name=dir_name)
        os.chdir('../../')
        os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
        os.chdir('.//Separate_pickles')


    # # I can write some code in such a way that it will read the batteries that were eused in each dataset from the
    # # report. But this wasn't implemented earlier.

    # # Training files:
    # Train_list = []
    # for file_name in file_list:
    #     if file_name in train:
    #         with open(file_name, 'rb') as f:
    #             Battery = pickle.load(f)
    #         Train_list.append(Battery)
    #
    #
    # train_dataset = Torch_Dataset_LSTM(Train_list, DELTA,  # Dataset
    #                                    print_bool=True, time_step=time_step, fixed_len=fixed_len)
    #
    # if train_dataset.fixed_len is None:
    #     train_loader = DataLoader(dataset=train_dataset, batch_size=1,  # Loader
    #                               shuffle=True, num_workers=0)
    # else:
    #     train_loader = DataLoader(dataset=train_dataset, batch_size=batch_size,  # Loader
    #                               shuffle=True, num_workers=0)
    #
    # if train_dataset.fixed_len is None:
    #     sim_train_loader = DataLoader(dataset=train_dataset, batch_size=1, shuffle=False, num_workers=0)
    # else:
    #     sim_train_loader = DataLoader(dataset=train_dataset, batch_size=len(train_dataset), shuffle=False, num_workers=0)
    #
    # simulate(sim_train_loader, Model, block=False)
    #
    #
    # # Validation files:
    # Valid_list = []
    # for file_name in file_list:
    #     if (file_name not in train) and (file_name not in ignore):
    #         with open(file_name, 'rb') as f:
    #             Battery = pickle.load(f)
    #         Valid_list.append(Battery)
    #
    # valid_dataset = Torch_Dataset_LSTM(Valid_list, DELTA,  # Dataset
    #                                    print_bool=True, time_step=time_step,
    #                                    norm=train_dataset.norm, fixed_len=train_dataset.fixed_len)
    #
    # if train_dataset.fixed_len is None:
    #     valid_loader = DataLoader(dataset=valid_dataset, batch_size=1,  # Loader
    #                               shuffle=False, num_workers=0)
    # else:
    #     valid_loader = DataLoader(dataset=valid_dataset, batch_size=len(valid_dataset),  # Loader
    #                           shuffle=False, num_workers=0)
    #
    # simulate(valid_loader, Model, block=False)


    #
    # os.chdir('.//2021-06-02 10:29:23.188704/Epochs')
    # file_list = os.listdir()
    # file_list.sort()
    # N_epoch = len(file_list)
    # TE = th.zeros(N_epoch)
    # VE = th.zeros(N_epoch)
    # Train_info = Training_Class(Nb_Epochs, nn.MSELoss(), th.optim.Adam(Model.parameters(), lr=Learning_Rate),
    #                             Learning_Rate, batch_size)
    # for i in range(len(file_list)):
    #     if file_list[i] == '.DS_Store':
    #         continue
    #     Model.load_state_dict(th.load(file_list[i]))
    #     error = simulate_epoch(sim_train_loader, Model, block=False, plot_bool=False)
    #     TE[i] = error
    #     error = simulate_epoch(valid_loader, Model, block=False, plot_bool=False)
    #     VE[i] = error
    #     Train_info.add_epoch(TE[i], VE[i], i)
    #
    # plt.plot(TE)
    # plt.plot(VE)
    # plt.show()

    # simulate(sim_train_loader, Model, report=True, file_name='Training')
    # simulate(valid_loader, Model, report=True, file_name='Validation')
