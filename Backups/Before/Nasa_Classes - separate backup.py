import os
import numpy as np
from datetime import datetime
import scipy.io
import time
import pickle


class Dataset_Nasa:
    """ Nasa Dataset class. This is based on the "Battery Data Set" by B. Saha and K. Goebel.

    One Dataset_Nasa object corresponds to one battery/one '.mat' file.
    The class is structured as follows:

    self.Name       : the battery ID (example: 'B0005') [string]
    self.cycle      : list of the cycles tested [list]
    self.cycle[j]   : a Nasa_Cycle object for the (j+1)th cycle (since j starts at 0) [Nasa_Cycle]
        See Nasa_Cycle and Nasa_Data classes for more information on the data for each cycle.
        In general, to reach into the measured data: self.cycle[j].data.meas_v
        (meas_v here stands for measured voltage, more information in the Nasa_Data class)

    self.header, self.version and self.globals contain information present in the '.mat' file but that
    shouldn't be useful to our analysis.

    Full reference: B. Saha and K. Goebel (2007). "Battery Data Set", NASA Ames Prognostics Data Repository
    (https://ti.arc.nasa.gov/tech/dash/groups/pcoe/prognostic-data-repository/#battery),
    NASA Ames Research Center, Moffett Field, CA
    """
    def __init__(self, file_name):

        Mat  = scipy.io.loadmat(file_name)          # Loading the .mat file
        Name = file_name.split('.')[0]              # Just taking the name of the battery
        #print(Name) # For bug fixing
        shp  = Mat[Name]['cycle'][0, 0].shape       # Shape of the cycle matrix

        # Reshaping from (1,N) to (N,) for practical reasons, saving it as cycle.
        cycle = np.reshape(Mat[Name]['cycle'][0, 0], shp[1])

        # Initializing the self.cycle list
        self.cycle = []

        # Since the type of cycle (and thus its attributes, measurements etc.) vary according to the
        # cycle, we have no choice but check them one by one. This only takes 0.02s and only have to
        # be done once though.

        for i in cycle:
            c_i = Nasa_Cycle(i)     # Go to Nasa_Cycle class.
            self.cycle.append(c_i)

        # Other information present in the .mat file, just in case:
        self.header  = Mat['__header__']
        self.version = Mat['__version__']
        self.globals = Mat['__globals__']
        # Let's also save its name:
        self.Name = Name


class Nasa_Cycle:
    """ Nasa Cycle class. This includes information about the cycle in question.

    It's attributes are:
    self.type       : the type of cycle (either 'charge', 'discharge' or 'impedance') [string]
    self.amb_T      : the ambient temperature during measurements. [int] {ºC}
        (The raw data was already an integer, I just kept it this way)
    self.time       : the date and time of the experiment [datetime.datetime] {Year-Month-Day Hour:Minutes:Seconds}
    self.unix_time  : the date and time of the experiment expressed in unix time [float] {s}
    self.data       : Nasa_Data object containing all of the actual data. [Nasa_Data]
        See Nasa_Data for more information.
    """
    def __init__(self, CYCLE):

        self.type  = str(CYCLE['type'][0])                      # The type of cycle (charge, discharge or impedance)
        self.amb_T = int(CYCLE['ambient_temperature'][0,0])     # The ambient temperature of the measurements

        # Time conversion for datetime.datetime object and UNIX
        Y = str(int(CYCLE['time'][0, 0]))
        m = str(int(CYCLE['time'][0, 1]))
        d = str(int(CYCLE['time'][0, 2]))
        H = str(int(CYCLE['time'][0, 3]))
        M = str(int(CYCLE['time'][0, 4]))
        S = str(int(CYCLE['time'][0, 5]))
        time_str = Y + '/' + m + '/' + d + ' ' + H + ':' + M + ':' + S

        self.time = datetime.strptime(time_str, '%Y/%m/%d %H:%M:%S')    # Time as datetime object
        self.unix_time = self.time.timestamp()                          # Time in UNIX format (seconds since 1970)

        #print(self.time) # for bugfixing
        self.data = Nasa_Data(CYCLE['data'], self.type)                 # The actual data, see Nasa_Data class

class Nasa_Data:
    """ Nasa Data class. Includes all of the actually measured data.

    The attributes of this class depend on the cycle type ('charge', 'discharge', 'impedance').
    That's the reason why this is a required input.

    The data is stored in the following attributes:
        (All are 2D [np.ndarrays] unless otherwise mentioned in brackets)
        Descriptions taken directly from 'README' files from the raw datasets

    Impedance data:
    self.sense_i        : Current in sens branch {A}
    self.battery_i      : Current in battery branch {A}
    self.ratio_i        : Ratio of the above currents {}
    self.battery_z      : Battery Impedance computed from raw data {Ohms}
    self.rectified_z    : Calibrated and smoothed battery impedance {Ohms}
    self.Re             : Estimated electrolyte resistance {Ohms} [float]
    self.Rct            : Estimated charge transfer resistance {Ohms} [float]

    Charge data:
    self.meas_v         : Battery terminal voltage {V}
    self.meas_i         : Battery output current {A}
    self.meas_t         : Battery temperature {ºC}
    self.charge_i       : Current measured at charger {A}
    self.charge_v       : Voltage measured at charger {V}
    self.time           : Time vector for the cycle {s}

    Discharge data:
    self.meas_v         : Battery terminal voltage {V}
    self.meas_i         : Battery output current {A}
    self.meas_t         : Battery temperature {ºC}
    self.load_i         : Current measured at load {A}
    self.load_v         : Voltage measured at load {V}
    self.capacity       : Battery capacity for discharge till 2.7V {A.h} [float]
    self.time           : Time vector for the cycle {s}
    """
    def __init__(self, Data, Type):

        # Data is Cycle['data'] whilst Type is Cycle['type']

        if Type == 'impedance':
            #(!!!) AN ADAPTATION OF SHAPES AND MATRICES MAY BE NECESSARY (?)
            # In this case we have as attributes:
            # Sense_current
            self.sense_i = Data['Sense_current'][0,0]
            # Battery_current
            self.battery_i = Data['Battery_current'][0,0]
            # Current_ratio
            self.ratio_i = Data['Current_ratio'][0,0]
            # Battery_impedance
            self.battery_z = np.transpose(Data['Battery_impedance'][0,0])
            # Rectified_Impedance
            self.rectified_z = np.transpose(Data['Rectified_Impedance'][0,0])
            # Re  (estimated electrolyte resistance in Ohms)
            self.Re = Data['Re'][0,0][0,0]
            # Rct (estimated charge transfer resistance in Ohms)
            self.Rct = Data['Rct'][0,0][0,0]

        else:   # If the type isn't impedance, then it's either charge or discharge which have a lot in common:

            # Voltage_measured
            self.meas_v = Data['Voltage_measured'][0,0]
            # Current_measured
            self.meas_i = Data['Current_measured'][0,0]
            # Temperature_measured
            self.meas_t = Data['Temperature_measured'][0,0]
            # Time
            self.time = Data['Time'][0, 0]

            if Type == 'charge':
                # Current_charge
                self.charge_i = Data['Current_charge'][0,0]
                # Voltage_charge
                self.charge_v = Data['Voltage_charge'][0,0]
            else:
                # Current_load
                self.load_i = Data['Current_load'][0,0]
                # Voltage_load
                self.load_v = Data['Voltage_load'][0,0]
                # Capacity
                # The following 'try:' statement is necessary because
                # some files have missing Capacity data
                try:
                    self.capacity = Data['Capacity'][0,0][0,0]
                except IndexError:
                    print('No Capacity value')
                    # maybe not the best way of dealing with this
                    self.capacity = float('nan')


########################################################################################



if __name__ == '__main__':

    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')

    # All of Nasa's Datasets:
    os.chdir('.//Compiled_datasets')

    start = time.time()

    for i in os.listdir():
        if i == '.DS_Store':
            continue

        print(i)
        B = Dataset_Nasa(i)

        os.chdir('../Separate_pickles')


        with open(B.Name+'.txt', 'wb') as file:
            pickle.dump(B, file)  # 359,2 MB

        os.chdir('../Compiled_datasets')

    end = time.time()
    print(end - start) # 4 seconds (since it's printing stuff)

    # B0052.mat and B0050.mat have missing values of Capacity (B0052.mat in particular)
