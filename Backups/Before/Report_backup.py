from Nasa_Classes import *
import torch as th
import pickle
import re
import torch.nn as nn
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from datetime import datetime


def Report(Model, train_loader, valid_loader, Training_info=None,
           file_name='Report', remove_files=False):
    """
    This function creates a Report text file containing information about the
    model's training and saves important information in separate files.

    This function is called by the `Train` function in Training.py at the end
    of the last epoch.

    The text report will include:

    - Information about the model itself (specificities).
    - Training information.
        Learning rate, number of epochs, batch size, best and last epochs and
        their losses, loss function and optimizer.
    - Information about the training and validation datasets.

    This function will also save the best and last epochs' models
    (`best_model.txt` and `base_model.txt`) and the Training_Class object with
    all of the training information.

    .. note::
        The model's parameters are changed to the best epoch's.

    Parameters
    ----------
    Model : torch.nn.Module
        The trained model.
    train_loader : torch.utils.data.DataLoader
        The training dataset's DataLoader class.
    valid_loader : torch.utils.data.DataLoader
        The validation dataset's DataLoader class.
    Training_info : Training_Class, optional
        The Training_Class where the training information is stored.
    file_name : str, default = 'Report'
        The name of the report file.
    remove_files : bool, default = False
        Whether or not to remove the Epochs' files after saving the last and
        best epochs' results. This may be desirable if many epochs were run.
    """
    # Opening the file
    FILE = open(file_name + '.txt', 'w')
    # Writing about the model
    FILE.write('MODEL\n'
               + '  Model: ' + Model.model_name + '\n'
               + '  Version: ' + str(Model.version) + '\n'
               + '  Dataset Format: ' + Model.dataset_format + '\n'
               )
    # Some model specificities:
    if Model.model_name == 'LSTM':
        FILE.write('  Pooling Layer: ' + Model.pooling_layer + '\n'
                   + '  Delta: ' + str(Model.delta) + '\n'
                   + "  Hidden layer dimension: " + str(Model.hidden_dim) + '\n'
                   + '  Dropout probability: ' + str(Model.dropout_value) + '\n'
                   + '  Number of linear layers: ' + str(Model.nb_layers) + '\n'
                   )
        if Model.nb_layers > 1:
            FILE.write('  Linear layer dropout:' + str(Model.lin_dropout_fn.p)
                       )
    elif Model.model_name == 'FFNN':
        FILE.write("  Number of k-means' clusters: " + str(Model.nb_clusters) + '\n'
                   + "  Hidden layer dimension: " + str(Model.hidden_dim) + '\n'
                   + "  Dropout probability: " + str(Model.dropout.p) + '\n'
                   )
    # Writing about the training:
    if Training_info is not None:
        FILE.write('\n'
                   + 'TRAINING\n'
                   + '  Number of Epochs: ' + str(Training_info.nb_epochs) + '\n'
                   + '  Learning rate: ' + str(Training_info.learning_rate) + '\n'
                   + '  Batch size: ' + str(Training_info.batch_size) + '\n'
                   + '  Epoch with smallest validation loss: ' + str(Training_info.best_epoch.nb) + '\n'
                   + '    (Epoch number starting at 0)' + '\n'
                   + '    Training loss: ' + str(Training_info.best_epoch.train_loss.item()) + '\n'
                   + '    Validation loss: ' + str(Training_info.best_epoch.valid_loss.item()) + '\n'
                   + "  Last epoch's loss:" + '\n'
                   + '    Training loss:' + str(Training_info.epoch[-1].train_loss.item()) + '\n'
                   + '    Validation loss: ' + str(Training_info.epoch[-1].valid_loss.item()) + '\n'
                   + '  Loss function: ' + str(Training_info.loss_fn) + '\n'
                   + '  Optimizer: ' + str(Training_info.optimizer) + '\n'
                   )
    # Writing about the training dataset
    FILE.write('\n'
               + 'TRAINING DATASET\n'
               + '  Batch size: ' + str(train_loader.batch_size) + '\n'
               + '  Size of the training set: ' + str(len(train_loader.dataset)) + '\n'
               # + '  Outlier zlim: ' + str(train_loader.dataset.z_lim) + '\n'
               # + '  Current cut-off (charge cycle): ' + str(train_loader.dataset.i_cutoff) + '\n'
               + '  Threshold (minimum capacity): ' + str(train_loader.dataset.threshold) + '\n'
               + '  Minimum length: ' + str(train_loader.dataset.min_len) + '\n'
               )
    # Model specificities:
    if Model.model_name == 'FFNN':
        FILE.write('  Normalized histograms: ' + str(train_loader.dataset.mean_hist) + '\n'
                   )

        if train_loader.dataset.resampling:
            FILE.write('  Resampling time step: ' + str(train_loader.dataset.time_step) + '\n'
                       )
        else:
            FILE.write('  Resampling: False' + '\n'
                       )
    elif Model.model_name == 'LSTM':
        FILE.write('  Resampling time step: ' + str(train_loader.dataset.time_step) + '\n'
                   + '  Fixed Length: ' + str(train_loader.dataset.fixed_len) + '\n'
                   )
    FILE.write('  Batteries used: ' + str(train_loader.dataset.batteries) + '\n'
               )
    FILE.write('  Normalization information: ' + str(train_loader.dataset.norm) + '\n'
               )
    # Writing about the validation dataset information
    FILE.write('\n'
               + 'VALIDATION DATASET\n'
               + '  Batch Size: ' + str(valid_loader.batch_size) + '\n'
               + '  Size of the validation set: ' + str(len(valid_loader.dataset)) + '\n'
               # + '  Outlier zlim: ' + str(valid_loader.dataset.z_lim) + '\n'
               # + '  Current cut-off (charge cycle): ' + str(valid_loader.dataset.i_cutoff) + '\n'
               + '  Threshold (minimum capacity): ' + str(valid_loader.dataset.threshold) + '\n'
               + '  Minimum length: ' + str(valid_loader.dataset.min_len) + '\n'
               + '  Batteries used: ' + str(valid_loader.dataset.batteries) + '\n'
               + '  Normalization information: ' + str(valid_loader.dataset.norm) + '\n'
               )
    # Closing the file:
    FILE.close()

    # Saving the Training information class
    with open('Training_Info.txt', 'wb') as f:
        pickle.dump(Training_info, f)

    # Saving the datasets: (they were too big so I've disabled this option)
    # with open('train_dataset.txt', 'wb') as f:
    #     pickle.dump(train_loader.dataset, f)
    #
    # with open('valid_dataset.txt', 'wb') as f:
    #     pickle.dump(valid_loader.dataset, f)

    # Saving the last epoch's model:
    with open('base_model.txt', 'wb') as f:
        Model = Model.to('cpu')
        pickle.dump(Model, f)

    # Saving the best epoch's model:
    try:
        os.chdir('.//Epochs')
        Model.load_state_dict(th.load('Epoch_'+str(Training_info.best_epoch.nb + 1)+'.txt',
                                      map_location=th.device('cpu')))
        # Now that we have already loaded the best epoch's state_dict(), we can delete the epochs' files
        if remove_files:            # If we want to
            files = os.listdir()    # All files in the folder
            for file_name in files:
                match = re.search(r'Epoch_\d{1}', file_name)  # Only take into account the Epoch ones (just to be safe)
                # the battery files
                if match == None:
                    continue
                else:
                    os.remove(file_name)
            os.chdir('../')
            os.rmdir('Epochs')  # Remove the empty directory altogether
        else:
            os.chdir('../')

        # Save the model with the best epoch's state_dict()
        with open('best_model.txt', 'wb') as f:
            pickle.dump(Model, f)

    except FileNotFoundError:
        print('Epochs folder not created.')

class Training_Class:
    """
    This class was created to keep track of training and epoch information.

    Parameters
    ----------
    N_epochs : int
        Number of epochs in the training.
    loss_fn : a torch.nn. Loss function
        The loss function used in training.
    optimizer : a torch.nn Optimizer
        The optimizer used in training.
    learning_rate : float
        Training learning_rate.
    batch_size : int
        Training batch_size.

    Attributes
    ----------
    nb_epochs : int
        The number of epochs.
    loss_fn : a torch.nn. Loss function
        The loss function used in training.
    optimizer : a torch.nn Optimizer
        The optimizer used in training.
    learning_rate : float
        Training learning_rate.
    batch_size : int
        Training batch_size.
    epoch : list[Epoch_Class]
        A list of all of the training epochs. To add an epoch to this list, the
        method ``add_epoch`` is used. See Epoch_Class for more.
    best_epoch : Epoch_Class
        Epoch information about the best epoch. It is defined through the method
        ``best_epoch``.
    train : list[Training_Class]
        A list with the information of the previous times the model has been
        trained. To add a Training_Class to the list, the method ``add_epoch``
        is used.
    """
    def __init__(self, N_epochs, loss_fn, optimizer, learning_rate, batch_size):

        self.nb_epochs = N_epochs
        self.loss_fn = loss_fn
        self.optimizer = optimizer
        self.learning_rate = learning_rate
        self.batch_size = batch_size
        self.epoch = []
        self.trains = []

    def add_epoch(self, train_loss, valid_loss, nb):
        """
        Adds an epoch to self.epoch list. The epoch itself is a separate
        Epoch_Class object.

        Parameters
        ----------
        train_loss : th.Tensor
            The training dataset loss.
        valid_loss : th.Tensor
            The validation dataset loss.
        nb : int
            The epoch's number
        """

        self.epoch.append(Epoch_Class(train_loss, valid_loss, nb))

    def best_epoch_method(self):
        """
        Calculates which epoch has the lowest validation loss and defines it
        as the attribute best_epoch (Epoch_Class).
        """

        if len(self.epoch) != 0:
            valid_loss = th.zeros(len(self.epoch))

            for i in range(len(self.epoch)):
                valid_loss[i] = self.epoch[i].valid_loss

            nb = th.argmin(valid_loss).item()
            self.best_epoch = Epoch_Class(self.epoch[nb].train_loss, self.epoch[nb].valid_loss, nb)

        else:
            print('No epochs saved')

    def add_train(self, Training_info):
        """
        Stores information from a past training when a model has already been
        trained before.
        Not very useful and not very well implemented, works just as a history
        and is very rarely used.

        Parameters
        ----------
        Training_info : Training_Class
            The training information we are aggregating.
        """
        self.trains.append(Training_info)


class Epoch_Class:
    """
    A class for keeping track of the training epochs.

    Parameters
    ----------
    train_loss : th.Tensor
        The training dataset loss.
    valid_loss : th.Tensor
        The validation dataset loss.
    nb : int
        The epoch's number
    """

    def __init__(self, train_loss, valid_loss, nb=None):
        self.train_loss = train_loss
        self.valid_loss = valid_loss
        self.nb = nb
