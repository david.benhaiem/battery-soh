from Nasa_Classes import *
import torch as th
import pandas as pd
from PyAstronomy import pyasl
import sys
import matplotlib.pyplot as plt
from scipy import stats

def clean(Cycle, time_step=30, i_cutoff=2e-2, min_len=1, resampling=True):
    """This function serves to clean charge and discharge data by taking away
    the first and last points (when the current value is being adjusted), as
    well as resampling the data to a given time_step.

    Inputs:
        Cycle           : The cycle we want to treat [Nasa_Cycle object]
                            See Nasa_Classes.py for more information
        time_step       : The time step (in seconds) for the new resampling {s} [int]
        i_cutoff        : The cut-off voltage value that determines the end of the charge
                          cycle {A} [float]
        min_len         : The minimum sequence length acceptable. If the final resampled
                          curve has less than min_len points, it's "discarded": None is
                          returned for all of the tensors (which tells other codes to skip
                          the curve in question). [int]
                            It has to be greater or equal to one, otherwise problems may
                            arise from tensors of length 0 (though rare).
        resampling      : Determines whether or not to resample the data.
                            Resampling may not be done for the k-means approach, although
                            this is still in consideration.

    Outputs:
        Cycle_t         : The cycle's time stamps after resampling and cleaning {s} [numpy array]
        Cycle_i         : The cycle's current (the physical property) values after resampling
                          and cleaning {A} [numpy array]
        Cycle_v         : The cycle's voltage valuess after resampling and cleaning {V} [numpy array]
        Cycle_T         : The cycle's temperaturee after resampling and cleaning {ºC} [numpy array]
    """

    assert Cycle.type != 'impedance', 'Only charge and discharge cycles are allowed.'
    assert min_len >= 1, 'min_len must be greater or equal to 1'

    # print(Cycle.number)

    # Numpy arrays
    Cycle_t = Cycle.data.time       # Time data
    Cycle_i = Cycle.data.meas_i     # Current data
    Cycle_v = Cycle.data.meas_v     # Voltage data
    Cycle_T = Cycle.data.meas_t     # Temperature data

    # Taking away outliers that may mess up with the rest of the cleaning:
    Cycle_t, Cycle_i, Cycle_v, Cycle_T = Input_outliers(Cycle_t, Cycle_i, Cycle_v, Cycle_T,)

    if Cycle.type == 'charge':
        # Cutting out the beginning of the CCCV curve
        try:
            idx, _ = np.where(Cycle_i > 0.90 * max(Cycle_i))
            idx_1 = idx[0]  # Taking the very first value that is > 0.9*max(Cycle_i)
            # We check where the current is > 0.9*max(i), then we take away everything that comes before it,
            # which corresponds to the part where the current hasn't yet stabilized at the CC value.
        except:
            # Just in case, I don't think this is even mathematically possible.
            return None, None, None, None

        # Now cutting the final part of the charge curve
        if Cycle_i[-1, 0] <= i_cutoff:  # If the current reaches the cut-off value:
            idx, _ = np.where(Cycle_i[idx_1:, :] < i_cutoff)        # Then check when it becomes less than it
            idx_2 = idx[0] + idx_1                                  # And save the first value
            # Notice that we add idx_1 because we called np.where for Cycle_i[idx_1:, :] and not simply Cycle_i
            # This is because there may be points at the beginning of the curve that satisfy the condition
        else:   # If the current never reaches the cut-off value, then:
            idx_2 = len(Cycle_i)    # idx_2 is the last point in Cycle_i

    elif Cycle.type == 'discharge':
        # (!!!) DISCLAIMER: this fisrt part only works for CC discharge, and will have to be adapted for more complex
        # discharge cycles. The discharge current must also be negative.
        # We start by taking away the first few points, where the current hasn't yet reached its max (min because
        # it's negative) value.
        try:    # Analogous to the charge cycle.
            idx, _ = np.where(Cycle_i < 0.90 * min(Cycle_i))
            idx_1 = idx[0]
        except:
            # Just in case, I don't think this is even mathematically possible.
            return None, None, None, None

        # Here we cut off the last bit of the curve where the current is yet again lifted
        # It has been written in function of a Voltage cut-off instead, since this is more general and should
        # work for more complex discharge cycles (to be studied).
        # It definitely works for harmonic discharge cycles
        if Cycle_v[-1, 0] >= min(Cycle_v):  # Analogous to the charge cycle.
            idx, _ = np.where(Cycle_v[idx_1:, :] <= min(Cycle_v))
            idx_2 = idx[0] + idx_1
        else:
            idx_2 = len(Cycle_v)

    # Taking only the relevant data
    Cycle_t = Cycle_t[idx_1:idx_2, :]
    Cycle_i = Cycle_i[idx_1:idx_2, :]
    Cycle_v = Cycle_v[idx_1:idx_2, :]
    Cycle_T = Cycle_T[idx_1:idx_2, :]

    # Resampling
    if resampling:
        Cycle_t, Cycle_i, Cycle_v, Cycle_T = Resampling(Cycle_t, Cycle_i, Cycle_v, Cycle_T, time_step)

    # Checking if there are sill any NaNs after resampling interpolation:
    if np.any(np.isnan(np.reshape(Cycle_i, -1))).item():
        if resampling:
            Nb_i = len(np.nonzero(np.isnan(np.reshape(Cycle_i, -1))))
            Nb_v = len(np.nonzero(np.isnan(np.reshape(Cycle_i, -1))))
            Nb_T = len(np.nonzero(np.isnan(np.reshape(Cycle_i, -1))))
            print('Cycle number: ', Cycle.number)
            print(Nb_i, ' NaNs in I')
            print(Nb_v, ' NaNs in V')
            print(Nb_T, ' NaNs in T')
            return None, None, None, None
        else:
            # If there hasn't been any resampling, then we'll just take away any left-over NaNs in the data.
            # Since the overwhelming majority of the points is ok, this shouldn't bring any problems.
            Cycle_t = Cycle_t[~np.isnan(Cycle_t)]
            Cycle_i = Cycle_i[~np.isnan(Cycle_i)]
            Cycle_v = Cycle_v[~np.isnan(Cycle_v)]
            Cycle_T = Cycle_T[~np.isnan(Cycle_T)]

    if len(Cycle_t) < min_len: # If the Cycle is too short, than we'll return None to skip this cycle.
        return None, None, None, None

    return Cycle_t, Cycle_i, Cycle_v, Cycle_T


# def clean_test(Cycle, time_step=30, i_cutoff=2e-2, min_len=1, resampling=True):
#     """This function serves to clean charge and discharge data by taking away
#     the first and last points (when the current value is being adjusted), as
#     well as resampling the data to a given time_step.
#
#     Inputs:
#         Cycle           : The cycle we want to treat [Nasa_Cycle object]
#                             See Nasa_Classes.py for more information
#         time_step       : The time step (in seconds) for the new resampling {s} [int]
#         i_cutoff        : The cut-off voltage value that determines the end of the charge
#                           cycle {A} [float]
#         min_len         : The minimum sequence length acceptable. If the final resampled
#                           curve has less than min_len points, it's "discarded": None is
#                           returned for all of the tensors (which tells other codes to skip
#                           the curve in question). [int]
#                             It has to be greater or equal to one, otherwise problems may
#                             arise from tensors of length 0 (though rare).
#         resampling      : Determines whether or not to resample the data.
#                             Resampling may not be done for the k-means approach, although
#                             this is still in consideration.
#
#     Outputs:
#         Cycle_t         : The cycle's time stamps after resampling and cleaning {s} [numpy array]
#         Cycle_i         : The cycle's current (the physical property) values after resampling
#                           and cleaning {A} [numpy array]
#         Cycle_v         : The cycle's voltage valuess after resampling and cleaning {V} [numpy array]
#         Cycle_T         : The cycle's temperaturee after resampling and cleaning {ºC} [numpy array]
#     """
#
#     assert Cycle.type != 'impedance', 'Only charge and discharge cycles are allowed.'
#     assert min_len >= 1, 'min_len must be greater or equal to 1'
#
#     # print(Cycle.number)
#
#     # Numpy arrays
#     Cycle_t = Cycle.data.time       # Time data
#     Cycle_i = Cycle.data.meas_i     # Current data
#     Cycle_v = Cycle.data.meas_v     # Voltage data
#     Cycle_T = Cycle.data.meas_t     # Temperature data
#
#     # Taking away outliers that may mess up with the rest of the cleaning:
#     df = Input_outliers(Cycle_t, Cycle_i, Cycle_v, Cycle_T,)
#
#     if Cycle.type == 'charge':
#         # Cutting out the beginning of the CCCV curve
#         idx_1 = df.index[df['Current'] > 0.9*df['Current'].max()].tolist()[0]
#         df = df[idx_1:]
#
#         # Now cutting the final part of the charge curve
#         # if Cycle_i[-1, 0] <= i_cutoff:  # If the current reaches the cut-off value:
#         print(len(df))
#         if df['Current'][len(df) - 1] <= i_cutoff:  # If the current reaches the cut-off value:
#             print('Hi')
#             idx_2 = df.index[df['Current'] < i_cutoff].tolist()[0]
#             df = df[:idx_2]
#
#     elif Cycle.type == 'discharge':
#         # (!!!) DISCLAIMER: this fisrt part only works for CC discharge, and will have to be adapted for more complex
#         # discharge cycles. The discharge current must also be negative.
#         # We start by taking away the first few points, where the current hasn't yet reached its max (min because
#         # it's negative) value.
#         # Analogous to the charge cycle.
#         idx_1 = df.index[df['Current'] < 0.9 * df['Current'].min()].tolist()[0]
#         df = df[idx_1:]
#
#         # Here we cut off the last bit of the curve where the current is yet again lifted
#         # It has been written in function of a Voltage cut-off instead, since this is more general and should
#         # work for more complex discharge cycles (to be studied).
#         # It definitely works for harmonic discharge cycles
#         if df['Voltage'][len(df) - 1] >= min(df['Voltage']):  # Analogous to the charge cycle.
#             idx_2 = df.index[df['Voltage'] == df['Voltage'].min()].tolist()[0]
#             df[:idx_2]
#
#     # Resampling
#     if resampling:
#         Cycle_t, Cycle_i, Cycle_v, Cycle_T = Resampling_df(df, time_step)
#
#     # Checking if there are sill any NaNs after resampling interpolation:
#     if np.any(np.isnan(np.reshape(Cycle_i, -1))).item():
#         if resampling:
#             Nb_i = len(np.nonzero(np.isnan(np.reshape(Cycle_i, -1))))
#             Nb_v = len(np.nonzero(np.isnan(np.reshape(Cycle_i, -1))))
#             Nb_T = len(np.nonzero(np.isnan(np.reshape(Cycle_i, -1))))
#             print('Cycle number: ', Cycle.number)
#             print(Nb_i, ' NaNs in I')
#             print(Nb_v, ' NaNs in V')
#             print(Nb_T, ' NaNs in T')
#             return None, None, None, None
#         else:
#             # If there hasn't been any resampling, then we'll just take away any left-over NaNs in the data.
#             # Since the overwhelming majority of the points is ok, this shouldn't bring any problems.
#             Cycle_t = Cycle_t[~np.isnan(Cycle_t)]
#             Cycle_i = Cycle_i[~np.isnan(Cycle_i)]
#             Cycle_v = Cycle_v[~np.isnan(Cycle_v)]
#             Cycle_T = Cycle_T[~np.isnan(Cycle_T)]
#
#     if len(Cycle_t) < min_len: # If the Cycle is too short, than we'll return None to skip this cycle.
#         return None, None, None, None
#
#     return Cycle_t, Cycle_i, Cycle_v, Cycle_T
#
# def Input_outliers(Cycle_t, Cycle_i, Cycle_v, Cycle_T, window=10):
#
#     # Concatenating the time and the data
#     Cat_Data = np.concatenate((Cycle_t, Cycle_i, Cycle_v, Cycle_T), axis=1)  # Cat stands for concatenated
#
#     # Creating a data frame
#     df = pd.DataFrame(data=Cat_Data,
#                       columns=['Time', 'Current', 'Voltage', 'Temperature'])
#
#     df_stats = pd.DataFrame(columns=['meani', 'stdi', 'meanv', 'stdv', 'meant', 'stdt'])
#
#
#     df_stats['meani'] = df['Current'].rolling(window).mean()
#     df_stats['stdi'] = df['Current'].rolling(window).std()
#     df_stats['meanv'] = df['Voltage'].rolling(window).mean()
#     df_stats['stdv'] = df['Voltage'].rolling(window).std()
#     df_stats['meant'] = df['Temperature'].rolling(window).mean()
#     df_stats['stdt'] = df['Temperature'].rolling(window).std()
#
#     df_stats = df_stats.shift(-(window//2), fill_value=0)
#     df_stats['meani'][len(df_stats) - (window // 2):] = df_stats['meani'][len(df_stats) - 1 - (window//2)]
#     df_stats['stdi'][len(df_stats) - (window // 2):] = df_stats['stdi'][len(df_stats) - 1 - (window//2)]
#     df_stats['meani'][0:window//2] = df_stats['meani'][window//2]
#     df_stats['stdi'][0:window//2] = df_stats['stdi'][window//2]
#     # df_stats['meani'][0:window] = df_stats['meani'][window]
#     # df_stats['stdi'][0:window] = df_stats['stdi'][window]
#
#     df_stats['meanv'][len(df_stats) - (window // 2):] = df_stats['meanv'][len(df_stats) - 1 - (window//2)]
#     df_stats['stdv'][len(df_stats) - (window // 2):] = df_stats['stdv'][len(df_stats) - 1 - (window//2)]
#     df_stats['meanv'][0:window//2] = df_stats['meanv'][window//2]
#     df_stats['stdv'][0:window//2] = df_stats['stdv'][window//2]
#     # df_stats['meanv'][0:window] = df_stats['meanv'][window]
#     # df_stats['stdv'][0:window] = df_stats['stdv'][window]
#
#     df_stats['meant'][len(df_stats) - (window // 2):] = df_stats['meant'][len(df_stats) - 1 - (window//2)]
#     df_stats['stdt'][len(df_stats) - (window // 2):] = df_stats['stdt'][len(df_stats) - 1 - (window//2)]
#     df_stats['meant'][0:window//2] = df_stats['meant'][window//2]
#     df_stats['stdt'][0:window//2] = df_stats['stdt'][window//2]
#     # df_stats['meant'][0:window] = df_stats['meant'][window]
#     # df_stats['stdt'][0:window] = df_stats['stdt'][window]
#
#     Outliers_i = (df.Current <= df_stats['meani'] + 2 * df_stats['stdi']) & (df.Current >= df_stats['meani'] - 2 * df_stats['stdi']).to_numpy()
#     Outliers_v = (df.Voltage <= df_stats['meanv'] + 2 * df_stats['stdv']) & (df.Voltage >= df_stats['meanv'] - 2 * df_stats['stdv']).to_numpy()
#     Outliers_T = (df.Temperature <= df_stats['meant'] + 2 * df_stats['stdt']) & (df.Temperature >= df_stats['meant'] - 2 * df_stats['stdt']).to_numpy()
#
#     Outliers = Outliers_i & Outliers_v & Outliers_T
#
#     df = df[Outliers]
#     plt.plot(df['Current'])
#     plt.show()
#
#     return df

def Input_outliers(Cycle_t, Cycle_i, Cycle_v, Cycle_T):

    outlier_set = set()
    outliers_i = Outliers_idx(Cycle_i)
    outliers_v = Outliers_idx(Cycle_v)
    outliers_T = Outliers_idx(Cycle_T)

    outlier_set = outlier_set.union(outliers_i, outliers_v, outliers_T)
    # outlier_list = list(outliers_i)
    outlier_list = list(outlier_set)
    outlier_list.sort(reverse=True)
    outlier_array = np.ones(len(Cycle_t))
    for i in outlier_list:
        outlier_array[i] = 0
    outlier_array = (outlier_array == 1)
    Cycle_t = Cycle_t[outlier_array]
    Cycle_i = Cycle_i[outlier_array]
    Cycle_v = Cycle_v[outlier_array]
    Cycle_T = Cycle_T[outlier_array]

    return Cycle_t, Cycle_i, Cycle_v, Cycle_T

def Resampling(Time, Current, Voltage, Temperature, time_step=30):
    """ This function serves to resample the current and voltage data for a desired time step.

    Inputs:
        Time            : Time tensor in seconds. [numpy array]
        Current         : Current tensor. [numpy array]
        Voltage         : Voltage tensor. [numpy array]
        Temperature     : Temperature tensor. [numpy array]
        time_step       : The desired time_step for resampling. [int]

    Outputs:
        New_Time        : The new, resampled, time tensor. [numpy array]
        New_Current     : The new, resampled, Current tensor. [numpy array]
        New_Voltage     : The new, resampled, Voltage tensor. [numpy array]
        New_Temperature : The new, resampled, Temperature tensor. [numpy array]
    """

    # Concatenating the time and the data
    Cat_Data = np.concatenate((Time, Current, Voltage, Temperature), axis=1)     # Cat stands for concatenated

    # Creating a data frame
    df = pd.DataFrame(data=Cat_Data,
                      columns=['Time', 'Current', 'Voltage', 'Temperature'])

    # Checking if the time_step is greater than the biggest sampling step:
    df['delta_time'] = df['Time'].diff()    # The time difference between each step
    # This check is done in order to avoid the creation of NaN entries.
    if df['delta_time'].max() > time_step:  # If the biggest sample time step is greater than the time_step
        print('time_step too small',        # A Warning, this will not stop the code
              str(df['delta_time'].max()), '>', str(time_step))

    # Transforming the time data in datetime form:
    df['Time'] = pd.to_datetime(df['Time']*1e9)     # This line is 10 times faster than the next two (literally) sorry
    # df['Time'] = df['Time'].apply(  # Telling the data frame that the time is in seconds
    # lambda t: pd.to_datetime('1970-01-01') + pd.Timedelta(seconds=t))

    df.set_index('Time', inplace=True)
    # Taking any NaN's away:
    df = df.dropna()
    # Resampling
    df = df.resample(str(time_step)+'s').mean()

    # Saving the new data in pytorch tensor form
    New_Time = (df.index.astype('int64').to_numpy()/1e9)   # Time (in seconds)
    New_Current = (df['Current'].to_numpy())               # Current
    New_Voltage = (df['Voltage'].to_numpy())               # Voltage
    New_Temperature = (df['Temperature'].to_numpy())       # Temperature

    return New_Time, New_Current, New_Voltage, New_Temperature

def Resampling_df(df, time_step=30):
    """ This function serves to resample the current and voltage data for a desired time step.

    Inputs:
        Time            : Time tensor in seconds. [numpy array]
        Current         : Current tensor. [numpy array]
        Voltage         : Voltage tensor. [numpy array]
        Temperature     : Temperature tensor. [numpy array]
        time_step       : The desired time_step for resampling. [int]

    Outputs:
        New_Time        : The new, resampled, time tensor. [numpy array]
        New_Current     : The new, resampled, Current tensor. [numpy array]
        New_Voltage     : The new, resampled, Voltage tensor. [numpy array]
        New_Temperature : The new, resampled, Temperature tensor. [numpy array]
    """

    # Checking if the time_step is greater than the biggest sampling step:
    df['delta_time'] = df['Time'].diff()    # The time difference between each step
    # This check is done in order to avoid the creation of NaN entries.
    if df['delta_time'].max() > time_step:  # If the biggest sample time step is greater than the time_step
        print('time_step too small',        # A Warning, this will not stop the code
              str(df['delta_time'].max()), '>', str(time_step))

    # Transforming the time data in datetime form:
    df['Time'] = pd.to_datetime(df['Time']*1e9)     # This line is 10 times faster than the next two (literally) sorry
    # df['Time'] = df['Time'].apply(  # Telling the data frame that the time is in seconds
    # lambda t: pd.to_datetime('1970-01-01') + pd.Timedelta(seconds=t))

    df.set_index('Time', inplace=True)
    # Taking any NaN's away:
    df = df.dropna()
    # Resampling
    df = df.resample(str(time_step)+'s').mean()

    # Saving the new data in pytorch tensor form
    New_Time = (df.index.astype('int64').to_numpy()/1e9)   # Time (in seconds)
    New_Current = (df['Current'].to_numpy())               # Current
    New_Voltage = (df['Voltage'].to_numpy())               # Voltage
    New_Temperature = (df['Temperature'].to_numpy())       # Temperature

    return New_Time, New_Current, New_Voltage, New_Temperature

def Outliers(inputs, output, z_lim=3):
    """Removes outliers in the battery output capacity data.

    As of know, it removes outliers based on moving window averages (generalized Extreme
    Studentized Deviate (ESD) test - implemented through PyAstronomy's pyasl.pointDistGESD)
    and on the total average (by taking away any remaining points with a z-score > z_lim).
        Where z_score_i = x_i - mean(x)/std(x)

    Inputs:
        inputs      : the inputs list. [list of torch tensors]
        output      : the output list. [list of numpy arrays]
        z_lim       : the maximum zscore allowed for the last step. [float]

    Outputs:
        inputs      : the normalized inputs. [list of torch tensors]
        output      : the normalized output. [list of floats]
    """

    # print(type(output), len(output))
    # Turning the output list into a numpy array:
    output = np.array(output)
    # print(type(output), len(output))

    # Taking away local outliers (based on moving windows)
    r = pyasl.pointDistGESD(output, 10, alpha=0.01)
    outlier_array = np.ones(len(output))

    r[1].sort(reverse=True)             # We have to sort the r[1] list for the next codes to work
    for i in range(len(r[1])):
        outlier_array[r[1][i]] = 0      # Array for the outlier indexes
        inputs.pop(r[1][i])             # Updating the inputs (taking away the outliers)
        # That's why sorting r[1] in reverse order is important. Otherwise, the indexes of the other inputs
        # would change as we starting deleting them.

    outlier_array = outlier_array == 1  # boolean array where True means the point is not an Outlier.
    output = output[outlier_array]      # Updating the outputs (only the non-outliers)

    # Taking away any bigger outliers that may have standed the first test:
    outlier_array = (np.abs(stats.zscore(output)) < z_lim).all(axis=1)  # We check which points have a z_score < z_lim
    outlier_index = np.flip(np.where(outlier_array == False)[0])        # Where the outliers are (z_score > z_lim)

    while len(outlier_index) > 0:
        for i in range(len(outlier_index)):
            inputs.pop(outlier_index[i])        # updating the inputs
        output = output[outlier_array]          # updating the outputs
        # Restarting to see how it goes:
        outlier_array = (np.abs(stats.zscore(output)) < z_lim).all(axis=1)
        outlier_index = np.flip(np.where(outlier_array == False)[0])  # where the outliers are

    output = th.from_numpy(output).float().tolist()     # Turning the numpy array to a list
    # I don't remember why I had to pass by Pytorch before doing so
    # I think there's a detail that doesn't work otherwise

    return inputs, output


def Outliers_idx(inputs):
    """Removes outliers in the battery output capacity data.

    As of know, it removes outliers based on moving window averages (generalized Extreme
    Studentized Deviate (ESD) test - implemented through PyAstronomy's pyasl.pointDistGESD)
    and on the total average (by taking away any remaining points with a z-score > z_lim).
        Where z_score_i = x_i - mean(x)/std(x)

    Inputs:
        inputs      : the inputs list. [list of torch tensors]
        output      : the output list. [list of numpy arrays]
        z_lim       : the maximum zscore allowed for the last step. [float]

    Outputs:
        inputs      : the normalized inputs. [list of torch tensors]
        output      : the normalized output. [list of floats]
    """

    try:
        # Taking away local outliers (based on moving windows)
        r = pyasl.pointDistGESD(inputs, 5, alpha=0.01)
    except ZeroDivisionError:
        r = [0, 1]
        r[1] = [i for i in range(len(inputs))]

    return r[1]

def Normalize(inputs, output, norm_type='zscore', norm=None, concatenate=False):
    """Normalizes both the inputs and outputs based either on z-score standardization
    or on min-max normalization.

    Inputs:
        inputs      : the inputs list. [list of torch tensors]
        output      : the output list. [list of floats]
        norm_type   : the type of normalization. [string]
                        As of now, can either be 'zscore' or 'minmax'.
        norm        : normalization information carried over from another normalization. [tuple]
                        norm[4] is a string indicating the norm type [string]
                        if norm[4] is 'zscore', then:
                            norm[0] and norm[1] are the inputs' mean and standard deviation [torch tensors]
                            norm[2] and norm[3] are the output's mean and standard deviation [torch tensors]
                        if norm[4] is 'minmax', then:
                            norm[0] and norm[1] are the inputs' min and max values [torch tensors]
                            norm[2] and norm[3] are the output's min and max values [torch tensors]
        concatenate : boolean variable that indicates whether or not we can concatenate the inputs.
                        This is best for performance, since the normalization can be done all at once,
                        but is impossible when sequences have different sizes, and has to be done one
                        example at a time.
                        This is still being developed and will be improved for the LSTM.
                            This also changes the type of the 'inputs' output (see the Output part below)

    Output:
        inputs      : the normalized inputs [list of torch tensors or torch tensor]
                        if concatenate = True, then it's a simple [torch tensor].
                        This does not cause any problems later on because of how the torch dataset works,
                        and this is the last step.
        output_stack: the normalized outputs [torch tensor]
        norm        : normalization information that can be used to normalize other
                      data. [tuple]

    """

    # Stacking inputs. This can be done for calculating the normalization information but the actual normalization
    # may have to be done case by case (concatenate = False).
    inputs_stack = th.cat(inputs, dim=0)
    # Stacking outputs.
    output_stack = th.FloatTensor(output)

    if norm is not None:        # If a norm tuple has been specified
        norm_type = norm[4]     # The norm_type is overridden by the one contained in the norm.

    if norm_type == 'zscore':
        if norm is None:        # If no norm tuple has been specified

            # Checking if there are entries for which the standard deviation
            # is zero:
            std_in = th.std(inputs_stack, dim=0)    # inputs
            bool_std = (std_in == 0)                # if yes, then:
            std_in[bool_std] = 1                    # Those entries are set to 1, in order to avoid division by zero.

            std_out = th.std(output_stack, dim=0)   # outputs
            bool_std = (std_out == 0)               # Analogously
            std_out[bool_std] = 1

            norm = (th.mean(inputs_stack, dim=0), std_in,               # creation of the norm tuple
                    th.mean(output_stack, dim=0), std_out, norm_type)

        output_stack = (output_stack - norm[2])/norm[3]                 # output normalization

        if concatenate:     # If we can concatenate, then:
            inputs = (inputs_stack - norm[0]) / norm[1]     # the inputs are overriden by the normalized input stack
        else:
            for i in range(len(inputs)):    # Unfortunately this is necessary for the LSTM if the sequences are of
                                            # different lengths.
                inputs[i] = (inputs[i] - norm[0]) / norm[1]

    elif norm_type == 'minmax':

        if norm is None: # no norm tuple has been specified.
            min_in = th.amin(inputs_stack, dim=0)
            max_in = th.amax(inputs_stack, dim=0)
            min_out = th.amin(output_stack, dim=0)
            max_out = th.amax(output_stack, dim=0)

            assert ~(min_in == max_in).all(), "Constant inputs"     # asserting that the inputs are not all constant.
            assert ~(min_out == max_out), "Constant output"         # asserting that the output is not constant

            bool_minmax = (min_in == max_in)    # Some inputs can, however, be constant. This is specially true for
            min_in[bool_minmax] = 1             # the k-means approach, where some batteries may have 0 points at some
            max_in[bool_minmax] = 2             # clusters. In that case, (max - min) is set to 1 to avoid division by 0

            norm = (min_in, max_in,
                    min_out, max_out, norm_type)    # creation of the norm tuple

        output_stack = (output_stack - norm[2])/(norm[3] - norm[2])     # output normalization

        if concatenate:     # Analogous to the z-score normalization
            inputs = (inputs_stack - norm[0]) / (norm[1] - norm[0])
        else:
            for i in range(len(inputs)):
                inputs[i] = (inputs[i] - norm[0])/(norm[1] - norm[0])

    return inputs, output_stack, norm

if __name__ == '__main__':

    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    os.chdir('.//Separate_pickles')

    ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt'] # Files to be ignored.