from Nasa_Classes import *
import matplotlib.pyplot as plt

def watch(file_name, type, extra=False, stop=False, dt = 0.1, fixed_lims=True):
    """Function for watching the evolution of I, V curves. Either for charge or discharge.
        (the type is chosen through the variable type which can be either 'charge' or 'discharge')

    The correct folder must be chosen through os.chdir(...) before the function is called with the
    file name.

    The voltage information is plotted in red, whilst the current in blue. By default, only
    measured values are plotted. Load values can be plotted in the same graph by setting extra = True.

    The time each image is shown is dt = 0.1. To stop continuous view, set stop = True.
        Then, the next image will only be shown after the first one is closed.

    Variables:
    file_name           : name of the file containing the Dataset_Nasa class with the battery data.
    type                : either 'charge' or 'discharge', to chose which cycle to watch.
    extra               : if load values should also be plotted. This can clog the image a little.
    stop                : stop continuous viewing and see each image one by one.
    dt                  : the time interval each image is shown when stop = False
    fixed_lims          : fixes the time interval. Better for visualizing evolution, but the intervals
                          depends on the battery, so it may not always be optimal. The values here were
                          picked from battery 29 ('B0029.txt') and extrapolated for discharge.
    """
    with open(file_name,'rb') as f:
        B0029 = pickle.load(f)

    size = len(B0029.cycle)

    if type == 'charge':
        for i in range(size):
            if B0029.cycle[i].type == 'charge':
                fig, ax1 = plt.subplots()
                color = 'tab:red'
                ax1.plot(B0029.cycle[i].data.time, B0029.cycle[i].data.meas_v, color=color)
                if extra:
                    ax1.plot(B0029.cycle[i].data.time, B0029.cycle[i].data.charge_v, color='firebrick')
                ax1.tick_params(axis='y', labelcolor=color)
                if fixed_lims:
                    ax1.set(xlim = (-5, 1e4))
                ax1.set(ylim=(1.7, 5))
                ax2 = ax1.twinx()

                color = 'tab:blue'
                ax2.plot(B0029.cycle[i].data.time, B0029.cycle[i].data.meas_i, color=color)
                if extra:
                    ax2.plot(B0029.cycle[i].data.time, B0029.cycle[i].data.charge_i, color='royalblue')
                ax2.tick_params(axis='y', labelcolor=color)
                if fixed_lims:
                    ax2.set(xlim=(-5, 1e4))
                ax2.set(ylim=(0, 2))
                fig.tight_layout()
                plt.title('Charge cycle '+str(i))
                if stop:
                    plt.show()
                else:
                    plt.show(block = False)
                    plt.pause(dt)
                    plt.close(fig)
            else:
                continue

    elif type == 'discharge':
        for i in range(size):
            if B0029.cycle[i].type == 'discharge':
                fig, ax1 = plt.subplots()
                color = 'tab:red'
                ax1.plot(B0029.cycle[i].data.time, B0029.cycle[i].data.meas_v, color=color)
                if extra:
                    ax1.plot(B0029.cycle[i].data.time, B0029.cycle[i].data.load_v, color='firebrick')
                ax1.tick_params(axis='y', labelcolor=color)
                if fixed_lims:
                    ax1.set(xlim=(-5, 4*1800/max(-B0029.cycle[i].data.meas_i)))
                ax1.set(ylim=(0, 5))
                ax2 = ax1.twinx()

                color = 'tab:blue'
                ax2.plot(B0029.cycle[i].data.time, B0029.cycle[i].data.meas_i, color=color)
                if extra:
                    ax2.plot(B0029.cycle[i].data.time, -B0029.cycle[i].data.load_i, color='royalblue')
                ax2.tick_params(axis='y', labelcolor=color)
                if fixed_lims:
                    ax2.set(xlim=(-5, 4*1800/max(-B0029.cycle[i].data.meas_i)))
                ax2.set(ylim=(-4.5, 0.5))
                fig.tight_layout()
                plt.title('Discharge cycle '+str(i))
                if stop:
                    plt.show()
                else:
                    plt.show(block=False)
                    plt.pause(dt)
                    plt.close(fig)
            else:
                continue

if __name__ == '__main__':

    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    os.chdir('.//Separate_pickles')

    watch('B0025.txt', 'discharge')

    """
    with open('B0025.txt', 'rb') as f:
        B0029 = pickle.load(f)

    size = len(B0029.cycle)
    extra = True
    fixed_lims = False
    stop = True
    dt = 0.1

    for i in range(size):
        if B0029.cycle[i].type == 'discharge':
            fig, ax1 = plt.subplots()

            #
            color = 'tab:red'
            ax1.plot(B0029.cycle[i].data.time, B0029.cycle[i].data.meas_v, color=color)
            if extra:
                ax1.plot(B0029.cycle[i].data.time, B0029.cycle[i].data.load_v, color='firebrick')
            ax1.tick_params(axis='y', labelcolor=color)
            if fixed_lims:
                ax1.set(xlim=(-5, 4 * 1800 / max(-B0029.cycle[i].data.meas_i)))
            ax1.set(ylim=(0, 5))
            #

            #
            ax2 = ax1.twinx()
            color = 'tab:blue'
            ax2.plot(B0029.cycle[i].data.time, B0029.cycle[i].data.meas_i, color=color)
            if extra:
                ax2.plot(B0029.cycle[i].data.time, -B0029.cycle[i].data.load_i, color='royalblue')
            ax2.tick_params(axis='y', labelcolor=color)
            if fixed_lims:
                ax2.set(xlim=(-5, 4 * 1800 / max(-B0029.cycle[i].data.meas_i)))
            ax2.set(ylim=(-4.5, 0.5))
            #

            fig.tight_layout()
            plt.title('Discharge cycle ' + str(i))

            if stop:
                plt.show()
            else:
                plt.show(block=False)
                plt.pause(dt)
                plt.close(fig)
        else:
            continue
    """