from Cleaning import *
from torch.utils.data import Dataset
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans


class Torch_Dataset_KMEANS(Dataset):
    """ Creates a pytorch Dataset class that can readily be loaded with pytorch's Loader class
    for training and validation.

    The datasets created by this class' initialization follow You et al.'s * history-based approach.
    In it, the k-means method is employed to distribute (I, V, T) points into clusters. These points
    come from both charge (I > 0) and discharge curves (I < 0). A Feed Forward Neural Network (FFNN)
    is used for battery SOH estimation. As input, the network receives the amount of (I, V, T) points
    in each cluster (input size = number of clusters) since the battery's Beginning of Life (BoL).
    This cumulative property of the inputs is why this approach is called "history-based".
    In the article, the authors use a FFNN with one hidden layer with as many neurons as the input layer
    (which is the same number as the number of clusters = 80 in their case). They've also used dropout
    for regularization and in order to avoid overfitting.
        * Full reference: Gae-won You, Sangdo Park, Dukjin Oh, Real-time state-of-health estimation for
        electric vehicle batteries: A data-driven approach, Applied Energy, Volume 176, 2016, Pages 92-103,
        ISSN 0306-2619, https://doi.org/10.1016/j.apenergy.2016.05.051.

    This class was made to treat Dataset_Nasa classes and for the FFNN class FFNN_v1 in FFNN.py.
        The network's inputs are the number of points in each cluster, and its output is the battery's
        capacity in A.h.

    Some attributes:
        self.dataset_name   : the name of the dataset (Dataset_Nasa). [string]
        self.dataset_format : a string indicating the type of format used: 'You et al. [51]'. [string]
        self.inputs         : a list with the XT tensors for each cycle. [list of pytorch tensors]
        self.output         : a list with the output capacity tensors for each cycle. [list of pytorch tensors]

    Inputs:
        Battery_list        : list with the Batteries that we want to use for creating the dataset.
                              [list of Dataset_Nasa objects]
        Nb_clusters         : number of clusters for the k-means clustering algorithm. [int]
        kmeans              : The kmeans class if the method was done before-hand (recommended).
                              [sklearn.cluster._kmeans.KMeans object]
        ignore              : list with the name of the files that should be ignored (not used for
                              the dataset generation). [list of strings]
                                It's useful because we already have a complete pickle with all of
                                the batteries. That way we can use this file and just say which
                                batteries to ignore.
        print_bool          : if we want to print some information while the code runs. [bool]
                              Namely, if we want to print the battery's name and the number of inputs.
        time_step           : the desired time step for the resampling procedure. [int]
        i_cutoff            : the current cutoff at the end of the charging procedure. [float]
        min_len             : minimum sequence length for the cycle to be taken into account. [int]
                                Must be greater or equal to 1.
        threshold            : the minimum value of capacity to be taken into account. [float]
                                Values under the treshold will be ignored during the construction
                                of the dataset.
        path, transform     : necessary arguments for Pytorch's Dataset class but that are unused. [None]
    """
    def __init__(self, Battery_list, Nb_clusters, kmeans=None,
                 ignore = None, print_bool=False,
                 time_step=30, i_cutoff=2e-2,
                 min_len=1, threshold=0.01,
                 path=None, transform=None):                    # Just necessary arguments for pytorch's Dataset class

        super().__init__()

        if isinstance(ignore, type(None)):
            ignore = []

        if isinstance(kmeans, type(None)):
            print('No k-means was informed!')
            print('Running k-means...')
            # Doing the k-means clustering algorithm with the
            # batteries in battery list:
            IVT0 = th.zeros(1, 3)
            for Battery in Battery_list:
                print(Battery.Name)
                for Cycle in Battery.cycle:
                    if Cycle.type == 'impedance':
                        continue
                    Cycle_t, Cycle_i, Cycle_v, Cycle_T = clean(Cycle)
                    if Cycle_t is None: # When there are too few points:
                        continue
                    length = len(Cycle_t)
                    IVT = th.cat([Cycle_i.view(length, 1),
                                  Cycle_v.view(length, 1),
                                  Cycle_T.view(length, 1)], dim=1)
                    IVT0 = th.cat([IVT0, IVT], dim=0)

            IVT = IVT0[1:].view(len(IVT0)-1, 3).detach().numpy()
            kmeans = KMeans(n_clusters=Nb_clusters).fit(IVT)
            print('Done!')
        else:
            Informed_Nb_clusters = Nb_clusters
            Nb_clusters = kmeans.cluster_centers_.shape[0]
            if Informed_Nb_clusters != Nb_clusters:
                print('The informed number of clusters (', Informed_Nb_clusters,
                      ') was incorrect. ', Nb_clusters, ' will be used.')

        self.dataset_name = type(Battery_list[0]).__name__      # Basically the dataset used (Nasa,...)
        self.dataset_format = 'You et al. [51]'
        self.nb_clusters = Nb_clusters
        self.kmeans = kmeans
        self.time_step = time_step                              # The time step chosen for resampling
        self.i_cutoff = i_cutoff                                # The current cut-off chosen for cleaning
        self.threshold = threshold                              # The threshold chosen for cleaning
        self.min_len = min_len

        ent = []                                    # Input list
        sai = []                                    # Output list

        # History of the inputs:
        cumulative_input = th.zeros(Nb_clusters)

        for Battery in Battery_list:
            if Battery.Name + str('.txt') in ignore:
                continue
            if print_bool:
                print(Battery.Name)

            for Cycle in Battery.cycle:

                if Cycle.type == 'charge':

                    Cycle_t, Cycle_i, Cycle_v, Cycle_T = clean(Cycle, time_step=time_step,
                                                               i_cutoff=i_cutoff, min_len=min_len)

                    if Cycle_t is None:
                        continue

                    # Concatenating the IVT values
                    IVT = th.cat([Cycle_i.view(len(Cycle_i), 1),
                                  Cycle_v.view(len(Cycle_v), 1),
                                  Cycle_T.view(len(Cycle_T), 1)], dim=1)

                    IVT = IVT.detach().numpy()

                    # Calculating which cluster center is closer to each point
                    cluster_values = th.from_numpy(kmeans.predict(IVT))
                    # Calculating the number of points in each cluster.
                    inputs = th.bincount(cluster_values, minlength=Nb_clusters)
                    # Adding to the history:
                    cumulative_input += inputs

                elif Cycle.type == 'discharge':

                        Capacity = Cycle.data.capacity*th.ones(1)
                        if Capacity.item() < threshold:
                            continue

                        Cycle_t, Cycle_i, Cycle_v, Cycle_T = clean(Cycle, time_step=time_step,
                                                                   i_cutoff=i_cutoff, min_len=min_len)

                        if Cycle_t is None:
                            continue

                        # Concatenating the IVT values
                        IVT = th.cat([Cycle_i.view(len(Cycle_i), 1),
                                      Cycle_v.view(len(Cycle_v), 1),
                                      Cycle_T.view(len(Cycle_T), 1)], dim=1)

                        IVT = IVT.detach().numpy()

                        # Calculating which cluster center is closer to each point
                        cluster_values = th.from_numpy(kmeans.predict(IVT))
                        # Calculating the number of points in each cluster.
                        inputs = th.bincount(cluster_values, minlength=Nb_clusters)
                        # Adding to the history:
                        cumulative_input += inputs

                        # Then:
                        ent.append(th.clone(cumulative_input))  # The input for this capacity value
                        sai.append(Capacity)                    # The capacity is one output

        assert len(ent) == len(sai), 'Unequal input and output lengths'

        if print_bool:
            print(len(ent))

        self.inputs = ent
        self.output = sai
        self.path = path
        self.transform = transform

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, item):
        ent = self.inputs[item]
        sai = self.output[item]
        return ent, sai


if __name__ == '__main__':

    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    os.chdir('.//Separate_pickles')

    Nb_clusters = 80

    # K-MEANS:
    try:        # If a K-means clustering has already been done with
                # that number of clusters:
        with open('kmeans' + str(Nb_clusters) + '.txt', 'rb') as f:
            kmeans = pickle.load(f)
        print(type(kmeans))
    except FileNotFoundError:   # If not:

        # Opening the complete, clean, IVT data:
        with open('IVT_Data.txt', 'rb') as f:
            IVT = pickle.load(f)
        IVT = IVT.detach().numpy()  # Converting to numpy

        # The actual kmeans clustering:
        print('Starting k-means...')
        start = time.time()
        kmeans = KMeans(n_clusters=Nb_clusters).fit(IVT)
        end = time.time()
        print('Finished: ' + str(Nb_clusters) +
              ' cluster k-means in ' + str(end-start) + ' seconds')

        # Saving the class:
        with open('kmeans' + str(Nb_clusters) + '.txt', 'wb') as f:
            pickle.dump(kmeans, f)

    # Opening the battery for creating the dataset:
    with open('B0005.txt','rb') as f:
        Battery = pickle.load(f)

    # Creating the dataset:
    Battery_Dataset = Pytorch_Dataset_KMEANS([Battery], Nb_clusters, kmeans=kmeans, print_bool=True)

    # Visualizing the input "histograms":
    cluster_names = [str(i) for i in range(Nb_clusters)]
    for k in range(len(Battery_Dataset)):
        plt.bar(cluster_names, Battery_Dataset.inputs[k])
        plt.show(block=False)
        plt.pause(0.1)
        plt.close()

    """
    ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt', 'B0029.txt', '.DS_Store'] # Files to be ignored.
    test = ['B0005.txt', 'B0006.txt', 'B0007.txt', 'B0018.txt',
            'B0033.txt', 'B0034.txt', 'B0036.txt',
            'B0042.txt', 'B0043.txt', 'B0044.txt',
            'B0045.txt', 'B0046.txt', 'B0047.txt', 'B0048.txt',
            'B0053.txt', 'B0054.txt', 'B0055.txt', 'B0056.txt']

    # K-MEANS ANALYSIS

    ## Creating the file list
    files = os.listdir()  # All files in the folder
    file_list = []  # List with the valid files
    for file_name in files:
        if file_name in ignore:
            continue
        match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
        # the battery files
        if match == None:
            continue
        else:
            file_list.append(file_name)

    file_list.sort()

    ## Creating the complete IVT data for doing the k-means analysis
    IVT0 = th.zeros(1, 3)

    #file_list = ['B0005.txt', 'B0006.txt']

    for file_name in file_list:

        print(file_name)
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)

        for Cycle in Battery.cycle:
            if Cycle.type == 'impedance':
                continue
            Cycle_t, Cycle_i, Cycle_v, Cycle_T = clean(Cycle)

            if Cycle_t == None:
                continue

            length = len(Cycle_t)
            IVT = th.cat([Cycle_i.view(length, 1),
                          Cycle_v.view(length, 1),
                          Cycle_T.view(length, 1)], dim=1)

            IVT0 = th.cat([IVT0, IVT], dim=0)
        #print(IVT0.shape)

    IVT = IVT0[1:].view(len(IVT0) - 1, 3)
    print(IVT.shape)

    with open('IVT_Data.txt', 'wb') as f:
        pickle.dump(IVT, f)
    """


    """
    ## Visualization:
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.scatter(IVT[:, 0], IVT[:, 1], IVT[:, 2])
    plt.show()
    """

    """
    ## The K-means analysis itself
    Nb_clusters = 10
    _, cluster_centers = kmeans(IVT, Nb_clusters)

    with open('Cluster_centers.pkl', 'wb') as f:
        pickle.dump(cluster_centers, f)

    with open('B0029.txt', 'rb') as f:
        Battery = pickle.load(f)

    Battery_list = []
    for file_name in file_list:
        if file_name not in test:
            continue
        print(file_name)
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)
        Battery_list.append(Battery)

    Battery_Dataset = Pytorch_Data_Set_51(Battery_list, 10, print_bool=True)

    cluster_names = [str(i) for i in range(Nb_clusters)]

    for k in len(Battery_Dataset):
        plt.bar(cluster_names,Battery_Dataset.inputs[k])
        plt.show(block=False)
        plt.pause(0.2)
        plt.close()
    """
    """
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    IVT = th.cat([I.view(len(I), 1),
                  V.view(len(V), 1),
                  T.view(len(T), 1)], dim=1)

    #ax.scatter(IVT[:, 0], IVT[:, 1], IVT[:, 2])
    #plt.show()


    Nb_clusters = 10
    cluster_ids, cluster_centers = kmeans(IVT, Nb_clusters)

    Cluster_list = []

    for nb in range(Nb_clusters):
        bool = (cluster_ids == nb)
        Cluster_list.append(IVT[bool])
        ax.scatter(IVT[bool, 0], IVT[bool, 1], IVT[bool, 2])

    plt.show()

    print(cluster_ids, cluster_centers)

    IVT = IVT.view(1, len(IVT), 3)
    cluster_centers = cluster_centers.view(1, Nb_clusters, 3)

    cluster_tsr = th.cdist(IVT.double(), cluster_centers.double())

    cluster_values = th.argmin(cluster_tsr, dim=2).view(IVT.shape[1])

    IVT = IVT.view(IVT.shape[1],3)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    Cluster_list = []
    for nb in range(Nb_clusters):
        bool = (cluster_values == nb)
        Cluster_list.append(IVT[bool])
        ax.scatter(IVT[bool, 0], IVT[bool, 1], IVT[bool, 2])

    plt.show()


    A = cluster_ids == cluster_values
    for i in range(len(A)):
        if A[i] == False:
            print(i)
            print(cluster_values[i])
            print(cluster_ids[i])

    print(th.bincount(cluster_ids))
    print(th.bincount(cluster_values))


    sys.exit()

    # ax.set_xlabel('I')
    # ax.set_ylabel('V')
    # ax.set_zlabel('T')
    # plt.show()

    print(bool.shape)

    print(IVT[bool].shape)

    print(IVT.shape)
    print(cluster_ids.shape)



    count = th.bincount(cluster_ids) # Counts how many points in each cluster
    print(count)

    cluster_tsr = th.zeros(Nb_clusters, IVT.shape[0], IVT.shape[1])

    for i in range(len(cluster_ids)):
        current_id = cluster_ids[i]
        cluster_tsr[current_id,i,:] = IVT[i,:]
    """
    """
    k = 0
    for Cycle in Battery.cycle:
        k+=1
        if Cycle.type == 'charge':
            res = stats.linregress(np.arange(0,len(Cycle.data.time[:,0])),Cycle.data.time[:,0])
            print(res.rvalue**2, res.slope, len(Cycle.data.time[:,0]))
    """
    """
    nb = 20
    print(Battery.cycle[nb].data.time.shape)
    print(Battery.cycle[nb].data.time[:,0].shape)

    a = len(Battery.cycle[nb].data.time)
    k = np.reshape(np.arange(a),(1,a))
    k2 = np.reshape(Battery.cycle[nb].data.time,(1,a))
    start = time.time()
    res = stats.linregress(k, k2)
    end = time.time()
    print('time', end - start)
    print(res.rvalue**2)
    plt.plot(Battery.cycle[nb].data.time)
    plt.plot(np.transpose(res.slope*k) + res.intercept)
    plt.show()
    plt.plot(Battery.cycle[nb].data.time,Battery.cycle[nb].data.meas_v)
    plt.plot(Battery.cycle[nb].data.time,Battery.cycle[nb].data.meas_i)

    plt.show()
    """




    """
    for file_name in os.listdir():
        if file_name in ignore:
            continue
        else:
            with open(file_name, 'rb') as f:
                Battery = pickle.load(f)

            Battery_list = [Battery]
            A = Pytorch_Data_Set(Battery_list, 10, ignore=ignore, print_bool=True)

            #with open('th_'+file_name, 'wb') as f:
            #    pickle.dump(A, f)
    """


    """
    For training:
    with open('B0005.txt', 'rb') as f:
        B0005 = pickle.load(f)

    with open('B0006.txt', 'rb') as f:
        B0006 = pickle.load(f)


    with open('th_B0005.txt', 'rb') as f:
        th_B0005 = pickle.load(f)

    with open('th_B0006.txt', 'rb') as f:
        th_B0006 = pickle.load(f)

    ld_B0005 = DataLoader(dataset=th_B0005, batch_size=1, shuffle=True, num_workers=0)
    ld_B0006 = DataLoader(dataset=th_B0006, batch_size=1, shuffle=True, num_workers=0)
    _,_ = TreinoComum_v0(ld_B0005, ld_B0006, model,
                        N_epochs=3, LR=1e-3, sim_tt=ld_B0005,
                        plot=1)
    """

    """
    # Opening the file
    with open('B0029.txt', 'rb') as f:
        B0029 = pickle.load(f)

    # The number of cycles
    size = len(B0029.cycle)

    # In principle, this would only be done for the charge graphs, but I'll add
    # The option to use discharge data as well.

    I = th.zeros(1)
    V = th.zeros(1)

    cycle_type = 'charge'
    for i in range(size):
        if B0029.cycle[i].type == cycle_type:
            # Concatenating the current I, V and T tensors with the previous ones.
            I = th.cat([I, th.from_numpy(B0029.cycle[i].data.meas_i).view(-1)])
            V = th.cat([V, th.from_numpy(B0029.cycle[i].data.meas_v).view(-1)])
        else:
            continue

    plt.plot(I[1:])
    plt.show()
    """

    """
    # K-MEANS ANALYSIS (TO BE FINISHED)
    IVT = Save_IVT_Data('B0029.txt')

    Nb_clusters = 4
    cluster_ids, cluster_centers = kmeans(IVT, Nb_clusters)

    print(IVT.shape)
    print(cluster_ids, cluster_centers)
    print(cluster_ids.shape)



    count = th.bincount(cluster_ids) # Counts how many points in each cluster
    print(count)

    cluster_tsr = th.zeros(Nb_clusters, IVT.shape[0], IVT.shape[1])

    for i in range(len(cluster_ids)):
        current_id = cluster_ids[i]
        cluster_tsr[current_id,i,:] = IVT[i,:]


    """

    """
    # COMPLETE IVT DATA ANALYSIS
    for file in os.listdir():
        if (file == '.DS_Store') or (file in ignore):
            print(file, '- skipped')
            continue

        print(file)
        IVT = Save_IVT_Data(file)

        IVT0 = th.cat([IVT0, IVT])

    print(IVT0.shape)
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    ax.scatter(IVT0[1:,0],IVT0[1:,1],IVT0[1:,2])
    ax.set_xlabel('I')
    ax.set_ylabel('V')
    ax.set_zlabel('T')
    plt.show(block=False)
    plt.pause(0.1)
    ax.clear()


    # with open(file, 'rb') as f:
    with open('B0038.txt', 'rb') as f:
        B0029 = pickle.load(f)

    size = len(B0029.cycle)

    I = th.zeros(1)
    V = th.zeros(1)
    T = th.zeros(1)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    for i in range(size):
        try:
            I = th.cat([I, th.from_numpy(B0029.cycle[i].data.meas_i).view(-1)])
            V = th.cat([V, th.from_numpy(B0029.cycle[i].data.meas_v).view(-1)])
            T = th.cat([T, th.from_numpy(B0029.cycle[i].data.meas_t).view(-1)])

        except AttributeError:
            continue

        # if i%int(0.1*size) == 0:
        ax.scatter(I[1:], V[1:], T[1:])
        ax.set_xlabel('I')
        ax.set_ylabel('V')
        ax.set_zlabel('T')
        plt.show(block = False)
        plt.savefig('B0039'+str(i)+'.png')
        plt.pause(0.1)
        ax.clear()

    """



    """
    # Opening the file
    with open('B0029.txt', 'rb') as f:
        B0029 = pickle.load(f)

    # The number of cycles
    size = len(B0029.cycle)

    # In principle, this would only be done for the charge graphs, but I'll add
    # The option to use discharge data as well.

    I = th.zeros(1)
    V = th.zeros(1)

    cycle_type = 'charge'
    for i in range(size):
        if B0029.cycle[i].type == cycle_type:
            # Concatenating the current I, V and T tensors with the previous ones.
            I = th.cat([I, th.from_numpy(B0029.cycle[i].data.meas_i).view(-1)])
            V = th.cat([V, th.from_numpy(B0029.cycle[i].data.meas_v).view(-1)])
        else:
            continue

    plt.plot(I[1:])
    plt.show()
    """




    """
    # K-MEANS ANALYSIS (TO BE FINISHED)
    IVT = Save_IVT_Data('B0029.txt')

    Nb_clusters = 4
    cluster_ids, cluster_centers = kmeans(IVT, Nb_clusters)

    print(IVT.shape)
    print(cluster_ids, cluster_centers)
    print(cluster_ids.shape)



    count = th.bincount(cluster_ids) # Counts how many points in each cluster
    print(count)

    cluster_tsr = th.zeros(Nb_clusters, IVT.shape[0], IVT.shape[1])

    for i in range(len(cluster_ids)):
        current_id = cluster_ids[i]
        cluster_tsr[current_id,i,:] = IVT[i,:]


    """






    """
    # COMPLETE IVT DATA ANALYSIS
    for file in os.listdir():
        if (file == '.DS_Store') or (file in ignore):
            print(file, '- skipped')
            continue

        print(file)
        IVT = Save_IVT_Data(file)

        IVT0 = th.cat([IVT0, IVT])

    print(IVT0.shape)
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    ax.scatter(IVT0[1:,0],IVT0[1:,1],IVT0[1:,2])
    ax.set_xlabel('I')
    ax.set_ylabel('V')
    ax.set_zlabel('T')
    plt.show(block=False)
    plt.pause(0.1)
    ax.clear()


    # with open(file, 'rb') as f:
    with open('B0038.txt', 'rb') as f:
        B0029 = pickle.load(f)

    size = len(B0029.cycle)

    I = th.zeros(1)
    V = th.zeros(1)
    T = th.zeros(1)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    for i in range(size):
        try:
            I = th.cat([I, th.from_numpy(B0029.cycle[i].data.meas_i).view(-1)])
            V = th.cat([V, th.from_numpy(B0029.cycle[i].data.meas_v).view(-1)])
            T = th.cat([T, th.from_numpy(B0029.cycle[i].data.meas_t).view(-1)])

        except AttributeError:
            continue

        # if i%int(0.1*size) == 0:
        ax.scatter(I[1:], V[1:], T[1:])
        ax.set_xlabel('I')
        ax.set_ylabel('V')
        ax.set_zlabel('T')
        plt.show(block = False)
        plt.savefig('B0039'+str(i)+'.png')
        plt.pause(0.1)
        ax.clear()
        
    """
