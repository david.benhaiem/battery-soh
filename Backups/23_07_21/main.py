from LSTM_tests import *
from Nasa_Classes import *
from Coin_Classes import *
from Severson_Classes import *

device = th.device('cuda:0' if th.cuda.is_available() else 'cpu')
print(th.cuda.is_available())
print('device:', device)

DELTA = 10
LSTM_version = 1
norm_type = 'zscore'
fixed_len=150

# NASA
time_step = 30
os.chdir('.//Datasets/NASA/B. Saha and K Goebel/Separate_pickles')
files = os.listdir()  # All files in the folder
file_list = []  # List with the valid files
for file_name in files:
    match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
    # the battery files
    if match == None:
        continue
    else:
        file_list.append(file_name)
file_list.sort()

ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt']

# The big one
train = ['B0018.txt',  # 24ºC
         #  'B0025.txt', 'B0026.txt', 'B0027.txt',  # 24ºC
         'B0029.txt', 'B0030.txt', 'B0031.txt',  # 43ºC
         'B0036.txt',  # 24ºC
         'B0038.txt', 'B0039.txt',  # 44ºC et 24ºC
         'B0042.txt', 'B0043.txt',  # 44ºC et 4ºC
         'B0045.txt', 'B0046.txt',  # 4ºC
         'B0053.txt', 'B0055.txt']  # 4ºC

Train_list = []
for file_name in file_list:
    if file_name in train:
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)

        # print(Battery.policy)
        # Train_list.append(Battery)
        Train_list.append(Torch_Dataset_LSTM([Battery], DELTA, LSTM_version,  # Dataset
                                  print_bool=True, time_step=time_step,
                                  fixed_len=fixed_len, norm_type=norm_type))

# NASA_dataset = Torch_Dataset_LSTM(Train_list, DELTA, LSTM_version,  # Dataset
#                                   print_bool=True, time_step=time_step,
#                                   fixed_len=fixed_len, norm_type=norm_type)

# Validation files:
Valid_list = []
for file_name in file_list:
    if (file_name not in train) and (file_name not in ignore):
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)
        Valid_list.append(Torch_Dataset_LSTM([Battery], DELTA, LSTM_version,  # Dataset
                                   print_bool=True, time_step=time_step,
                                   fixed_len=fixed_len))

# NASA_v_dataset = Torch_Dataset_LSTM(Valid_list, DELTA, LSTM_version,  # Dataset
#                                    print_bool=True, time_step=time_step,
#                                    norm=NASA_dataset.norm,
#                                    fixed_len=fixed_len)
# COIN
os.chdir('../../../../')
os.chdir('.//Datasets/Tarascon/Coins/Clean_Batteries')
files = os.listdir()  # All files in the folder
file_list = []  # List with the valid files
for file_name in files:
    match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
    # the battery files
    if match == None:
        continue
    else:
        file_list.append(file_name)
file_list.sort()

train = ['B' + str(i).zfill(4) + '.txt' for i in range(1, 5)]
ignore = train + ['B0008.txt']
# Train_list = []
for file_name in file_list:
    if file_name in train:
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)

        # print(Battery.policy)
        # Train_list.append(Battery)
        Train_list.append(Torch_Dataset_LSTM([Battery], DELTA, LSTM_version,  # Dataset
                                  print_bool=True, time_step=time_step,
                                  fixed_len=fixed_len, norm_type=norm_type))

# COIN_dataset = Torch_Dataset_LSTM(Train_list, DELTA, LSTM_version,  # Dataset
#                                   print_bool=True, time_step=time_step,
#                                   fixed_len=fixed_len, norm_type=norm_type)

# Validation files:
# Valid_list = []
for file_name in file_list:
    if (file_name not in train) and (file_name not in ignore):
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)
        Valid_list.append(Torch_Dataset_LSTM([Battery], DELTA, LSTM_version,  # Dataset
                                   print_bool=True, time_step=time_step,
                                   fixed_len=fixed_len))

# COIN_v_dataset = Torch_Dataset_LSTM(Valid_list, DELTA, LSTM_version,  # Dataset
#                                    print_bool=True, time_step=time_step,
#                                    norm=COIN_dataset.norm,
#                                    fixed_len=fixed_len)

# MIT (SEVERSON)
time_step=5
os.chdir('../../../../')
os.chdir('.//Datasets/Severson et al./Cleaned_v2/2. 2017-06-30')
files = os.listdir()  # All files in the folder
file_list = []  # List with the valid files
for file_name in files:
    match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
    # the battery files
    if match == None:
        continue
    else:
        file_list.append(file_name)
file_list.sort()

train_len = 36  # Doesn't do anything right now
valid_len = 12

train = ['B0005.txt', 'B0002.txt']
# train = ['B'+str(i).zfill(4)+'.txt' for i in range(12, 48)]
ignore = ['B' + str(i).zfill(4) + '.txt' for i in range(46)]

# print(train, '\n', ignore)

# Train_list = []
for file_name in file_list:
    if file_name in train:
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)

        # print(Battery.policy)
        Train_list.append(Torch_Dataset_LSTM([Battery], DELTA, LSTM_version,  # Dataset
                                  print_bool=True, time_step=time_step,
                                  fixed_len=fixed_len, norm_type=norm_type))

# MIT_dataset = Torch_Dataset_LSTM(Train_list, DELTA, LSTM_version,  # Dataset
#                                   print_bool=True, time_step=time_step,
#                                   fixed_len=fixed_len, norm_type=norm_type)

# Validation files:
# Valid_list = []
for file_name in file_list:
    if (file_name not in train) and (file_name not in ignore):
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)
        Valid_list.append(Torch_Dataset_LSTM([Battery], DELTA, LSTM_version,  # Dataset
                                   print_bool=True, time_step=time_step,
                                   fixed_len=fixed_len))

# MIT_v_dataset = Torch_Dataset_LSTM(Valid_list, DELTA, LSTM_version,  # Dataset
#                                    print_bool=True, time_step=time_step,
#                                    norm=MIT_dataset.norm,
#                                    fixed_len=fixed_len)


os.chdir('../../../../')

# COMPLETE MIXED DATASETS:
from Training import Torch_Dataset_Mixer
train_dataset = Torch_Dataset_Mixer(Train_list, renorm=True)
valid_dataset = Torch_Dataset_Mixer(Valid_list, renorm=True)

train_loader = DataLoader(dataset=train_dataset, batch_size=1, shuffle=True)
simul_loader = DataLoader(dataset=train_dataset, batch_size=len(train_dataset), shuffle=False)
valid_loader = DataLoader(dataset=valid_dataset, batch_size=len(valid_dataset), shuffle=False)

HIDDEN_DIM = 50
DROPOUT = 0
pool_type = 'bi_mean'

Model = LSTM_v1(DELTA, HIDDEN_DIM, pooling_layer=pool_type,
               drop_out=DROPOUT, version=LSTM_version,
               norm = None, lin_layers=1).to(device)

simulate(simul_loader, Model, device=device, denormalize=False)
simulate(valid_loader, Model, device=device, denormalize=False)

Nb_Epochs = 75
Learning_Rate = 1e-6

Train_info, _, _, date = Train(train_loader, valid_loader, Model, Nb_Epochs,
                               plot_bool=True, LR=Learning_Rate, report=True,
                               print_bool=True, device=device, remove_bool=True)

print(date)

# # Best model
simulate(simul_loader, Model, block=True, denormalize=False, device=device)
simulate(valid_loader, Model, block=True, denormalize=False, device=device)
