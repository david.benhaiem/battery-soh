from Nasa_Classes import *
import torch as th
import torch.nn as nn
import matplotlib.pyplot as plt
from Report import *
from torch.utils.data import Dataset, DataLoader
from datetime import datetime

def Train(train_loader, valid_loader, model, N_epochs, loss_fn=None,
          optimizer=None, parameters=None, LR=0.001, plot_bool=False,
          print_bool=True, report=False, remove_bool=False, file_name='TEST',
          sim_loader=None, Training_info=None, device='cpu'):

    """
    Function for training pytorch neural network models.
    If plot_bool is set to True, then it will create a plot of the training
    (blue) and validation (orange) losses in function of the epoch number.

    Parameters
    ----------
    train_loader : torch.utils.data.DataLoader
        The training dataset's DataLoader class.
    valid_loader : torch.utils.data.DataLoader
        The validation dataset's DataLoader class.
    model : torch.nn.Module
        The model to be trained.
    N_epochs : int
        The number of epochs.
    loss_fn : th.nn Loss function, optional
        The loss function.
    optimizer : th.nn Optimizer, optional
        The optimizer.
    LR : float, default = 0.01
        The learning rate.
    plot_bool : bool, default = False
        If we want to plot the loss function x epoch.
    print_bool : bool, default = False
        If we want to print the epochs' losses during calculation [bool]
    report : bool, default = False
        If we want to write a report file on the training. Check Report.py for
        more information on the report itself.
    file_name : str, default = 'TEST'
        The file name to be added to the report and image files.
    sim_loader : torch.utils.data.DataLoader, optional
        A DataLoader to be simulated during training, plotted in "real time" as
        the model learns. Not a necessary input,
        of course.
    Training_info : Training_Class, optional
        A Training_Class object to keep track of the training.
        Only entered as input if the model has already been trained.
    device : torch.device, default = 'cpu'
        The device where the training should be run.

    Returns
    -------
    Training_info : Training_Class
        The Training_Class object with all of the information related to the
        training.
    Total_error[-1] : torch.Tensor([float])
        Last epoch's training error.
    Valid_error[-1] : torch.Tensor([float])
        Last epoch's validation error [float]

    Examples
    --------
    >>> from LSTM import *
    >>> delta = 10
    >>> hidden_dim = 20
    >>> version = 4
    >>> model = LSTM_v4(delta, hidden_dim, pooling_layer='bi_mean', version=version)
    >>> with open('B0005.txt', 'rb') as f:
    ...     Battery = pickle.load(f)
    ...
    >>> train_dataset = Torch_Dataset_LSTM([Battery], delta, version, fixed_len=150)
    >>> train_loader = DataLoader(dataset=train_dataset, batch_size=1, shuffle=True)
    >>> with open('B0006.txt', 'rb') as f:
    ...     Battery = pickle.load(f)
    ...
    >>> valid_dataset = Torch_Dataset_LSTM([Battery], delta, version, fixed_len=150)
    >>> valid_loader = DataLoader(dataset=valid_dataset, batch_size=len(valid_dataset),
    ... shuffle=False)
    >>> N_epochs = 2
    >>> Learning_Rate = 1e-5
    >>> Train(train_loader, valid_loader, model, N_epochs, loss_fn=nn.L1Loss(),
    ... optimizer=th.optim.Adam(model.parameters(), Learning_Rate),
    ... LR = Learning_Rate)
    1 . Training loss:  tensor(0.8822)
    1 . Validation loss:  tensor(0.5137)
    2 . Training loss:  tensor(0.8365)
    2 . Validation loss:  tensor(0.4762)
    """

    # Initialization of loss function and optimizer:
    if isinstance(loss_fn, type(None)):
        loss_fn = nn.MSELoss()
    if isinstance(optimizer, type(None)):
        if parameters is None:
            optimizer = th.optim.Adam(model.parameters(), lr=LR)
        else:
            optimizer = th.optim.Adam(parameters, lr=LR)

    if Training_info is None:
        Training_info = Training_Class(N_epochs, loss_fn, optimizer, LR,
                                       train_loader.batch_size)
    else:
        if Training_info.learning_rate != LR:
            Training_info_save = Training_info
            Training_info = Training_Class(N_epochs, loss_fn, optimizer, LR)
            Training_info.add_train(Training_info_save)

    # If a simulation loader has been called:
    if isinstance(sim_loader, type(None)):
        sim_bool = False
    else:
        sim_bool = True

    date = str(
    datetime.today())  # The current time will be used to distinguish it from others
    date = date.replace(':', '_')
    if report:  # If report mode is on
        try:
            os.chdir('.//Reports')
        except FileNotFoundError:
            os.mkdir('Reports')             # We'll create a reports folder
            os.chdir('.//Reports')
        os.mkdir(date)                      # Creating the report directory
        os.chdir('.//' + date)              # Entering it

    # Tensors for keeping the epochs' losses:
    Total_error = th.zeros(N_epochs)     # Total training loss (for each epoch)
    Valid_error = th.zeros(N_epochs)     # Total validation loss (for each epoch)

    # Epoch Loop:
    for E in range(N_epochs):
        Epoch_error = 0         # Initializing this epoch's loss
        model.train()           # Making sure that the model is in training mode

        # Loader loop:
        for data, label in train_loader:
            # per batch:
            data = data.to(device)      # Bringing the data and the
            label = label.to(device)    # "labels" to the chosen device (cuda or cpu)
            optimizer.zero_grad()       # Resetting the gradients
            # forward:
            outputs = model.forward(data)       # Calculating the model's outputs
            loss = loss_fn(outputs, label.view(len(outputs), -1))  # Model's loss
            # backward:
            loss.backward()                     # Gradient backpropagation
            optimizer.step()                    # Parameter updates
            Epoch_error += loss.item()          # Adding this batch's loss to the the epoch loss.

        Total_error[E] = Epoch_error / len(train_loader)  # Saving the mean epoch error.

        if print_bool:
            print(E + 1, '. Training loss: ', Total_error[E])    # Printing epoch error


        # VALIDATION ERROR:
        Partial_Valid_error = 0  # Initializing this epoch's validation loss
        with th.no_grad():  # Ignoring gradients, since no parameter updates will happen
            model.eval()    # Setting the model to evaluation mode (no dropout etc.)

            for valid_data, valid_label in valid_loader:
                # per batch:
                valid_data = valid_data.to(device)      # Bringing the data and the
                valid_label = valid_label.to(device)    # "labels" to the chosen device (cuda or cpu)
                # forward:
                valid_outputs = model.forward(valid_data)   # Model output
                loss = loss_fn(valid_outputs, valid_label)  # Validation error
                Partial_Valid_error += loss.item()          # Adding to the epoch's validation loss

            Valid_error[E] = Partial_Valid_error / len(valid_loader)    # Saving the mean epoch validation error

            if print_bool:
                print(E + 1, '. Validation loss: ', Valid_error[E])   # Printing epoch's validation error

            ## For saving each epoch's state_dict():
            try:
                os.chdir('.//Epochs')
            except FileNotFoundError:
                os.mkdir('Epochs')
                os.chdir('.//Epochs')
            th.save(model.state_dict(), 'Epoch_' + str(E+1) + '.txt')
            os.chdir('../')
            ##

            # Concomitant simulation (not recommended for big datasets)
            if sim_bool:
                if report == False:
                    sim_out = []    # model outputs
                    sim_lbl = []    # model labels
                    with th.no_grad():  # Ignoring gradients, since no parameter updates will happen
                        model.eval()    # Setting the model to evaluation mode (no dropout etc.)
                        for sim_data, sim_label in sim_loader:
                            # per batch:
                            sim_data = sim_data.to(device)          # Bringing the data and the
                            sim_label = sim_label.to(device)        # "labels" to the chosen device (cuda or cpu)
                            # forward:
                            sim_output = model.forward(sim_data)    # Model output
                            sim_out.append(sim_output)              # Adding the output to the list
                            sim_lbl.append(sim_label)               # Adding the "label" to the list

                    plt.plot(sim_out)           # Plotting the Simulation
                    plt.plot(sim_lbl)           # And the "labels", together
                    plt.show(block=False)       # Stopping the plot from blocking the code
                    plt.pause(1)                # Pausing the code for only one second
                    plt.close()                 # Automatically closing the plot window

        model.train()  # Setting the model to training mode again (just in case)

        # Adding the epoch to the training class:
        Training_info.add_epoch(Total_error[E], Valid_error[E], E)

    # Calculating the best epoch:
    Training_info.best_epoch_method()

    if plot_bool:   # If we want to plot the loss evolution:
        plt.plot(Total_error.detach().numpy())   # Plotting the training lossees
        plt.plot(Valid_error.detach().numpy())   # Plotting the validation losses
        plt.show()

    if report:  # In case we want to create a report of the training
        try:    # Try to enter an image folder
            os.chdir('.//Images')
        except FileNotFoundError:   # If there's no Image folder
            os.mkdir('Images')      # Create one
            os.chdir('.//Images')   # Enter it

        plt.figure(figsize=(19, 10))            # Creating a figure
        plt.plot(range(1, N_epochs+1), Total_error.detach().numpy())  # Plotting the training lossees
        plt.plot(range(1, N_epochs+1), Valid_error.detach().numpy())  # Plotting the validation losses
        plt.title('Loss')                       # Plot title
        plt.ioff()                              # Don't show the plot
        plt.savefig(file_name + '_losses.png')       # Saving the image
        plt.close()                             # Close the image
        os.chdir('../')                         # Go back to the previous folder

        Report(model, train_loader, valid_loader, Training_info, file_name=file_name+'_report', remove_files=remove_bool)

        model = model.to(device)

        if sim_bool:
            with open('base_model.txt', 'rb') as f:
                Model = pickle.load(f)

            Model = Model.to(device)
            simulate(sim_loader, Model, report=True, file_name='Training_base', title='Training Dataset (Final epoch)', plot_bool=False, device=device)
            simulate(valid_loader, Model, report=True, file_name='Validation_base', title='Validation Dataset (Final epoch)', plot_bool=False, device=device)

            try:
                with open('best_model.txt', 'rb') as f:
                    Model = pickle.load(f)
                Model = Model.to(device)
                simulate(sim_loader, Model, report=True, file_name='Training_best', title='Training Dataset (Best epoch)', plot_bool=False, device=device)
                simulate(valid_loader, Model, report=True, file_name='Validation_best', title='Validation Dataset (Best epoch)', plot_bool=False, device=device)

            except FileNotFoundError:
                print('No best model found.')

    print('Best validation error: ', th.argmin(Valid_error).item()+1)

    if report:
        os.chdir('../../')

    return Training_info, Total_error[-1], Valid_error[-1], date     # Gives back the Training Class and the last epoch's training and validation errors.


def simulate(sim_loader, model, report=False, plot_bool=True,
             file_name='Capacity', device='cpu', Time_stamps=None,
             block=True, denormalize=True, title='Simulation', dir_name='',
             fixed_lims=False):

    """
    Function for simulating a model's performance on a dataset.
    This will create a plot where the model's outputs are shown in blue and the
    labels/real values are shown in orange.

    If report = True, then it will save the plot as an image in the "Images"
    folder.

    Parameters
    ----------
    sim_loader : torch.utils.data.DataLoader
        The dataset's DataLoader.
    model : torch.nn.Module
        The model we want to test.
    report : bool, default = False
        Whether or not we want to save an image of the simulation.
    plot_bool : bool, default = True
        Whether or not we want to show the plot.
    file_name : str, default = Capacity
        File name for the saved plot.
    device : torch.device or str, default = 'cpu'
        The device where we want to do the calculations on.
    Time_stamps : array_like, optional
        Dataset's time stamps.
    block: bool, default = True
        Whether or not the plot should stop the code from continuing.
        if block == False, then the plot will be shown for 5 seconds.
    denormalize : bool, default = True
        Whether or not we want the plot to show the de-normalized values or not.
        if denormalize == False, then the plot will show the model's output and
        the labels in their normalized form.
    title : str, default = 'Simulation'
        The plot's title.
    dir_name : str, optional
        A string to add to the Image directory name in order to differentiate
        it. For instance, if dir_name='_Final' then the images will be saved in
        directory 'Images_Final' (if the directory doesn't exist it will be
        created).
    """

    try:
        norm = sim_loader.dataset.norm
    except AttributeError:
        norm = None

    with th.no_grad():  # Ignoring gradients, since no parameter updates will happen

        model.eval()  # Setting the model to evaluation mode

        sim_output_plot = []    # For plotting purposes
        sim_labels_plot = []
        for data, labels in sim_loader:
            # per batch:
            data = data.to(device)          # Bringing the data and the
            labels = labels.to(device)      # "labels" to the chosen device (cuda or cpu)
            # forward
            sim_output = model.forward(data)  # Saída do modelo
            sim_labels = labels

            #sim_output = sim_output.to('cpu')
            #sim_labels = sim_labels.to('cpu')

            # Adding them to the list for plotting:
            sim_output_plot.append(sim_output)
            sim_labels_plot.append(sim_labels)

        # Turning the lists into torch tensors
        sim_output_plot = th.stack(sim_output_plot)
        sim_labels_plot = th.stack(sim_labels_plot)

        sim_output_plot = sim_output_plot.cpu()
        sim_labels_plot = sim_labels_plot.cpu()

        # Adjusting their shape
        sim_output_plot = sim_output_plot.view(max(sim_output_plot.shape), 1)
        sim_labels_plot = sim_labels_plot.view(max(sim_labels_plot.shape), 1)

        if denormalize:
            if norm is not None:
                if norm[4] == 'zscore':
                    sim_output_plot = sim_output_plot*norm[3]+norm[2]
                    sim_labels_plot = sim_labels_plot*norm[3]+norm[2]
                elif norm[4] == 'minmax':
                    sim_output_plot = sim_output_plot*(norm[3] - norm[2])+norm[2]
                    sim_labels_plot = sim_labels_plot*(norm[3] - norm[2])+norm[2]

        if report:

            try:  # Try to enter an image folder
                os.chdir('.//Images'+dir_name)
            except FileNotFoundError:  # If there's no Image folder
                os.mkdir('Images'+dir_name)  # Create one
                os.chdir('.//Images'+dir_name)  # Enter it

            plt.figure(figsize=(14, 5))            # Creating a figure

            if Time_stamps is not None:     # If the time stamps are available
                if len(Time_stamps) == 1:
                    plt.scatter(Time_stamps, sim_output_plot.cpu(), label='Model')
                    plt.scatter(Time_stamps, sim_labels_plot.cpu(), label='Experiment')
                else:
                    plt.plot(Time_stamps, sim_output_plot.cpu(), label='Model')
                    plt.plot(Time_stamps, sim_labels_plot.cpu(), label='Experiment')
            else:                       # If not
                if len(sim_output_plot) == 1:
                    plt.scatter(0, sim_output_plot.cpu(), label='Model')
                    plt.scatter(0, sim_labels_plot.cpu(), label='Experiment')
                else:
                    plt.plot(sim_output_plot.cpu(), label='Model')
                    plt.plot(sim_labels_plot.cpu(), label='Experiment')

            plt.title(title)         # Plot title
            plt.xlabel('Cycle number')
            plt.ylabel('SOH')

            if fixed_lims:
                ylim_max = 1.1
                ylim_min = -0.1
            else:
                ylim_max = max(th.amax(sim_output_plot).item() + 0.1, 1.1)
                ylim_min = min(th.amin(sim_output_plot).item() - 0.1, -0.1)
            # plt.xlim([-5, 170])
            plt.ylim([ylim_min, ylim_max])

            plt.legend()
            plt.ioff()                      # Stopping a plot window to be made
            plt.savefig(file_name + '.png') # Saving the figure
            plt.close()                     # Closing the figure

            os.chdir('../')                 # Going back to the original folder

    if plot_bool:
        if Time_stamps is not None:  # If the time stamps are available
            if len(Time_stamps) == 1:
                plt.scatter(Time_stamps, sim_output_plot.cpu(), label='Model')
                plt.scatter(Time_stamps, sim_labels_plot.cpu(), label='Experiment')
            else:
                plt.plot(Time_stamps, sim_output_plot.cpu(), label='Model')
                plt.plot(Time_stamps, sim_labels_plot.cpu(), label='Experiment')
        else:  # If not
            if len(sim_output_plot) == 1:
                plt.scatter(0, sim_output_plot.cpu(), label='Model')
                plt.scatter(0, sim_labels_plot.cpu(), label='Experiment')
            else:
                plt.plot(sim_output_plot.cpu(), label='Model')
                plt.plot(sim_labels_plot.cpu(), label='Experiment')
        plt.title(title)
        if fixed_lims:
            ylim_max = 1
            ylim_min = 0
        else:
            ylim_max = max(th.amax(sim_output_plot).item() + 0.1, 1.1)
            ylim_min = min(th.amin(sim_output_plot).item() - 0.1, -0.1)
        plt.xlabel('Cycle number')
        plt.ylabel('SOH')
        plt.ylim([ylim_min, ylim_max])
        plt.legend()
        if block:
            plt.show()
        else:
            plt.show(block=False)
            plt.pause(5)
            plt.close()

class Torch_Dataset_Mixer(Dataset):
    """Class for mixing different dataset types.

    .. warning::
        Tests weren't very successful so development has been halted. Missing
        functionalities and attributes will cause ``AttributeError``s in other
        functions.

    Parameters
    ----------
    dataset_list : list(Torch_Dataset_LSTM)
        A list of the different datasets to be used.
    renorm : bool, optional
        Whether or not to renormalize all the data with respect to the whole
        set. Experimental, didn't yield good results.
    """

    def __init__(self, dataset_list, renorm=False):

        super().__init__()
        from Cleaning import Denormalize, Normalize
        self.dataset_list = dataset_list
        self.inputs = []
        self.output = th.Tensor([])
        inputs_list = []
        output_list = []
        for dataset in dataset_list:
            if renorm:
                for tensor in dataset.inputs:
                    inputs_list.append(Denormalize(tensor, dataset.norm, inputs=True))
                output_list = output_list + Denormalize(dataset.output, dataset.norm, inputs=False).tolist()
            else:
                self.inputs = self.inputs + dataset.inputs
                self.output = th.cat([self.output, dataset.output], dim=0)

        if renorm:
            inputs, outputs, norm = Normalize(inputs_list, output_list)
            self.inputs = inputs
            self.output = outputs
            self.norm = norm
        else:
            self.norm = None

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, item):
        ent = self.inputs[item]
        sai = self.output[item]
        return ent, sai

    def report(self):
        string = ''
        for dataset in self.dataset_list:
            string += '\n\n'+str(dataset)+'\n'
            string += dataset.report()
        return string

    def dataset_info(self, name):
        #TODO
        pass