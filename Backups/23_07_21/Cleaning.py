import matplotlib.pyplot as plt
from Nasa_Classes import *
import torch as th
import pandas as pd
from PyAstronomy import pyasl
from scipy import stats
import numpy as np

def clean_cycle(Cycle, i_cutoff=2e-2, min_len=1):
    """
    Function for cleaning current, voltage and temperature data for a given
    cycle.

    The cleaning is done by removing outliers and then the first few and last
    points of the cycle, when the current is being adjusted.
    Impedance cycles are not supported, only charge and discharge.

    This function is called upon initialization of a Clean_Battery class
    (see Nasa_Classes.py for more information).

    .. note::
        This function uses the convention where the discharge current is
        negative and the charge current positive.
        If the cycle is deemed as too short or abnormal, then all of the arrays
        are returned as None.

    .. warning::
        This discharge cycle cleaning only works for constant current (CC)
        discharge cycles. This must be adapted for more complex discharge
        curves if they are to be used in the future.

    Parameters
    ----------
    Cycle : Nasa_Cycle
        The Cycle we want to clean.
    i_cutoff : float, default = 2e-2
        The current cut-off value to be used to determine the end of the charge
        cycles [A].
    min_len : int, default = 1
        The minimum amount of points a charge cycle must have to be considered
        as relevant. Equal to one by default. Something similar is done during
        the resampling stages in the dataset creation.

    Returns
    -------
    Cycle_t : ndarray[float]
        The cycle's time stamps after cleaning [s].
    Cycle_i : ndarray[float]
        The cycle's current values after cleaning [A].
    Cycle_v : ndarray[float]
        The cycle's voltage values after cleaning [V].
    Cycle_T : ndarray[float]
        The cycle's temperature after cleaning [ºC].

    Raises
    ------
    AssertionError
        If `Cycle.type` == 'impedance'.
        If `min_len` < 1.
    """

    assert Cycle.type != 'impedance', 'Only charge and discharge cycles are allowed.'
    assert min_len >= 1, 'min_len must be greater or equal to 1'

    # Numpy arrays
    Cycle_t = Cycle.data.time  # Time data
    Cycle_i = Cycle.data.meas_i  # Current data
    Cycle_v = Cycle.data.meas_v  # Voltage data
    Cycle_T = Cycle.data.meas_t  # Temperature data

    # Taking away outliers that may mess up with the rest of the cleaning:
    Cycle_t, Cycle_i, Cycle_v, Cycle_T = Input_outliers(Cycle_t, Cycle_i,
                                                        Cycle_v, Cycle_T)

    if Cycle.type == 'charge':
        # Cutting out the beginning of the CCCV curve
        try:
            idx, _ = np.where(Cycle_i > 0.90 * max(Cycle_i))
            idx_1 = idx[0]  # Taking the very first value that is > 0.9*max(Cycle_i)
            # We check where the current is > 0.9*max(i), then we take away everything that comes before it,
            # which corresponds to the part where the current hasn't yet stabilized at the CC value.
        except:
            # Just in case, I don't think this is even mathematically possible.
            return None, None, None, None

        # Now cutting the final part of the charge curve
        if Cycle_i[-1, 0] <= i_cutoff:  # If the current reaches the cut-off value:
            idx, _ = np.where(Cycle_i[idx_1:, :] < i_cutoff)  # Then check when it becomes less than it
            idx_2 = idx[0] + idx_1  # And save the first value
            # Notice that we add idx_1 because we called np.where for Cycle_i[idx_1:, :] and not simply Cycle_i
            # This is because there may be points at the beginning of the curve that satisfy the condition
        else:  # If the current never reaches the cut-off value, then:
            idx_2 = len(Cycle_i)  # idx_2 is the last point in Cycle_i

    elif Cycle.type == 'discharge':
        # We start by taking away the first few points, where the current hasn't yet reached its max (min because
        # it's negative) value.
        try:  # Analogous to the charge cycle.
            idx, _ = np.where(Cycle_i < 0.90 * min(Cycle_i))
            idx_1 = idx[0]
        except:
            # Just in case, I don't think this is even mathematically possible.
            return None, None, None, None

        # Here we cut off the last bit of the curve where the current is yet again lifted
        # It has been written in function of a Voltage cut-off instead, since this is more general and should
        # work for more complex discharge cycles (to be studied).
        # It definitely works for harmonic discharge cycles
        if Cycle_v[-1, 0] >= min(Cycle_v):  # Analogous to the charge cycle.
            idx, _ = np.where(Cycle_v[idx_1:, :] <= min(Cycle_v))
            idx_2 = idx[0] + idx_1
        else:
            idx_2 = len(Cycle_v)

    # Taking only the relevant data
    Cycle_t = Cycle_t[idx_1: idx_2, :]
    Cycle_i = Cycle_i[idx_1: idx_2, :]
    Cycle_v = Cycle_v[idx_1: idx_2, :]
    Cycle_T = Cycle_T[idx_1: idx_2, :]

    # Checking if there are any NaNs and taking them away.aa
    if np.any(np.isnan(np.reshape(Cycle_i, -1))).item():

        nan_t = ~np.isnan(Cycle_t)
        nan_i = ~np.isnan(Cycle_i)
        nan_v = ~np.isnan(Cycle_v)
        nan_T = ~np.isnan(Cycle_T)
        isnt_nan = nan_t & nan_i & nan_T & nan_v

        Cycle_t = Cycle_t[isnt_nan]
        Cycle_i = Cycle_i[isnt_nan]
        Cycle_v = Cycle_v[isnt_nan]
        Cycle_T = Cycle_T[isnt_nan]

        Cycle_t = Cycle_t.reshape((len(Cycle_t), 1))
        Cycle_i = Cycle_i.reshape((len(Cycle_i), 1))
        Cycle_v = Cycle_v.reshape((len(Cycle_v), 1))
        Cycle_T = Cycle_T.reshape((len(Cycle_T), 1))

    if len(Cycle_t) < min_len:  # If the Cycle is too short, than we'll return None to skip this cycle.
        return None, None, None, None

    return Cycle_t, Cycle_i, Cycle_v, Cycle_T

def Input_outliers(Cycle_t, Cycle_i, Cycle_v, Cycle_T):
    """
    This function identifies and takes away the outliers present in the
    dataset.

    Most of the work is done by the function `Outliers_idx`.

    Parameters
    ----------
    Cycle_t : ndarray[float]
        The cycle's time stamps [s].
    Cycle_i : ndarray[float]
        The cycle's current values [A].
    Cycle_v : ndarray[float]
        The cycle's voltage values [V].
    Cycle_T : ndarray[float]
        The cycle's temperature [ºC].

    Returns
    -------
    Cycle_t : ndarray[float]
        The cycle's time stamps without the outliers[s].
    Cycle_i : ndarray[float]
        The cycle's current values without the outliers[A].
    Cycle_v : ndarray[float]
        The cycle's voltage values without the outliers[V].
    Cycle_T : ndarray[float]
        The cycle's temperature without the outliers[ºC].
    """

    # Creating a set
    outlier_set = set()
    # Fetching the outliers' indexes for each variable (excluding the time)
    outliers_i = Outliers_idx(Cycle_i)
    outliers_v = Outliers_idx(Cycle_v)
    outliers_T = Outliers_idx(Cycle_T)
    # Making a set with all of them (this way there are no repeated values)
    outlier_set = outlier_set.union(outliers_i, outliers_v, outliers_T)
    # Turning it into a list again
    outlier_list = list(outlier_set)
    outlier_list.sort(reverse=True)
    # Making a bool array
    outlier_array = np.ones(len(Cycle_t), dtype=bool)
    for i in outlier_list:
        outlier_array[i] = False
    # Redefining the arrays without the outliers
    Cycle_t = Cycle_t[outlier_array]
    Cycle_i = Cycle_i[outlier_array]
    Cycle_v = Cycle_v[outlier_array]
    Cycle_T = Cycle_T[outlier_array]

    return Cycle_t, Cycle_i, Cycle_v, Cycle_T

def Input_outliers_v2(Data, outlier_lim=20):
    """
    This function identifies and takes away the outliers present in the
    dataset.

    This is a second version of the previous `Input_outliers` function. It is
    more general in the sense that it takes as input a whole dataset matrix
    instead of the separate vectors and can thus study differently sized data.

    Most of the work is still done by the function `Outliers_idx`.

    .. note::
        The ``Data`` array MUSTN'T CONTAIN THE TIME STAMPS.

    Parameters
    ----------
    Data : ndarray[float]
        The cycle's data. Each column must be a different feature.

    Returns
    -------
    An np.ndarray of the `New_Data`, without the outliers, and a boolean
    np.ndarray `outlier_array` with information about where the outliers are
    located.
    """

    # Creating a set
    outlier_set = set()
    # Fetching the outliers' indexes for each variable (excluding the time)
    features = Data.shape[1]

    for i in range(features):
        outliers = Outliers_idx(Data[:, i], outlier_lim=outlier_lim)
        outlier_set = outlier_set.union(outliers)

    # Turning it into a list again
    outlier_list = list(outlier_set)
    outlier_list.sort(reverse=True)
    # Making a bool array
    outlier_array = np.ones(len(Data), dtype=bool)
    for i in outlier_list:
        outlier_array[i] = False
    # Redefining the arrays without the outliers
    New_Data = Data[outlier_array]

    return New_Data, outlier_array


def Input_outliers_v3(Data, feature_index=0):
    """
    This function identifies and takes away the outliers present in the
    dataset.

    This is a second version of the previous `Input_outliers` function. It is
    more general in the sense that it takes as input a whole dataset matrix
    instead of the separate vectors and can thus study differently sized data.

    Most of the work is still done by the function `Outliers_idx`.

    .. note::
        The ``Data`` array MUSTN'T CONTAIN THE TIME STAMPS.

    Parameters
    ----------
    Data : ndarray[float]
        The cycle's data. Each column must be a different feature.

    Returns
    -------
    An np.ndarray of the `New_Data`, without the outliers, and a boolean
    np.ndarray `outlier_array` with information about where the outliers are
    located.
    """

    features = Data.shape[1]
    df = pd.DataFrame(data=Data, columns=[str(i) for i in range(features)])

    # Outlier detection
    threshold = 3
    window = 50
    df['median'] = df[str(feature_index)].rolling(window, center=True).median()
    df['std'] = df[str(feature_index)].rolling(window, center=True).std()
    df = df.fillna(method='bfill')
    df = df.fillna(method='ffill')
    difference = np.abs(df[str(feature_index)] - df['median']) / df['std']
    outlier_idx = ~(difference > threshold)
    df = df[outlier_idx]

    df.drop(['median', 'std'], axis=1, inplace=True)
    cycle_data = df.to_numpy()

    return cycle_data, outlier_idx


def Outliers_idx(inputs, force=False, outlier_lim=20):
    """
    Identifies outliers in a data array and outputs their indexes.

    As of know, it only removes outliers based on moving window averages
    (generalized Extreme Studentized Deviate (ESD) test - implemented through
    PyAstronomy's pyasl.pointDistGESD).

    Parameters
    ----------
    inputs : ndarray
        The array of data we want to find the outliers of.
    force : bool, optional
        Whether or not to let the data pass (don't take away any outliers) when
        ZeroDivisionError is raised during outlier identification.

        This error may be raised when the array is completely constant (but not
        always, apparently...) - depending on what we're studying, it may make
        sense to discard the data (force = False) and in others not (force =
        True).

    Returns
    -------
    list[ints]
        A list containing the index of each outlier.
    """

    try:
        # Finding the local outliers (based on moving windows)
        r = pyasl.pointDistGESD(inputs, outlier_lim, alpha=0.01)
    except ZeroDivisionError:
        r = [0, 1]
        if force:
            r[1] = []   # No outliers
        else:
            r[1] = [i for i in range(len(inputs))]  # All outliers

    return r[1]

def Resampling_v3(Cycle, time_step=30., min_len=1, nan_lim=50,
                  time_diff_lim=None):
    """
    This function resamples a cycle's data to a desired time step, while also
    keeping an eye for NaNs and correcting them when necessary.

    .. note::
        If the cycle is deemed as too short or abnormal, then all of the arrays
        are returned as `None`.

    Parameters
    ----------
    Cycle : Clean_Cycle or Nasa_Cycle
        A cycle_like object containing time, current, voltage and temperature
        data (in [s], [A], [V] and [ºC], respectively).
    time_step : float, default = 30
        The desired time step for resampling [s].
    min_len : int, default = 1
        The minimum length (number of points) the cycle must have at the end of
        the resampling for it to be considered relevant.
    nan_lim : int, default = 50
        The amount of NaNs the resampled vector must have to print a warning and
        return None.
    time_diff_lim : float, optional
        The biggest time jump the raw dataset can have before resampling.

        If the dataset has a time jump that is too big, resampling is
        compromised and the data cannot be used (simple interpolation can't
        reproduce the missing data), returns ``None``s.
        If no value is suggested, it defaults to 10 times the ``time_step``
        size.

    Returns
    -------
    New_Time : ndarray[float]
        The new, resampled, time tensor [s].
    New_Current : ndarray[float]
        The new, resampled, Current tensor [A].
    New_Voltage : ndarray[float]
        The new, resampled, Voltage tensor [V].
    New_Temperature : ndarray[float]
        The new, resampled, Temperature tensor [ºC].

    Raises
    ------
    AssertionError
        If Cycle.type == 'impedance'.
        If min_len < 1.
    """

    assert Cycle.type != 'impedance', 'Only charge and discharge cycles are allowed.'
    assert min_len >= 1, 'min_len must be greater or equal to 1'

    if time_diff_lim is None:
        time_diff_lim = 5*time_step

    # print(Cycle.number)
    # Numpy arrays
    Time = Cycle.data.time  # Time data

    # Checking right away
    if len(Time) < min_len:
        return None, None, None, None
    Current = Cycle.data.meas_i  # Current data
    Voltage = Cycle.data.meas_v  # Voltage data
    Temperature = Cycle.data.meas_t  # Temperature data

    # Concatenating the time and the data
    Cat_Data = np.concatenate((Time, Current, Voltage, Temperature), axis=1)  # Cat stands for concatenated

    # Creating a data frame
    df = pd.DataFrame(data=Cat_Data,
                      columns=['Time', 'Current', 'Voltage', 'Temperature'])

    # Checking if the time_step is greater than the biggest sampling step:
    df['delta_time'] = df['Time'].diff()  # The time difference between each step

    # This check is done in order to avoid the creation of NaN entries.
    if df['delta_time'].max() > time_diff_lim:
        # If the biggest sample time step is greater than the imposed
        # time_diff_lim
        # print('Big time jump: ', str(df['delta_time'].max()),
        #       '>', str(time_diff_lim), 's.')

        return None, None, None, None

    # Transforming the time data in datetime form:
    df['Time'] = pd.to_datetime(df['Time'] * 1e9)

    df.set_index('Time', inplace=True)
    # Taking any NaN's away:
    # print('df w/ NaNs: ', len(df))
    df = df.dropna()
    # print('df w/o NaNs: ', len(df))
    # plt.clf()
    # plt.plot(df.index, df.Current)
    # plt.plot(df.index, df.Voltage)

    # Resampling
    df = df.resample(str(time_step) + 's').mean()

    # Checking for NaNs created during resampling:
    if df.isnull().values.any():        # If there's any
        idx_list = df.index.tolist()
        nan_idx0 = np.unique(np.where(df.isnull())[0]).tolist() # The lines
        nan_idx0.sort(reverse=True)                             # reverse order

        # Warning message is printed if too many NaNs
        if len(nan_idx0) > nan_lim:
            # print("Warning, ", len(nan_idx0),
            #       " NaNs in this Cycle after resampling")

            return None, None, None, None
            # plt.plot(df.index, df.Current)
            # plt.plot(df.index, df.Voltage)
            # plt.show()

        # Find the groups of NaNs
        groups = ranges(nan_idx0)   # ranges is a function, see below.
        groups.sort(reverse=True)   # reverse order

        for tuple in groups:
            i, j = tuple
            if i == 0 or j == len(df) - 1:
                # If the first or last lines are NaN, then we have to
                # take away all of the NaN points in this group because no
                # simple interpolation is possible
                df.drop(df.index[[k for k in range(i, j+1)]], inplace=True)
            else:
                for k in range(i, j+1):
                    # mean interpolation
                    df.loc[idx_list[k]] = (df.loc[idx_list[k-1]] + df.loc[idx_list[j+1]])/2

    # if there are still any NaNs:
    if df.isnull().values.any():
        print("Persistent NaNs.")
        print('Cycle number: ', Cycle.number)
        return None, None, None, None

    # Saving the new data
    New_Time = (df.index.astype('int64').to_numpy() / 1e9)  # Time (in seconds)
    New_Current = (df['Current'].to_numpy())                # Current
    New_Voltage = (df['Voltage'].to_numpy())                # Voltage
    New_Temperature = (df['Temperature'].to_numpy())        # Temperature

    # If the length is too short:
    if len(New_Time) < min_len:
        return None, None, None, None

    return New_Time, New_Current, New_Voltage, New_Temperature

def Resampling_flexible(Cycle, variable='Voltage', time_step=0.01, min_var=None,
                        max_var=None, min_len=1, nan_lim=1, time_diff_lim=None):
    """
    This function resamples a cycle's data to a desired step, while also
    keeping an eye for NaNs and correcting them when necessary.

    It differs from Resampling_v3 because it is more general: it can resample
    data based on variables other than time. In particular, it's used for
    resampling capacity data in function of voltage for the Severson dataset
    creation (Torch_Dataset_SEV in FFNN.py). In it, features are extracted from
    the difference between two Q(V) curves.

    .. note::
        If the cycle is deemed as too short or abnormal, then all of the arrays
        are returned as `None`.

    Parameters
    ----------
    Cycle : Clean_Sev_Cycle or Clean_Cycle
        A cycle_like object containing time, current, voltage and temperature
        data (for a Clean_Cycle class), or time, current, voltage, temperature
        and capacity data (for Clean_Sev_Cycle classes).

        Units: [s], [A], [V], [ºC] and [A.h] (when applicable) respectively.

    variable : str, default = 'Voltage'
        The desired variable to use as a base for the resampling.
        Can either be 'Time', 'Voltage', 'Current', 'Temperature' or 'Capacity'.
    time_step : float, default = 0.01
        The desired step for resampling. The unit will depend on the variable
        but it has to follow the input convention.
    min_var : float, default = 2.
        The lower bound of the resampling.
    max_var : float, default = 4.
        The higher bound of the resampling.
    min_len : int, default = 1
        The minimum length (number of points) the cycle must have at the end of
        the resampling for it to be considered relevant.
    nan_lim : int, default = 1
        The amount of NaNs the resampled vector must have to print a warning and
        return None.
    time_diff_lim : float, optional
        The biggest time jump the raw dataset can have before resampling.

        If the dataset has a time jump that is too big, resampling is
        compromised and the data cannot be used (simple interpolation can't
        reproduce the missing data), returns ``None``.
        If no value is suggested, it defaults to 10 times the ``time_step``
        size.

    Returns
    -------
    An np.ndarray with the resampled data.

    Raises
    ------
    AssertionError
        If Cycle.type == 'impedance' (when the input Cycle is a Clean_Cycle).
        If min_len < 1.
    NameError
        If the dataset type is unknown. Only Nasa and Severson dataset types are
        currently supported (``Clean_Cycle`` and ``Clean_Sev_Cycle`` classes).
    """
    Dataset_type = type(Cycle).__name__

    if Dataset_type == 'Clean_Cycle':
        assert Cycle.type != 'impedance', 'Only charge and discharge cycles are allowed.'
    assert min_len >= 1, 'min_len must be greater or equal to 1'

    if time_diff_lim is None:
        time_diff_lim = 10*time_step

    # Numpy arrays
    Time = Cycle.data.time  # Time data

    # Checking right away
    if len(Time) < min_len:
        return None

    Current = Cycle.data.meas_i  # Current data
    Voltage = Cycle.data.meas_v  # Voltage data

    if Dataset_type == 'Clean_Sev_Cycle':
        Temperature = Cycle.data.meas_t  # Temperature data
        if Cycle.type == 'charge':
            Capacity = Cycle.data.chr_capacity
        else:
            Capacity = Cycle.data.dis_capacity
        # Concatenating the time and the data
        Cat_Data = np.concatenate((Time, Current, Voltage,
                                   Temperature, Capacity), axis=1)  # Cat stands for concatenated
        features = ['Time', 'Current', 'Voltage', 'Temperature', 'Capacity']
    elif Dataset_type == 'Clean_Cycle':
        Temperature = Cycle.data.meas_t  # Temperature data
        Cat_Data = np.concatenate((Time, Current, Voltage,
                                   Temperature), axis = 1)
        features = ['Time', 'Current', 'Voltage', 'Temperature']
    elif 'Clean_Coin_Battery':
        Capacity = Cycle.data.meas_q
        Cat_Data = np.concatenate((Time, Current, Voltage, Capacity),
                                  axis=1)  # Cat stands for concatenated
        features = ['Time', 'Current', 'Voltage', 'Capacity']
    else:
        raise NameError("Unknown dataset type: "+Dataset_type)

    # Creating a data frame
    df = pd.DataFrame(data=Cat_Data,
                      columns=features)

    # plt.plot(df['Time'], df['Voltage'])
    # plt.show(block=False)
    # plt.pause(0.1)

    # Checking if the step is greater than the biggest sampling step:
    # df['delta_time'] = df[variable].diff()  # The time difference between each step

    # This check is done in order to avoid the creation of NaN entries.
    # if df['delta_time'].max() > time_diff_lim:
    #     # If the biggest sample time step is greater than the time_step
    #     print('Big time jump: ', str(df['delta_time'].max()),
    #           '>', str(time_diff_lim), '.')
    #
    #     return None, None, None, None

    # Transforming the time data in datetime form:
    # df['Time'] = pd.to_datetime(df['Time'] * 1e9)

    df.set_index(variable, inplace=True)

    # Taking any NaN's away:
    df = df.dropna()

    # Taking away any duplicated values:
    df = df[~df.index.duplicated()]

    # Resampling:
    if min_var is None:
        min_var = df.index.min()
    if max_var is None:
        max_var = df.index.max()

    resampled_variable = np.linspace(min_var, max_var,
                                     int((max_var-min_var)/time_step))

    df = df.reindex(df.index.union(resampled_variable)).interpolate('values').loc[resampled_variable]
    # df = df.resample(str(time_step) + 's').mean()
    df.index.name = variable

    # Checking for NaNs created during resampling:
    if df.isnull().values.any():        # If there's any

        idx_list = df.index.tolist()
        nan_idx0 = np.unique(np.where(df.isnull())[0]).tolist() # The lines
        nan_idx0.sort(reverse=True)                             # reverse order

        # Warning message is printed if too many NaNs
        if len(nan_idx0) > nan_lim:
            print("Warning, ", len(nan_idx0),
                  " NaNs in this Cycle after resampling")

            return None

        # Find the groups of NaNs
        groups = ranges(nan_idx0)   # ranges is a function, see below.
        groups.sort(reverse=True)   # reverse order

        for tuple in groups:
            i, j = tuple
            if i == 0 or j == len(df) - 1:
                # If the first or last lines are NaN, then we have to
                # take away all of the NaN points in this group because no
                # simple interpolation is possible
                df.drop(df.index[[k for k in range(i, j+1)]], inplace=True)
            else:
                for k in range(i, j+1):
                    # mean interpolation
                    df.loc[idx_list[k]] = (df.loc[idx_list[k-1]] + df.loc[idx_list[j+1]])/2

    # if there are still any NaNs:
    if df.isnull().values.any():
        print("Persistent NaNs.")
        print('Cycle number: ', Cycle.number)
        return None

    # I should test turning df directly into a Data thing
    # Saving the new data
    df = df.reset_index(level=[variable])
    Data = df.to_numpy()

    if len(Data) < min_len:
        return None


    return Data

def ranges(nums):
    """
    Finds groups of consecutive numbers in a list and outputs a tuple with the
    first and last values of each group.

    Parameters
    ----------
    nums : list[int]
        The list with the integers.

    Returns
    -------
    list[tuple(int,int)]
        A list containing the tuples.

    Examples
    --------
    >>> A_list = [1, 2, 3, 4, 7, 9, 10]
    >>> ranges(A_list)
    [(1,4), (7,7), (9,10)]
    """
    nums = sorted(set(nums))
    gaps = [[s, e] for s, e in zip(nums, nums[1:]) if s + 1 < e]
    edges = iter(nums[:1] + sum(gaps, []) + nums[-1:])
    return list(zip(edges, edges))

def Outliers(cycle_couple, capacity_curve, z_lim=3):
    """
    Removes outliers from the battery's output capacity data.

    As of know, it removes outliers based on moving window averages (generalized
    Extreme Studentized Deviate (ESD) test - implemented through PyAstronomy's
    pyasl.pointDistGESD) and on zscore (by taking away any remaining points with
    a z-score > z_lim). Where z_score_i = x_i - mean(x)/std(x).

    This function is called by the class `Clean_Battery` in Nasa_Classes.py.

    Parameters
    ----------
    cycle_couple : list[tuple(Clean_Cycle)]
        The list of tuples associated to each cycle couple. It's a necessary
        input because it will be updated as we remove outliers.
    capacity_curve : list[tuple(float)]
        List of tuples (`capacity points`). Each tuple corresponds to one
        capacity point. The first entry is the cycle_couple index associated to
        the capacity value - which is the second entry.
    z_lim : float
        The maximum zscore allowed when searching for outliers.

    Returns
    -------
    cycle_couple : list[tuple(Clean_Cycle)]
        The list of tuples associated to cycle couple after outlier removal.
    capacity_curve : ndarray
        A 2D array containing in the cycle_couple index in the first column and
        the associated capacity value in the second.
    """

    # Turning the output list into a numpy array:
    capacity_curve = np.array(capacity_curve)

    # Taking away local outliers (based on moving windows)
    outlier_index = Outliers_idx(capacity_curve[:, 1], force=True)
    cycle_numbers = list(capacity_curve[outlier_index, 0])
    cycle_numbers.sort(reverse=True)
    # print("Number of outliers: ", len(cycle_numbers))

    # Only the non_outliers:
    for i in cycle_numbers:
        cycle_couple.pop(int(i))             # Updating the cycle_couple (taking away the outliers)

    # Updating the capacity curve (important because of the change in indexing!)
    capacity_curve = []
    for i in range(len(cycle_couple)):
        capacity_point = (i, cycle_couple[i][1].data.capacity)
        capacity_curve.append(capacity_point)
    capacity_curve = np.array(capacity_curve)

    # Taking away bigger outliers:
    # We check which points have a z_score < z_lim
    bool_array = (np.abs(stats.zscore(capacity_curve[:, 1:2])) < z_lim)
    outlier_array = bool_array.all(axis=1)
    # Outlier indexes:
    outlier_index = np.flip(np.where(outlier_array == False)[0])

    while len(outlier_index) > 0:
        # print("Number of outliers: ", len(outlier_index))
        cycle_numbers = list(capacity_curve[outlier_index, 0])
        cycle_numbers.sort(reverse=True)
        # Removing outliers:
        for i in cycle_numbers:
            cycle_couple.pop(int(i))

        # Recreating the capacity curve list with the new numbering:
        capacity_curve = []
        for i in range(len(cycle_couple)):
            capacity_point = (i, cycle_couple[i][1].data.capacity)
            capacity_curve.append(capacity_point)

        # Restarting to see how it goes:
        capacity_curve = np.array(capacity_curve)
        bool_array = (np.abs(stats.zscore(capacity_curve[:, 1:2])) < z_lim)
        outlier_array = bool_array.all(axis=1)
        # Outlier indexes:
        outlier_index = np.flip(np.where(outlier_array == False)[0])

    return cycle_couple, capacity_curve

def Normalize(inputs, output, norm_type='zscore', norm=None, concatenate=False):
    """
    Normalizes both the inputs and outputs based either on z-score or on min-max
    normalization.

    All of the necessary normalization information is stored in the `norm` tuple
    as follows:

    If `norm_type` == 'zscore':
        norm = (inputs' mean, inputs' standard deviation,
                outputs' mean, outputs' standard deviation, 'zscore')
    elif `norm_type` == 'minmax':
        norm = (min(inputs), max(inputs),
                min(output), max(output), 'minmax')

    This normalization information can then be used as an input for this
    function for normalizing other datasets.

    .. note::
        If a `norm` tuple is specified, the specified `norm_type` will be
        overridden by the one in `norm[4]`.

    The normalization procedure does take into account cases where the standard
    deviation or the difference max - min equals zero (constant inputs or
    outputs). The standard deviation and (max - min) values may be overridden to
    1 in order to avoid division by zero. In some cases, an AssertionError may
    be raised.

    Parameters
    ----------
    inputs : list[Tensor]
        The list containing the input tensors.
    output : list[float]
        The list containing the output capacity values.
    norm_type : {'zscore', 'minmax'}
        A string indicating which kind of normalization to do.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.
        In entered as input, `norm_type` will be overridden by norm[4] and the
        given information will be used for the normalization
    concatenate : bool
        Whether or not to return the normalized concatenated inputs
        instead of normalizing each input separately and returning a list.

    Returns
    -------
    inputs : list[Tensor] or Tensor
        The normalized inputs. If concatenate == True, then it's a simple Tensor,
        otherwise, it's a list of tensors.
    output_stack: Tensor
        The normalized outputs.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string)
        The normalization information used for normalizing the inputs and the
        outputs.

    Raises
    ------
    AssertionError
        When all of the inputs or all of the outputs are constant.
        When normalization type is not supported
    """

    # Stacking inputs. This can be done for calculating the normalization information but the actual normalization
    # may have to be done case by case (concatenate = False).
    inputs_stack = th.cat(inputs, dim=0)
    # Stacking outputs.
    output_stack = th.FloatTensor(output)

    if norm is not None:        # If a norm tuple has been specified
        norm_type = norm[4]     # The norm_type is overridden by the one contained in the norm.

    assert norm_type in ['zscore', 'minmax'], 'Normalization type not supported'

    if norm_type == 'zscore':
        if norm is None:        # If no norm tuple has been specified

            # Checking if there are entries for which the standard deviation
            # is zero:
            std_in = th.std(inputs_stack, dim=0)    # inputs
            bool_std = (std_in == 0)                # if yes, then:
            std_in[bool_std] = 1                    # Those entries are set to 1, in order to avoid division by zero.

            std_out = th.std(output_stack, dim=0)   # outputs
            bool_std = (std_out == 0)               # Analogously
            std_out[bool_std] = 1

            norm = (th.mean(inputs_stack, dim=0), std_in,               # creation of the norm tuple
                    th.mean(output_stack, dim=0), std_out, norm_type)

        output_stack = (output_stack - norm[2])/norm[3]                 # output normalization

        if concatenate:     # If we can concatenate, then:
            inputs = (inputs_stack - norm[0]) / norm[1]     # the inputs are overridden by the normalized input stack
        else:
            inputs_2 = []
            for i in range(len(inputs)):    # Unfortunately this is necessary for the LSTM if the sequences are of
                                            # different lengths.
                inputs_2.append((inputs[i] - norm[0]) / norm[1])

    elif norm_type == 'minmax':

        if norm is None:  # no norm tuple has been specified.
            min_in = th.amin(inputs_stack, dim=0)
            max_in = th.amax(inputs_stack, dim=0)
            min_out = th.amin(output_stack, dim=0)
            max_out = th.amax(output_stack, dim=0)

            assert ~(min_in == max_in).all(), "Constant inputs"     # asserting that the inputs are not all constant.
            assert ~(min_out == max_out), "Constant output"         # asserting that the output is not constant

            bool_minmax = (min_in == max_in)    # Some inputs can, however, be constant. This is specially true for
            # min_in[bool_minmax] = 1             # the k-means approach, where some batteries may have 0 points at some
            max_in[bool_minmax] += 1             # clusters. In that case, (max - min) is set to 1 to avoid division by 0

            norm = (min_in, max_in,
                    min_out, max_out, norm_type)    # creation of the norm tuple

        output_stack = (output_stack - norm[2])/(norm[3] - norm[2])     # output normalization

        if concatenate:     # Analogous to the z-score normalization
            inputs = (inputs_stack - norm[0]) / (norm[1] - norm[0])
        else:
            inputs_2 = []
            for i in range(len(inputs)):
                inputs_2.append((inputs[i] - norm[0])/(norm[1] - norm[0]))

    return inputs_2, output_stack, norm

def Denormalize(tensor, norm, inputs=False):

    # For outputs!
    assert norm is not None, "'norm' tuple is None."
    tensor = tensor.to('cpu')

    if inputs:
        if norm[4] == 'zscore':
            tensor = (tensor * norm[1].cpu()) + norm[0].cpu()
        elif norm[4] == 'minmax':
            tensor = (tensor * (norm[1].cpu() - norm[0].cpu())) + norm[0].cpu()
        else:
            raise AssertionError(norm[4], ' normalization not supported')
    else:
        if norm[4] == 'zscore':
            tensor = (tensor * norm[3].cpu()) + norm[2].cpu()
        elif norm[4] == 'minmax':
            tensor = (tensor * (norm[3].cpu() - norm[2].cpu())) + norm[2].cpu()
        else:
            raise AssertionError(norm[4], ' normalization not supported')

    return tensor

if __name__ == '__main__':
    import os
    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    os.chdir('.//Separate_pickles')

    ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt'] # Files to be ignored.