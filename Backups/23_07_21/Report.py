from Nasa_Classes import *
import torch as th
import pickle
import re


def Report(Model, train_loader, valid_loader, Training_info=None,
           file_name='Report', remove_files=False):
    """
    This function creates a Report text file containing information about the
    model's training and saves important information in separate files.

    This function is called by the `Train` function in Training.py at the end
    of the last epoch.

    The text report will include:

    - Information about the model itself (specificities).
    - Training information.
        Learning rate, number of epochs, batch size, best and last epochs and
        their losses, loss function and optimizer.
    - Information about the training and validation datasets.

    This function will also save the best and last epochs' models
    (`best_model.txt` and `base_model.txt`) and the Training_Class object with
    all of the training information.

    .. note::
        The model's parameters are changed to the best epoch's.

    Parameters
    ----------
    Model : torch.nn.Module
        The trained model.
    train_loader : torch.utils.data.DataLoader
        The training dataset's DataLoader class.
    valid_loader : torch.utils.data.DataLoader
        The validation dataset's DataLoader class.
    Training_info : Training_Class, optional
        The Training_Class where the training information is stored.
    file_name : str, default = 'Report'
        The name of the report file.
    remove_files : bool, default = False
        Whether or not to remove the Epochs' files after saving the last and
        best epochs' results. This may be desirable if many epochs were run.
    """
    # Opening the file
    FILE = open(file_name + '.txt', 'w')
    # Writing about the model
    FILE.write('MODEL\n' + Model.report())

    # Writing about the training itself:
    if Training_info is not None:
        FILE.write('\nTRAINING\n' + Training_info.report())

    # Writing about the training dataset:
    FILE.write('\nTRAINING DATASET\n'
               + '  Batch size: ' + str(train_loader.batch_size) + '\n'
               + train_loader.dataset.report()
               )

    # Writing about the validation dataset:
    FILE.write('\nVALIDATION DATASET\n'
               + '  Batch Size: ' + str(valid_loader.batch_size) + '\n'
               + valid_loader.dataset.report()
               )

    # Closing the file:
    FILE.close()

    # Saving the Training information class
    with open('Training_Info.txt', 'wb') as f:
        pickle.dump(Training_info, f)

    # Saving dataset information that can be loaded by the load_save:
    train_loader.dataset.dataset_info('train')
    valid_loader.dataset.dataset_info('valid')

    # Saving the datasets: (they were too big so I've disabled this option)
    # with open('train_dataset.txt', 'wb') as f:
    #     pickle.dump(train_loader.dataset, f)
    #
    # with open('valid_dataset.txt', 'wb') as f:
    #     pickle.dump(valid_loader.dataset, f)

    # Saving the last epoch's model:
    with open('base_model.txt', 'wb') as f:
        Model = Model.to('cpu')
        pickle.dump(Model, f)

    th.save(Model.state_dict(), 'base_model_dict.txt')

    # Saving the best epoch's model:
    try:
        os.chdir('.//Epochs')
        Model.load_state_dict(
            th.load('Epoch_'+str(Training_info.best_epoch.nb + 1)+'.txt',
                                      map_location=th.device('cpu')))
        # Now that we have already loaded the best epoch's state_dict(),
        # we can delete the epochs' files
        if remove_files:            # If we want to
            files = os.listdir()    # All files in the folder
            for file_name in files:
                # Only take into account the Epoch ones (just to be safe)
                match = re.search(r'Epoch_\d{1}', file_name)
                if match is None:
                    continue
                else:
                    os.remove(file_name)
            os.chdir('../')
            os.rmdir('Epochs')  # Remove the empty directory altogether
        else:
            os.chdir('../')

        # Save the model with the best epoch's state_dict()
        with open('best_model.txt', 'wb') as f:
            pickle.dump(Model, f)

        th.save(Model.state_dict(), 'best_model_dict.txt')

    except FileNotFoundError:
        print('Epochs folder not created.')

class Training_Class:
    """
    This class was created to keep track of training and epoch information.

    Parameters
    ----------
    N_epochs : int
        Number of epochs in the training.
    loss_fn : a torch.nn. Loss function
        The loss function used in training.
    optimizer : a torch.nn Optimizer
        The optimizer used in training.
    learning_rate : float
        Training learning_rate.
    batch_size : int
        Training batch_size.

    Attributes
    ----------
    nb_epochs : int
        The number of epochs.
    loss_fn : a torch.nn. Loss function
        The loss function used in training.
    optimizer : a torch.nn Optimizer
        The optimizer used in training.
    learning_rate : float
        Training learning_rate.
    batch_size : int
        Training batch_size.
    epoch : list[Epoch_Class]
        A list of all of the training epochs. To add an epoch to this list, the
        method ``add_epoch`` is used. See Epoch_Class for more.
    best_epoch : Epoch_Class
        Epoch information about the best epoch. It is defined through the method
        ``best_epoch``.
    trains : list[Training_Class]
        A list with the information of the previous times the model has been
        trained. To add a Training_Class to the list, the method ``add_epoch``
        is used.
    """
    def __init__(self, N_epochs, loss_fn, optimizer, learning_rate, batch_size):

        self.nb_epochs = N_epochs
        self.loss_fn = loss_fn
        self.optimizer = optimizer
        self.learning_rate = learning_rate
        self.batch_size = batch_size
        self.epoch = []
        self.trains = []
        self.supp_info = ''

    def add_epoch(self, train_loss, valid_loss, nb):
        """
        Adds an epoch to self.epoch list. The epoch itself is a separate
        Epoch_Class object.

        Parameters
        ----------
        train_loss : th.Tensor
            The training dataset loss.
        valid_loss : th.Tensor
            The validation dataset loss.
        nb : int
            The epoch's number
        """

        self.epoch.append(Epoch_Class(train_loss, valid_loss, nb))

    def best_epoch_method(self):
        """
        Calculates which epoch has the lowest validation loss and defines it
        as the attribute best_epoch (Epoch_Class).
        """

        if len(self.epoch) != 0:
            valid_loss = th.zeros(len(self.epoch))

            for i in range(len(self.epoch)):
                valid_loss[i] = self.epoch[i].valid_loss

            nb = th.argmin(valid_loss).item()
            self.best_epoch = Epoch_Class(self.epoch[nb].train_loss,
                                          self.epoch[nb].valid_loss, nb)

        else:
            print('No epochs saved')

    def add_train(self, Training_info):
        """
        Stores information from a past training when a model has already been
        trained before.
        Not very useful and not very well implemented, works just as a history
        and is very rarely used.

        Parameters
        ----------
        Training_info : Training_Class
            The training information we are aggregating.
        """
        self.trains.append(Training_info)

    def supplementary_information(self, string):
        """Adds supplementary information to the report method.

        In order to adjust it to the formatting of the report, it's recommended
        to start the supplementary information with two spaces and skip a line
        ('\n') for each new item.

        Parameters
        ----------
        string : str
            The string we want to add to the report method.
        """

        self.supp_info += string

    def report(self):
        """Creates a little report with all of the information about the class.

        Returns
        -------
            A string with the report information.
        """

        string = '  Number of Epochs: ' + str(self.nb_epochs) + '\n' \
           + '  Learning rate: ' + str(self.learning_rate) + '\n' \
           + '  Batch size: ' + str(self.batch_size) + '\n' \
           + '  Epoch with smallest validation loss: ' \
                 + str(self.best_epoch.nb) + '\n' \
           + '    (Epoch number starting at 0)' + '\n' \
           + '    Training loss: ' + str(self.best_epoch.train_loss.item()) \
                 + '\n' \
           + '    Validation loss: ' + str(self.best_epoch.valid_loss.item()) \
                 + '\n' \
           + "  Last epoch's loss:" + '\n' \
           + '    Training loss:' + str(self.epoch[-1].train_loss.item()) \
                 + '\n' \
           + '    Validation loss: ' + str(self.epoch[-1].valid_loss.item()) \
                 + '\n' \
           + '  Loss function: ' + str(self.loss_fn) + '\n' \
           + '  Optimizer: ' + str(self.optimizer) + '\n' + self.supp_info

        return string


class Epoch_Class:
    """
    A class for keeping track of the training epochs.

    Parameters
    ----------
    train_loss : th.Tensor
        The training dataset loss.
    valid_loss : th.Tensor
        The validation dataset loss.
    nb : int
        The epoch's number
    """

    def __init__(self, train_loss, valid_loss, nb=None):
        self.train_loss = train_loss
        self.valid_loss = valid_loss
        self.nb = nb
