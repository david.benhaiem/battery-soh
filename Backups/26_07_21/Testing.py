from LSTM import *



class Torch_Dataset_LSTM_test(Dataset):
    """
    Creates a pytorch Dataset class that can readily be loaded with torch's
    Loader class for training and validation.

    The datasets created by this class' initialization follow You et al.'s †
    snapshot approach, where I, V data for a charge cycle are used for battery
    capacity prediction. A number ∆ (delta) of sequential (I, V) points are
    collected and turned into a 1D vector x_i. This window of points slides
    through time and each vector x_i becomes the sequential inputs of an LSTM.
    Here, these x_i vectors are already grouped in order in a XT 2D tensor and
    stored into the list 'ent'. One XT tensor = One charge cycle = One sequence.
    The list is later saved as attribute 'inputs' (self.inputs = ent).

    The output for each XT (each charge cycle) is a capacity value (in A.h).
    However, capacity values are only measured during discharge cycles. Because
    of this, the capacity values taken as outputs are picked from the discharge
    cycle that comes immediately before the charge cycle.

    .. note::
        † Full reference: G. You, S. Park and D. Oh, "Diagnosis of Electric
        Vehicle Batteries Using Recurrent Neural Networks," in IEEE Transactions
        on Industrial Electronics, vol. 64, no. 6, pp. 4885-4893, June 2017,
        doi: 10.1109/TIE.2017.2674593.

    Parameters
    ----------
    Battery_list : list[Clean_Battery]
        List with the Batteries that we want to use for creating the dataset.
    delta : int
        The size of the sliding window used to generate the x_i's.
    model_version : {1, 2, 3}
        An integer indicating which LSTM version to use.
        if `version` == 1: then only I and V data will be used.
        elif `version` == 2: then I, V and T will be taken into account.
        elif `version` == 3: then I, V and amb_T will be taken into account.
    print_bool : bool, default = False
        Whether or not to print some information while the code runs.
        Namely, if we want to print the battery's name and the number of points.
    time_step : int, default = 30
        The desired time step for the resampling procedure.
    threshold : float, default = 0.1
        The minimum capacity value to be considered as relevant [A.h].
    min_len : int, default = 10
        The minimum amount of points a charge cycle must have to be considered
        as relevant.
    fixed_len : int, optional
        if different than `None` (default value), then the charge and discharge
        curves will be limited to `fixed_len` points. This will override
        `min_len`.
    norm_type : {'zscore', 'minmax'}
        A string indicating which kind of normalization to do.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.
        In entered as input, `norm_type` will be overridden by norm[4] and the
        given information will be used for the normalization

    Attributes
    ----------
    dataset_name : str
        The name of the dataset ('Dataset_Nasa', for instance).
    batteries : list[str]
        A list with the file name of each battery used.
    dataset_format : str
        A string indicating the type of data format used: 'You et al. [56]'.
    inputs : list[Tensors]
        A list with the input tensors for each cycle.
    output : Tensor
        A tensor with the output capacity tensors for each cycle.

    Raises
    ------
    AssertionError
        If the specified value of `delta` is < 1.
        If no batteries are given.
        If the specified version is invalid.
    ValueError
        If the model_version is unknown.

    Examples
    --------
    >>> from torch.utils.data import Dataset
    >>> from LSTM import Torch_Dataset_LSTM
    >>> delta = 10
    >>> version = 2
    >>> with open('B0005.txt', 'rb') as f:
    ...     Battery = pickle.load(f)
    ...
    >>> train_dataset = Torch_Dataset_LSTM([Battery], delta, version,
    ...                                    fixed_len=150, print_bool=True)
    B0005
    165
    """
    def __init__(self, Battery_list, delta, model_version, print_bool=False,
                 time_step=30, threshold=0.1, min_len=10, fixed_len=None,
                 norm_type='zscore', norm=None,
                 ):

        super().__init__()

        assert delta >= 1, 'delta must be greater or equal to one'
        assert len(Battery_list) > 0, 'No Batteries informed'
        assert model_version in [1, 2, 3, 4, 5], 'Invalid version'

        if fixed_len is None:
            if delta != 1:
                min_len = max(delta - 1, min_len)
                # min_len is the minimum sequence length (number of points) that
                # the charge cycle must have to be taken into account.
            else:
                min_len = 1
        else:
            min_len = fixed_len + delta

        # Dataset related
        self.dataset_name = type(Battery_list[0]).__name__  # The dataset used
        self.batteries = []
        for Battery in Battery_list:
            self.batteries.append(Battery.Name+'.txt')
        self.dataset_format = 'You et al. [56]'   # The data format used
        # LSTM related
        self.delta = delta                        # The amount of points per xi
        self.model_version = model_version        # The model version
        # Cleaning related:
        self.time_step = time_step                # The time step for resampling
        self.threshold = threshold                # The threshold for cleaning
        self.min_len = min_len
        self.fixed_len = fixed_len

        ent = []        # Inputs list
        sai = []        # Output list

        if self.dataset_name == 'Clean_Coin_Battery':
            new_battery_list = []
            for Battery in Battery_list:
                for cell in Battery.cell:
                    new_battery_list.append(cell)

            Battery_list = new_battery_list
        elif self.dataset_name == 'Clean_Battery': # Nasa
            C_value = 2.1
        elif self.dataset_name == 'Clean_MIT_Battery':
            C_value = 1.1
        else:
            raise NameError("Unknown battery type: "+self.dataset_name)

        for Battery in Battery_list:
            # print('battery')
            if self.dataset_name == 'Clean_Coin_Battery':
                mass = Battery.mass
                threshold = threshold/mass
                C_value = 1.88/mass

            # max_v = 0
            # min_v = 1000
            # for cycle_couple in Battery.cycle_couple:
            #     max_t = np.amax(cycle_couple[0].data.meas_v)
            #     min_t = np.amin(cycle_couple[0].data.meas_v)
            #     if max_t > max_v:
            #         max_v = max_t
            #     if min_t < min_v:
            #         min_v = min_t

            if print_bool:
                print(Battery.Name)
            for cycle_couple in Battery.cycle_couple:
                # print('couple')
                chr_cycle = cycle_couple[0]
                dis_cycle = cycle_couple[1]

                if fixed_len is not None:
                    ti = chr_cycle.data.time[0].item()
                    tf = chr_cycle.data.time[-1].item()
                    time_step = (tf-ti)/(fixed_len+100)
                    # print(ti, tf, time_step)

                Data = Resampling_flexible(chr_cycle, variable='Time',
                                           time_step=time_step, min_len=min_len)

                if Data is None:
                    # When there aren't enough points in the cycle, Cycle_t is
                    # returned as None
                    # print('none')
                    continue
                Cycle_t = Data[:, 0]
                Cycle_i = Data[:, 1]/C_value
                Cycle_v = Data[:, 2]
                # Cycle_v = (Data[:, 2] - min_v)/(max_v - min_v)
                Cycle_T = Data[:, 3] # NOT THE TEMPERATURE IF DATASET_TYPE =
                                       # COIN

                # Cleaning will take away NaN's, the beginning and the end of
                # the discharge curves (that is, the part before the current
                # reaches the CC value and the part after when the current
                # reaches its cut-off value `i_cutoff`) and resample the time
                # according to the given time step `time_step`.

                # Calculating the real number of xis we've got
                if fixed_len is None:
                    Nb = len(Cycle_i) - delta + 1  # Number of xis in this cycle
                else:
                    Nb = fixed_len
                # min_len already take care of the cases where Nb <= 0.

                if self.model_version == 1:
                    XT = np.zeros((Nb, 2 * delta))
                    # Regrouped xi info for one whole cycle

                    for t in range(Nb):  # We're going to make one xi for each t
                        xi = np.concatenate([Cycle_i[t:t + delta],  # Current
                                             Cycle_v[t:t + delta]])  # Voltage
                        # "Add" it to the XT tensor that regroups them.
                        XT[t, :] = xi

                elif self.model_version == 2:
                    XT = np.zeros((Nb, 3 * delta))
                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             Cycle_T[t:t + delta]])# Temperature

                        XT[t, :] = xi

                elif (self.model_version == 3) or (self.model_version == 4):
                    XT = np.zeros((Nb, 2 * delta + 1))

                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             chr_cycle.amb_T*np.ones([1])])
                                             # Ambient temperature

                        XT[t, :] = xi

                elif self.model_version == 5:
                    XT = np.zeros((Nb, 3 * delta + 1))

                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             Cycle_T[t:t + delta],
                                             chr_cycle.amb_T*np.ones([1])])

                        XT[t, :] = xi
                else:
                    raise ValueError("Unknown version: "+str(self.model_version))

                Capacity = dis_cycle.data.capacity*np.ones(1)
                if Capacity.item() < threshold:
                    # If it's considered to be too close to zero
                    continue

                # The XT tensor represents one cycle and is thus one input
                ent.append(th.from_numpy(XT).float())
                # The capacity is one output.
                sai.append(Capacity)

            # The number of inputs and outputs must be the same
            assert len(ent) == len(sai), 'Unequal input and output lengths'

        print(ent[-1])


        inputs, output, norm = Normalize(ent, sai, norm_type=norm_type,
                                         norm=norm)

        if print_bool:
            print(len(inputs))

        self.inputs = inputs  # saving the inputs
        self.output = output  # saving the outputs
        self.norm = norm  # saving the norm

        self.path = None
        self.transform = None

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, item):

        ent = self.inputs[item]
        sai = self.output[item]

        return ent, sai

    def report(self):
        """Creates a little report with all of the information about the class.

        Returns
        -------
            A string with the report information.
        """
        string = '  Size of the training set: ' + str(len(self.inputs)) \
                 + '\n' \
               + '  Threshold (minimum capacity): ' + str(self.threshold) \
                 + '\n' \
               + '  Minimum length: ' + str(self.min_len) + '\n' \
               + '  Resampling time step: ' + str(self.time_step) + '\n' \
               + '  Fixed Length: ' + str(self.fixed_len) + '\n'\
               + '  Batteries used: ' + str(self.batteries) + '\n' \
               + '  Normalization information: ' + str(self.norm) + '\n'

        return string

    def dataset_info(self, name):
        """Method for creating and information tuple that can be loaded for
        creating a similar dataset in the future.

        Parameters
        ----------
        name : str
            The name of the file where the tuple will be dumped.
        """
        info_tuple = (self.batteries, self.time_step, self.fixed_len, self.norm)

        with open(name+'_info_tuple_.txt', 'wb') as f:
            pickle.dump(info_tuple, f)

class Torch_Dataset_LSTM_Predictions(Dataset):
    """Creates a pytorch Dataset class that can readily be loaded with pytorch's
     Loader class for training and validation.

    The datasets created by this class' initialization follow Severson et al.'s
    approach described in their article (full reference in the note). In it,
    the authors have developed a few different models, all of them based on a
    simple linear framework (here replaced by a FFNN), in order to predict
    battery life. That is, the number of cycles that a battery lasts before its
    capacity fades by 20% - which corresponds to 0.22 A.h in their dataset
    (batteries of 1.1 A.h nominal capacity). Update: Actually they predict
    log(battery life).

    Each of the different models developed in the article have similar
    structure, but different inputs. The model type can be picked through the
    variable ``model_type``, which is an initialization parameter.
    They correspond to the proposed models as follows:

    "Variance" model (``model_type = 1``):
        The model recieves as input solely the log variance of ∆Q(V)100-10.
        In detail: during a cycle's discharge, the cumulative capacity
        (Q = ∫ I dt) and the voltage vary over time. We take this curve defined
        by Q(V) for cycles 10 and 100 and subtract them (this requires a voltage
        sampling step in order to ensure logical comparison).
        The result is an array of ∆Q points, which we then take the variance and
        (then) the log (base 10) of.
    "Discharge" model (``model_type = 2``):
        This model builds on the previous one, taking other features from the
        ∆Q(V)100-10 curve (variance, minimum, skewness, kurtosis), as well as
        the discharge capacity of cycle 2 and the difference between the max
        discharge capacity (before cycle 100) and the discharge capacity of
        cycle 2.
    "Full" model
        Not yet implemented.

    .. note::
        Full reference: Severson, K.A., Attia, P.M., Jin, N. et al.
        Data-driven prediction of battery cycle life before capacity
        degradation. Nat Energy 4, 383–391 (2019).
        <https://doi.org/10.1038/s41560-019-0356-8>

    Parameters
    ----------
    Battery_list : list[Clean_Battery]
        List with the Batteries that we want to use for creating the dataset.
    begin_cycle : int, default = 10
        The cycle number for the first Q(V) curve.
    end_cycle : int, default = 100
        The cycle number for the second Q(V) curve.
    Q_lim : float, default = 0.88
        The capacity threshold for which we want to predict battery life.

        When equal to 0.88, this class will automtically get the cycle number
        through the ``cycle_life`` attribute present in the Clean_MIT_Battery
        class.

        When different than 0.88, this class will search the cycle number for
        which the battery capacity becomes lower than ``Q_lim`` and use it as
        the output.
    time_step : float, default = 0.001
        The VOLTAGE step for resampling [V].
    min_var : float, default = 2.
        The lower bound of the resampling.
    max_var : float, default = 4.
        The higher bound of the resampling.
    lim_cycle : int, optional
        If an integer is given, then the dataset output will be changed to the
        capacity at cycle `lim_cycle`. That is, the model is changed so that it
        predicts the capacity at a given cycle, instead of the cycle where a
        certain capacity value is reached.
      min_len : int, default = 10
        The minimum amount of points a charge cycle must have to be considered
        as relevant.
    model_version : {1, 2}
        An integer indicating which model type to use.
        if ``model_type`` == 1: then the "Variance" model is picked.
        elif ``version`` == 2: then the "Discharge" model is picked.
        elif ``version`` == 3: then the "Full model is picked. (NOT IMPLEMENTED)
    fixed_len : int, optional
        if different than `None` (default value), then the capacity(voltage)
        curves will be limited to `fixed_len` points. This *will* override
        `min_len`.
    time_diff_lim : float, default = 0.1
        The biggest variable jump the raw dataset can have before resampling.

        If the dataset has a value jump that is too big, resampling is
        compromised and the data cannot be used (simple interpolation can't
        reproduce the missing data). The cycle is thenskipped.

    norm_type : {'zscore', 'minmax'}
        A string indicating which kind of normalization to do.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.
        In entered as input, `norm_type` will be overridden by norm[4] and the
        given information will be used for the normalization
    print_bool : bool, default = False
        Whether or not to print some information while the code runs.
        Namely, if we want to print the battery's name and the number of points.

    Raises
    ------
    NameError
        If an unknown dataset type is given. This class currently supports Nasa
        and Severson dataset types (``Clean_Battery`` and ``Clean_MIT_Battery``
        classes).
    NotImplementedError
        If the given model_type is 3.
    Value Error
        If the given model_type is different from 1, 2 or 3.
        If there are no entries (inputs).

    """
    def __init__(self, Battery_list, delta=1, begin_cycle=10, end_cycle=100, Q_lim=0.88,
                 time_step=0.01, min_var=2., max_var=4., lim_cycle=None,
                 min_len=10,  model_version=1, fixed_len=None, time_diff_lim=0.1,
                 norm_type='zscore', norm=None, print_bool=False,
                 ):

        super().__init__()

        if fixed_len is None:
            if delta != 1:
                min_len = max(delta - 1, min_len)
                # min_len is the minimum sequence length (number of points) that
                # the charge cycle must have to be taken into account.
            else:
                min_len = 1
        else:
            min_len = fixed_len + delta

        # Dataset related
        self.dataset_name = type(Battery_list[0]).__name__      # Basically the dataset used (Nasa,...)
        self.batteries = []
        for Battery in Battery_list:                            # The batteries used in this dataset creation
            self.batteries.append(Battery.Name+'.txt')
        self.dataset_format = 'Severson et al.'                 # The format of the torch dataset (related to the net)
        # Cleaning
        self.time_step = time_step                              # The time step chosen for resampling
        self.time_diff_lim = time_diff_lim                      # The limit time jump allowed during resampling
        self.min_len = min_len                                  # Minimum cycle length required to take it into account
        self.fixed_len = fixed_len                              # The fixed length used.

        self.model_version = model_version
        self.begin_cycle = begin_cycle
        self.end_cycle = end_cycle
        self.min_var = min_var
        self.max_var = max_var
        self.lim_cycle = lim_cycle
        self.Q_lim = Q_lim
        self.delta = delta

        ent = []                                    # Input list
        sai = []                                    # Output list

        if self.dataset_name == 'Clean_Coin_Battery':
            new_battery_list = []
            for Battery in Battery_list:
                for cell in Battery.cell:
                    new_battery_list.append(cell)

            Battery_list = new_battery_list


        for Battery in Battery_list:
            if print_bool:
                print(Battery.Name)

            Q_list = []
            V_list = []

            # If the number of cycles is too small:
            if len(Battery.cycle_couple) < end_cycle+1:
                print(Battery.Name, ' length = ', len(Battery.cycle_couple),
                      ' < end_cycle = ', end_cycle)
                continue
            elif lim_cycle is not None:
                if len(Battery.cycle_couple) < lim_cycle + 1:
                    print(Battery.Name, ' length = ', len(Battery.cycle_couple),
                          ' < lim_cycle = ', lim_cycle)
                    continue


            if lim_cycle is None:
                # Checking if the capacity value for the cycle end_cycle
                # is still greater than Q_lim:
                end_capacity = Battery.cycle_couple[end_cycle][1].data.capacity
                last_capacity = Battery.cycle_couple[-1][1].data.capacity
                if end_capacity < Q_lim:
                    print(Battery.Name, ': end_capacity = ', end_capacity,
                          ' < Q_lim = ', Q_lim)
                    continue
                elif last_capacity > Q_lim:
                    print(Battery.Name, ': last_capacity = ', last_capacity,
                          ' > Q_lim = ', Q_lim)
                    continue

            for times in range(2):
                if times == 0:
                    nb = begin_cycle
                else:
                    nb = end_cycle

                cycle_couple = Battery.cycle_couple[nb]
                dis_cycle = cycle_couple[1]
                amb_T = dis_cycle.amb_T

                # Data cleaning:
                if self.dataset_name == 'Clean_Battery':
                    import scipy.integrate as integrate
                    # t, I, V, T = Resampling_flexible(dis_cycle, time_step=time_step,
                    Data = Resampling_flexible(dis_cycle, time_step=time_step,
                                               variable='Voltage',
                                               time_diff_lim=time_diff_lim,
                                               min_len=min_len,
                                               min_var=min_var,
                                               max_var=max_var)
                    if Data is None:
                        continue

                    V = Data[:, 0:1]
                    t = Data[:, 1:2]
                    I = Data[:, 2:3]
                    # T = Data[:, 3:4]

                    Q = np.zeros([len(I)])
                    I = I.reshape(-1)
                    t = t.reshape(-1)

                    for i in range(len(I)):

                        Q[i] = -integrate.simps(I[:i + 1], t[:i + 1] / 3600)

                    Q = Q.reshape([len(Q), 1])

                elif self.dataset_name == 'Clean_MIT_Battery':
                    Data = Resampling_flexible(dis_cycle, time_step=time_step,
                                               variable='Voltage',
                                               min_len=min_len,
                                               min_var=min_var,
                                               max_var=max_var)

                    if Data is None:
                        continue


                    V = Data[:, 0:1]
                    # t = Data[:, 1:2]
                    # I = Data[:, 2:3]
                    # T = Data[:, 3:4]
                    Q = Data[:, 4:5]

                else:
                    raise NameError("Unknown Dataset: "+self.dataset_name)

                if fixed_len is not None:
                    V = V[:fixed_len]
                    Q = Q[:fixed_len]

                V_list.append(V)
                Q_list.append(Q)

            # Common values of voltage:
            intersection = np.intersect1d(V_list[0], V_list[1])

            # Indexes of the intersection:
            idx_list_0 = [i for i in range(len(V_list[0])) if
                       V_list[0][i] in intersection] # list of indexes
            idx_list_1 = [i for i in range(len(V_list[1])) if
                       V_list[1][i] in intersection] # list of indexes

            # Only considering the capacity values for the intersection.
            Q_list[0] = Q_list[0][idx_list_0]
            Q_list[1] = Q_list[1][idx_list_1]

            Q_V = Q_list[1] - Q_list[0] # The Q(V) curve.



            if fixed_len is None:
                Nb = len(Q_V) - 2*delta  # Number of xis in this cycle
                # Nb = len(Q_V) - delta  # Number of xis in this cycle
            else:
                Nb = fixed_len
            # min_len already take care of the cases where Nb <= 0.

            if self.model_version == 1:
                XT = np.zeros((Nb, 2 * delta))
                # Regrouped xi info for one whole cycle

                for t in range(Nb):  # We're going to make one xi for each t
                    try:
                        xi = Q_V[t: t + 2*delta].reshape(2*delta)
                    except ValueError:
                        print(Q_V.shape, t, Nb, Q_V[t:t+2*delta].shape, 2*delta)
                        raise ValueError("Error")
                    # "Add" it to the XT tensor that regroups them.
                    XT[t, :] = xi

            elif self.model_version == 2:
                XT = np.zeros((Nb, 3 * delta))
                for t in range(Nb):
                    xi = Q_V[t: t + 3*delta].reshape(3*delta)

                    XT[t, :] = xi

            elif (self.model_version == 3) or (self.model_version == 4):
                XT = np.zeros((Nb, 2 * delta + 1))

                for t in range(Nb):
                    xi = np.concatenate([Q_V[t: t + 2*delta].reshape(delta),
                                         amb_T])
                    # Ambient temperature

                    XT[t, :] = xi

            elif self.model_version == 5:
                XT = np.zeros((Nb, 3 * delta + 1))

                for t in range(Nb):
                    xi = np.concatenate([Q_V[t: t + 3*delta].reshape(delta),
                                         amb_T])

                    XT[t, :] = xi
            else:
                raise ValueError("Unknown version: " + str(self.model_version))

            if lim_cycle is None:
                last_cycle = np.where(Battery.capacity_curve[:,1] < Q_lim)[0][0]
                last_cycle = (Battery.cycle_couple[last_cycle][0].number)
                # last_cycle = np.log(Battery.cycle_couple[last_cycle][0].number)
                if (Q_lim == 0.88) and (self.dataset_name=='Clean_MIT_Battery'):
                    last_cycle = (Battery.cycle_life)
                    # last_cycle = np.log(Battery.cycle_life)
            else:
                last_cycle = Battery.cycle_couple[lim_cycle][1].data.capacity

            # if model_type == 1:
            #     entry = th.Tensor([Var_dQ]).view(1)
            # elif model_type == 2:
            #     entry = th.Tensor([Min_dQ, Var_dQ, Skw_dQ,
            #                        Kut_dQ, Dis_c2, Diff_Q]).view(6)
            # elif model_type == 3:
            #     raise NotImplementedError("Not implemented.")
            # else:
            #     raise ValueError("Unknown version/model_type: "+str(model_type))

            entry = th.from_numpy(XT).float()

            ent.append(entry)
            sai.append(th.Tensor([last_cycle]).view(1))

            assert len(ent) == len(sai), 'Unequal input and output lengths'

        if print_bool:
            print(len(ent))

        # Input and output normalization, for the whole dataset:
        if len(ent) == 0:
            raise ValueError('No entries')

        inputs, output, norm = Normalize(ent, sai, norm_type=norm_type, norm=norm)
                                         # concatenate=True)

        self.inputs = inputs    # saving the inputs
        self.output = output    # saving the outputs
        self.norm = norm        # saving the norm

        self.path = None
        self.transform = None

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, item):

        ent = self.inputs[item]
        sai = self.output[item]

        return ent, sai

    def report(self):
        """Creates a little report with all of the information about the class.

        Returns
        -------
            A string with the report information.
        """
        string = '  Size of the training set: ' + str(len(self.inputs)) + '\n' \
               + '  Minimum length: ' + str(self.min_len) + '\n' \
               + '  Model version: ' + str(self.model_version) + '\n' \
               + "  ∆Q's first cycle: " + str(self.begin_cycle) + '\n'\
               + "  ∆Q's last cycle: " + str(self.end_cycle) + '\n'\
               + '  Resampling step: ' + str(self.time_step) + '\n'\
               + '  Resampling range: [' + str(self.min_var) + ',' \
                + str(self.max_var) + ']' + '\n'\
               + '  Fixed length: ' + str(self.fixed_len) + '\n'

        if self.lim_cycle is not None:
            string += '  Cycle number for Q prediction: ' + str(self.lim_cycle) + '\n'
        else:
            string += '  Q value for cycle prediction: ' + str(self.Q_lim) + '\n'

        string += '  Batteries used: ' + str(self.batteries) + '\n' \
                + '  Normalization information: ' + str(self.norm) + '\n'

        return string

    def dataset_info(self, name):
        """Method for creating and information tuple that can be loaded for
        creating a similar dataset in the future.

        Parameters
        ----------
        name : str
            The name of the file where the tuple will be dumped.
        """
        info_tuple = (self.batteries, self.delta,
                      self.begin_cycle, self.end_cycle,
                      self.time_step, self.Q_lim, self.lim_cycle,
                      self.model_version, self.min_var, self.max_var,
                      self.norm)

        with open(name+'_info_tuple_.txt', 'wb') as f:
            pickle.dump(info_tuple, f)

if __name__ == '__main__':

    print(min([1,2]))

    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    os.chdir('.//Separate_pickles')

    with open('B0005.txt', 'rb') as f:
        battery = pickle.load(f)

    dataset = Torch_Dataset_LSTM_test([battery], 10, 4)
    # from Coin_Classes import *
    #
    # os.chdir('.//Datasets/Tarascon/Coins/Raw_Data')
    # for i in range(1, k):
    #     Battery = Dataset_Coin(i)
    #     Clean_Battery = Clean_Coin_Battery(Battery)
    #     os.chdir('../')
    #     os.chdir('.//Clean_Batteries')
    #     with open(Clean_Battery.Name + '.txt', 'wb') as f:
    #         pickle.dump(Clean_Battery, f)
    #     os.chdir('../')
    #     os.chdir('.//Raw_Data')


    # os.chdir('.//Reports/2021-06-17 11_44_09.411163')
    #
    # with open('Training_Info.txt', 'rb') as f:
    #     Training_INFO = pickle.load(f)
    # with open('base_model.txt', 'rb') as f:
    #     Model = pickle.load(f)
    #
    # os.chdir('../../')
    #
    #
    # os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    # os.chdir('.//Separate_pickles')
    #
    # device = th.device('cuda:0' if th.cuda.is_available() else 'cpu')
    # print(th.cuda.is_available())
    # print('device:', device)
    #
    # # Model = Model.to(device)
    #
    #
    # DELTA = 10
    # time_step = 60
    # fixed_len = 75  # 150 * 30 = 4500s = 1h15min
    # batch_size = 1
    # version = 4 #Version 1
    # lin_dropout = 0.3
    #
    # HIDDEN_DIM = 20
    # DROPOUT = 0
    #
    # Nb_Epochs = 100
    # Learning_Rate = 1e-5
    #
    # Model = LSTM_v4(DELTA, HIDDEN_DIM, pooling_layer='bi_mean',
    #                drop_out=DROPOUT, version=version, lin_layers=1,
    #                lin_dropout=0.3).to(device)
    #
    # files = os.listdir()  # All files in the folder
    # file_list = []  # List with the valid files
    # for file_name in files:
    #     match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
    #     # the battery files
    #     if match == None:
    #         continue
    #     else:
    #         file_list.append(file_name)
    # file_list.sort()
    #
    # # Files to be ignored:
    # ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt', 'B0054.txt']
    #
    # # The big one
    # train = ['B0018.txt',  # 24ºC
    #          #  'B0025.txt', 'B0026.txt', 'B0027.txt',  # 24ºC
    #          'B0029.txt', 'B0030.txt', 'B0031.txt',  # 43ºC
    #          'B0036.txt',  # 24ºC
    #          'B0038.txt', 'B0039.txt',  # 44ºC et 24ºC
    #          'B0042.txt', 'B0043.txt',  # 44ºC et 4ºC
    #          'B0045.txt', 'B0046.txt',  # 4ºC
    #          'B0053.txt', 'B0055.txt']  # 4ºC
    #
    # # with open('B0042.txt', 'rb') as f:
    # #     Battery = pickle.load(f)
    # #
    # # train_dataset = Torch_Dataset_LSTM([Battery], DELTA, Model.version,  # Dataset
    # #                                    print_bool=True, time_step=time_step, fixed_len=fixed_len)
    #
    # # Training files:
    # Train_list = []
    # for file_name in file_list:
    #     if file_name in train:
    #         with open(file_name, 'rb') as f:
    #             Battery = pickle.load(f)
    #         Train_list.append(Battery)
    #
    # train_dataset = Torch_Dataset_LSTM(Train_list, DELTA, Model.version,  # Dataset
    #                                    print_bool=True, time_step=time_step, fixed_len=fixed_len)
    #
    # if train_dataset.fixed_len is None:
    #     train_loader = DataLoader(dataset=train_dataset, batch_size=1,  # Loader
    #                               shuffle=True, num_workers=0)
    # else:
    #     train_loader = DataLoader(dataset=train_dataset, batch_size=batch_size,  # Loader
    #                               shuffle=True, num_workers=0)
    #
    # if train_dataset.fixed_len is None:
    #     sim_train_loader = DataLoader(dataset=train_dataset, batch_size=1, shuffle=False, num_workers=0)
    # else:
    #     sim_train_loader = DataLoader(dataset=train_dataset, batch_size=len(train_dataset), shuffle=False, num_workers=0)
    #
    # simulate(sim_train_loader, Model, block=False, device=device)
    #
    #
    # # Validation files:
    # Valid_list = []
    # for file_name in file_list:
    #     if (file_name not in train) and (file_name not in ignore):
    #         with open(file_name, 'rb') as f:
    #             Battery = pickle.load(f)
    #         Valid_list.append(Battery)
    #
    # valid_dataset = Torch_Dataset_LSTM(Valid_list, DELTA, Model.version,  # Dataset
    #                                    print_bool=True, time_step=time_step,
    #                                    norm=train_dataset.norm, fixed_len=train_dataset.fixed_len)
    #
    # if train_dataset.fixed_len is None:
    #     valid_loader = DataLoader(dataset=valid_dataset, batch_size=1,  # Loader
    #                               shuffle=False, num_workers=0)
    # else:
    #     valid_loader = DataLoader(dataset=valid_dataset, batch_size=len(valid_dataset),  # Loader
    #                           shuffle=False, num_workers=0)
    #
    # #simulate(valid_loader, Model, block=False, device=device)
    #
    # os.chdir('../../../../')
    #
    # Train_info, _, _, date = Train(train_loader, valid_loader, Model, Nb_Epochs,
    #                                plot_bool=True, LR=Learning_Rate,
    #                                report=True, sim_loader=sim_train_loader,
    #                                print_bool=True, device=device)
    #
    # simulate(sim_train_loader, Model, block=True, device=device)
    # simulate(valid_loader, Model, block=True, device=device)
    #
    # os.chdir('.//Reports/'+date)
    # load_info = (Train_info.best_epoch.nb + 1, train, ignore, fixed_len, time_step, True, train_dataset.norm)
    # with open('load_info.txt', 'wb') as f:
    #     # best_epoch, train, ignore, fixed_len, time_step, resampling, norm = pickle.load(f)
    #     pickle.dump(load_info, f)
    # os.chdir('../../')
    #
    # from Load_Save import *
    #
    # Load_Save(date, device)