import os
import torch as th
from RNN import *
from Training import *
from torch.utils.data import DataLoader
from datetime import datetime
import re
from Coin_Classes import *
from Testing import *
# from LSTM_tests import *
from LSTM import *
from MIT_Classes import *
import random
from Load_Save import *
import LSTM_with_norm as wn
import Load_Save_with_norm as ls
import LSTM_tests_resampling as tr
import seaborn as sns
sns.set()

device = th.device('cuda:0' if th.cuda.is_available() else 'cpu')

report_name = '2021-07-12 11_45_20.807808-MIT_v1'
os.chdir('.//Reports/'+report_name)
with open('best_model.txt', 'rb') as f:
    base_model = pickle.load(f)
if base_model.version == 1:
    Model = wn.LSTM_v1(
        base_model.delta, base_model.hidden_dim, norm=base_model.norm,
        version=base_model.version, state_dict=[base_model.lstm.state_dict(),
                                                base_model.lin1.state_dict()]
        )
elif base_model.version == 4:
    Model = wn.LSTM_v4(
        base_model.delta, base_model.hidden_dim, norm=base_model.norm,
        version=base_model.version, state_dict=[base_model.lstm.state_dict(),
                                                base_model.lin1.state_dict()]
        )
else:
    raise NameError("Invalid version: ", str(base_model.version))

os.chdir('../../')

DELTA = base_model.delta
fixed_len = 150
batch_size = 1
time_step = None
LSTM_version = base_model.version
norm_type = base_model.norm[4]
adimensionalize=True

cycle_dict = {0:10, 1:20, 2:30, 3:50, 4:75, 5:100, 6:150, 7:200}

for i in range(6):
    cycle_lim = cycle_dict[i]

    # os.chdir('.//Datasets/NASA/B. Saha and K Goebel/Separate_pickles')
    # dataset_name = 'Nasa'

    # os.chdir('.//Datasets/Severson et al./Cleaned_v2/2. 2017-06-30')
    # dataset_name = 'Sev'

    os.chdir('.//Datasets/Tarascon/Coins/Clean_Batteries')
    dataset_name = 'Coin'

    files = os.listdir()  # All files in the folder
    file_list = []  # List with the valid files

    for file_name in files:
        match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
        # the battery files
        if match == None:
            continue
        else:
            file_list.append(file_name)
    file_list.sort()

    if dataset_name == 'Nasa':
        ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt']

        # The big one
        train = ['B0018.txt',  # 24ºC
                 #  'B0025.txt', 'B0026.txt', 'B0027.txt',  # 24ºC
                 'B0029.txt', 'B0030.txt', 'B0031.txt',  # 43ºC
                 'B0036.txt',  # 24ºC
                 'B0038.txt', 'B0039.txt',  # 44ºC et 24ºC
                 'B0042.txt', 'B0043.txt',  # 44ºC et 4ºC
                 'B0045.txt', 'B0046.txt',  # 4ºC
                 'B0053.txt', 'B0055.txt']  # 4ºC

        # train = ['B0006.txt', 'B0041.txt', 'B0045.txt']

    elif dataset_name == 'Coin':

        complete = ['B' + str(i).zfill(4) + '.txt' for i in range(1, 24)]

        train = complete[:5] + complete[15:19]
        # train = ['B0001.txt', 'B0003.txt', 'B0005.txt', 'B0006.txt',
        #          'B0012.txt', 'B0013.txt', 'B0014.txt',
        #          'B0018.txt', 'B0020.txt', 'B0021.txt']
        # train = ['B0001.txt']
        ignore = ['B0008.txt', 'B0019.txt', 'B0023.txt']

    elif dataset_name == 'Sev':

        train_len = 36  # Doesn't do anything right now
        valid_len = 12

        train = ['B0001.txt']
        # train = ['B'+str(i).zfill(4)+'.txt' for i in range(12, 48)]
        ignore = ['B' + str(i).zfill(4) + '.txt' for i in range(15, 48)]

        # print(train, '\n', ignore)

        Train_list = []
        for file_name in file_list:
            if file_name in train:
                with open(file_name, 'rb') as f:
                    Battery = pickle.load(f)

                # print(Battery.policy)
                Train_list.append(Battery)

    # Training files:
    Train_list = []
    for file_name in file_list:
        if file_name not in train:
            continue
        else:
            with open(file_name, 'rb') as f:
                Battery = pickle.load(f)

            Train_list.append(Battery)

    # train_dataset = Torch_Dataset_LSTM(Train_list, DELTA, LSTM_version,  # Dataset
    #                                    print_bool=True, time_step=time_step,
    #                                    fixed_len=fixed_len, norm_type=norm_type,
    #                                    adimensionalize=adimensionalize,
    #                                    cycle_lim=cycle_lim)

    train_dataset = wn.Torch_Dataset_LSTM(Train_list, DELTA, LSTM_version,  # Dataset
                                          print_bool=True, time_step=time_step,
                                          fixed_len=fixed_len, norm_type=norm_type,
                                          adimensionalize=adimensionalize,
                                          cycle_lim=cycle_lim)

    # Validation files:
    Valid_list = []
    for file_name in file_list:
        if (file_name not in train) and (file_name not in ignore):
            with open(file_name, 'rb') as f:
                Battery = pickle.load(f)
            Valid_list.append(Battery)

    # valid_dataset = Torch_Dataset_LSTM(Valid_list, DELTA, LSTM_version, # Dataset
    #                                    print_bool=True, time_step=train_dataset.time_step,
    #                                    norm=train_dataset.norm,
    #                                    fixed_len=fixed_len,
    #                                    adimensionalize=adimensionalize,
    #                                    cycle_lim=cycle_lim)

    valid_dataset = wn.Torch_Dataset_LSTM(Valid_list, DELTA, LSTM_version,  # Dataset
                                          print_bool=True, time_step=train_dataset.time_step,
                                          norm=train_dataset.norm,
                                          fixed_len=fixed_len,
                                          adimensionalize=adimensionalize,
                                          cycle_lim=cycle_lim)

    # train_dataset.norm = valid_dataset.norm


    if train_dataset.fixed_len is None:
        train_loader = DataLoader(dataset=train_dataset, batch_size=1,  # Loader
                                  shuffle=True, num_workers=0)
    else:
        train_loader = DataLoader(dataset=train_dataset, batch_size=batch_size,  # Loader
                                  shuffle=True, num_workers=0)

    if train_dataset.fixed_len is None:
        sim_train_loader = DataLoader(dataset=train_dataset, batch_size=1,
                                      shuffle=False)
    else:
        sim_train_loader = DataLoader(dataset=train_dataset,
                                      batch_size=len(train_dataset), shuffle=False)

    if train_dataset.fixed_len is None:
        valid_loader = DataLoader(dataset=valid_dataset, batch_size=1,  # Loader
                                  shuffle=False, num_workers=0)
    else:
        valid_loader = DataLoader(dataset=valid_dataset, batch_size=len(valid_dataset),  # Loader
                                  shuffle=False, num_workers=0)

    os.chdir('../../../../')


    # HIDDEN_DIM = 50
    # DROPOUT = 0
    # pool_type = 'bi_mean'

    # Model = LSTM_v4(DELTA, HIDDEN_DIM, pooling_layer=pool_type,
    #                drop_out=DROPOUT, version = LSTM_version,
                #    norm = train_dataset.norm, lin_layers=1,
    #                lin_dropout=0.3,
    #                 # norm=norm
    #                 ).to(device)

    Model = Model.to(device)

    simulate(sim_train_loader, Model, device=device, denormalize=False, report=True, file_name='base_norm_train', block=False)
    simulate(valid_loader, Model, device=device, denormalize=False, report=True, file_name = 'base_norm_valid', block=False)

    if base_model.version == 1:
        Model = wn.LSTM_v1(
            base_model.delta, base_model.hidden_dim, norm=train_dataset.norm,
            version=base_model.version, state_dict=[base_model.lstm.state_dict(),
                                                    base_model.lin1.state_dict()]
            )
    elif base_model.version == 4:
        Model = wn.LSTM_v4(
            base_model.delta, base_model.hidden_dim, norm=train_dataset.norm,
            version=base_model.version, state_dict=[base_model.lstm.state_dict(),
                                                    base_model.lin1.state_dict()]
            )
    else:
        raise NameError("Invalid version: ",str(base_model.version))

    Model = Model.to(device)

    simulate(sim_train_loader, Model, device=device, denormalize=False, report=True, file_name='dataset_norm_train', plot_bool=False, block=False)
    simulate(valid_loader, Model, device=device, denormalize=False, report=True, file_name='dataset_norm_valid', plot_bool=False, block=False)

    Nb_Epochs = 40
    Learning_Rate = 1e-5

    Train_info, _, _, date = Train(train_loader, valid_loader, Model, Nb_Epochs,
                                   plot_bool=False, LR=Learning_Rate, report=True,
                                   sim_loader=None, print_bool=False,
                                   device=device, remove_bool=True)

    print(date)

    # # Best model
    simulate(sim_train_loader, Model, block=False, denormalize=False, device=device, report=True, file_name='after_training_train', plot_bool=False)
    simulate(valid_loader, Model, block=False, denormalize=False, device=device, report=True, file_name='after_training_valid', plot_bool=False)

    ls.Load_Save(date, device=device, dataset_name=dataset_name, denormalize_bool=False)
    # ls.Load_Save_v2(date, dataset_name, device=device, time_step=train_dataset.time_step,
    #              norm_test='Model', cycle_lim=cycle_lim, denormalize_bool=False)