from RNN import *
from Training import *
from torch.utils.data import DataLoader
from datetime import datetime
import re
from Coin_Classes import *
from Testing import *
# from LSTM_tests import *
from LSTM import *
from MIT_Classes import *
import random
from Load_Save import *

device = th.device('cuda:0' if th.cuda.is_available() else 'cpu')
# device = 'cpu'
print(th.cuda.is_available())
print('device:', device)

report_name = '2021-07-12 09_28_11.807309-Nasa_v4' # Nasa minmax
os.chdir('.//Reports/'+report_name)
with open('best_model.txt', 'rb') as f:
    Model = pickle.load(f)
Model = Model.to(device)
os.chdir('../../')

DELTA = 10
fixed_len = 150
batch_size = 1
time_step = None
LSTM_version = 4
norm_type = Model.norm[4]
adimensionalize=True
cycle_lim = 1e6

# os.chdir('.//Datasets/NASA/B. Saha and K Goebel/Separate_pickles')
# dataset_name = 'Nasa'

os.chdir('.//Datasets/Severson et al./Cleaned_v2/2. 2017-06-30')
dataset_name = 'Sev'

# os.chdir('.//Datasets/Tarascon/Coins/Clean_Batteries')
# dataset_name = 'Coin'

files = os.listdir()  # All files in the folder
file_list = []  # List with the valid files

for file_name in files:
    match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
    # the battery files
    if match == None:
        continue
    else:
        file_list.append(file_name)
file_list.sort()

if dataset_name == 'Nasa':
    ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt']

    # The big one
    train = ['B0018.txt',  # 24ºC
             #  'B0025.txt', 'B0026.txt', 'B0027.txt',  # 24ºC
             'B0029.txt', 'B0030.txt', 'B0031.txt',  # 43ºC
             'B0036.txt',  # 24ºC
             'B0038.txt', 'B0039.txt',  # 44ºC et 24ºC
             'B0042.txt', 'B0043.txt',  # 44ºC et 4ºC
             'B0045.txt', 'B0046.txt',  # 4ºC
             'B0053.txt', 'B0055.txt']  # 4ºC

elif dataset_name == 'Coin':

    complete = ['B' + str(i).zfill(4) + '.txt' for i in range(1, 24)]

    train = complete[:5] + complete[15:19]
    ignore = ['B0008.txt', 'B0023.txt']

elif dataset_name == 'Sev':

    train_len = 36  # Doesn't do anything right now
    valid_len = 12

    train = ['B' + str(i).zfill(4) + '.txt' for i in range(5)]
    # train = ['B'+str(i).zfill(4)+'.txt' for i in range(12, 48)]
    ignore = ['B' + str(i).zfill(4) + '.txt' for i in range(15, 48)]

    # print(train, '\n', ignore)

    # Train_list = []
    # for file_name in file_list:
    #     if file_name in train:
    #         with open(file_name, 'rb') as f:
    #             Battery = pickle.load(f)
    #
    #         # print(Battery.policy)
    #         Train_list.append(Battery)

# Training files:
Train_list = []
for file_name in file_list:
    if file_name not in train:
        continue
    else:
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)

        Train_list.append(Battery)

train_dataset = Torch_Dataset_LSTM(Train_list, DELTA, LSTM_version,  # Dataset
                                      print_bool=True, time_step=time_step,
                                      fixed_len=fixed_len,
                                      norm_type=norm_type,
                                      # norm=Model.norm,
                                      adimensionalize=adimensionalize,
                                      cycle_lim=cycle_lim)

# Validation files:
Valid_list = []
for file_name in file_list:
    if (file_name not in train) and (file_name not in ignore):
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)
        Valid_list.append(Battery)

valid_dataset = Torch_Dataset_LSTM(Valid_list, DELTA, LSTM_version,  # Dataset
                                      print_bool=True, time_step=train_dataset.time_step,
                                      norm=train_dataset.norm,
                                      fixed_len=fixed_len,
                                      adimensionalize=adimensionalize,
                                      cycle_lim=cycle_lim)

# train_dataset.norm = valid_dataset.norm


if train_dataset.fixed_len is None:
    train_loader = DataLoader(dataset=train_dataset, batch_size=1,  # Loader
                              shuffle=True, num_workers=0)
else:
    train_loader = DataLoader(dataset=train_dataset, batch_size=batch_size,  # Loader
                              shuffle=True, num_workers=0)

if train_dataset.fixed_len is None:
    sim_train_loader = DataLoader(dataset=train_dataset, batch_size=1,
                                  shuffle=False)
else:
    sim_train_loader = DataLoader(dataset=train_dataset,
                                  batch_size=len(train_dataset), shuffle=False)

if train_dataset.fixed_len is None:
    valid_loader = DataLoader(dataset=valid_dataset, batch_size=1,  # Loader
                              shuffle=False, num_workers=0)
else:
    valid_loader = DataLoader(dataset=valid_dataset, batch_size=len(valid_dataset),  # Loader
                              shuffle=False, num_workers=0)

os.chdir('../../../../')

HIDDEN_DIM = 50
DROPOUT = 0
pool_type = 'bi_mean'

print(train_dataset.norm)

# Model = LSTM_v4(DELTA, HIDDEN_DIM, pooling_layer=pool_type,
#                drop_out=DROPOUT, version=LSTM_version, lin_layers=1,
#                lin_dropout=0.3, norm=train_dataset.norm).to(device)

# Model.lstm.load_state_dict(base_model.lstm.state_dict())
# Model.lin1.load_state_dict(base_model.lin1.state_dict())
# Model.lstm.requires_grad_(False)
# Model.lin1.requires_grad_(False)

# sim_train_loader.dataset.norm = Model.norm

# print(Denormalize(train_dataset.inputs[0], train_dataset.norm, inputs=True))
# print(Denormalize(train_dataset.output[0], train_dataset.norm, inputs=False))
# print(Denormalize(Model(train_dataset.inputs[0].to(device)), train_dataset.norm))

simulate(sim_train_loader, Model, device=device, denormalize=True)
simulate(valid_loader, Model, device=device, denormalize=True)

Nb_Epochs = 50
Learning_Rate = 1e-5


Train_info, _, _, date = Train(train_loader, valid_loader, Model, Nb_Epochs,
                               plot_bool=True, LR=Learning_Rate, report=True,
                               sim_loader=None, print_bool=True,
                               device=device, remove_bool=True)

print(date)

# # Best model
simulate(sim_train_loader, Model, denormalize=True, device=device)
simulate(valid_loader, Model, denormalize=True, device=device)

Load_Save(date, device=device, dataset_name=dataset_name, denormalize_bool=True)