import torch as th
import optuna
from optuna.trial import TrialState

from LSTM import *
from FFNN import *
from RNN import *

# Optuna objective function
def objective(trial):

    # Training hyperparameters:
    learning_rate = trial.suggest_float('learning_rate', 1e-6, 1e-3)
    nb_epochs = trial.suggest_int('nb_epochs', 10, 75)
    #fixed_len = trial.suggest_int('fixed_len', 50, 300)
    #time_step = trial.suggest_int('time_step', 15, 120)
    # drop_out = trial.suggest_float('drop_out', 0, 1)
    drop_out = 0
    # hidden_dim = trial.suggest_int('hidden_dim', 10, 80)


    # print('LR: ', learning_rate)
    # print('nb_epochs: ', nb_epochs)

    # Optimizer depends on the learning_rate
    if MODEL_type == 'FFNN':
        model = FFNN_v1(nb_clusters, hidden_dim, drop_out=drop_out, norm=train_dataset.norm)
    elif MODEL_type == 'LSTM':
        model = LSTM_v1(delta, hidden_dim, pooling_layer='bi_mean', drop_out=drop_out, version=version).to(device)
    optimizer = th.optim.Adam(model.parameters(), lr=learning_rate)

    # Tensors for keeping the epochs' losses:
    Total_error = th.zeros(nb_epochs)     # Total training loss (for each epoch)
    Valid_error = th.zeros(nb_epochs)     # Total validation loss (for each epoch)

    # Epoch Loop:
    for E in range(nb_epochs):
        # print('Epoch:', E)
        Epoch_error = 0  # Initializing this epoch's loss
        model.train()  # Making sure that the model is in training mode

        # Loader loop:
        for data, label in train_loader:
            # per batch:
            data = data.to(device)  # Bringing the data and the
            label = label.to(device)  # "labels" to the chosen device (cuda or cpu)
            optimizer.zero_grad()  # Resetting the gradients
            # forward:
            outputs = model.forward(data)  # Calculating the model's outputs
            loss = loss_fn(outputs, label)  # Model's loss
            # backward:
            loss.backward()  # Gradient backpropagation
            optimizer.step()  # Parameter updates
            Epoch_error += loss.item()  # Adding this batch's loss to the the epoch loss.

        Total_error[E] = Epoch_error / len(train_loader)  # Saving the mean epoch error.

        # VALIDATION ERROR:
        Partial_Valid_error = 0  # Initializing this epoch's validation loss
        with th.no_grad():  # Ignoring gradients, since no parameter updates will happen
            model.eval()  # Setting the model to evaluation mode (no dropout etc.)

            for valid_data, valid_label in valid_loader:
                # per batch:
                valid_data = valid_data.to(device)  # Bringing the data and the
                valid_label = valid_label.to(device)  # "labels" to the chosen device (cuda or cpu)
                # forward:
                valid_outputs = model.forward(valid_data)  # Model output
                loss = loss_fn(valid_outputs, valid_label)  # Validation error
                Partial_Valid_error += loss.item()  # Adding to the epoch's validation loss

            Valid_error[E] = Partial_Valid_error / len(valid_loader)  # Saving the mean epoch validation error

            trial.report(Valid_error[E], E)

            if trial.should_prune():
                raise optuna.exceptions.TrialPruned()


    return Valid_error[-1]

if __name__ == "__main__":

    # Dataset creation:
    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    os.chdir('.//Separate_pickles')

    # Battery files:
    ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt']

    # The big one
    train = ['B0005.txt',  # 24ºC
             'B0025.txt', 'B0026.txt', 'B0027.txt',  # 24ºC
             'B0029.txt', 'B0030.txt', 'B0031.txt',  # 43ºC
             'B0036.txt',  # 24ºC
             'B0038.txt', 'B0039.txt',  # 44ºC et 24ºC
             'B0042.txt', 'B0043.txt',  # 44ºC et 4ºC
             'B0045.txt', 'B0046.txt',  # 4ºC
             'B0053.txt', 'B0055.txt']  # 4ºC

    device = th.device('cuda:0' if th.cuda.is_available() else 'cpu')
    print(th.cuda.is_available())
    print('device:', device)
    loss_fn = nn.MSELoss()

    MODEL_type = 'LSTM'

    if MODEL_type == 'LSTM':
        hidden_dim = 20
        delta = 10
        version = 2
        fixed_len = 150
        time_step = 30
        batch_size=1

    elif MODEL_type == 'FFNN':
        # Not yet changing:
        fixed_len = None
        time_step = 30
        batch_size = 1
        nb_clusters = 100
        mean_hist = True
        resampling = True

        # K-MEANS:
        try:  # If a K-means clustering has already been done with
            # that number of clusters:
            with open('kmeans' + str(nb_clusters) + '.txt', 'rb') as f:
                kmeans = pickle.load(f)
        except FileNotFoundError:  # If not:

            # Opening the complete, clean, IVT data:
            with open('IVT_Data.txt', 'rb') as f:
                IVT = pickle.load(f)
            IVT = IVT.detach().numpy()  # Converting to numpy

            # The actual kmeans clustering:
            print('Running k-means...')
            start = time.time()
            kmeans = KMeans(n_clusters=nb_clusters).fit(IVT)
            end = time.time()
            print('Finished: ' + str(nb_clusters) +
                  ' cluster k-means in ' + str(end - start) + ' seconds')

            # Saving the class:
            with open('kmeans' + str(nb_clusters) + '.txt', 'wb') as f:
                pickle.dump(kmeans, f)

    files = os.listdir()  # All files in the folder
    file_list = []  # List with the valid files
    for file_name in files:
        match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
        # the battery files
        if match == None:
            continue
        else:
            file_list.append(file_name)
    file_list.sort()

    # Files to be ignored:
    ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt',
              'B0025.txt', 'B0026.txt', 'B0027.txt', 'B0028.txt',
              'B0029.txt', 'B0030.txt', 'B0031.txt', 'B0032.txt']

    # The big one
    train = ['B0029.txt', 'B0031.txt', 'B0032.txt',  # 43ºC
             'B0038.txt', 'B0039.txt',  # 44ºC et 24ºC
             'B0042.txt', 'B0044.txt',  # 44ºC et 4ºC
             'B0046.txt',  # 4ºC
             'B0055.txt',  # 4ºC
             'B0018.txt']  # 4ºC

    # Training files:
    Train_list = []
    for file_name in file_list:
        if file_name in train:
            with open(file_name, 'rb') as f:
                Battery = pickle.load(f)
            Train_list.append(Battery)

    if MODEL_type == 'FFNN':
        # Creating the dataset:
        train_dataset = Torch_Dataset_KMEANS(Train_list, nb_clusters, kmeans=kmeans, print_bool=True,
                                             mean_hist=mean_hist, fixed_len=fixed_len, resampling=resampling,
                                             time_step=time_step)
    elif MODEL_type == 'LSTM':
        train_dataset = Torch_Dataset_LSTM(Train_list, delta, version,  # Dataset
                                           print_bool=True, time_step=time_step, fixed_len=fixed_len)

    train_loader = DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True, num_workers=0)

    # Validation files:
    Valid_list = []
    for file_name in file_list:
        if (file_name not in train) and (file_name not in ignore):
            with open(file_name, 'rb') as f:
                Battery = pickle.load(f)
            Valid_list.append(Battery)

    if MODEL_type == 'FFNN':
        valid_dataset = Torch_Dataset_KMEANS(Valid_list, nb_clusters, kmeans=kmeans, print_bool=True,
                                             norm=train_dataset.norm, mean_hist=mean_hist,
                                             fixed_len=fixed_len, resampling=resampling, time_step=time_step)
    elif MODEL_type == 'LSTM':
        valid_dataset = Torch_Dataset_LSTM(Valid_list, delta, version,  # Dataset
                                           print_bool=True, time_step=time_step,
                                           norm=train_dataset.norm, fixed_len=train_dataset.fixed_len)

    valid_loader = DataLoader(dataset=valid_dataset, batch_size=len(valid_dataset), shuffle=False, num_workers=0)

    os.chdir('../../../../')

    study = optuna.create_study(study_name='hyperparameters')
    study.optimize(objective, n_trials=100)

    pruned_trials = study.get_trials(deepcopy=False, states=[TrialState.PRUNED])
    complete_trials = study.get_trials(deepcopy=False, states=[TrialState.COMPLETE])

    print("Study statistics: ")
    print("  Number of finished trials: ", len(study.trials))
    print("  Number of pruned trials: ", len(pruned_trials))
    print("  Number of complete trials: ", len(complete_trials))

    print("Best trial:")
    trial = study.best_trial

    print("  Value: ", trial.value)

    print("  Params: ")
    for key, value in trial.params.items():
        print("    {}: {}".format(key, value))