from Cleaning import *
from Training import *
from torch.utils.data import Dataset, DataLoader
import torch.nn.functional as F
import torch.nn as nn
import re
import warnings

class LSTM_v1(nn.Module):
    """LSTM type neural net for Battery SOH estimation.

    This model acts just like :class:`LSTM.LSTM_v1`, but includes the
    normalisation parameters as optimizable model parameters. Of course, this
    also means that the normalisation is done inside the model itself in the
    :func:`forward` method.

    For more information about the model's architecture, check the documentation
    for :class:`LSTM.LSTM_v1`.

    Parameters
    ----------
    delta : int
        The size of the sliding window used to generate the :math:`x_i` s.
    hidden_dim : int
        Hidden dimension of the LSTM (also its main output dimension).
    pooling_layer : {'last', 'mean', 'bi_mean"}, default = 'bi_mean'
        Decides the type of pooling layer:

        * ``'last'`` (only takes the last result),
        * ``'mean'`` (takes the mean of the results) and
        * ``'bi_mean'`` (takes the mean of the product).

        The default value is strongly recommended. It's the one that's had the
        best results, in the article and in our tests.

    drop_out : float
        The probability of the dropout layer that comes right after the LSTM.
        Must be a number between 0 and 1.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.

        .. warning::
            Although the model *can* be initialized with a ``None`` norm,
            the :func:`forward` method won't be able to be used. This should be
            done solely if the user intends to initialize it right after through
            the :func:`from_model` method.

    version : int, default = 1
        The model version to be used.

        * ``version = 1`` corresponds to the model created by the article cited.
        * ``version = 2`` corresponds to the same model, but adapted to take the
        cycle's temperature data as well.

        * ``version = 3`` is similar to version 1, but takes the ambient
        temperature as an extra input.

    lin_layers : int, default = 1
        The number of linear regression layers. As of now, cannot be greater
        than two (**any value different than 1 will set it to two**).
    lin_dropout : float, optional
        The value for the dropout layer that comes after the first linear layer
        in the regression step. Will only be taken into accouunt if
        ``lin_layers`` > 1.

    Raises
    ------
    AssertionError
        If the version in different than 1, 2 or 3.
        If an unsupported pooling_layer is informed.

    Examples
    --------
    >>> from torch.utils.data import Dataset
    >>> from LSTM import *
    >>> delta = 10
    >>> hidden_dim = 20
    >>> version = 2
    >>> model = LSTM_v1(delta, hidden_dim, pooling_layer='last', version=version)
    >>> with open('B0005.txt', 'rb') as f:
    ...     Battery = pickle.load(f)
    ...
    >>> train_dataset = Torch_Dataset_LSTM([Battery], delta, version, fixed_len=150)
    >>> estimated_capacity = model(train_dataset.inputs[0])
    >>> loss_function = th.nn.MSELoss()
    >>> loss = loss_function(estimated_capacity, train_dataset.output[0])
    2.2862
    """
    def __init__(self, delta, hidden_dim, pooling_layer='bi_mean', drop_out=0,
                 norm=None, version=1, lin_layers=1, lin_dropout=0):

        super().__init__()

        assert version in [1, 2, 3], \
            "Unsupported version. For versions 4 and 5, call LSTM_v4 instead."

        # Model information:
        self.model_name = 'LSTM'                    # The kind of model
        self.version = version                      # The kind of model
        self.variant = 'with norm'
        self.dataset_format = 'You et al. [56]'     # The dataset format
        self.pooling_layer = pooling_layer          # The type of pooling layer
        # Normalization information:
        self.norm = norm                            # The normalization used

        # Important dimensions
        self.delta = delta                          # The sliding window size
        self.hidden_dim = hidden_dim                # Hidden dimension (LSTM)

        # Input Dimension (number of input variables)
        if version == 1:
            self.inputs_dim = 2 * delta             # (I, V) * delta
        elif version == 2:
            self.inputs_dim = 3 * delta             # (I, V, T) * delta
        elif version == 3:
            self.inputs_dim = 2 * delta + 1         # (I, V) * delta + T_amb
        self.output_dim = 1

        if norm is None:
            self.norm_params_in = None
            self.norm_params_out = None

        else:
            self.norm_params_in = th.nn.Parameter(th.stack(norm[:2]),
                                                  requires_grad=True)
            self.norm_params_out = th.nn.Parameter(th.stack(norm[2:4]),
                                                   requires_grad=True)


        self.dropout_value = drop_out

        assert (pooling_layer == 'last') or (pooling_layer == 'mean') or\
               (pooling_layer == 'bi_mean'), "pooling_layer"\
               " either 'last', 'mean' or 'bi_mean"

        # If the pooling layer requires the LSTM to be of bidirectional type
        if pooling_layer == 'bi_mean':
            bi_bool = True
        else:
            bi_bool = False

        # Definition of the LSTM itself
        self.lstm = nn.LSTM(self.inputs_dim, hidden_dim, batch_first=True,
                            bidirectional=bi_bool)

        # The linear layer that maps from hidden state space to tag space
        if lin_layers == 1:
            self.lin1 = nn.Linear(hidden_dim, self.output_dim)
            self.nb_layers = 1
        else:
            self.nb_layers = 2
            self.lin0 = nn.Linear(hidden_dim, hidden_dim//2)
            self.actf = nn.LeakyReLU()
            self.lin2 = nn.Linear(hidden_dim//2, self.output_dim)
            # self.lin3 = nn.Linear(hidden_dim//4, self.output_dim)
            self.lin_dropout_fn = nn.Dropout(lin_dropout)
            self.lin1 = nn.Sequential(
                self.lin0,
                self.actf,
                self.lin_dropout_fn,
                self.lin2,
            )

    def from_model(self, old_model, disable_training=True):
        """Method for defining the model's network parameters based on an old
        model.

        Parameters
        ----------
        old_model : LSTM.LSTM_v1
            The model that we want to take the parameters of.

        disable_training : bool, default = True
            Whether or not we want to disable the training of the network
            layers.
        """
        assert old_model.version in [1,2,3], "Incompatible version, call" \
                                             "LSTM_v4 instead."

        try:
            lin_dropout = old_model.lin_dropout_fn.p
        except AttributeError:
            lin_dropout = 0

        # Adapting sizes etc.
        self.__init__(
            old_model.delta, old_model.hidden_dim,
            pooling_layer=old_model.pooling_layer,
            drop_out=old_model.dropout_value,
            norm=old_model.norm, version=old_model.version,
            lin_layers=old_model.nb_layers, lin_dropout=lin_dropout,
        )

        # Adapting parameters
        self.lstm.load_state_dict(old_model.lstm.state_dict())
        self.lin1.load_state_dict(old_model.lin1.state_dict())

        if disable_training:
            self.disable_training()

    def enable_training(self):
        """Enables the training of the LSTM's and Linear layers' parameters.
        """
        self.lstm.requires_grad_(True)
        self.lin1.requires_grad_(True)

    def disable_training(self):
        """Disables the training of the LSTM's and Linear layers' parameters.
        """
        self.lstm.requires_grad_(False)
        self.lin1.requires_grad_(False)

    def change_norm(self, norm):
        """Changes the model's ``norm`` attribute as well as the optimizable
        parameters' values.

        Parameters
        ----------
        norm : tuple
            The new normalisation tuple.
        """
        self.norm = norm                            # The normalization used
        if norm is None:
            warnings.warn("A None was passed as new norm.")
            self.norm_params_in = None
            self.norm_params_out = None
        else:
            self.norm_params_in = th.nn.Parameter(th.stack(norm[:2]),
                                                  requires_grad=True)
            self.norm_params_out = th.nn.Parameter(th.stack(norm[2:4]),
                                                   requires_grad=True)

    def update_norm(self):
        """Updates the model's ``norm`` attribute with the parameters' values.
        """
        self.norm = (
            self.norm_params_in[0].data, self.norm_params_in[1].data,
            self.norm_params_out[0].data, self.norm_params_out[1].data,
            self.norm[4]
        )

    def forward(self, inputs):
        """

        Parameters
        ----------
        inputs

        Returns
        -------

        Raises
        ------
        NotImplementedError
            When the normalization type is unknown;
            When the pooling layer type is unknown.

        """

        # Input dimensions: [Batch size, Number of Xis (sequence length), 2*Delta]
        if len(inputs.shape) < 3:   # When Batch size equals 1, the first dimension is ignored,
                                    # but it's necesssary for the LSTM. So we have to reshape the inputs.
            inputs = inputs.view(1, inputs.shape[0], inputs.shape[1])

        if self.norm[4] == 'zscore':
            inputs = (inputs - self.norm_params_in[0])/self.norm_params_in[1]
        elif self.norm[4] == 'minmax':
            inputs = (inputs - self.norm_params_in[0])/(self.norm_params_in[1] - self.norm_params_in[0])
        else:
            raise NotImplementedError("Unsupported normalization type.")

        lstm_out, _ = self.lstm(inputs)     # LSTM output
        shp_out = lstm_out.shape            # Its shape ([Batch size, Number of Xis (sequence length), self.hidden_dim])
        lstm_out = F.dropout(lstm_out, self.dropout_value)      # dropout layer

        # Pooling layer, according to the type chosen:
        if self.pooling_layer == 'last':
            pool_out = lstm_out[:, -1, :].view(shp_out[0], self.hidden_dim)         # Only the last result
        elif self.pooling_layer == 'mean':
            pool_out = th.mean(lstm_out, dim=1).view(shp_out[0], self.hidden_dim)   # Mean of the results
        elif self.pooling_layer == 'bi_mean':
            pool_out = th.mean(lstm_out[:, :, :self.hidden_dim]*                    # Mean of the multiplication
                               lstm_out[:, :, self.hidden_dim:], dim=1).view(shp_out[0], self.hidden_dim)
        else:
            raise NotImplementedError("Unknown pooling layer type: "+\
                                      self.pooling_layer)

        # Linear layer
        output = self.lin1(pool_out)

        if self.norm[4] == 'zscore':
            output = (output*self.norm_params_out[1]) + self.norm_params_out[0]
        elif self.norm[4] == 'minmax':
            output = output*(self.norm_params_out[1] - self.norm_params_out[0]) + self.norm_params_out[0]
        else:
            raise NotImplementedError("Unsupported normalization type.")

        return output

    def report(self):
        """Creates a little report with all of the information about the class.

        Returns
        -------
        str
            A string with the report information.
        """

        string = \
            f"\tModel name: {self.model_name}\n" \
            f"\tVariant: {self.variant}\n" \
            f"\tVersion: {self.version}\n" \
            f"\tDataset Format: {self.dataset_format}\n" \
            f"\tDelta: {self.delta}\n" \
            f"\tHidden layer dimension: {self.hidden_dim}\n" \
            f"\tPooling_layer: {self.pooling_layer}\n" \
            f"\tDropout prob.: {self.dropout_value}\n" \
            f"\tNumber of linear layers: {self.nb_layers}\n"

        try:
            string += f"\tDropout prob. (lin layers): {self.lin_dropout_fn.p}\n"
        except AttributeError:
            pass

        return string

class LSTM_v4(nn.Module):
    """LSTM type neural net for Battery SOH estimation.

    This model acts just like :class:`LSTM.LSTM_v4`, but includes the
    normalisation parameters as optimizable model parameters. Of course, this
    also means that the normalisation is done inside the model itself in the
    :func:`forward` method.

    For more information about the model's architecture, check the documentation
    for :class:`LSTM.LSTM_v4`.

    Parameters
    ----------
    delta : int
        The size of the sliding window used to generate the :math:`x_i` s.
    hidden_dim : int
        Hidden dimension of the LSTM (also its main output dimension).
    pooling_layer : {'last', 'mean', 'bi_mean"}, default = 'bi_mean'
        Decides the type of pooling layer:

        * ``'last'`` (only takes the last result),
        * ``'mean'`` (takes the mean of the results) and
        * ``'bi_mean'`` (takes the mean of the product).

        The default value is strongly recommended. It's the one that's had the
        best results, in the article and in our tests.

    drop_out : float
        The probability of the dropout layer that comes right after the LSTM.
        Must be a number between 0 and 1.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.

        .. warning::
            Although the model *can* be initialized with a ``None`` norm,
            the :func:`forward` method won't be able to be used. This should be
            done solely if the user intends to initialize it right after through
            the :func:`from_model` method.

    version : int, default = 4
        The model version to be used.
        It will take the ambient temperature
        as an input directly into the linear layers.

        * ``version`` = 4 is analogous to an :class:`LSTM.LSTM_v1`'s version 1
        * ``version`` = 5 is analogous to an :class:`LSTM.LSTM_v1`'s version 2.

    lin_layers : int, default = 1
        The number of linear regression layers. As of now, cannot be greater
        than two (**any value different than 1 will set it to two**).
    lin_dropout : float, optional
        The value for the dropout layer that comes after the first linear layer
        in the regression step. Will only be taken into accouunt if
        ``lin_layers`` > 1.
    
    Raises
    ------
    AssertionError
        If the version in different than 4 or 5.
        If an unsupported pooling_layer is informed.

    Examples
    --------
    >>> from torch.utils.data import Dataset
    >>> from LSTM import *
    >>> delta = 10
    >>> hidden_dim = 20
    >>> version = 4
    >>> model = LSTM_v4(delta, hidden_dim, pooling_layer='last', version=version)
    >>> with open('B0005.txt', 'rb') as f:
    ...     Battery = pickle.load(f)
    ...
    >>> train_dataset = Torch_Dataset_LSTM([Battery], delta, version, fixed_len=150)
    >>> estimated_capacity = model(train_dataset.inputs[0])
    >>> loss_function = th.nn.MSELoss()
    >>> loss = loss_function(estimated_capacity, train_dataset.output[0])
    2.2862

    """
    def __init__(self, delta, hidden_dim, pooling_layer='bi_mean', drop_out=0,
                 norm=None, version=4, lin_layers=1, lin_dropout=0):

        super().__init__()

        assert version in [4, 5], "This version is not supported. For versions 1 to 3, call LSTM_v1 instead."

        # Model information:
        self.model_name = 'LSTM'                    # The kind of model
        self.version = version                      # The kind of model
        self.variant = 'with norm'
        self.dataset_format = 'You et al. [56]'     # The dataset format
        self.pooling_layer = pooling_layer          # The type of pooling layer
        # Normalization information:
        self.norm = norm                            # The normalization used

        # Important dimensions
        self.delta = delta                          # The sliding window size
        self.hidden_dim = hidden_dim                # Hidden dimension (LSTM)

        # Input Dimension (number of input variables)
        if version == 4:
            self.inputs_dim = 2 * delta         # (I, V) * delta + T_amb
        if version == 5:
            self.inputs_dim = 3 * delta         # (I, V) * delta + T_amb
        self.output_dim = 1

        if norm is None:
            norm = (th.zeros(self.inputs_dim), th.ones(self.inputs_dim),
                    th.zeros(self.output_dim), th.ones(self.output_dim),
                    'zscore')
        else:
            pass
        self.norm_params_in = th.nn.Parameter(th.stack(norm[:2]),
                                              requires_grad=True)
        self.norm_params_out = th.nn.Parameter(th.stack(norm[2:4]),
                                               requires_grad=True)

        self.dropout_value = drop_out

        assert (pooling_layer == 'last') or (pooling_layer == 'mean') or\
               (pooling_layer == 'bi_mean'), "pooling_layer either 'last', " \
                                             "'mean' or 'bi_mean"

        # If the pooling layer requires the LSTM to be of bidirectional type
        if pooling_layer == 'bi_mean':
            bi_bool = True
        else:
            bi_bool = False

        # Definition of the LSTM itself
        self.lstm = nn.LSTM(self.inputs_dim, hidden_dim, batch_first=True,
                            bidirectional=bi_bool)

        # The linear layer that maps from hidden state space to tag space
        if lin_layers == 1:
            self.lin1 = nn.Linear(hidden_dim+1, self.output_dim)
            self.nb_layers = 1
        else:
            self.nb_layers = 2
            self.lin0 = nn.Linear(hidden_dim+1, hidden_dim // 2)
            self.actf = nn.LeakyReLU()
            self.lin2 = nn.Linear(hidden_dim // 2, self.output_dim)
            # self.lin3 = nn.Linear(hidden_dim//4, self.output_dim)
            self.lin_dropout_fn = nn.Dropout(lin_dropout)
            self.lin1 = nn.Sequential(
                self.lin0,
                self.actf,
                self.lin_dropout_fn,
                self.lin2,
            )

    def from_model(self, old_model, disable_training=True):
        """Method for defining the model's network parameters based on an old
        model.

        Parameters
        ----------
        old_model : LSTM.LSTM_v1
            The model that we want to take the parameters of.

        disable_training : bool, default = True
            Whether or not we want to disable the training of the network
            layers.
        """
        assert old_model.version in [4, 5], "Incompatible version, call" \
                                             "LSTM_v4 instead."

        try:
            lin_dropout = old_model.lin_dropout_fn.p
        except AttributeError:
            lin_dropout = 0

        # Adapting sizes etc.
        self.__init__(
            old_model.delta, old_model.hidden_dim,
            pooling_layer=old_model.pooling_layer,
            drop_out=old_model.dropout_value,
            norm=old_model.norm, version=old_model.version,
            lin_layers=old_model.nb_layers, lin_dropout=lin_dropout,
        )

        # Adapting parameters
        self.lstm.load_state_dict(old_model.lstm.state_dict())
        self.lin1.load_state_dict(old_model.lin1.state_dict())

        if disable_training:
            self.disable_training()

    def enable_training(self):
        self.lstm.requires_grad_(True)
        self.lin1.requires_grad_(True)

    def disable_training(self):
        self.lstm.requires_grad_(False)
        self.lin1.requires_grad_(False)

    def change_norm(self, norm):
        self.norm = norm                            # The normalization used
        if norm is None:
            warnings.warn("A None was passed as new norm. Norm is reset.")
            norm = (th.zeros(self.inputs_dim), th.ones(self.inputs_dim),
                    th.zeros(self.output_dim), th.ones(self.output_dim),
                    'zscore')
        else:
            pass
        self.norm_params_in = th.nn.Parameter(th.stack(norm[:2]),
                                              requires_grad=True)
        self.norm_params_out = th.nn.Parameter(th.stack(norm[2:4]),
                                               requires_grad=True)

    def update_norm(self):
        self.norm = (
            self.norm_params_in[0].data, self.norm_params_in[1].data,
            self.norm_params_out[0].data, self.norm_params_out[1].data,
            self.norm[4]
        )

    def forward(self, inputs):

        # Input dimensions: [Batch size,Number of Xis (sequence length),2*Delta]
        if len(inputs.shape) < 3:
            # When Batch size equals 1, the first dimension is ignored,
            # but it's necesssary for the LSTM. So we have to reshape the inputs.
            inputs = inputs.view(1, inputs.shape[0], inputs.shape[1])

        if self.norm[4] == 'zscore':
            inputs = (inputs - self.norm_params_in[0])/self.norm_params_in[1]
        elif self.norm[4] == 'minmax':
            inputs = (inputs - self.norm_params_in[0])/(self.norm_params_in[1] - self.norm_params_in[0])
        else:
            raise NotImplementedError("Unsupported normalization type.")

        amb_T = inputs[:, 0, -1]  # It doesn't change throughout the cycle
        lstm_inputs = inputs[:, :, :-1]

        lstm_out, _ = self.lstm(lstm_inputs)  # LSTM output
        shp_out = lstm_out.shape              # Its shape
        lstm_out = F.dropout(lstm_out, self.dropout_value)  # dropout layer

        # Pooling layer, according to the type chosen:
        if self.pooling_layer == 'last':
            pool_out = lstm_out[:, -1, :].view(shp_out[0], self.hidden_dim)
        elif self.pooling_layer == 'mean':
            pool_out = th.mean(lstm_out, dim=1).view(shp_out[0],
                                                     self.hidden_dim)
        elif self.pooling_layer == 'bi_mean':
            pool_out = th.mean(lstm_out[:, :, :self.hidden_dim]*
                               lstm_out[:, :, self.hidden_dim:],
                               dim=1).view(shp_out[0], self.hidden_dim)
        else:
            raise NameError("Unknown pooling layer type: "+self.pooling_layer)

        # Linear layer
        output = self.lin1(th.cat([pool_out, amb_T.view(shp_out[0], 1)], dim=1))

        if self.norm[4] == 'zscore':
            output = (output*self.norm_params_out[1]) + self.norm_params_out[0]
        elif self.norm[4] == 'minmax':
            output = output*(self.norm_params_out[1] - self.norm_params_out[0]) + self.norm_params_out[0]
        else:
            raise NotImplementedError("Unsupported normalization type.")

        return output

    def report(self):
        """Creates a little report with all of the information about the class.

        Returns
        -------
        str
            A string with the report information.
        """

        string = \
            f"\tModel name: {self.model_name}\n" \
            f"\tVariant: {self.variant}\n" \
            f"\tVersion: {self.version}\n" \
            f"\tDataset Format: {self.dataset_format}\n" \
            f"\tDelta: {self.delta}\n" \
            f"\tHidden layer dimension: {self.hidden_dim}\n" \
            f"\tPooling_layer: {self.pooling_layer}\n" \
            f"\tDropout prob.: {self.dropout_value}\n" \
            f"\tNumber of linear layers: {self.nb_layers}\n"

        try:
            string += f"\tDropout prob. (lin layers): {self.lin_dropout_fn.p}\n"
        except AttributeError:
            pass

        return string

class Torch_Dataset_LSTM(Dataset):
    """
    Creates a pytorch Dataset class that can readily be loaded with torch's
    Loader class for training and validation.

    .. note::
        As of know, the values for adimensionalizing variables are defined in
        this class. Ideally, they should be defined in the battery classes.
        That's something that can be improved in the future.

    .. note::
        This class can only take in one type of dataset at a time.

    Parameters
    ----------
    Battery_list : list[Clean_Battery]
        List with the Batteries that we want to use for creating the dataset.
    delta : int
        The size of the sliding window used to generate the x_i's.
    model_version : {1, 2, 3, 4, 5}
        An integer indicating which LSTM version to use.
        if *version* == 1: then only I and V data will be used.
        elif *version* == 2: then I, V and T will be taken into account.
        elif *version* == 3: then I, V and amb_T will be taken into account.
        elif *version* == 4: then I, V and amb_T will be taken into account,
        but amb_T will enter directly in the last step, the NN.
        elif *version* == 5: then I, V, T and amb_T will be taken into account,
        mixing models 2 and 4. Redundant.
    print_bool : bool, default = False
        Whether or not to print some information while the code runs.
        Namely, if we want to print the battery's name and the number of points.
    time_step : int, optional
        The desired time step for the resampling procedure.
        If ``None`` is given, than a time step will be automatically generated
        by the method ``time_step_function``.
    threshold : float, default = 0.1
        The minimum capacity value to be considered as relevant [A.h].
    min_len : int, default = 10
        The minimum amount of points a charge cycle must have to be considered
        as relevant.
    fixed_len : int, optional
        if different than ``None`` (default value), then the charge and discharge
        curves will be limited to ``fixed_len`` points. This will override
        ``min_len``.
    norm_type : {'zscore', 'minmax'}
        A string indicating which kind of normalization to do.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.
        In entered as input, ``norm_type`` will be overridden by norm[4] and the
        given information will be used for the normalization
    adimensionalize : bool, default = True
        Whether or not to make variables dimensionless before adding them to the
        inputs and output lists.
        The Capacity (output) is normalized by the nominal capacity
        (``Q_nominal``);
        The Current is normalized by the C rate (``C_rate``) -
        which is the current necessary for charging the battery in one hour
        (theoretically);
        The Voltage is normalized by the nominal voltage (``V_nominal``),
        the voltage at the plateau in the CV part of the charge curve.
    cycle_lim : int, optional
        The greatest cycle number to take into account for the dataset creation.
        Default equals ``1e6`` meaning that no cycles will be ignored.

    Attributes
    ----------
    dataset_name : str
        The name of the dataset ('Dataset_Nasa', for instance).
    batteries : list[str]
        A list with the file name of each battery used.
    dataset_format : str
        A string indicating the type of data format used: 'You et al. [56]'.
    inputs : list[Tensors]
        A list with the input tensors for each cycle.
    output : Tensor
        A tensor with the output capacity tensors for each cycle.

    Raises
    ------
    AssertionError
        If the specified value of ``delta`` is < 1.
        If no batteries are given.
        If the specified version is invalid.
        If ``model_version`` is 2 or 5 and ``self.dataset_name`` is
        ``Clean_Coin_Battery``.
    ValueError
        If the model_version is unknown.

    Examples
    --------
    >>> from torch.utils.data import Dataset
    >>> from LSTM import Torch_Dataset_LSTM
    >>> delta = 10
    >>> version = 2
    >>> with open('B0005.txt', 'rb') as f:
    ...     Battery = pickle.load(f)
    ...
    >>> train_dataset = Torch_Dataset_LSTM([Battery], delta, version,
    ...                                    fixed_len=150, print_bool=True)
    B0005
    165
    """
    def __init__(self, Battery_list, delta, model_version, print_bool=False,
                 time_step=None, threshold=0.1, min_len=150, fixed_len=150,
                 norm_type='zscore', norm=None, adimensionalize=True,
                 cycle_lim=1e6
                 ):

        super().__init__()

        assert delta >= 1, 'delta must be greater or equal to one'
        assert len(Battery_list) > 0, 'No Batteries informed'
        assert model_version in [1, 2, 3, 4, 5], 'Invalid version'

        if fixed_len is None:
            if delta != 1:
                min_len = max(delta - 1, min_len)
                # min_len is the minimum sequence length (number of points) that
                # the charge cycle must have to be taken into account.
            else:
                min_len = 1
        else:
            min_len = fixed_len + delta

        # Dataset related
        self.dataset_name = type(Battery_list[0]).__name__  # The dataset used
        self.batteries = []
        self.adimensionalize = adimensionalize

        for Battery in Battery_list:
            self.batteries.append(Battery.Name+'.txt')

        if self.dataset_name == 'Clean_Coin_Battery':
            assert model_version in [1, 3, 4], "Can't use models 2 or 5 with" \
                "Clean_Coin_Battery. No data on temperature during cycling."
            new_battery_list = []
            for Battery in Battery_list:
                for cell in Battery.cell:
                    new_battery_list.append(cell)
            Battery_list = new_battery_list

        elif self.dataset_name == 'Clean_Battery':
            C_rate = 2.1    # A
            V_nominal = 4.2 # V
            Q_nominal = C_rate

        elif self.dataset_name == 'Clean_MIT_Battery':
            C_rate = 1.1    # A
            V_nominal = 3.6 # V
            Q_nominal = C_rate

        self.min_len = min_len
        self.fixed_len = fixed_len
        self.Battery_list = Battery_list

        if time_step is None:
            time_step = self.time_step_function()

        self.dataset_format = 'You et al. [56]'   # The data format used
        # LSTM related
        self.delta = delta                        # The amount of points per xi
        self.model_version = model_version        # The model version
        # Cleaning related:
        self.time_step = time_step                # The time step for resampling
        self.threshold = threshold                # The threshold for cleaning
        self.cycle_lim = cycle_lim

        ent = []        # Inputs list
        sai = []        # Output list


        for Battery in Battery_list:
            if self.dataset_name == 'Clean_Coin_Battery':
                mass = Battery.mass
                threshold = threshold/mass
                C_rate = 0.1284*mass    # A
                Q_nominal = 0.1284
                V_nominal = 4.3         # V

            if print_bool:
                print(Battery.Name)
            for index, cycle_couple in enumerate(Battery.cycle_couple):
                if index > cycle_lim:
                    # If we've gone over the limit number of cycles to test.
                    break
                chr_cycle = cycle_couple[0]
                dis_cycle = cycle_couple[1]

                # print(time_step)
                Data = Resampling_flexible(chr_cycle, variable='Time',
                                           time_step=time_step, min_len=min_len)

                if Data is None:
                    # When there aren't enough points in the cycle, Cycle_t is
                    # returned as None
                    continue
                # Cycle_t = Data[:, 0]

                Cycle_i = Data[:, 1]
                Cycle_v = Data[:, 2]
                Cycle_T = Data[:, 3] # NOT THE TEMPERATURE IF DATASET_TYPE =
                                     # COIN

                if adimensionalize:
                    Cycle_i = Cycle_i/C_rate
                    Cycle_v = Cycle_v/V_nominal

                # Cleaning will take away NaN's, the beginning and the end of
                # the discharge curves (that is, the part before the current
                # reaches the CC value and the part after when the current
                # reaches its cut-off value ``i_cutoff``) and resample the time
                # according to the given time step ``time_step``.

                # Calculating the real number of xis we've got
                if fixed_len is None:
                    Nb = len(Cycle_i) - delta + 1  # Number of xis in this cycle
                else:
                    Nb = fixed_len
                # min_len already take care of the cases where Nb <= 0.

                if self.model_version == 1:
                    XT = np.zeros((Nb, 2 * delta))
                    # Regrouped xi info for one whole cycle

                    for t in range(Nb):  # We're going to make one xi for each t
                        xi = np.concatenate([Cycle_i[t:t + delta],  # Current
                                             Cycle_v[t:t + delta]])  # Voltage
                        # "Add" it to the XT tensor that regroups them.
                        XT[t, :] = xi

                elif self.model_version == 2:
                    XT = np.zeros((Nb, 3 * delta))
                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             Cycle_T[t:t + delta]])# Temperature

                        XT[t, :] = xi

                elif (self.model_version == 3) or (self.model_version == 4):
                    XT = np.zeros((Nb, 2 * delta + 1))

                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             chr_cycle.amb_T*np.ones([1])])
                                             # Ambient temperature

                        XT[t, :] = xi

                elif self.model_version == 5:
                    XT = np.zeros((Nb, 3 * delta + 1))

                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             Cycle_T[t:t + delta],
                                             chr_cycle.amb_T*np.ones([1])])

                        XT[t, :] = xi
                else:
                    raise ValueError("Unknown version: "+str(self.model_version))

                Capacity = dis_cycle.data.capacity*np.ones(1)

                if Capacity.item() < threshold:
                    # If it's considered to be too close to zero
                    continue

                if adimensionalize:
                    Capacity=Capacity/Q_nominal

                # The XT tensor represents one cycle and is thus one input
                ent.append(th.from_numpy(XT).float())
                # The capacity is one output.
                sai.append(Capacity)

            # The number of inputs and outputs must be the same
            assert len(ent) == len(sai), 'Unequal input and output lengths'

        inputs, output, norm = Normalize(ent, sai, norm_type=norm_type,
                                         norm=norm)

        if print_bool:
            print(len(inputs))

        sai_2 = []
        for entry in sai:
            sai_2.append(th.from_numpy(entry))

        self.inputs = ent  # saving the inputs
        self.output = th.stack(sai_2).float() # saving the outputs
        self.norm = norm  # saving the norm

        self.path = None
        self.transform = None

    def time_step_function(self):
        """Function for generating a time step when ``None`` is informed.

        This function makes some extremely arbitrary choices, but that are based
        on values of ``time_step`` that are known to work well with the
        available datasets.

        The function calculates the mean duration of the discharge cycles in the
        given ``Battery_list``, and divides it by ``2*fixed_len`` to get the
        time_step. The factor 2 is there to ensure that most of the cycles will
        have more than ``fixed_len`` points after resampling. It could be lower
        or higher, but the results currently obtained for the available datasets
        match well the manually tested ones.

        Returns
        -------
        time_step : float
            The time step [s].
        """

        time_tensor = th.Tensor([[0, 0]]) # empty tensor
        for Battery in self.Battery_list:
            for cycle_couple in Battery.cycle_couple:
                chr_cycle = cycle_couple[0]
                dt = th.Tensor([[chr_cycle.data.time[0,0].item(),
                                 chr_cycle.data.time[-1,0].item()]])

                time_tensor = th.cat([time_tensor,
                                      dt], dim=0)

        time_tensor = time_tensor[1:, :]
        mean_times = th.mean(time_tensor, dim=0)
        time_step = (mean_times[1] - mean_times[0])/(2*self.min_len)
        print(time_step.item())
        return time_step.item()

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, item):

        ent = self.inputs[item]
        sai = self.output[item]

        return ent, sai

    def report(self):
        """Creates a little report with all of the information about the class.

        Returns
        -------
        str
            A string with the report information.
        """

        string = \
            f"\tDataset Name: {self.dataset_name}\n" \
            f"\tSize of the training set: {len(self.inputs)}\n" \
            f"\tThreshold (minimum capacity): {self.threshold}\n" \
            f"\tDimensionless variables: {self.adimensionalize}\n" \
            f"\tMinimum length: {self.min_len}\n" \
            f"\tResampling time step: {self.time_step}\n" \
            f"\tFixed Length: {self.fixed_len}\n" \
            f"\tBatteries used: {self.batteries}\n" \
            f"\tNormalization information: {self.norm}\n"

        if self.cycle_lim != 1e6:
            string += f"\tMaximum cycle number: {self.cycle_lim}\n"

        return string

    def dataset_info(self, name):
        """Method for creating and information tuple that can be loaded for
        creating a similar dataset in the future.

        Parameters
        ----------
        name : str
            The name of the file where the tuple will be dumped.
        """
        info_tuple = (self.batteries, self.time_step, self.fixed_len, self.norm)

        with open(name+'_info_tuple_.txt', 'wb') as f:
            pickle.dump(info_tuple, f)

if __name__ == '__main__':

    # os.chdir('.//Reports/2021-06-17 11_44_09.411163')
    #
    # with open('Training_Info.txt', 'rb') as f:
    #     Training_INFO = pickle.load(f)
    # with open('base_model.txt', 'rb') as f:
    #     Model = pickle.load(f)
    #
    # os.chdir('../../')


    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    os.chdir('.//Separate_pickles')

    device = th.device('cuda:0' if th.cuda.is_available() else 'cpu')
    print(th.cuda.is_available())
    print('device:', device)

    # Model = Model.to(device)


    DELTA = 10
    time_step = 60
    fixed_len = 75  # 150 * 30 = 4500s = 1h15min
    batch_size = 1
    version = 4 #Version 1
    lin_dropout = 0.3

    HIDDEN_DIM = 20
    DROPOUT = 0

    Nb_Epochs = 100
    Learning_Rate = 1e-5

    Model = LSTM_v4(DELTA, HIDDEN_DIM, pooling_layer='bi_mean',
                   drop_out=DROPOUT, version=version, lin_layers=1,
                   lin_dropout=0.3).to(device)

    files = os.listdir()  # All files in the folder
    file_list = []  # List with the valid files
    for file_name in files:
        match = re.search(r'B\d{4}.txt', file_name)  # Only take into acount
        # the battery files
        if match == None:
            continue
        else:
            file_list.append(file_name)
    file_list.sort()

    # Files to be ignored:
    ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt', 'B0054.txt']

    # The big one
    train = ['B0018.txt',  # 24ºC
             #  'B0025.txt', 'B0026.txt', 'B0027.txt',  # 24ºC
             'B0029.txt', 'B0030.txt', 'B0031.txt',  # 43ºC
             'B0036.txt',  # 24ºC
             'B0038.txt', 'B0039.txt',  # 44ºC et 24ºC
             'B0042.txt', 'B0043.txt',  # 44ºC et 4ºC
             'B0045.txt', 'B0046.txt',  # 4ºC
             'B0053.txt', 'B0055.txt']  # 4ºC

    # with open('B0042.txt', 'rb') as f:
    #     Battery = pickle.load(f)
    #
    # train_dataset = Torch_Dataset_LSTM([Battery], DELTA, Model.version,  # Dataset
    #                                    print_bool=True, time_step=time_step, fixed_len=fixed_len)

    # Training files:
    Train_list = []
    for file_name in file_list:
        if file_name in train:
            with open(file_name, 'rb') as f:
                Battery = pickle.load(f)
            Train_list.append(Battery)

    train_dataset = Torch_Dataset_LSTM(Train_list, DELTA, Model.version,  # Dataset
                                       print_bool=True, time_step=time_step, fixed_len=fixed_len)

    if train_dataset.fixed_len is None:
        train_loader = DataLoader(dataset=train_dataset, batch_size=1,  # Loader
                                  shuffle=True, num_workers=0)
    else:
        train_loader = DataLoader(dataset=train_dataset, batch_size=batch_size,  # Loader
                                  shuffle=True, num_workers=0)

    if train_dataset.fixed_len is None:
        sim_train_loader = DataLoader(dataset=train_dataset, batch_size=1, shuffle=False, num_workers=0)
    else:
        sim_train_loader = DataLoader(dataset=train_dataset, batch_size=len(train_dataset), shuffle=False, num_workers=0)

    simulate(sim_train_loader, Model, block=False, device=device)


    # Validation files:
    Valid_list = []
    for file_name in file_list:
        if (file_name not in train) and (file_name not in ignore):
            with open(file_name, 'rb') as f:
                Battery = pickle.load(f)
            Valid_list.append(Battery)

    valid_dataset = Torch_Dataset_LSTM(Valid_list, DELTA, Model.version,  # Dataset
                                       print_bool=True, time_step=time_step,
                                       norm=train_dataset.norm, fixed_len=train_dataset.fixed_len)

    if train_dataset.fixed_len is None:
        valid_loader = DataLoader(dataset=valid_dataset, batch_size=1,  # Loader
                                  shuffle=False, num_workers=0)
    else:
        valid_loader = DataLoader(dataset=valid_dataset, batch_size=len(valid_dataset),  # Loader
                              shuffle=False, num_workers=0)

    #simulate(valid_loader, Model, block=False, device=device)

    os.chdir('../../../../')

    Train_info, _, _, date = Train(train_loader, valid_loader, Model, Nb_Epochs,
                                   plot_bool=True, LR=Learning_Rate,
                                   report=True, sim_loader=sim_train_loader,
                                   print_bool=True, device=device)

    simulate(sim_train_loader, Model, block=True, device=device)
    simulate(valid_loader, Model, block=True, device=device)

    os.chdir('.//Reports/'+date)
    load_info = (Train_info.best_epoch.nb + 1, train, ignore, fixed_len, time_step, True, train_dataset.norm)
    with open('load_info.txt', 'wb') as f:
        # best_epoch, train, ignore, fixed_len, time_step, resampling, norm = pickle.load(f)
        pickle.dump(load_info, f)
    os.chdir('../../')

    from Load_Save import *

    Load_Save(date, device)