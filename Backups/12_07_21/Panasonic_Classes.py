import matplotlib.pyplot as plt
import os
import numpy as np
from datetime import datetime
import scipy.io
import time
import pickle
import re
from Cleaning import *
import math
import h5py

class Dataset_Panasonic:
    """

    """
    def __init__(self, battery):

        self.cycle = []
        Cycle = Severson_Cycle(battery, Cycle_nb, charge_time)
        self.cycle.append(Cycle)


class Severson_Cycle:
    """

    """
    def __init__(self, battery, Cycle_nb, charge_time):

        self.number = Cycle_nb

        I_ref = battery['I'][Cycle_nb, 0]
        V_ref = battery['V'][Cycle_nb, 0]
        t_ref = battery['t'][Cycle_nb, 0]
        T_ref = battery['T'][Cycle_nb, 0]
        Qd_ref = battery['Qd'][Cycle_nb, 0]

        I = f[I_ref]
        V = f[V_ref]
        t = f[t_ref]
        T = f[T_ref]
        Qd = f[Qd_ref]

        np_I = np.zeros([I.shape[1], 1])
        np_V = np.zeros([I.shape[1], 1])
        np_t = np.zeros([I.shape[1], 1])
        np_T = np.zeros([I.shape[1], 1])
        np_Qd = np.zeros([1, 1])

        for i in range(I.shape[1]):
            np_I[i, 0] = I[0, i]
            np_V[i, 0] = V[0, i]
            np_t[i, 0] = t[0, i]
            np_T[i, 0] = T[0, i]

        np_Qd = Qd[0, -1]

        self.amb_T  = 30

        idx = np.where(np_t > charge_time)[0][0]
        np_t_chr = np_t[:idx]
        np_I_chr = np_I[:idx]
        np_V_chr = np_V[:idx]
        np_T_chr = np_T[:idx]

        tIVT = np.concatenate([np_t_chr,
                               np_I_chr,
                               np_V_chr,
                               np_T_chr], axis=1)

        self.data = Severson_Data(tIVT, 'charge', np_Qd)  # The actual data

class Severson_Data:
    """
    """
    def __init__(self, Data, Type, Qd = None):

        self.Data = Data
        self.time = Data[:,0:1]
        self.meas_i = Data[:,1:2]
        self.meas_v = Data[:,2:3]
        self.meas_t = Data[:,3:]
        self.capacity = Qd

        # # Data is Cycle['data'] whilst Type is Cycle['type']
        #
        # if Type == 'impedance':
        #     #(!!!) AN ADAPTATION OF SHAPES AND MATRICES MAY BE NECESSARY (?)
        #     # In this case we have as attributes:
        #     # Sense_current
        #     self.sense_i = np.transpose(Data['Sense_current'][0,0])
        #     # Battery_current
        #     self.battery_i = np.transpose(Data['Battery_current'][0,0])
        #     # Current_ratio
        #     self.ratio_i = np.transpose(Data['Current_ratio'][0,0])
        #     # Battery_impedance
        #     self.battery_z = Data['Battery_impedance'][0,0]
        #     # Rectified_Impedance
        #     self.rectified_z = Data['Rectified_Impedance'][0,0]
        #     # Re  (estimated electrolyte resistance in Ohms)
        #     self.Re = Data['Re'][0,0][0,0].item()
        #     # Rct (estimated charge transfer resistance in Ohms)
        #     self.Rct = Data['Rct'][0,0][0,0].item()
        #
        # else:   # If the type isn't impedance, then it's either charge or discharge which have a lot in common:
        #
        #     # Voltage_measured
        #     self.meas_v = np.transpose(Data['Voltage_measured'][0,0])
        #     # Current_measured
        #     self.meas_i = np.transpose(Data['Current_measured'][0,0])
        #     # Temperature_measured
        #     self.meas_t = np.transpose(Data['Temperature_measured'][0,0])
        #     # Time
        #     self.time = np.transpose(Data['Time'][0, 0])
        #
        #     if Type == 'charge':
        #         # Current_charge
        #         self.charge_i = np.transpose(Data['Current_charge'][0,0])
        #         # Voltage_charge
        #         self.charge_v = np.transpose(Data['Voltage_charge'][0,0])
        #     else:
        #         # Current_load
        #         self.load_i = np.transpose(Data['Current_load'][0,0])
        #         # Voltage_load
        #         self.load_v = np.transpose(Data['Voltage_load'][0,0])
        #         # Capacity
        #         # The following 'try:' statement is necessary because
        #         # some files have missing Capacity data
        #         try:
        #             self.capacity = float(Data['Capacity'][0,0][0,0])
        #         except IndexError:
        #             print('No Capacity value')
        #             # maybe not the best way of dealing with this
        #             self.capacity = float('nan')

class Clean_Sev_Battery:
    """
    """
    def __init__(self, Battery, i_cutoff=0.022, min_len=1, threshold=0.1,
                 zlim=3):

        # self.Name = Battery.Name
        self.capacity_curve = []
        self.couples = []
        self.cycle_couple = []

        for cycle in Battery.cycle: # For each charge/discharge couple:
            # Saving the cycles
            print(cycle.number)

            # For the discharge cycle first:
            if math.isnan(cycle.data.capacity): # if the capacity is nan:
                continue    # skip this couple
            elif cycle.data.capacity < threshold:
                continue

            # t, i, v, T = clean_cycle(dis_cycle, i_cutoff=i_cutoff, min_len=min_len) # cleaning the data
            # if t is None:
            #     continue
            # dis_data = np.concatenate([t, i, v, T], axis=1)
            # dis_clean = Clean_Cycle(dis_cycle, dis_data)  # Creating the Clean_Cycle class for this discharge cycle

            ##

            # Now for the charge cycle:
            # t, i, v, T = clean_cycle(cycle, i_cutoff=i_cutoff, min_len=min_len)
            # if t is None:
            #     continue

            chr_data = cycle.data.Data
            chr_clean = Clean_Sev_Cycle(cycle, chr_data)

            self.cycle_couple.append((chr_clean, chr_clean))  # Saving the cycle couple

        for i in range(len(self.cycle_couple)):
            capacity_point = (i, self.cycle_couple[i][1].data.capacity)     # Defining the capacity point
            self.capacity_curve.append(capacity_point)                      # Saving the capacity point

        # Searching for outliers in the capacity data
        cycle_couple, capacity_curve = Outliers(self.cycle_couple, self.capacity_curve)

        self.cycle_couple = cycle_couple
        self.capacity_curve = capacity_curve

        self.raw_couples = self.couples
        self.couples = []
        for cycle_couple in self.cycle_couple:
            chr_nb = cycle_couple[0].number
            dis_nb = cycle_couple[1].number
            self.couples.append((chr_nb, dis_nb))


class Clean_Sev_Cycle:
    """

    """
    def __init__(self, Cycle, Data):
        self.type = 'charge'
        self.number = Cycle.number
        self.amb_T = Cycle.amb_T
        self.data = Clean_Sev_Data(Data, Cycle.data.capacity)

class Clean_Sev_Data:
    """
    """

    def __init__(self, Data, Capacity=None):
        # Time
        self.time = Data[:, 0:1]*60
        # Current_measured
        self.meas_i = Data[:, 1:2]
        # Voltage_measured
        self.meas_v = Data[:, 2:3]
        # Temperature_measured
        self.meas_t = Data[:, 3:]
        # Capacity
        self.capacity = Capacity


########################################################################################



if __name__ == '__main__':

    os.chdir('.//Datasets/Panasonic 18650PF Data/Panasonic 18650PF Data/25degC'
             '/C20 OCV and 1C discharge tests_start_of_tests')

    Mat = scipy.io.loadmat('03-09-17_15.13 3349_Charge1.mat')  # Loading the .mat file
    # Mat = mat73.loadmat('2017-06-30_batchdata_updated_struct_errorcorrect.mat')

    print(Mat.keys())
    print(len(Mat['meas']))
    # print(Mat['meas'][0, 0])
    print(len(Mat['meas'][0, 0]))
    print(type(Mat['meas'][0, 0]))
    print(len(Mat['meas'][0, 0][0]))

    Battery = Mat['meas'][0, 0]

    Battery_Class = Dataset_Panasonic(Battery)
    # dataset = f['batch']['cycle_life']
    # np_dataset = np.zeros([len(dataset), 1])
    # for i in range(len(dataset)):
    #     ref = dataset[i, 0]
    #     np_dataset[i, 0] = f[ref][0, 0]
    #
    # print(np_dataset)

    #
    # for Battery_nb in range(1, 48):
    #     print(Battery_nb)
    #     battery_ref = f['batch']['cycles'][0, 0]
    #
    #     battery = f[battery_ref]
    #
    #     B = Dataset_MIT(battery, charge_time=10)
    #     # Much more info that I haven't included
    #     B_Clean = Clean_MIT_Battery(B)
    #     with open('B'+str(Battery_nb)+'.txt', 'wb') as f:
    #         pickle.dump(B_Clean, f)
    #
    #     del battery_ref

    # from LSTM import *
    # from Severson_Classes import *
    #
    # os.chdir('.//Reports/2021-06-17 11_20_39.769117-Prés_new')
    # with open('best_model.txt', 'rb') as f:
    #     model = pickle.load(f)
    # with open('load_info.txt', 'rb') as f:
    #     _, _, _, _, _, _, norm = pickle.load(f)
    # os.chdir('../../')
    # os.chdir('.//Datasets/Severson et al./Raw Dataset')
    # os.chdir('.//2. 2017-06-30')
    # with open('cleaned_test.txt', 'rb') as f:
    #     Battery = pickle.load(f)
    # Battery.Name = 'Hi'
    # dataset = Torch_Dataset_LSTM([Battery], 10, 4, norm=norm)
    # loader = DataLoader(dataset=dataset, batch_size=1, shuffle=False)
    # simulate(loader, model)