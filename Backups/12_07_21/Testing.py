from Cleaning import *
from Training import *
from torch.utils.data import Dataset, DataLoader
import torch.nn.functional as F
import torch.nn as nn
import re


class Torch_Dataset_LSTM(Dataset):
    """
    Creates a pytorch Dataset class that can readily be loaded with torch's
    Loader class for training and validation.

    The datasets created by this class' initialization follow You et al.'s †
    snapshot approach, where I, V data for a charge cycle are used for battery
    capacity prediction. A number ∆ (delta) of sequential (I, V) points are
    collected and turned into a 1D vector x_i. This window of points slides
    through time and each vector x_i becomes the sequential inputs of an LSTM.
    Here, these x_i vectors are already grouped in order in a XT 2D tensor and
    stored into the list 'ent'. One XT tensor = One charge cycle = One sequence.
    The list is later saved as attribute 'inputs' (self.inputs = ent).

    The output for each XT (each charge cycle) is a capacity value (in A.h).
    However, capacity values are only measured during discharge cycles. Because
    of this, the capacity values taken as outputs are picked from the discharge
    cycle that comes immediately before the charge cycle.

    .. note::
        † Full reference: G. You, S. Park and D. Oh, "Diagnosis of Electric
        Vehicle Batteries Using Recurrent Neural Networks," in IEEE Transactions
        on Industrial Electronics, vol. 64, no. 6, pp. 4885-4893, June 2017,
        doi: 10.1109/TIE.2017.2674593.

    Parameters
    ----------
    Battery_list : list[Clean_Battery]
        List with the Batteries that we want to use for creating the dataset.
    delta : int
        The size of the sliding window used to generate the x_i's.
    model_version : {1, 2, 3, 4, 5}
        An integer indicating which LSTM version to use.
        if `version` == 1: then only I and V data will be used.
        elif `version` == 2: then I, V and T will be taken into account.
        elif `version` == 3: then I, V and amb_T will be taken into account.
        elif `version` == 4: then I, V and amb_T will be taken into account,
        but amb_T will enter directly in the last step, the NN.
        elif `version` == 5: then I, V, T and amb_T will be taken into account,
        mixing models 2 and 4. Redundant.
    print_bool : bool, default = False
        Whether or not to print some information while the code runs.
        Namely, if we want to print the battery's name and the number of points.
    time_step : int, optional
        The desired time step for the resampling procedure.
        If ``None`` is given, than a time step will be automatically generated
        by the method ``time_step_function``.
    threshold : float, default = 0.1
        The minimum capacity value to be considered as relevant [A.h].
    min_len : int, default = 10
        The minimum amount of points a charge cycle must have to be considered
        as relevant.
    fixed_len : int, optional
        if different than `None` (default value), then the charge and discharge
        curves will be limited to `fixed_len` points. This will override
        `min_len`.
    norm_type : {'zscore', 'minmax'}
        A string indicating which kind of normalization to do.
    norm : tuple(Tensor, Tensor, Tensor, Tensor, string), optional
        A tuple containing all of the information necessary for normalization.
        In entered as input, `norm_type` will be overridden by norm[4] and the
        given information will be used for the normalization
    adimensionalize : bool, default = True
        Whether or not to make variables dimensionless before adding them to the
        inputs and output lists.
        The Capacity (output) is normalized by the nominal capacity
        (``Q_nominal``);
        The Current is normalized by the C rate (``C_rate``) -
        which is the current necessary for charging the battery in one hour
        (theoretically);
        The Voltage is normalized by the nominal voltage (``V_nominal``),
        the voltage at the plateau in the CV part of the charge curve.
    cycle_lim : int, optional
        The greatest cycle number to take into account for the dataset creation.
        Default equals ``1e6`` meaning that no cycles will be ignored.

    Attributes
    ----------
    dataset_name : str
        The name of the dataset ('Dataset_Nasa', for instance).
    batteries : list[str]
        A list with the file name of each battery used.
    dataset_format : str
        A string indicating the type of data format used: 'You et al. [56]'.
    inputs : list[Tensors]
        A list with the input tensors for each cycle.
    output : Tensor
        A tensor with the output capacity tensors for each cycle.

    Raises
    ------
    AssertionError
        If the specified value of `delta` is < 1.
        If no batteries are given.
        If the specified version is invalid.
        If ``model_version`` is 2 or 5 and ``self.dataset_name`` is
        ``Clean_Coin_Battery``.
    ValueError
        If the model_version is unknown.

    Examples
    --------
    >>> from torch.utils.data import Dataset
    >>> from LSTM import Torch_Dataset_LSTM
    >>> delta = 10
    >>> version = 2
    >>> with open('B0005.txt', 'rb') as f:
    ...     Battery = pickle.load(f)
    ...
    >>> train_dataset = Torch_Dataset_LSTM([Battery], delta, version,
    ...                                    fixed_len=150, print_bool=True)
    B0005
    165
    """
    def __init__(self, Battery_list, delta, model_version, print_bool=False,
                 time_step=None, threshold=0.1, min_len=10, fixed_len=None,
                 norm_type='zscore', norm=None, adimensionalize=True,
                 cycle_lim=1e6
                 ):

        super().__init__()

        assert delta >= 1, 'delta must be greater or equal to one'
        assert len(Battery_list) > 0, 'No Batteries informed'
        assert model_version in [1, 2, 3, 4, 5], 'Invalid version'

        if fixed_len is None:
            if delta != 1:
                min_len = max(delta - 1, min_len)
                # min_len is the minimum sequence length (number of points) that
                # the charge cycle must have to be taken into account.
            else:
                min_len = 1
        else:
            min_len = fixed_len + delta

        # Dataset related
        self.dataset_name = type(Battery_list[0]).__name__  # The dataset used
        self.batteries = []
        self.adimensionalize = adimensionalize

        for Battery in Battery_list:
            self.batteries.append(Battery.Name+'.txt')

        if self.dataset_name == 'Clean_Coin_Battery':
            assert model_version in [1, 3, 4], "Can't use models 2 or 5 with" \
                "Clean_Coin_Battery. No data on temperature during cycling."
            new_battery_list = []
            for Battery in Battery_list:
                for cell in Battery.cell:
                    new_battery_list.append(cell)
            Battery_list = new_battery_list

        elif self.dataset_name == 'Clean_Battery':
            C_rate = 2.1    # A
            V_nominal = 4.2 # V
            Q_nominal = C_rate

        elif self.dataset_name == 'Clean_Sev_Battery':
            C_rate = 1.1    # A
            V_nominal = 3.6 # V
            Q_nominal = C_rate

        self.min_len = min_len
        self.fixed_len = fixed_len
        if time_step is None:
            self.Battery_list = Battery_list
            time_step = self.time_step_function()

        self.dataset_format = 'You et al. [56]'   # The data format used
        # LSTM related
        self.delta = delta                        # The amount of points per xi
        self.model_version = model_version        # The model version
        # Cleaning related:
        self.time_step = time_step                # The time step for resampling
        self.threshold = threshold                # The threshold for cleaning
        self.cycle_lim = cycle_lim

        ent = []        # Inputs list
        sai = []        # Output list


        for Battery in Battery_list:
            if self.dataset_name == 'Clean_Coin_Battery':
                mass = Battery.mass
                threshold = threshold/mass
                C_rate = 0.1284*mass    # A
                Q_nominal = 0.1284
                V_nominal = 4.3         # V

            if print_bool:
                print(Battery.Name)
            for index, cycle_couple in enumerate(Battery.cycle_couple):
                if index > cycle_lim:
                    # If we've gone over the limit number of cycles to test.
                    break
                chr_cycle = cycle_couple[0]
                dis_cycle = cycle_couple[1]

                # print(time_step)
                Data = Resampling_flexible(chr_cycle, variable='Time',
                                           time_step=time_step, min_len=min_len)

                if Data is None:
                    # When there aren't enough points in the cycle, Cycle_t is
                    # returned as None
                    continue
                # Cycle_t = Data[:, 0]

                Cycle_i = Data[:, 1]
                Cycle_v = Data[:, 2]
                Cycle_T = Data[:, 3] # NOT THE TEMPERATURE IF DATASET_TYPE =
                                     # COIN

                if adimensionalize:
                    Cycle_i = Cycle_i/C_rate
                    Cycle_v = Cycle_v/V_nominal

                # Cleaning will take away NaN's, the beginning and the end of
                # the discharge curves (that is, the part before the current
                # reaches the CC value and the part after when the current
                # reaches its cut-off value `i_cutoff`) and resample the time
                # according to the given time step `time_step`.

                # Calculating the real number of xis we've got
                if fixed_len is None:
                    Nb = len(Cycle_i) - delta + 1  # Number of xis in this cycle
                else:
                    Nb = fixed_len
                # min_len already take care of the cases where Nb <= 0.

                if self.model_version == 1:
                    XT = np.zeros((Nb, 2 * delta))
                    # Regrouped xi info for one whole cycle

                    for t in range(Nb):  # We're going to make one xi for each t
                        xi = np.concatenate([Cycle_i[t:t + delta],  # Current
                                             Cycle_v[t:t + delta]])  # Voltage
                        # "Add" it to the XT tensor that regroups them.
                        XT[t, :] = xi

                elif self.model_version == 2:
                    XT = np.zeros((Nb, 3 * delta))
                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             Cycle_T[t:t + delta]])# Temperature

                        XT[t, :] = xi

                elif (self.model_version == 3) or (self.model_version == 4):
                    XT = np.zeros((Nb, 2 * delta + 1))

                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             chr_cycle.amb_T*np.ones([1])])
                                             # Ambient temperature

                        XT[t, :] = xi

                elif self.model_version == 5:
                    XT = np.zeros((Nb, 3 * delta + 1))

                    for t in range(Nb):
                        xi = np.concatenate([Cycle_i[t:t + delta],
                                             Cycle_v[t:t + delta],
                                             Cycle_T[t:t + delta],
                                             chr_cycle.amb_T*np.ones([1])])

                        XT[t, :] = xi
                else:
                    raise ValueError("Unknown version: "+str(self.model_version))

                Capacity = dis_cycle.data.capacity*np.ones(1)

                if Capacity.item() < threshold:
                    # If it's considered to be too close to zero
                    continue

                if adimensionalize:
                    Capacity=Capacity/Q_nominal

                # The XT tensor represents one cycle and is thus one input
                ent.append(th.from_numpy(XT).float())
                # The capacity is one output.
                sai.append(Capacity)

            # The number of inputs and outputs must be the same
            assert len(ent) == len(sai), 'Unequal input and output lengths'

        inputs, output, norm = Normalize(ent, sai, norm_type=norm_type,
                                         norm=norm)

        if print_bool:
            print(len(inputs))

        self.inputs = inputs  # saving the inputs
        self.output = output  # saving the outputs
        self.norm = norm  # saving the norm

        self.path = None
        self.transform = None

    def time_step_function(self):
        """Function for generating a time step when ``None`` is informed.

        This function makes some extremely arbitrary choices, but that are based
        on values of ``time_step`` that are known to work well with the
        available datasets.

        The function calculates the mean duration of the discharge cycles in the
        given ``Battery_list``, and divides it by ``2*fixed_len`` to get the
        time_step. The factor 2 is there to ensure that most of the cycles will
        have more than ``fixed_len`` points after resampling. It could be lower
        or higher, but the results currently obtained for the available datasets
        match well the manually tested ones.

        Returns
        -------
        time_step : float
            The time step [s].
        """

        time_tensor = th.Tensor([[0, 0]]) # empty tensor
        for Battery in self.Battery_list:
            for cycle_couple in Battery.cycle_couple:
                chr_cycle = cycle_couple[0]
                dt = th.Tensor([[chr_cycle.data.time[0,0].item(),
                                 chr_cycle.data.time[-1,0].item()]])

                time_tensor = th.cat([time_tensor,
                                      dt], dim=0)

        time_tensor = time_tensor[1:,:]
        mean_times = th.mean(time_tensor, dim=0)
        time_step = (mean_times[1] - mean_times[0])/(2*self.fixed_len)
        print(time_step.item())
        return time_step.item()

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, item):

        ent = self.inputs[item]
        sai = self.output[item]

        return ent, sai

    def report(self):
        """Creates a little report with all of the information about the class.

        Returns
        -------
            A string with the report information.
        """
        string = '  Size of the training set: ' + str(len(self.inputs)) \
                 + '\n' \
               + '  Threshold (minimum capacity): ' + str(self.threshold) \
                 + '\n' \
               + '  Dimensionless variables: ' + str(self.adimensionalize) \
               + '  Minimum length: ' + str(self.min_len) + '\n' \
               + '  Resampling time step: ' + str(self.time_step) + '\n' \
               + '  Fixed Length: ' + str(self.fixed_len) + '\n'\
               + '  Batteries used: ' + str(self.batteries) + '\n' \
               + '  Normalization information: ' + str(self.norm) + '\n'

        if self.cycle_lim != 1e6:
            string += ' Maximum cycle number: '+str(self.cycle_lim)+'\n'

        return string

    def dataset_info(self, name):
        """Method for creating and information tuple that can be loaded for
        creating a similar dataset in the future.

        Parameters
        ----------
        name : str
            The name of the file where the tuple will be dumped.
        """
        info_tuple = (self.batteries, self.time_step, self.fixed_len, self.norm)

        with open(name+'_info_tuple_.txt', 'wb') as f:
            pickle.dump(info_tuple, f)
