from Nasa_Classes import *
from matplotlib import pyplot as plt
import torch as th
from Cleaning import *

def Save_IVT_Data(file_name, cycle_type = 'all'):
    """
    Regroups all of measured I, V and T data for one battery in three distinct tensors.
    As it is, this will not make any distinction between cycle number, of course (the Dataset_Nasa
    file already does this).

    This function is particulatly useful for the k-means method proposed by You et al. (see note for
    the full reference), although the method still has to be developed and the function may need
    adaptation in the future.

    .. note::
        Full reference: Gae-won You, Sangdo Park, Dukjin Oh, Real-time state-of-health estimation
        for electric vehicle batteries: A data-driven approach, Applied Energy, Volume 176, 2016,
        Pages 92-103, ISSN 0306-2619, https://doi.org/10.1016/j.apenergy.2016.05.051.

    Parameters
    ----------
    file_name : str
        Name of the file.
    cycle_type : str, default = 'all'
        Defines which types of cycles to take into account.
        Can either be 'charge', 'discharge' or 'all' (default).

    Returns
    -------
    tuple(torch.Tensor, ...)
        Tensors for the current, the voltage and the temperature, respectively.
    """

    # Opening the file
    with open(file_name, 'rb') as f:
        B0029 = pickle.load(f)

    # The number of cycles
    size = len(B0029.cycle)

    # Initializing torch tensors
    # (this first entry that equals zero will be removed by the end)
    I = th.zeros(1)
    V = th.zeros(1)
    T = th.zeros(1)

    if cycle_type == 'all':
        for i in range(size):
            try:
                # Concatenating the current I, V and T tensors with the previous ones.
                I = th.cat([I, th.from_numpy(B0029.cycle[i].data.meas_i).view(-1)])
                V = th.cat([V, th.from_numpy(B0029.cycle[i].data.meas_v).view(-1)])
                T = th.cat([T, th.from_numpy(B0029.cycle[i].data.meas_t).view(-1)])

            except AttributeError:
                continue
    else:
        for i in range(size):
            if B0029.cycle[i].type == cycle_type:
                # Concatenating the current I, V and T tensors with the previous ones.
                I = th.cat([I, th.from_numpy(B0029.cycle[i].data.meas_i).view(-1)])
                V = th.cat([V, th.from_numpy(B0029.cycle[i].data.meas_v).view(-1)])
                T = th.cat([T, th.from_numpy(B0029.cycle[i].data.meas_t).view(-1)])

            else:
                continue

    return I[1:], V[1:], T[1:]

def Save_IVT_Data2(file_list, cycle_type='all', resampling=False, time_step=30, min_len=10, fixed_len=None, file='IVT'):
    """
    Regroups all of measured I, V and T data for one battery in three distinct tensors.
    As it is, this will not make any distinction between cycle number, of course (the Dataset_Nasa
    file already does this).

    This function is particulatly useful for the k-means method proposed by You et al. (see note for
    full reference), although the method still has to be developed and the function may need adaptation
    in the future.

    .. note::
        Full reference: Gae-won You, Sangdo Park, Dukjin Oh, Real-time state-of-health estimation
        for electric vehicle batteries: A data-driven approach, Applied Energy, Volume 176, 2016,
        Pages 92-103, ISSN 0306-2619, https://doi.org/10.1016/j.apenergy.2016.05.051.


    Parameters
    ----------
    file_name : str
        Name of the file.
    cycle_type : str, default = 'all'
        Defines which types of cycles to take into account.
        Can either be 'charge', 'discharge' or 'all' (default).

    Returns
    -------
    torch.Tensor
        A tensor that concatenates data for the current, the voltage and the temperature, respectively (one for each
        column).
    """

    # Initializing torch tensors
    # (this first entry that equals zero will be removed by the end)
    I = th.zeros(1)
    V = th.zeros(1)
    T = th.zeros(1)

    # Opening the file
    for file_name in file_list:
        print(file_name)
        with open(file_name, 'rb') as f:
            Battery = pickle.load(f)

        for Cycle in Battery.cycle:
            if Cycle.type == 'impedance':
                continue

            if cycle_type == 'all':
                _, Ic, Vc, Tc = clean(Cycle, time_step=time_step, min_len=min_len, resampling=resampling)

                if Ic is None:
                    continue

                if fixed_len is not None:
                    Ic = Ic[:fixed_len]
                    Vc = Vc[:fixed_len]
                    Tc = Tc[:fixed_len]

                # Concatenating the current I, V and T tensors with the previous ones.
                I = th.cat([I, th.from_numpy(Ic).view(-1)])
                V = th.cat([V, th.from_numpy(Vc).view(-1)])
                T = th.cat([T, th.from_numpy(Tc).view(-1)])

            elif Cycle.type == cycle_type:
                _, Ic, Vc, Tc = clean(Cycle, time_step=time_step, min_len=min_len, resampling=resampling)
                if Ic is None:
                    continue
                if fixed_len is not None:
                    Ic = Ic[:fixed_len]
                    Vc = Vc[:fixed_len]
                    Tc = Tc[:fixed_len]
                # Concatenating the current I, V and T tensors with the previous ones.
                I = th.cat([I, th.from_numpy(Ic).view(-1)])
                V = th.cat([V, th.from_numpy(Vc).view(-1)])
                T = th.cat([T, th.from_numpy(Tc).view(-1)])

    I = I[1:]
    V = V[1:]
    T = T[1:]

    IVT = th.cat([I.view(len(I), 1),
                  V.view(len(V), 1),
                  T.view(len(T), 1)], dim=1)

    with open(file+'.txt', 'wb') as f:
        pickle.dump(IVT, f)

    return IVT

if __name__ == '__main__':

    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    os.chdir('.//Separate_pickles')

    # The big one
    train = ['B0029.txt', 'B0031.txt', 'B0032.txt',  # 43ºC
             'B0038.txt', 'B0039.txt',  # 44ºC et 24ºC
             'B0042.txt', 'B0044.txt',  # 44ºC et 4ºC
             'B0046.txt',  # 4ºC
             'B0055.txt',  # 4ºC
             'B0018.txt']  # 4ºC

    train.sort()

    IVT = Save_IVT_Data2(train, resampling=True, time_step=30, file='Discharge_IVT', cycle_type='discharge')
    print(IVT.shape)

    # I, V, T = Save_IVT_Data('B0029.txt', 'charge')
    #
    #
    # print('mean I', th.mean(I))
    # print('stdv I', th.std(I))
    # print('mean V', th.mean(V))
    # print('stdv I', th.std(V))
    #
    # IVT = th.cat([I.view(len(I), 1),
    #               V.view(len(V), 1),
    #               T.view(len(T), 1)], dim=1)
    #
    # fig = plt.figure()
    # ax = fig.add_subplot(projection='3d')
    #
    # ax.scatter(IVT[:,0], IVT[:,1], IVT[:,2])
    # ax.set_xlabel('I')
    # ax.set_ylabel('V')
    # ax.set_zlabel('T')
    # plt.show()
