from Nasa_Classes import *
import torch as th
import pandas as pd
from PyAstronomy import pyasl
import sys

import matplotlib.pyplot as plt
from scipy import stats

def clean(Cycle, time_step=30, i_cutoff=2e-2, min_len=1, resampling=True):
    """This function serves to clean charge and discharge data by taking away
    the first and last points (when the current value is being adjusted), as
    well as resampling the data to a given time_step.

    Inputs:
        Cycle           : The cycle we want to treat [Nasa_Cycle object]
        time_step       : The time step (in seconds) for the new resampling {s} [int]
        i_cutoff        : The cut-off voltage value that determines the end of the charge
                          cycle {A} [float]
        min_len         : The minimum sequence length acceptable. If the final resampled
                          curve has les than min_len points, it's "discarded": None is
                          returned for all of the tensors (which tells other codes to skip
                          the curve in question). [int]
                            It has to be greater or equal to one, otherwise problems may
                            arise from tensors of length 0 (though rare).
        resampling      : Determines wheter or not to resample the data.
                            Resampling may not be done for the k-means approach, although
                            this is still in consideration.

    Outputs:
        Cycle_t         : The cycle's time stamps after resampling and cleaning {s} [numpy array]
        Cycle_i         : The cycle's current (the physical property) values after resampling
                          and cleaning {A} [numpy array]
        Cycle_v         : The cycle's voltage valuess after resampling and cleaning {V} [numpy array]
        Cycle_T         : The cycle's temperaturee after resampling and cleaning {ºC} [numpy array]
    """

    assert Cycle.type != 'impedance', 'Only charge and discharge cycles are allowed.'
    assert min_len >= 1, 'min_len must be greater or equal to 1'

    # Numpy arrays
    Cycle_t = Cycle.data.time       # Time data
    Cycle_i = Cycle.data.meas_i     # Current data
    Cycle_v = Cycle.data.meas_v     # Voltage data
    Cycle_T = Cycle.data.meas_t     # Temperature data

    # plt.plot(Cycle_v)
    # plt.show()

    if Cycle.type == 'charge':

        # Cutting out the unwanted beginning and end of the cycles:
        idx = 0
        try:
            while Cycle_i[idx, 0] < 0.90 * max(Cycle.data.meas_i):
                idx += 1
            idx_1 = idx
        except IndexError:
            return None, None, None, None

        if Cycle_i[-1, 0] <= i_cutoff:
            while Cycle_i[idx, 0] > i_cutoff:
                idx += 1
            idx_2 = idx
        else:
            idx_2 = len(Cycle_i)

    elif Cycle.type == 'discharge':

        idx = 0
        try:
            while Cycle_i[idx, 0] > 0.90 * min(Cycle.data.meas_i):
                idx += 1
            idx_1 = idx
        except IndexError:
            return None, None, None, None

        if Cycle_v[-1, 0] >= min(Cycle.data.meas_v):
            while Cycle_v[idx, 0] > min(Cycle.data.meas_v):
                idx += 1
            idx_2 = idx
        else:
            idx_2 = len(Cycle_v)

    # Taking only the relevant data
    Cycle_t = Cycle_t[idx_1:idx_2, :]
    Cycle_i = Cycle_i[idx_1:idx_2, :]
    Cycle_v = Cycle_v[idx_1:idx_2, :]
    Cycle_T = Cycle_T[idx_1:idx_2, :]

    # plt.plot(Cycle_v)
    # plt.show()

    # Resampling
    if resampling:
        Cycle_t, Cycle_i, Cycle_v, Cycle_T = Resampling(Cycle_t, Cycle_i, Cycle_v, Cycle_T, time_step)

    # Checking if there are sill any NaN's after interpolation:
    if np.any(np.isnan(np.reshape(Cycle_i, -1))).item():
        Nb_i = len(np.nonzero(np.isnan(np.reshape(Cycle_i, -1))))
        Nb_v = len(np.nonzero(np.isnan(np.reshape(Cycle_i, -1))))
        Nb_T = len(np.nonzero(np.isnan(np.reshape(Cycle_i, -1))))
        print('Cycle number: ', Cycle.number)
        print(Nb_i, ' NaNs in I')
        print(Nb_v, ' NaNs in V')
        print(Nb_T, ' NaNs in T')
        return None, None, None, None

    if len(Cycle_t) < min_len:
        return None, None, None, None

    return Cycle_t, Cycle_i, Cycle_v, Cycle_T

def Resampling(Time, Current, Voltage, Temperature, time_step=30):
    """ This function serves to resample the current and voltage data for a desired time step.

    Inputs:
        Time            : Time tensor in seconds. [numpy array]
        Current         : Current tensor. [numpy array]
        Voltage         : Voltage tensor. [numpy array]
        Temperature     : Temperature tensor. [numpy array]
        time_step       : The desired time_step for resampling. [int]

    Outputs:
        New_Time        : The new, resampled, time tensor. [numpy array]
        New_Current     : The new, resampled, Current tensor. [numpy array]
        New_Voltage     : The new, resampled, Voltage tensor. [numpy array]
        New_Temperature : The new, resampled, Temperature tensor. [numpy array]
    """

    # Concatenating the time and the data
    Cat_Data = np.concatenate((Time, Current, Voltage, Temperature), axis=1)     # Cat stands for concatenated

    # Creating a data frame
    df = pd.DataFrame(data=Cat_Data,
                      columns=['Time', 'Current', 'Voltage', 'Temperature'])

    # Checking if the time_step is greater than the biggest sampling step:
    df['delta_time'] = df['Time'].diff()    # The time difference between each step
    # This check is done in order to avoid the creation of NaN entries.
    if df['delta_time'].max() > time_step:  # If the biggest sample time step is greater than the time_step
        print('time_step too small',        # A Warning, this will not stop the code
              str(df['delta_time'].max()), '>', str(time_step))

    # Transforming the time data in datetime form:
    df['Time'] = pd.to_datetime(df['Time']*1e9)     # This line is 10 times faster than the next two
    # df['Time'] = df['Time'].apply(  # Telling the data frame that the time is in seconds
    # lambda t: pd.to_datetime('1970-01-01') + pd.Timedelta(seconds=t))

    df.set_index('Time', inplace=True)
    # Taking any NaN's away:
    df = df.dropna()
    # Resampling
    df = df.resample(str(time_step)+'s').mean()

    # Saving the new data in pytorch tensor form
    New_Time = (df.index.astype('int64').to_numpy()/1e9)   # Time (in seconds)
    New_Current = (df['Current'].to_numpy())               # Current
    New_Voltage = (df['Voltage'].to_numpy())               # Voltage
    New_Temperature = (df['Temperature'].to_numpy())       # Temperature

    return New_Time, New_Current, New_Voltage, New_Temperature

def Outliers(inputs, output, z_lim=3):
    """Removes outliers in the battery output capacity data.

    (!!!) Something that should be taken care of: nothing has been done about the case where
    std = 0 (division by 0).

    As of know, it removes outliers based on moving window averages (generalized Extreme
    Studentized Deviate (ESD) test - implemented through PyAstronomy's pyasl.pointDistGESD)
    and on the total average (by taking away any remaining points with a z score > z_lim).
        Where zscore_i = x_i - mean(x)/std(x)

    Inputs:
        inputs      : the inputs list. [list of torch tensors]
        output      : the output list. [list of numpy arrays]
        z_lim       : the maximum zscore allowed for the last step. [float]

    Outputs:
        inputs      : the normalized inputs. [list of torch tensors]
        output      : the normalized output. [list of floats]
    """

    # Turning the output list into a numpy array:
    output = np.array(output)

    # Taking away local outliers (based on moving windows)
    r = pyasl.pointDistGESD(output, 10, alpha=0.01)
    outlier_array = np.ones(len(output))

    # Taking away the outliers from the input and output lists:
    for i in range(len(r[1])):
        outlier_array[r[1][i]] = 0              # array for the outlier indexes
        inputs.pop(r[1][i] - i)                 # updating the inputs
    outlier_array = outlier_array == 1
    output = output[outlier_array]              # only the non-outliers

    # Taking away any bigger outliers that may have standed the first test:
    outlier_array = (np.abs(stats.zscore(output)) < z_lim).all(axis=1)
    outlier_index = np.where(outlier_array == False)[0]             # where the outliers are
    for i in range(len(outlier_index)):
        inputs.pop(outlier_index[i]-i)                              # updating the inputs
    output = th.from_numpy(output[outlier_array]).float().tolist()  # updating the outputs

    return inputs, output

def Normalize(inputs, output, norm_type='zscore', norm=None, concatenate=False):
    """Normalizes both the inputs and outputs based either on zscore standardization
    or on min-max normalization.

    Inputs:
        inputs      : the inputs list. [list of torch tensors]
        output      : the output list. [list of floats]
        norm_type   : the type of normalization. [string]
                    As of now, can either be 'zscore' or 'minmax'.
        norm        : normalization information carried over from another normalization. [tuple]
                        norm[4] is a string indicating the norm type [string]
                        if norm[4] is 'zscore', then:
                            norm[0] and norm[1] are the inputs' mean and standard deviation [torch tensors]
                            norm[2] and norm[3] are the output's mean and standard deviation [torch tensors]
                        if norm[4] is 'minmax', then:
                            norm[0] and norm[1] are the inputs' min and max values [torch tensors]
                            norm[2] and norm[3] are the output's min and max values [torch tensors]

    Output:
        inputs      : the normalized inputs [list of torch tensors or torch tensor]
                        if concatenate = True, then it's a simple torch tensor
        output_stack: the normalized outputs [torch tensor]
        norm        : normalization information that can be used to normalize other
                      data. [tuple]

    """
    # Now for the normalization
    inputs_stack = th.cat(inputs, dim=0)
    inputs_norm = th.sum(inputs_stack, dim=1)
    inputs_norm = (1/inputs_norm).T
    inputs
    output_stack = th.FloatTensor(output)

    if norm is not None:
        norm_type = norm[4]

    if norm_type == 'zscore':
        if norm is None:

            # Checking if there are entries for which the standard deviation
            # is zero:
            std_in = th.std(inputs_stack, dim=0)    # inputs
            bool_std = (std_in == 0)
            std_in[bool_std] = 1

            std_out = th.std(output_stack, dim=0)   # outputs
            bool_std = (std_out == 0)
            std_out[bool_std] = 1

            norm = (th.mean(inputs_stack, dim=0), std_in,
                    th.mean(output_stack, dim=0), std_out, norm_type)

        output_stack = (output_stack - norm[2])/norm[3]

        if concatenate:
            # inputs_stack = (inputs_stack - norm[0]) / norm[1]
            inputs = (inputs_stack - norm[0]) / norm[1]
            # inputs = inputs_stack.tolist()
        else:
            for i in range(len(inputs)):    # Unfortunately this is necessary for the LSTM...
                                            # stacking the tensors isn't possible,
                                            # As each one has a varying length... Something to be thought of
                                            # A way of optimizing this in the future should be envisionned.
                inputs[i] = (inputs[i] - norm[0]) / norm[1]

    elif norm_type == 'minmax':

        if norm is None:
            min_in = th.amin(inputs_stack, dim=0)
            max_in = th.amax(inputs_stack, dim=0)
            min_out = th.amin(output_stack, dim=0)
            max_out = th.amax(output_stack, dim=0)

            assert ~(min_in == max_in).all(), "Constant inputs"
            assert ~(min_out == max_out), "Constant output"

            bool_minmax = (min_in == max_in)
            min_in[bool_minmax] = 1
            max_in[bool_minmax] = 2

            norm = (min_in, max_in,
                    min_out, max_out, norm_type)

        output_stack = (output_stack - norm[2])/(norm[3] - norm[2])

        if concatenate:
            # inputs_stack = (inputs_stack - norm[0]) / (norm[1] - norm[0])
            inputs = (inputs_stack - norm[0]) / (norm[1] - norm[0])
            # inputs = inputs_stack.tolist()
        else:
            for i in range(len(inputs)):
                inputs[i] = (inputs[i] - norm[0])/(norm[1] - norm[0])

    return inputs, output_stack, norm

if __name__ == '__main__':

    os.chdir('.//Datasets/NASA/B. Saha and K Goebel')
    os.chdir('.//Separate_pickles')

    ignore = ['B0049.txt', 'B0050.txt', 'B0051.txt', 'B0052.txt'] # Files to be ignored.